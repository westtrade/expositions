'use strict';

console.log();

module.exports = {
	active: process.env.NODE_ENV === 'development',
	usePolling: false,
	dirs: [
		"api/models",
		"api/controllers",
		"api/services",
		"config/locales"
	],
	ignored: [
		// Ignore all files with .ts extension
		"**.ts"
	]
};
