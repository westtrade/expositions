'use strict';

const webpack = require('webpack');
const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('style/[name].css');

let options = {

	progress: true,
	entry: {
		front: './assets/front/index.js',
		dashboard: './assets/admin/index.js'
	},
	output: {
		// Make sure to use [name] or [id] in output.filename
		//  when using multiple entry points
		filename: '[name].bundle.js',
		chunkFilename: '[id].bundle.js',
		path: 'assets/build',
		// publicPath: 'http://localhost:3031/build/',
		publicPath: '/build/',
	},

	module: {
		loaders: [
			{
				test: require.resolve('tinymce/tinymce'),
				loaders: [
					'imports?this=>window',
					'exports?window.tinymce'
				]
			},
			{
				test: /tinymce\/(themes|plugins)\//,
				loaders: [
					'imports?this=>window'
				]
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components|client\/libs\/ws\/index[.]js)/,
				loaders: ['babel?presets[]=es2015',  'required'], // 'babel-loader' is also a valid name to reference
				// query: {
				// 	presets: ['es2015'],
				// 	// plugins: ['syntax-async-functions', 'transform-regenerator']
				// }
			},
			// { test: /\.scss$/, loaders: ['style', 'css?sourceMap', 'sass?sourceMap'] },
			// { test: /\.css$/, loader: 'style!css?sourceMap' },
			// { test: /\.pug$/, loader: 'pug'},

			{ test: /\.scss$/, loader: extractCSS.extract(['css?sourceMap', 'postcss','sass?sourceMap&importLoaders=1']) },
			{ test: /\.sass$/, loader: extractCSS.extract(['css?sourceMap', 'postcss','sass?sourceMap&importLoaders=1']) },
			{ test: /\.css$/, loader: extractCSS.extract(['css?sourceMap&importLoaders=1', 'postcss']) },
			{ test: /\.html/, loaders: ['ngtemplate', 'html']},

			{ test: /\.(eot|woff|woff2|gif|ttf|svg|png|je?pg)([?].*)?$/, loader: 'file?limit=30000&name=[name]-[hash].[ext]' },
            // { test: /\.marko$/, loader: 'marko' },
            // { test: /\.json/, loader: 'json' },

			// { test: /\.png$/, loader: 'url-loader?limit=100000' },
			// { test: /\.jpg$/, loader: 'file-loader' },
			// { test: /\.svg$/, loader: 'file-loader' },
			// { test: /\.(eot|woff|woff2|ttf|svg|png|jpg)([?].*)?$/, loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		// new webpack.ProgressPlugin((percentage, msg) => {
		// 	if (percentage) {
		// 		console.log('%s %, message: %s', percentage.toFixed(1), msg);
		// 	}
		// }),
		extractCSS,
		new webpack.optimize.DedupePlugin(),
		// https://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
		// new BrowserSyncPlugin(
		// 	// BrowserSync options
		// 	{
		// 		// browse to http://localhost:3000/ during development
		// 		host: 'localhost',
		// 		port: 3032,
		// 		// proxy the Webpack Dev Server endpoint
		// 		// (which should be serving on http://localhost:3100/)
		// 		// through BrowserSync
		// 		proxy: {
		// 			target: 'http://localhost:3030/',
		// 			ws: true,
		// 		},
		//
		// 		open: false,
		// 		notify: false,
		// 		files: ['public/html/*.html', 'public/html/**']
		// 	},
		// 	// plugin options
		// 	{
		// 		// prevent BrowserSync from reloading the page
		// 		// and let Webpack Dev Server take care of this
		// 		reload: true,
		// 		// open: false
		// 	}
		// ),
		// new webpack.optimize.UglifyJsPlugin({
		// 	compress: { warnings: false }
		// })
	],
	sassLoader: {
		includePaths: [
			path.resolve('./node_modules/sass-mediaqueries'),
	// 		// path.resolve('./src/client/admin'),
	// 		// path.resolve('./public'),
	// 		// ...bourbonPaths,
	// 		// ...bourbonNeatPaths,
		]
	},
	// watch: true,
	devtool: '#source-map',
	// node: {
	// 	fs: 'empty'
	// },
	// externals: {
	// 	primus: 'Primus'
	// }
};

// const cssnano = require('cssnano');
// let OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// webpackConfig.plugins.push(new OptimizeCssAssetsPlugin({
// 	// assetNameRegExp: /\.optimize\.css$/g,
// 	cssProcessor: cssnano,
// 	cssProcessorOptions: { discardComments: {removeAll: true } },
// 	canPrint: true
// }));

//
// const SpritesmithPlugin = require('webpack-spritesmith');
// const spritesmith = new SpritesmithPlugin({
// 	src: {
// 		cwd: path.resolve(__dirname, 'src/ico'),
// 		glob: '*.png'
// 	},
// 	target: {
// 		image: path.resolve(__dirname, 'src/spritesmith-generated/sprite.png'),
// 		css: path.resolve(__dirname, 'src/spritesmith-generated/sprite.styl')
// 	},
// 	apiOptions: {
// 		cssImageRef: '~sprite.png'
// 	}
// });
//
// webpackConfig.plugins.push(spritesmith);

module.exports.webpack = {
	options
};
