'use strict';

const validate = require('validate.js');
validate.Promise = Promise;

const i18n = require('./i18n/index.js');

validate.formatters.grouped = function(errors) {

	return errors.reduce((result = {}, {attribute, error}) => {

		const hasField = attribute in result;
		if (!hasField) {
			result[attribute] = [];
		}

		result[attribute].push(i18n.__(error));
		return result;

	}, {});
};


validate.validators.captcha = (value, options, key, attributes) => {
	return new validate.Promise((resolve) => {
		console.log(value);
		console.log(options);
		console.log(key);
		console.log(attributes);
		return resolve("is totally wrong");
	})
}



module.exports = validate;
