'use strict';

const translate = require('./translate');

class I18N {

	constructor() {

	}

	__(sentence) {

		if (sentence in translate) {
			sentence = translate[sentence];
		}

		return sentence;
	}
}

module.exports = new I18N();
