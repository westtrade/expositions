'use strict';

const { shortId } = require('./utils');

const TAB_ID = shortId();
let lastMessageId = null;


const getStorage = () => {
	try {
		if ('localStorage' in window && window['localStorage'] !== null) {
			return localStorage;
		}
	} catch (e) {
		return false;
	}

	return false;
};

const on = (type = 'message', messageReciever) => {
	window.addEventListener("storage", (event) => {
		if (event.key === type) {
			const {data, id} = JSON.parse(event.newValue);

			if (TAB_ID !== id && id) {
				messageReciever(event, data);
			}
		}
	});
};

const broadcast = (type, data = {}) => {


	const storage = getStorage();

	if (storage) {

		const eventData = {
			id: TAB_ID,
			messageId: shortId(),
			data
		};

		storage.setItem(type, JSON.stringify(eventData));

	} else {
		console.error('Local storage dosn\'t support!');
	}
};


const get = (type, defaultData = {}) => {
	const storage = getStorage();
	const eventData = storage.getItem(type);
	const {data = defaultData} = JSON.parse(eventData) || {};
	return data;
};


module.exports = {
	getStorage,
	on,
	broadcast,
	get,
};
