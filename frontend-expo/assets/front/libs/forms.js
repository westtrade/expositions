'use strict';

const validate = require('./validator');
const safeApply = require('./safeapply');
const qwery = require('qwery');

const getFormData = (form) => {
	return  Object.entries(form.elements || {}).reduce(toData, {});
};

const validateForm = (form, constraint = {}) => {
	const data = getFormData(form);

	const onlyExists = Object.entries(constraint).reduce((constraint, [fieldName, rule]) => {

		if (form.elements[fieldName]) {
			constraint[fieldName] = rule;
		}

		return constraint;

	}, {});

	// TODO REFACTOR!
	return validate.async(data, onlyExists).then((field) => {
		return {data, error: null, field};
	}).catch((error) => {
		return {data, error, field: null};
	});
};

// TODO add arrays support
const toData = (result = {}, [fieldName, input]) => {

	if (fieldName !== input.name) {
		return result;
	}

	let value = input.value;
	if (input.type === 'checkbox') {
		value = input.checked;
	}

	result[fieldName] = value;
	return result;
};


const resolveHTMLForms = ($element) => {

	if ($element instanceof HTMLFormElement) {
		return [$element];
	}

	let formElement = $element;
	if ($element.length === 1) {
		formElement = $element[0];
	}

	return qwery('form', formElement);
};



const formApply = ($scope, $element, constraint = {}, cb = (err, data) => {}) => {

	const [form] = resolveHTMLForms($element);

	let isDirty = false;

	$scope.form = $scope.form ? $scope.form : {};
	$scope.error = $scope.error ? $scope.error : {};

	let name = form.name;
	if (!name || !name.length) {
		name = Array.from(document.forms).indexOf(form).toString();
	}

	$scope.form[name] = $scope.form[name] ? $scope.form[name] : getFormData(form);


	const formUdate = (event) => {

		const constraintName = event.target.name;

		const fieldConstraint = constraintName in constraint
			? constraint[constraintName]
			: {};

		validateForm(form, { [constraintName]: fieldConstraint })
			.then(({error, data}) => {

				safeApply($scope, () => {
					$scope.form[name][constraintName] = data[constraintName];
				});

				safeApply($scope, () => {
					if (isDirty) {

						if (!error) {
							delete $scope.error[constraintName]
						} else {
							Object.assign($scope.error, error);
						}
					}
				});
			});
	};


	form.addEventListener('input', formUdate, true);
	form.addEventListener('change', formUdate, true);

	let validate = (cb = (error = null, result = null) => {}) => {

		isDirty = true;

		validateForm(form, constraint)
			.then(({error, data}) => {
				if (error) {
					safeApply($scope, () => {
						$scope.error = error;
					});
				}

				cb(error, data);

			}).catch(e => cb(e, null));
	}



	// $scope.form[name].validate = validate;
	form.validate = validate;


	const setValue = (fieldName, value, type) => {

		const field = form[fieldName];
		field.value = value;

		if (!type) {

			let inputEvent = new Event('input');
			field.dispatchEvent(inputEvent);

			let changeEvent = new Event('change');
			field.dispatchEvent(changeEvent);

		} else {
			let changeEvent = new Event(type);
			field.dispatchEvent(changeEvent);
		}

		return field;
	};

	// $scope.form[name].setValue = setValue;
	form.setValue = setValue;

	return form;
};

module.exports = {
	getFormData,
	validateForm,
	formApply,
	toData,
};
