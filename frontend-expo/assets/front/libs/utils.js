'use strict';


const shortId = () => {
	return ("0000" + (Math.random()*Math.pow(36,4) << 0).toString(36)).slice(-4)
};

const s4 = () => Math.floor((1 + Math.random()) * 0x10000)
	.toString(16)
	.substring(1);

const guid = () =>  s4() + s4() + '-' + s4() + '-' + s4() + '-'
	+ s4() + '-' + s4() + s4() + s4();


module.exports = {
	guid,
	shortId
};
