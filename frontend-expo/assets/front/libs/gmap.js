'use strict';

const {shortId} = require('./utils');

const GOOGLE_MAPS_KEY = 'AIzaSyD4cZ9kBDsr35AONvMT743XoBA_bwvkprE';
const YANDEX_MAPS_KEY = null;

const events = {};
const status = {};

const createScript = (src, randomCallbackName, resolve = () => {}) => {

	const script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = src;

	global[randomCallbackName] = () => {
		delete global[randomCallbackName];
		resolve();
	};

	document.body.appendChild(script);
};

let loadedMaps = [];

const scriptLoader = (loaderName, dataResolver = () => {}, resolve = () => {}) => {


	const loadStatus = status[loaderName] || 1;
	if (loadStatus === 3) {
		return resolve();
	}

	const {[loaderName]: stack = []} = events;
	stack.push(resolve);
	events[loaderName] = stack;

	if (loadStatus === 1) {
		status[loaderName] = 2;

		const [src, randomCallbackName] = dataResolver();

		createScript(src, randomCallbackName, () => {
			loadedMaps.push(loaderName);
			status[loaderName] = 3;
			stack.forEach((cb) => cb());
		});
	}
}

const loadYMap = (resolve, key) => {

	scriptLoader('ymap', () => {
		const randomCallbackName = 'fn' + shortId();

		return [
			`//api-maps.yandex.ru/2.1/?lang=ru_RU&load=package.standard&onload=${randomCallbackName}${ key ? `&key=${key}` : ''}`,
			randomCallbackName,
		]
	}, resolve);
};

const loadGMap = (resolve, key) => {

	scriptLoader('gmap', () => {
		const randomCallbackName = 'fn' + shortId();
		return [
			`//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=${randomCallbackName}${ key ? `&key=${key}` : ''}`,
			randomCallbackName,
		]

	}, resolve);
};

const loaders  = {
	google: loadGMap,
	yandex: loadYMap,
};

const loadMultiplie = (maps = [], keys = {}, resolve = () => {}) => {

	let loadStack = maps
		.map((mapType) => [loaders[mapType], keys[mapType]])
		.filter(([loader]) => !!loader)
	;

	const next = (caller) => {
		return function next (result) {
			const idx = loadStack.map(([caller]) => caller).indexOf(caller);
			if (idx >= 0) {
				loadStack.splice(idx, 1);
			}

			if (loadStack.length === 0) {
				resolve(loadedMaps);
				loadStack = null;
			}

			caller = null;
		}
	};

	loadStack.forEach(([loader, key]) => loader(next(loader), key));
};

module.exports = (maps = [], resolve) => {

	if (typeof maps === 'string') {
		maps = [maps];
	}

	if (!Array.isArray(maps)) {
		throw new Error('Argument maps must be array type');
	}

	loadMultiplie(maps, {google: GOOGLE_MAPS_KEY, yandex: YANDEX_MAPS_KEY}, resolve);
};
