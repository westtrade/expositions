'use strict';

class ExpoError  extends Error{

	constructor({message, stack}) {

		super(message)
		this.serverStack = stack;
	}

}


module.exports = ExpoError;
