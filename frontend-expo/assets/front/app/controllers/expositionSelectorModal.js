'use strict';

const angular = require('angular');
const safeapply = require('../../libs/safeapply');

const MODAL_MODES = ['catalog', 'filters'];
const subjects = require('../crutches/subjectsList');


module.exports = ['$scope', '$element', '$modals', ($scope, $element, $modals) => {

	if (!$scope.modalOpened) {
		return false;
	}

	const selected = $scope.initial.subjects.map(item => item.id);

	$scope.subjects = subjects.map(item => {
		item.selected = selected.indexOf(item.id) >= 0;
		return item;
	}).reduce((result, currentItem) => {

		const {category} = currentItem;
		if (!(category in result)) {
			result[category] = [];
		}
		result[category].push(currentItem);
		return result;
	}, {});

	$scope.mode = MODAL_MODES[0];

	$scope.switchMode = (mode) => {

		let modeId = MODAL_MODES.indexOf(mode);

		if (modeId < 0) {
			modeId === 0;
		}

		$scope.mode = MODAL_MODES[modeId];

	}



	$scope.close = () => $modals.close();
	$scope.select = () => {

		// const selected = Object.values($scope.subjects).reduce((result, items) =>  result
		// 	.concat(items.filter(item => item.selected))
		// 	.map(item => angular.copy(item)), []
		// );
		//
		$modals.close();
	}
}];
