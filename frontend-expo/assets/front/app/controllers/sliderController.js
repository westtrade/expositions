'use strict';
const safeApply = require('../../libs/safeapply');

module.exports = ['$scope', '$modals', '$state', ($scope, $modals, $state) => {

	$scope.$watchCollection('sliderList', (prev, next, $scope) => {
		$scope.$emit('slider:update');
	});

	$scope.sliderList = [
		{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {
				type: 'modal',
				args: ['offer-teaser-form']
			},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg',
		},
		{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg',
		},
		{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg',
		},
		{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {
				type: 'modal',
				args: ['subject-selector']
			},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg',
		},
	];

	$scope.openEditor = () => {
		const {sliderList} = $scope;
		$modals.open('slider-editor', {sliderList}, ({sliderList} = {}) => {
			safeApply(() => {
				$scope.sliderList = sliderList;
				if ($scope.$slider) {
					$scope.$slider.update();
				}
			})
		});
	}

	$scope.go = ({type, args = []} = {}) => {

		switch (type) {
			case 'modal':
				$modals.open(...args);
				break;

			case 'link':
				$state.go(...args);
				break;

			default:
		}

	}


	// $scope.openEditor();

	// console.log($scope);
	// console.log($scope.$parent.$parent.$slider.update());
	// console.log($scope.$slider);
	// $scope.$slider.update();

}];
