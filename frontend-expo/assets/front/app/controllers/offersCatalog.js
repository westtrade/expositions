'use strict';

const safeApply = require('../../libs/safeapply');
const ExpoError = require('../../libs/expoError');
const angular = require('angular');

const events = {
	scroll: null,
};

window.addEventListener('scroll', (event) => {
	if (events.scroll) {
		events.scroll(event);
	}
});


module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', 'User', ($scope, $element, cfpLoadingBar, $modals, User) => {

	const element = $element[0];
	$scope.flow = false;
	$scope.loading = false;

	$scope.filter = {
		page: 0,
		subjects : [],
		expositions : [
			// { name: 'Test', id: 1}
		]
	};

	$scope.selectSubjects = () => {

		const data = {
			initial: $scope.filter.subjects
		};

		$modals.open('subject-selector', data).then((subjects = []) => {
			safeApply($scope, () => {
				$scope.filter.subjects = [];
				angular.extend($scope.filter.subjects, subjects);
			});
		});
	}

	$scope.selectExpositions = () => {

		const data = {
			initial : $scope.filter
		};

		$modals.open('exposition-selector', data).then((expositionsList = []) => {
			safeApply($scope, () => {
				$scope.filter.expositions = [];
				$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
			});
		});
	}

	$scope.removeFilter = (removed, filterType) => {
		const filterTypeExists = filterType in $scope.filter;

		if (!filterTypeExists) {
			throw new Error('Wrong type of filter (filterType argumnt). It must be in allowed list of types: ' . Object.keys($scope.filter).join(', '))
		}

		$scope.filter[filterType] = $scope.filter[filterType].filter((current) => removed.id !== current.id);
	}

	$scope.catalog = [];
	$scope.loadMore = (cb = () => {}) => {

		if ($scope.loading) {
			return ;
		}


		cfpLoadingBar.start();
		$scope.loading = true;

		io.socket.get('/api/offers', $scope.filter, (catalog, jwr) => {
			let {error} = jwr;

			if (error) {
				throw new ExpoError(error);
			}

			$scope.catalog = $scope.catalog.concat(catalog);
			$scope.filter.page += 1;

			cfpLoadingBar.complete();
			$scope.loading = false;

			cb();
		});
	};

	$scope.loadMore();
	$scope.startFlow = () => {
		$scope.loadMore(() => {
			$scope.flow = true;
		});
	};

	events.scroll = (event) => {
		const scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
		const offset = 400;
		const contentScrolledHeight = (element.scrollHeight - scrollTop - offset);
		const contentIsScrolled =  contentScrolledHeight <= 0; //todo fix this bug

		if (contentIsScrolled && $scope.flow && !$scope.loading) {
			$scope.loadMore();
		};
	};


	$scope.replay = ($event, item) => {
		const user = User.getState();
		$modals.open('offer-teaser-form', {item, user});

		$event.stopPropagation();
	}

}];
