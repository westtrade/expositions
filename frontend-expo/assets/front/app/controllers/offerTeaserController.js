'use strict';

const safeapply = require('../../libs/safeapply');
const registrationConstraints = require('../../validators/registrationForm');
const { formApply }  = require('../../libs/forms');

const qwery = require('qwery');



module.exports = ['$scope', '$modals', '$element', 'User', '$timeout', ($scope, $element, $modals, User, $timeout) => {

	if (!$scope.modalOpened) {
		return false;
	}

	User.bind($scope);

	$timeout(() => {
		const [form] = qwery('#offer-teaser-form');

		formApply($scope, form, registrationConstraints);
		$scope.submit = ($event) => {
			form.validate((err = null, resultData = {}) => {



			});

			$event.preventDefault();
		}
	}, 0);
}];
