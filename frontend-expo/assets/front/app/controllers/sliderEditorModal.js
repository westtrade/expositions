'use strict';


const EDITOR_MODES = ['list', 'edit', 'success'];


module.exports = ['$scope', '$modals', ($scope, $modals) => {

	if (!$scope.modalOpened) {
		return false;
	}

	$scope.uploadConfig = {
		url : '/slider/cover',
	};

	$scope.mode = EDITOR_MODES[0];
	// $scope.mode = EDITOR_MODES[1];

	// $scope.slide = $scope.sliderList[0];

	$scope.delete = (index) => {
		$scope.sliderList.splice(parseInt(index), 1);
	}


	$scope.save = () => {
		$modals.close({sliderList});
	};

	$scope.create = () => {

	};


	$scope.saveSlider = () => {

	}


	$scope.editSlide = (slide) => {
		$scope.slide = slide;
		$scope.mode = EDITOR_MODES[1];
	}


	$scope.deleteCover = () => {
		$scope.slide.cover = '';
	}


	$scope.createSlide = () => {
		$scope.slide = {};
		$scope.mode = EDITOR_MODES[1];
	}

	$scope.toList = () => {
		$scope.slide = null;
		$scope.mode = EDITOR_MODES[0];
	}

	// $scope.close = () => {
	// 	const {sliderList} = $scope;
	// 	$modals.close({sliderList});
	// }

}];
