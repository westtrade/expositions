'use strict';

const angular = require('angular');

const subjects = require('../crutches/subjectsList');
const safeapply = require('../../libs/safeapply');

module.exports = ['$scope', '$element', '$modals', ($scope, $element, $modals) => {

	if (!$scope.modalOpened) {
		return false;
	}

	const selected = $scope.initial.map(item => item.id);

	$scope.subjects = subjects.map(item => {
		item.selected = selected.indexOf(item.id) >= 0;
		return item;
	}).reduce((result, currentItem) => {

		const {category} = currentItem;
		if (!(category in result)) {
			result[category] = [];
		}
		result[category].push(currentItem);
		return result;
	}, {});


	$scope.close = () => $modals.close();
	$scope.select = () => {

		const selected = Object.values($scope.subjects).reduce((result, items) =>  result
			.concat(items.filter(item => item.selected))
			.map(item => angular.copy(item)), []
		);

		$modals.close(selected);
	}
}];
