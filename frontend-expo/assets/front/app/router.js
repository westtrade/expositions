'use strict';

const angular = require('angular');
const safeApply = require('../libs/safeapply');
const debugPage = require('../views/debug.html');
const { formApply }  = require('../libs/forms');

const home = require('./farwater/home');
const notFound = require('./farwater/404');
const offers = require('./farwater/offers');
const auth = require('./farwater/auth');
const account = require('./farwater/account');
const exposition = require('./farwater/exposition');


const loader = (prefix) => ($stateProvider, [method, info]) => {
	$stateProvider.state(prefix + '-' + method, info);
	return $stateProvider;
};

const routes = function ($stateProvider, $locationProvider) {

	$locationProvider.html5Mode(true)

	$stateProvider
		.state('home', home)
		.state('404', notFound)
		.state('user-login', auth.login)
		.state('user-registration', auth.registration)
		.state('user-recovery', auth.recovery)
	;

	$stateProvider
		.state('change-password', account.changePassword)
		// .state('user-offers', offers.catalog)
		.state('user-events', account.eventsCatalog )
		.state('user-account', account.accountPage)
		.state('user-cart', account.cart);

	Object.entries(offers).reduce(loader('offer'), $stateProvider);
	Object.entries(exposition).reduce(loader('exposition'), $stateProvider);


	$stateProvider.state('user-logout', {
		url: '/user/logout',
		controller : ['$scope', '$state', 'User', ($scope, $state, User) => {
			User.logout().catch((e) => console.error(e)).then(() => {
				$state.go('home');
			});
		}]
	});


	$stateProvider.state('dev-debug', {
		url: '/dev/debug',
		templateUrl: debugPage,
		controller : ['$scope', 'User', '$timeout',  ($scope, User, $timeout) => {

			User.bind($scope);
			$scope.selectedUser = 1;
			$scope.changeUser = ($event) => {
				User.setState($scope.users[$scope.selectedUser]);
			}
			$scope.$watch('user.notificationsCount', (current, prev) => {
				if (current !== prev && typeof current === 'number') {
					User.setState('notificationsCount', current);
				}
			})

			$scope.users = [
				{
					displayName: 'Тумаков Олег Н',
					role: 'guest'
				},
				{
					displayName: 'Эрнест Днепропетровский А',
					role: 'exponent',
					notificationsCount: 5,
					id: '56faabde3a1a8cd34bae9584'
				},
				{
					displayName: 'Артем Вячеславович Е.',
					role: 'organizer'
				},
				{
					displayName: 'Модератор',
					role: 'moderator'
				},
				{
					displayName: 'Администратор',
					role: 'administrator'
				},
			];

			$timeout(() => {
				safeApply($scope, () => {
					$scope.users.push({
						displayName: 'Azaza',
						role: 'administrator'
					});
				})
			}, 1300)
		}]


	});

	$stateProvider.state('dev-styles', {
		url: '/dev/styles',
		templateUrl: debugPage,
		controller : ['$scope', ($scope) => {

		}]
	});
};

module.exports = routes;
