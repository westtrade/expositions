'use strict';

const itemTemplate = require('../../views/offer-page.html');
const formTemplate = require('../../views/form-exposition.html');
const qwery = require('qwery');

const { formApply }  = require('../../libs/forms');
const  safeApply  = require('../../libs/safeapply');

const EXPO_TYPES = [
	'Выставка',
	'Премия',
	'Конференция',
	'Форум',
];


const mainController = ['$scope', '$element', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', 'areYouSure',
	($scope, $element, $stateParams, User, cfpLoadingBar, $modals, $state, areYouSure) => {

	// const pageElement = $element[0];
	// const [form] = qwery('form', pageElement);

	User.bind($scope);
	const form = formApply($scope, $element);

	$scope.data = {
		test: 'azazaza',
	}

	form.addEventListener('input', () => {
		areYouSure.enable(true);
	}, true);

	$scope.submit = ($event) => {

		form.validate((error, data) => {

			if (error) {
				return safeApply($scope, () => {
					$scope.error = error;
				});
			}


			areYouSure.enable(false);
		});

		$event.preventDefault();
	}

	$scope.currentAdditionalValue = '';

	$scope.additionalFields = [];
	$scope.addField = () => {

		if (!$scope.currentAdditionalValue || !$scope.currentAdditionalValue.length) {
			return;
		}

		safeApply($scope, () => {
			$scope.additionalFields.unshift({
				idx: $scope.additionalFields.length,
				value: $scope.currentAdditionalValue,
			});

			$scope.currentAdditionalValue = '';
		});

	}

	$scope.removeField = (index) => {
		console.log(index);
		safeApply($scope, () => {
			$scope.additionalFields.splice(index, 1);
		});
	}

	$scope.subjects = [];
	$scope.openSubjectsSelector = () => {

		const data = {initial: $scope.subjects};
		$modals.open('subject-selector', data).then((subjects = []) => {
			safeApply($scope, () => {
				$scope.subjects = [];
				angular.extend($scope.subjects, subjects);
			});
		});
	}

	$scope.removeSubject = (index) => {
		$scope.subjects.splice(index, 1);
	}

	$scope.types = EXPO_TYPES;
}];



const create = {
	url: '/exposition/create',
	templateUrl: formTemplate,
	controller: mainController,
}

const update = {
	url: '/exposition/edit/:id',
	templateUrl: formTemplate,
	controller: mainController,
}


const index = {
	url: '/exposition/:id',
	templateUrl: itemTemplate,
	controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) => {

		$scope.offer = {};

		User.bind($scope);

		if (!$stateParams.id) {
			return $state.go('404');
		}

		const loadItem = ($scope, id, cb) => {

			cfpLoadingBar.start();
			$scope.loading = true;

			io.socket.get('/offers/offer', {id}, (offer, jwr) => {
				let {error} = jwr;

				//TODO 404 error

				if (error) {
					throw new ExpoError(error);
				}

				console.log(offer);

				safeApply($scope, () => {
					$scope.offer = offer;
					$scope.loading = false;
				})

				cfpLoadingBar.complete();
				cb();
			});
		};

		loadItem($scope, $stateParams.id, () => {
			console.log('Loaded');
		})

		$scope.replay = (item) => {
			const user = User.getState();
			$modals.open('offer-teaser-form', {item, user});
		}
	}]
};



module.exports = {
	create, update, index
};
