'use strict';

const changePasswordPage = require('../../views/change-password-page.html');
const { formApply }  = require('../../libs/forms');
const registrationConstraints = require('../../validators/registrationForm');



const changePassword = {
	url: '/user/change-password',
	templateUrl: changePasswordPage,
	controller : ['$scope', '$element', ($scope, $element) => {
		const form = $element.find('form')[0];
		formApply($scope, form, registrationConstraints);

		$scope.setMail = ($event) => {

			form.validate((error, formData) => {

			});

			$event.preventDefault();
		}
	}]
};




const eventsCatalog = {
	url: '/user/events',
	templateUrl: changePasswordPage,
	controller : ['$scope', ($scope) => {

	}]
};

const accountPage = {
	url: '/user/account',
	templateUrl: changePasswordPage,
	controller : ['$scope', ($scope) => {

	}]
};

const cart = {
	url: '/user/cart',
	templateUrl: changePasswordPage,
	controller : ['$scope', ($scope) => {

	}]
};



module.exports = {
	changePassword,
	eventsCatalog,
	accountPage,
	cart,
};
