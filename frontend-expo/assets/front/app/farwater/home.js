'use strict';

const home = require('../../views/home-page.html');
const offersCatalog = require('../../views/offers-catalog.html');

module.exports = {
	url: '/',
	templateUrl: home,
	controller: ['$scope', ($scope) => {
		$scope.catalog = offersCatalog;
	}]
};
