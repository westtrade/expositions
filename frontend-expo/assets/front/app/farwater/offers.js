'use strict';

const itemTemplate = require('../../views/offer-page.html');
const offerFormTemplate = require('../../views/form-offer.html');
const offersCatalogTemplate = require('../../views/offers-catalog.html');

const { formApply }  = require('../../libs/forms');
const  safeApply  = require('../../libs/safeapply');




const catalog = {
	url: '/offer',
	templateUrl: offersCatalogTemplate
};

const create = {
	url: '/offer/create',
	templateUrl: offerFormTemplate,
	controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) => {

	}],
}

const update = {
	url: '/offer/edit/:id',
	templateUrl: offerFormTemplate,
	controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) => {

	}],
}


const item = {
	url: '/offer/:id',
	templateUrl: itemTemplate,
	controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) => {

		User.bind($scope);

		$scope.offer = {};

		if (!$stateParams.id) {
			return $state.go('404');
		}

		const loadItem = ($scope, id, cb) => {

			cfpLoadingBar.start();
			$scope.loading = true;

			io.socket.get('/offers/offer', {id}, (offer, jwr) => {
				let {error} = jwr;

				if (error) {
					throw new ExpoError(error);
				}

				safeApply($scope, () => {
					$scope.offer = offer;
					$scope.loading = false;
				})

				cfpLoadingBar.complete();
				cb();
			});
		};


		loadItem($scope, $stateParams.id, () => {
			console.log('Loaded');
		})


		$scope.replay = (item) => {
			const user = User.getState();
			$modals.open('offer-teaser-form', {item, user});
		}
	}]
};

module.exports = {catalog, create, update, item};
