'use strict';
const { guid }  = require('../../libs/utils');

const loginTemplate = require('../../views/login-page.html');
const registrationTemplate = require('../../views/registration-page.html');
const recoveryTemplate = require('../../views/recovery-page.html');

const loginConstraints = require('../../validators/loginForm');
const registrationConstraints = require('../../validators/registrationForm');

const { formApply }  = require('../../libs/forms');


const login = {
	url: '/user/login',
	templateUrl: loginTemplate,
	controller: ['$scope', '$element', ($scope, $element) => {
		const form = $element.find('form')[0];
		formApply($scope, form, loginConstraints);

		$scope.submit = ($event) => {

			form.validate((error, formData) => {
				console.log(error, formData);
			});

			$event.preventDefault();
		}
	}],
};

const registration = {
	url: '/user/registration',
	templateUrl: registrationTemplate,
	controller : ['$scope', '$element', ($scope, $element) => {

		const form = $element.find('form')[0];

		formApply($scope, form, registrationConstraints);
		form.setValue('type', 'company');
		$scope.randomSeed = '';

		$scope.changeCaptcha = () => {
			form.setValue('captcha', '').focus();
			$scope.randomSeed = guid();
		}

		$scope.select = (type) => {
			form.setValue('type', type);
			console.log($scope.form);
		}

		$scope.submit = ($event) => {

			form.validate((error, formData) => {
				if (error) {
					$scope.changeCaptcha();
				}
			});

			$event.preventDefault();
		}

	}]
};

const recovery = {
	url: '/user/recovery',
	templateUrl: recoveryTemplate,
	controller : ['$scope', '$element', ($scope, $element) => {

		const form = $element.find('form')[0];
		formApply($scope, form, registrationConstraints);

		$scope.email = null;

		$scope.setMail = ($event) => {

			form.validate((error, formData) => {
				if (!error) {
					safeApply($scope, () => {
						$scope.email = formData.email;
					});
				}
			});

			$event.preventDefault();
		}

	}]
};


module.exports = {login, registration, recovery};
