'use strict';

let isDirty = false;

window.onbeforeunload = (event) => {

	return undefined;

	if (isDirty) {

		const $message = 'It looks like you have been editing something' +
		' - if you leave before saving, then your changes will be lost.'

		return $message;
	}

	return undefined;
}


module.exports = () => {

	return {

		enable(dirty) {
			isDirty = !!dirty;
		}
	};

};
