'use strict';

const safeApply = require('../../libs/safeapply');
const messages = require('../../libs/localStorage');

const angular = require('angular');

const ROLES = ['guest', 'exponent', 'organizer', 'moderator', 'administrator'];

const isOwner = (context, user) => {
	const {creator: {_id, id}} = Object.assign({}, {creator:{}}, context || {});
	const creatorId = id || _id;

	return user.id === creatorId;
};

const RULES = {
	'view navbar menu' : ['!guest'],
	'open slider editor' : ['administrator'],
	'change offer' : ['administrator', 'moderator', isOwner],
	'delete offer' : ['administrator', 'moderator', isOwner],
};

const ANONYMOUS_USER = {
	role: 'guest',
	displayName: 'Аноним',
};

class User {

	constructor() {

		this.watchers = [];
		this.state = {};

		const initState = messages.get('user', ANONYMOUS_USER);

		this.setState(initState, true);
		messages.on('user', (event, userState) => {
			this.setState(userState, true);
		});
	}

	bind($scope) {

		this.watchers.push($scope);

		safeApply($scope, () => {
			$scope.user = this.getState();
		});

		$scope.$on('$destroy', () => {
			const watcherIdx = this.watchers.indexOf($scope);
			this.watchers.splice(watcherIdx, 1);
		});
	}

	logout() {
		return new Promise((resolve, reject) => {
			this.setState(ANONYMOUS_USER);
			resolve();
		});
	}

	setState(...args) {

		let name = null, data, preventBroadcast = false;

		if (typeof args[0] === 'string') {
			name = args[0];
			data = args[1];
			preventBroadcast = typeof args[2] === 'undefined' ? preventBroadcast : !!args[2];
		} else {
			data = args[0];
			preventBroadcast = typeof args[1] === 'undefined' ? preventBroadcast : !!args[1];
		}

		if (name) {
			this.state[name] = data;
		} else {
			this.state = data;
		}

		this.watchers.forEach(($scope) => {
			safeApply($scope, () => {
				$scope.user = this.getState();
			});
		});

		if (!preventBroadcast) {
			messages.broadcast('user', this.getState());
		}
	}

	getState() {
		return angular.extend({}, this.state, {
			can: (ruleId, context) => {
				return this.can(ruleId, context);
			},

			logout: () => this.logout(),
		});
	}

	can(ruleId, context = {}) {

		const ruleExists = ruleId in RULES;

		if (!ruleExists) {
			return false;
		}

		const rule = RULES[ruleId];
		let result = false;

		const userData = this.getState();

		result = rule.reduce((result = { allow: null, hasDecision: false }, currentRule) => {

			let { allow = null, hasDecision = false } = result;

			if (hasDecision) {
				return result;
			}

			let ruleAllows = false;

			if (typeof currentRule === 'function') {
				ruleAllows = currentRule(context, userData);
			} else {
				const isNot = currentRule.indexOf('!') === 0;
				if (isNot) {
					currentRule = currentRule.replace(/^!/gim, '');
					ruleAllows = currentRule !== userData.role;
				} else {
					ruleAllows = currentRule === userData.role;
				}
			}

			if (allow === null) {
				allow = ruleAllows;
			} else if (ruleAllows) {
				hasDecision = true;
				allow = ruleAllows;
			}

			return {allow, hasDecision};

		}, { allow: null, hasDecision: false });

		return !!result.allow;
	}
}


const currentUser = new User();

module.exports = () => currentUser;
