'use strict';

const angular = require('angular');
const $body = angular.element(document.body);


const isFunction = (obj) => !!(obj && obj.constructor && obj.call && obj.apply);

const clean = (inputData = {}) => {
	const json = angular.toJson(inputData);
	return JSON.parse(json);
};

class ModalManager {

	constructor($rootScope) {

		this.active_window = null;
		this.$root = $rootScope;

		const actions = {
			27: () => this.close(),
		};

		document.addEventListener('keyup', (event) => {
			const {keyCode} = event;
			actions[keyCode] && actions[keyCode]();
		});

		this.promise = {
			reject: null,
			resolve: null,
		}
	}

	isActive(modalId) {
		return modalId && modalId.length
			? this.getActiveWindowID() === modalId
			: !!this.getActiveWindowID();
	}

	getActiveWindowID() {
		const openedModalID = $body.attr('data-active-modal');
		return openedModalID;
	}

	isOpened() {
		const modalWindowId = this.getActiveWindowID();
		return modalWindowId && modalWindowId.length;
	}

	open(openingModalWindowId, data = {}) {

		if (typeof data === 'function') {
			callback = data;
			data = {};
		}

		data = clean(data);
		// let activeModalID = this.getActiveWindowID();
		this.close();

		if (!openingModalWindowId) {
			//TODO error message
			return false;
		}

		$body.attr('data-active-modal', openingModalWindowId);
		this.$root.$broadcast('modal-toggled', this, true, data);

		return new Promise((resolve, reject) => {
			this.promise.resolve = resolve;
			this.promise.reject = reject;
		});
	}

	close(result) {

		const modalWindowId = this.getActiveWindowID();

		if (!modalWindowId || !modalWindowId.length) {
			return false;
		}

		this.$root.$broadcast('modal-toggled', this, false, result);
		$body.removeAttr('data-active-modal');

		if (result instanceof Error) {

			if (isFunction(this.promise.reject)) {
				this.promise.reject(result);
			}

		} else {

			if (isFunction(this.promise.resolve)) {
				this.promise.resolve(result)
			}

		}

		this.promise.reject = null;
		this.promise.resolve = null;

		return false;
	}

	toggleModal() {

		return this.isActive(openModalId)
			? this.close()
			: this.open(openModalId);
	}
}




module.exports = ['$rootScope', ($rootScope) => new ModalManager($rootScope)];
