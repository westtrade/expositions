'use strict';

const createAppend = (element) => {

	const appendElement = document.createElement('div');

	appendElement.classList.add('append');

	if (element.nextElementSibling) {
		element.parentNode.insertBefore(appendElement, element.nextElementSibling);
	} else {
		element.appendChild(appendElement);
	}

	return appendElement;
};


module.exports = () => {
	return {
		restrict: 'A',
		link($scope, $element, $attrs) {
			let {letterCounter = -1} = $attrs;
			const element = $element[0];


			const appendElement = element.nextElementSibling && element.nextElementSibling.classList.contains('append')
				? element.nextElementSibling
				: createAppend(element);

			letterCounter = parseInt(letterCounter);
			console.log(letterCounter);

			if (letterCounter > 0) {
				appendElement.innerHTML = `${element.value.length} / ${letterCounter}`;
			}

			element.addEventListener('input', (event) => {
				appendElement.innerHTML = `${element.value.length} / ${letterCounter}`;
			});

			element.addEventListener('keypress', (event) => {
				if (element.value.length >= letterCounter) {
					event.preventDefault();
				}
			});

		}
	};
};
