'use strict';

//
// const getChildren = (element) => {
// 	console.log(element);
// };



// onDelete - rebuild tree under element (nextSibling)
// onSize (element - maybe load image) change - rebuild tree under  (nextSibling) - image on load -> id -> rebuildElement by id (style changed ?)
// onAdd
//
// (element) => nextSibling -> whileEnd

const qwery = require('qwery');
const { markItem, onePixel } = require('./markRepeatItem');

const elementPosiition = (container, element) => {
	const containerPosition = container.getBoundingClientRect();
	const elementPosition = element.getBoundingClientRect();

	const width = elementPosition.width;
	const height = elementPosition.height;
	const left = elementPosition.left - containerPosition.left;
	const top = elementPosition.top - containerPosition.top;

	return {width, height, left, top};

	// const name = value;
};


const calculateGutter = (container, first) => {
	const modulo = container.offsetWidth % first.offsetWidth;
	const columns = (container.offsetWidth - modulo) / first.offsetWidth;
	const gutter = parseInt(modulo  / (columns - 1));

	return { gutter, columns };
};

const calculateContainerHeight = (container, info = {}) => {

	const { gutter, columns } = info;

	let height = 0;

	let lastElement = container.lastElementChild;
	if (!lastElement) {
		return height;
	}


	let count = columns;
	while (count-- && lastElement) {
		const position = elementPosiition(container, lastElement);
		const currentHeight = position.top + lastElement.offsetHeight;

		if (currentHeight > height) {
			height = currentHeight;
		}


		lastElement = lastElement.previousElementSibling;
	}

	return height;
};

const updateContainerHeight = (container, { gutter, columns } = {}) => {
	const containerHeight = calculateContainerHeight(container, { gutter, columns });
	container.style.height = containerHeight + 'px';
};

const rebuildItem = (container, current, { gutter = 32, columns = 4, onlyOne = true } = {}) => {

	const previous = current.previousElementSibling;
	if (!previous) {

		current.style.top = '0px';
		current.style.left = '0px';

		const info = calculateGutter(container, current);

		gutter = info.gutter;
		columns = info.columns;

		return { gutter, columns };
	}

	// const container = value;
	const positionPrev = elementPosiition(container, previous);
	const left = positionPrev.left + positionPrev.width + gutter;
	const isOverflowed = (left + current.offsetWidth) > container.offsetWidth;

	const childrenArray = Array.from(container.children);
	const currentPosition = childrenArray.indexOf(current);

	const prevUpperPosition = currentPosition - columns;
	const prevUpperElement = childrenArray[prevUpperPosition];

	let top = 0;
	if (prevUpperElement) {
		const upperPosition = elementPosiition(container, prevUpperElement);
		top = upperPosition.top + upperPosition.height + gutter;
	}

	if (!isOverflowed) {
		current.style.left = (left) + 'px';
	} else {
		current.style.left = '0px';
	}

	current.style.top = top + 'px';

	if (onlyOne) {
		updateContainerHeight(container, {gutter, columns});
	}

	return { gutter, columns };
};


const rebuildItems = (containerElement, startElement, endElement = null, { gutter = 32, columns = 4 } = {}) => {

	let info = {gutter, columns, onlyOne: false};

	// if (!endElement) {
	// 	return rebuildItem(containerElement, startElement, info);
	// }

	let current = startElement;
	while (current && current !== endElement) {
		info = rebuildItem(containerElement, current, info);
		current = current.nextElementSibling;
	}

	updateContainerHeight(containerElement, {gutter, columns});

	return info;
};

const customEvents = require('./customEvents');

const link = ($scope, $element, $attributes, $controller) => {

	const container = $element[0];
	let rebuildInfo = { gutter: 32, columns: 4 };


	// if (!element.hasChildNodes()) {
	// 	return;
	// }
	//
	// let currentElement = value;
	//
	// let name = value;

	// let elementsMap = {
	// 	new: {},
	//
	// };


	// listofupdates - after


	$scope.$watch(() => {

		if (!container.hasChildNodes()) {
			return;
		}

		const markers = Array.from(container.children)
			.filter(item => item.nodeType === Node.ELEMENT_NODE)
			.reduce(markItem, {added: [], changed: []});


		// const markers = qwery('[ng-repeat], [ng-repeat-start]', container)
		// 	.reduce(markItem, {added: [], changed: []});

		// console.log(markers);
		// debugger;
		// const repeatItems = qwery('[ng-repeat]');
	});


	container.addEventListener('load', (event) => {

		if (!container.hasChildNodes()) {
			return;
		}

		// console.log(event.target.nodeName);


		if (event.target.src.indexOf(onePixel) >= 0) {

			const marker = event.target;
			const cardElement = marker.parentNode;
			cardElement.removeChild(marker);

			const masonryId = cardElement.getAttribute('data-masonry-id', masonryId);
			const images = qwery('img', cardElement)
					.filter(item => item !== marker)
					.forEach((image) => {
						image.masonryId = masonryId;
						return image;
					});

			rebuildInfo = rebuildItem(container, cardElement, rebuildInfo);
			// rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);

			// 		console.log(images);
			// 		debugger;

			return ;
		}

		if (event.target instanceof HTMLImageElement && event.target.masonryId) {
			const [cardElement] = qwery(`[data-masonry-id="${event.target.masonryId}"]`);
			rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
		}

	}, true);


	container.addEventListener('error', (event) => {

		if (event.target instanceof HTMLImageElement && event.target.masonryId) {
			const [cardElement] = qwery(`[data-masonry-id="${event.target.masonryId}"]`);
			rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
		}

	}, true);

	customEvents.onResizeEnd.push(() => {
		// console.log('resized');

		const newGutter = calculateGutter(container, container.firstElementChild);
		if (newGutter.columns !== rebuildInfo.columns) {
			rebuildInfo = rebuildItems(container, container.firstElementChild, null, rebuildInfo);
			updateContainerHeight(container, rebuildInfo)
		}

	});





	// debugger;
	// const element = $element[0];
	// let pendingImages = 0;
	//
	// $scope.$watchCollection('catalog', () => {
	// 	console.log('DIGEST Collection');
	//
	// 	const cards = qwery('.card', element);
	//
	// 	for (let card of cards) {
	//
	// 		const $card = angular.element(card);
	// 		const $scope = $card.scope();
	//
	// 		console.log($scope);
	// 		console.log($scope.$id);
	//
	// 		let marker = new Image();
	// 		marker.setAttribute('ng-if', true);
	// 		marker.src = onePixel;
	// 		card.appendChild(marker);
	// 	}
	//
	// 	// pendingImages += cards.length * 3;
	//
	// 	// qwery('.card', element).forEach(item => {
	// 	// 	item.length
	// 	// 	console.log(item.offsetHeight);
	// 	// });
	//
	//
	//
	// 	// debugger;
	//
	// });
	//
	// //
	// element.addEventListener('load', (event) => {
	// 	if (event.target.src.indexOf(onePixel) >= 0) {
	// 		const marker = event.target;
	// 		const cardElement = marker.parentNode;
	// 		const images = qwery('img', cardElement).filter(item => item !== marker);
	//
	// 		console.log(images);
	//
	// 		debugger;
	// 	}
	// }, true);










	// $scope.$watch('catalog', () => {
	// 	console.log('DIGEST Collection');
	//
	// 	// console.log(qwery('.card', element));
	// 	debugger;
	// }, () => {
	// 	console.log('Changed');
	//
	// })




	// console.log(element, $scope);


	// $scope.$watch((...args) => {
	// 	console.log('DIGEST');
	// 	console.log(args);
	// })


	// debugger;

};


module.exports = [() => {

	const directive = {
		// priority: 1000,
		// terminal: true,
		restrict: 'CEA',
		link,
	};

	return directive;
}];
