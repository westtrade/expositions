'use strict';

const angular = require('angular');
const qwery = require('qwery');

const { shortId: newMarkerId } = require('../../../libs/utils');

const onePixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=';


// style

const createMarker = (masonryId) => {

	const marker = new Image();

	marker.setAttribute('data-masonry-id', masonryId);

	marker.src = onePixel;
	marker.style.position = 'fixed';
	marker.style.left = '0';
	marker.style.top = '0';

	return marker;
};

const markElement = (masonryId, item) => {
	const marker = createMarker(masonryId);
	item.setAttribute('data-masonry-id', masonryId);
	item.appendChild(marker);
};

const markNgRepeatStart = (masonryId, item) => {

};

const elementIsChanged = (masonryId, item) => {
	//todo check if element is changed
	return false;
};

const ngRepeatStartIsChanged = (masonryId, item) => {
	//todo check if element is changed
	return false;
};

const markItem = (info = {}, item) => {

	const {added = [], changed = []} = info;

	let masonryId = item.getAttribute('data-masonry-id');
	const ngRepeatStart = item.getAttribute('ng-repeat-start');

	if (masonryId) {
		const isChanged = ngRepeatStart
			? ngRepeatStartIsChanged(masonryId, item)
			: elementIsChanged(masonryId, item);

		if (isChanged) {
			changed.push(masonryId);
			info.changed = changed;
		}

		return info;
	}


	// const ngRepeat = item.getAttribute('ng-repeat');

	// if (!ngRepeat && !ngRepeatStart) {
	// 	return info;
	// }

	masonryId = newMarkerId();

	!!ngRepeatStart
		? markNgRepeatStart(masonryId, item)
		: markElement(masonryId, item);

	added.push(masonryId);
	info.added = added;

	return info;
};


module.exports = {
	markItem,
	onePixel,
};
