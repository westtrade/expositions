'use strict';

let customEvents = {
	onResizeEnd: []
};

const delta = 5;
let timer = false, startTime;

const onResizeEnd = () => {

	if (new Date - startTime < delta) {
		timer = setTimeout(onResizeEnd, delta);
	} else {
		timer = false;

		if ('onResizeEnd' in customEvents) {
			customEvents['onResizeEnd'].forEach((fn) => fn());
		}

	}
};

window.addEventListener('resize', () => {
	startTime = new Date;
	if (timer === false) {
		timer = setTimeout(onResizeEnd, delta);
	}
}, true);

module.exports = customEvents;
