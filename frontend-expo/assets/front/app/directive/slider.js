'use strict';

const qwery = require('qwery');
const safeApply = require('../../libs/safeapply');
const templateUrl = require('./expo-slider.html');

class SliderTimer {

	constructor() {
		this.currentTimer = null;
		this.$slider = null;
	}

	run ($slider = null) {

		if ($slider) {
			this.$slider = $slider;
		}

		if (!this.$slider) {
			return;
		}

		this.currentTimer = setTimeout(() => {

			this.$slider.next(true);
			this.currentTimer = clearTimeout(this.currentTimer);
			this.run();

		}, this.$slider.timeout);
	}

	pause() {
		if (this.currentTimer) {
			this.currentTimer = clearTimeout(this.currentTimer);
		}
	}

	resume() {
		if (!this.currentTimer) {
			this.currentTimer = clearTimeout(this.currentTimer);
			this.run();
		}
	}
};

const init = ($scope, container, sliderContent = []) => {

	let $slider = {
		timeout: 4000,
		total: 0,
		acive: 0,
		container: container,
		scope: $scope,
		timer: new SliderTimer,

		pause() {
			$slider.timer.pause();
		},

		resume() {
			$slider.timer.resume();
		},

		run() {
			$slider.timer.run($slider);
		},

		setActive(active, isAutomatic = true) {

			if (!isAutomatic) {
				$slider.pause();
			}

			safeApply($slider.scope, () => {

				$slider.active = active;
				qwery('.slide', $slider.container).forEach((item, idx) => {
					idx === active ? item.classList.add('active') : item.classList.remove('active')
				});

				$slider.resume();
			});
		},

		next(isAutomatic = false) {
			let current = parseInt($slider.active);
			const next = current + 1;
			current = next >= $slider.total ? 0 : next;
			$slider.setActive(current, isAutomatic);
		},

		prev(isAutomatic = false) {
			let current = parseInt($slider.active);
			const prev = current - 1;
			current = prev < 0 ?  $slider.total - 1 : prev;
			$slider.setActive(current, isAutomatic);
		},

		update() {

			let itemsCount = 0;
			let initialActive = 0;

			const sliderContent = qwery('.slide', $slider.container);
			sliderContent.forEach((current, key) => {

				if (!current.classList || !current.classList.contains('slide')) {
					return
				}

				if (current.classList.contains('active')) {
					initialActive = itemsCount;
				}

				itemsCount++;
			});

			$slider.total = itemsCount;
			$slider.active = 0;

			$slider.setActive(initialActive);

			if (itemsCount === 0) {
				setTimeout($slider.update, 20);
			}

			return $slider;
		},
	};

	$scope.$slider = $slider;
	return $slider;
};


module.exports = ['$rootScope', '$compile', ($rootScope, $compile) => {

	return {
		transclude: true,
		scope: true,
		templateUrl,
		compile: (tElement, tAttrs, transclude) => {
			return ($scope, $container, attrs) => {

				const $injector = $container.injector();
				const User = $injector.get('User');

				User.bind($scope);

				const $slider = init($scope, $container[0]);
				transclude($scope, (content) => {
					$container.append(content);
					$slider.update().run();
				});
			};
		}
	}
}];
