'use strict';

const templateUrl = require('./topbar.html');
const qwery = require('qwery');

const bindEvents = (element) => {

	const [userNav] = qwery('.user-nav', element);

	if (userNav) {

		userNav.addEventListener('mouseover', (event) => {
			document.body.classList.add('open-menu');
		});

		userNav.addEventListener('mouseleave', (event) => {
			document.body.classList.remove('open-menu');
		});

		userNav.addEventListener('click', (event) => {
			document.body.classList.remove('open-menu');
		});
	}
};

const controller = ['$scope', '$element', '$timeout', 'User', ($scope, $element, $timeout, User) => {
	User.bind($scope);
	const element = $element[0];
	bindEvents(element);

	$scope.$watch('user', () => {
		$timeout(() => {
			bindEvents(element);
		}, 0);
	});
}]

module.exports = () => {
	return {
		templateUrl,
		controller,
	}
};
