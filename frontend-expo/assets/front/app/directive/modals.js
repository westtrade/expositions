'use strict';

const angular = require('angular');
const $body = angular.element(document.body);
const templateUrl = require('./modals.html');

const qwery = require('qwery');


module.exports = ['$modals', '$rootScope', '$compile', ($modals, $rootScope, $compile) => {
	return {
		templateUrl,
		link:  ($scope, $element) => {
			const element = $element[0];
			const [background] = qwery('.background', element);
			background.addEventListener('click', (event) => {
				$modals.close();
				event.stopPropagation();
				event.preventDefault();
			});
		}
	};
}];
