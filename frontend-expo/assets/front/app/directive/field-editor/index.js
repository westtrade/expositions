'use strict';

const tinymce = require('tinymce/tinymce');
require('tinymce/themes/modern/theme');

const THEME = 'lightgray';
require('tinymce/skins/' + THEME + '/content.min.css');
require('tinymce/skins/' + THEME + '/skin.min.css');

require('./langs/ru');

require('tinymce/plugins/paste/plugin');
require('tinymce/plugins/advlist/plugin');
require('tinymce/plugins/autolink/plugin');
require('tinymce/plugins/image/plugin');
require('tinymce/plugins/lists/plugin');
require('tinymce/plugins/charmap/plugin');
require('tinymce/plugins/print/plugin');
require('tinymce/plugins/preview/plugin');
require('tinymce/plugins/link/plugin');
require('tinymce/plugins/autoresize/plugin');
require('tinymce/plugins/textpattern/plugin');

const content_style = `

body, html {
	padding: 0;
	margin: 0;
	font-family: "Roboto", sans-serif;
}

#tinymce {
	padding: 18px!important;

}

body {
	    padding: 18px;
		color: #3d3d3d;
}

p {
	margin: 0 0 5px 0;
}
`;

module.exports = ['$modals', ($modals) => {
	return {
		scope: true,
		require: '?ngModel',
		link($scope, $element, $attrs, $controllers) {

			const {ngModel} = $attrs;
			const element = $element[0];

			const instance = tinymce.init({
				target: element,
				setup(editor) {


					if (ngModel) {
						$scope.$parent.$watch(ngModel, (prev, value) => {

							console.log('Scope watch');

							console.log(element.value);

							// editor.setContent(value, {format: 'raw'});
							// element.value = editor.getContent();

							// const eventData = {
							// 	detail: {
							// 		from: 'editor'
							// 	}
							// };

							// let inputEvent = new CustomEvent('input', eventData);
							// let changeEvent = new CustomEvent('change', eventData);
							// element.dispatchEvent(inputEvent);
							// element.dispatchEvent(changeEvent);

						}, true);
					}

					const onChange = (event) => {


						const {from = 'outside'} = event.detail || {};
						console.log('on change from', from);

						if (from === 'outside') {
							editor.setContent(element.value);
						}
					};

					element.addEventListener('change', onChange);
					element.addEventListener('input', onChange);


					const updateFromEditor = (event) => {

						console.log('Editor changed');

						element.value = editor.getContent();
						let changeEvent = new CustomEvent(event.type, {
							detail: {
								from: 'editor'
							}
						});
						element.dispatchEvent(changeEvent);

					};


					editor.on('change', updateFromEditor);
					editor.on('input', updateFromEditor);

					editor.addButton('expoimage', {
						icon: 'image',
						onclick: function () {
							$modals.open('')
						}
					});

					editor.addButton('expolink', {
						icon: 'link',
						onclick: function () {

							$modals.open('')
						}
					});

				},
				visual: false,
				statusbar: false,
				content_style,
				// height: 500,
				menubar: false,
				// toolbar: 'undo redo | styleselect | bold italic | link image',
				toolbar: 'undo redo | bold italic | link image expoimage expolink',
				skin: false,
				// inline: true,
				plugins: 'paste link autoresize textpattern advlist autolink link image lists charmap print preview',
				// block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
				textpattern_patterns: [
					{start: '*', end: '*', format: 'italic'},
					{start: '**', end: '**', format: 'bold'},
					// {start: '#', format: 'h1'},
					// {start: '##', format: 'h2'},
					// {start: '###', format: 'h3'},
					// {start: '####', format: 'h4'},
					// {start: '#####', format: 'h5'},
					// {start: '######', format: 'h6'},
					{start: '1. ', cmd: 'InsertOrderedList'},
					{start: '* ', cmd: 'InsertUnorderedList'},
					{start: '- ', cmd: 'InsertUnorderedList'}
				]
			});


		}
	};
}];
