'use strict';

module.exports = ['$rootScope', '$compile', ($rootScope, $compile) => {
	return {
		// transclude: 'element',
		restrict: 'C',
		priority: 600,
		terminal: true,
		link($scope, $element, $attr, ctrl, $transclude) {

			let modalSource = $element[0].outerHTML;
			let modal;
			let $modalRoot = $element.parent();
			$element.remove();
			$rootScope.$on('modal-toggled', (event, modalService, isOpened, data) => {

				if (modalService.getActiveWindowID() != $attr.id) {
					return ;
				}

				if (isOpened) {

					let $modalWindow = angular.element(modalSource)
						.removeClass('modal-source')
						.addClass('modal selected');

					$modalRoot.append($modalWindow);

					const $modalScope = $rootScope.$new(true);

					Object.assign($modalScope, data);
					$modalScope.modalOpened = true;
					$modalScope.close = () => {
						modalService.close();
					}

					modal = $compile($modalWindow)($modalScope);

				} else {

					if(modal) {
						// console.log('Destroy modal');
						modal.removeClass('selected');
						modal.scope().$destroy();
						modal.remove();
						modal = null;
					}
				}

			});
		}
	}
}];
