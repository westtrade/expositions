'use strict';

// const Dropzone = require('dropzone');
const templateUrl = require('./template.html');
const qwery = require('qwery');
// require('dropzone/src/dropzone.scss');




module.exports = () => {

	return {
		restrict: 'EAC',
		templateUrl,
		transclude: true,
		replace: true,
		scope: true,
		link($scope, $element, $attrs, $controllers, $transclude) {

			const element = $element[0];
			const {url = '/', name = ''} = $attrs;
			element.classList.add('dropzone');

			const [inputField] = qwery('input', element);
			inputField.setAttribute('name', name);

			// element.addEventListener('click', (event) => {
			//
			// 	console.log('Test');
			//
			// 	// const clickEvent = new CustomEvent('click', {});
			// 	// inputField.dispatchEvent(clickEvent);
			//
			// 	// inputField.click();
			//
			// 	console.log(inputField);
			//
			//
			//
			// 	event.preventDefault();
			//
			// })


			const dragHandler = (event) => {

				// console.log(event.type);
				// event.preventDefault();

				switch (event.type) {
				case 'dragenter':
				case 'dragover':
					// event.preventDefault();

					element.classList.add('over');
					break;

				case 'drop':
				case 'dragleave':
					element.classList.remove('over');


					// event.preventDefault();
					break;
				}

			};

			element.addEventListener('drag', dragHandler, false);
			element.addEventListener('dragstart', dragHandler, false);
			element.addEventListener('dragend', dragHandler, false);
			element.addEventListener('dragover', dragHandler, false);
			element.addEventListener('dragenter', dragHandler, false);
			element.addEventListener('dragleave', dragHandler, false);
			element.addEventListener('drop', dragHandler, false);


			inputField.addEventListener('change', (event) => {
				console.log(event);
			})

			const {placeholder, multiple, accept} = $attrs;

			Object.entries({placeholder, multiple, accept})
			.filter(([key, value]) => value !== undefined)
			.reduce((inputField, [key, value]) => {
				inputField.setAttribute(key, value);
				return inputField;
			}, inputField);

			$scope.hasChilds = false;
			$scope.placeholder = placeholder;

			$transclude((clone) => {

				$scope.hasChilds = !clone.length;

				// console.log(clone.length);


				// const currentDropzone = new Dropzone(element, {
				// 	url
				// });

			});

		}
	};
};
