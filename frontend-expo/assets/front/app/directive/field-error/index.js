'use strict';

const templateUrl = require('./template.html');

const getErrors = (input = '') => {
	let result = [];

	if (input && input.length) {
		try {
			result = JSON.parse(input)
		} catch (e) {
			console.error(e)
		}
	}

	return result;
};

module.exports = ['$parse', ($parse) => {
	return {
		restrict: 'E',
		templateUrl,
		replace: true,
		scope: true,
		link: ($scope, $element, $attrs) => {
			$scope.error = [];
			$scope.$parent.$watch($attrs.error, (fieldErrors) => {
				$scope.error = fieldErrors;
			});
		}
	}

}];
