'use strict';

const qwery = require('qwery');
const { shortId } = require('../../libs/utils');

const restrict = 'ACE';

const createTabHead = () => {
	let tabHead = document.createElement('div');
	tabHead.className = 'head';

	return tabHead;
};


const setActiveTab = (activeIdx) => {
	return (tab, tabIdx) => {
		tab.classList.toggle('active', activeIdx === tabIdx);
	};
}

const link = ($scope, $element, $attrs) => {

	const element = $element[0];
	let [tabHead = createTabHead()] = qwery('.head', element);

	$scope.switchTab = (activeTab  = 0) => {

		activeTab = parseInt(activeTab);

		// console.log(tabHead, element);

		qwery('a', tabHead).forEach(setActiveTab(activeTab));
		qwery('.tab', element).forEach(setActiveTab(activeTab));
	}

	tabHead.addEventListener('click',  (event) => {

		const tabIndex = qwery('a', tabHead).indexOf(event.target);

		if (tabIndex < 0) {
			return
		}

		$scope.switchTab(tabIndex);

		event.preventDefault();
		event.stopPropagation();
	});

	const activeIdx =  qwery('.tab').reduce((activeIdx, tab, index) => {

		if (tab.classList.contains('active') && !activeIdx) {
			activeIdx = index;
		}

		return activeIdx;

	}, 0);

	let {activeTab = activeIdx} = $scope;
	activeTab = parseInt(activeTab);

	if (isNaN(activeTab)) {
		activeTab = activeIdx;
	}

	tabHead = qwery('.tab', element)
		.map((tab, idx) => [tab, tab.title || 'Без названия ' + idx])
		.reduce(({tabHead, current}, [tab, tabTile]) => {

			let link = document.createElement('a');
			link.href = '#';
			link.innerHTML = tabTile;

			const tabId = shortId();

			link.setAttribute('data-tab', tabId);
			tab.setAttribute('data-tab', tabId);

			const nextElement = current ? current.nextSibling : tabHead.firstChild;
			current = nextElement ? tabHead.insertBefore(link, nextElement) : tabHead.appendChild(link);

			return {tabHead, current};

		}, {tabHead}).tabHead;

	if (!tabHead.parentNode) {
		element.insertBefore(tabHead, element.firstChild);
	}

	$scope.switchTab(activeTab);
	$scope.$watch('activeTab', (prevTab, activeTab) => {

		if (activeTab === undefined) {
			return ;
		}

		$scope.switchTab(activeTab);
	});
};

module.exports = [() => {

	return {
		restrict,
		link,
		scope: {
			activeTab: '='
		},
	};

}];
