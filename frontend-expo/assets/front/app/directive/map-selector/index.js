'use strict';

const qwery = require('qwery');
const loadMap = require('../../../libs/gmap');
const GMap = require('gmaps');

const templateUrl = require('./template.html');

const centerMap = (map, {geometry: {location}} = {geometry: {}}, cb = () => {}) => {
	if (!location) {
		throw new Error('Location must be defined into information');
	}

	const {lat, lng} = location;
	map.setCenter(lat(), lng(), cb)
};

const link = ($scope, $element, $attrs, $transclude) => {

	const element = $element[0];
	const [mapElement] = qwery('.map', element);
	const [addressSelector] = qwery('input', element);

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.


	const $injector = $element.injector();
	const $timeout = $injector.get('$timeout')


	$scope.provider = 'yandex';


	loadMap($scope.provider, (maps) => {
		// const map = new GMap({ el: mapElement, lat: 0, lng: 0, zoom: 15 });

	});



	// $timeout(() => {
	// 	$scope.provider = 'google';
	// }, 5000);





		// const autocomplete = new ymaps.SuggestView(addressSelector);
		// console.log(autocomplete.events);
		//
		// autocomplete.events.add('select', (event) => {
		// 	// console.log(autocomplete.state);
		// 	console.log(event.get('item'));
		// })


		// const autocomplete = new google.maps.places.Autocomplete(addressSelector, {types: ['geocode']});
		// autocomplete.addListener('place_changed', () => {
		// 	const place = autocomplete.getPlace();
		//
		// 	// console.log(place);
		// 	// console.log(place.address_components);
		// 	centerMap(map, place);
		// });




		//
		// $scope.$watch('selector.address', (value) => {
		//
		// 	GMap.geocode({
		// 		address: value,
		// 		callback: (result, status) => {
		// 			console.log(result);
		// 		}
		// 	});
		//
		// }, 'AIzaSyCQS8Ysgs4w8GDUJhe-ge3bm2w42eZhNBU');



		// GMap.geocode({
		// 	address: 'Россия',
		// 	callback: ([location], status) => {
		//
		//
		//
		// 		const {geometry: {location: {lat, lng}}} = location;
		// 		map.setCenter(lat(), lng(), () => {
		// 			console.log('Centred');
		// 		})
		// 	}
		// });

	// });

};

module.exports = () => {
	return {
		scope: true,
		transclude: true,
		restrict: 'E',
		templateUrl,
		link,
		scope: true,
	}
};
