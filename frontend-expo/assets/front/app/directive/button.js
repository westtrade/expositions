'use strict';


module.exports = ($compile) => {
	return {
		restrict: 'EC',
		link: ($scope, $element) => {
			const element = $element[0];
			const spanWrapper = document.createElement('span');
			spanWrapper.innerHTML = element.innerHTML;
			element.innerHTML = '';
			element.appendChild(spanWrapper);
			$compile(spanWrapper)($scope);
		}
	}
} ;
