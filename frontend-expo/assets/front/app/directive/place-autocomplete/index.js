'use strict';

const loadMap = require('../../../libs/gmap');
const qwery = require('qwery');


const setValue = (inputElement, value, {input = false, change = true} = {}) => {

	inputElement.value = value;

	if (input) {
		let changeEvent = new Event('change');
		input.dispatchEvent(changeEvent);
	}

	if (change) {
		let inputEvent = new Event('input');
		input.dispatchEvent(inputEvent);
	}
};




const providers = {

	yandex: {

		init: (element) => {

			let suggestView = new global.ymaps.SuggestView(element);
			suggestView.events.add('select', (event) => {
				const {value, displayName} = event.get('item');

				const customEventsData =  {
					detail: {
						item: event.get('item')
					},
					// 	bubbles: true,
					// 	cancelable: true
				};

				let changeEvent = new CustomEvent('change', customEventsData);
				element.dispatchEvent(changeEvent);

				//
				// global.ymaps.geocode(value, {provider: 'yandex'}).then((result) => {
				// 	console.log(result.geoObjects);
				//
				// 	// debugger;
				// })

			});

			// global.ymaps.ready(() => {
				// console.log('ymaps ready');
			// })
			// suggestView.state.events.add('change', (event) => {
			//
			// 	let activeIndex = suggestView.state.get('activeIndex');
			// 	if (typeof activeIndex == 'number') {
			// 		let activeItem = suggestView.state.get('items')[activeIndex];
			// 		console.log(activeItem);
			// 		// if (activeItem && activeItem.value != input.value) {
			// 		// 	input.value = activeItem.value;
			// 		// }
			// 	}
			//
			// });

			element.$destroy = () => {
				suggestView.destroy();
				suggestView = null;
			}
		},

		destroy: (element) => {
			// element.$destroy();
			// element.$destroy = null;
		}
	},

	google: {

		init: () => {

		},

		destroy: () => {

		}
	},
};

const changeProvider = (element, currentProvider, prev) => {

	if (currentProvider === prev) {
		return currentProvider;
	}

	if (prev && prev in providers) {
		providers[prev].destroy(element);
	}

	if (currentProvider in providers) {
		setTimeout(() => loadMap(currentProvider, () => providers[currentProvider].init(element)), 0);
	}

	return currentProvider;
};

module.exports = () => {
	return {
		restrict: 'A',
		require: '?ngModel',
		link: ($scope, $element, $attrs, ngModel) => {

			const selectorElement = $element[0];

			// if (ngModel && ngModel.$modelValue) {
			// 	setValue(selectorElement, ngModel.$modelValue, {input: true, change: true});
			//
			// 	$scope.$watch($attrs.ngModel, (currentValue) => {
			// 		setValue(selectorElement, ngModel.$modelValue, {input: true, change: true});
			// 	})
			// }

			// setValue(selectorElement, 123123);

			let current = changeProvider(selectorElement, $attrs.provider || 'yandex');
			$attrs.$observe('provider', (next) => {
				current = changeProvider(selectorElement, next, current);
			});

		}
	};

};
