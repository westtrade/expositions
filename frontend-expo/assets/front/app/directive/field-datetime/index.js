'use strict';

const qwery = require('qwery');

const template = `
	<div
		class="input"
		moment-picker="value"
		locale="{{options.locale}}"
		today="true"
		format="L"
		max-view="month"
		start-view="month"
	>
	<span ng-class="{placeholder: !(value && value.length)}">
		{{ value && value.length ? value : options.placeholder }}
	</span>
	</div>
	<input type="hidden">
	<ng-transclude></ng-transclude>
`;

module.exports = () => {
	return {
		template,
		transclude: true,
		scope: {},
		link($scope, $element, $attrs, controller, $transclude) {

			const element = $element[0];
			const [input] = qwery('input', element);

			const {
				name = '', locale = 'ru', today = true,
				maxView = 'month', startView='month',
				format = 'L', minView='day', placeholder = '',
			} = $attrs;

			$scope.value = null;

			$scope.options = {
				locale, today, maxView, startView, format, minView, placeholder,
			}

			input.setAttribute('name', name);

			$scope.$watch('value', (value) => {
				if (name && name.length) {
					input.form.setValue(name, value);
				}
			});

			// $transclude((clone) => {
			//
			// })
		}
	};

};
