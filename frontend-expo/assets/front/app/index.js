'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
const uiDND = require('angular-dnd-module');

global.moment = require('moment');
require('angular-moment-picker');
require('angular-moment-picker/dist/angular-moment-picker.css');

require('angular-dnd-module/css/stylesheet.css');
require('angular-loading-bar/src/loading-bar.css');

const loadingBar = require('angular-loading-bar');
const ngAnimate = require('angular-animate');

const app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'moment-picker', 'chieffancypants.loadingBar']);

const router = require('./router.js');
app.config(router);


// const requireAll = (requireContext) => requireContext.keys().map(requireContext);
// const templates = requireAll(require.context('../views', true, /^\.\/.*\.html$/));
//
// console.log(templates);
//
// app.run(['$templateCache', ($templateCache) => {
//
//
// }]);


const common = require('./filter/common');
Object.entries(common).reduce((app, [name, filter]) => app.filter(name, filter), app);

const styledInput = require('./directive/checkbox');

app
	.directive('button', require('./directive/button'))
	.directive('expoSlider', require('./directive/slider'))
	.directive('topBar', require('./directive/topbar'))
	.directive('styledInput', styledInput())
	.directive('styledCheckbox', styledInput())
	.directive('socialLogin', require('./directive/socialLogin'))
	.directive('masonryLayout', require('./directive/masonryLayout'))
	.directive('expoModalManager', require('./directive/modals'))
	.directive('modalSource', require('./directive/modal'))
	.directive('expoTabs', require('./directive/tabs'))
	.directive('fieldError', require('./directive/field-error'))
	.directive('inputMap', require('./directive/map-selector'))
	.directive('placeAutocomplete', require('./directive/place-autocomplete'))
	.directive('dropzoneField', require('./directive/dropzone-field'))
	.directive('datetimeField', require('./directive/field-datetime'))
	.directive('fieldEditor', require('./directive/field-editor'))
	.directive('letterCounter', require('./directive/letter-counter'))
;


app
	.controller('offersCatalog', require('./controllers/offersCatalog'))
	.controller('subjectSelectorModal', require('./controllers/subjectSelectorModal'))
	.controller('sliderController', require('./controllers/sliderController'))
	.controller('expositionSelectorModal', require('./controllers/expositionSelectorModal'))
	.controller('sliderEditorModal', require('./controllers/sliderEditorModal'))
	.controller('offerTeaserController', require('./controllers/offerTeaserController'))
;


app
	.factory('$modals', require('./service/Modal'))
	.factory('User', require('./service/User'))
	.factory('areYouSure', require('./service/areYouSure'))
;

app.filter('capitalize', function() {
	return function(input) {
		return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	}
});

module.exports = app;
