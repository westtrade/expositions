'use strict';


module.exports = {
	range() {
		return (input, total) => {
			
			total = parseInt(total);

			for (var i=0; i<total; i++) {
				input.push(i);
			}

			return input;
		}
	},

	groupBy() {
		return (collection, property) => {

			if(!collection) {
				return collection;
			}

			const result =  collection.reduce((result, currentItem) => {

				const group = currentItem[property] || 'undefined';
				// console.log(group);
				if (!(group in result)) {
					result[group] = [];
				}

				result[group].push(currentItem);
				return result;
			}, {});

			// console.log(result);

			return result;
		}
	}
};
