'use strict';

require('normalize.css/normalize.css');
require('font-awesome/css/font-awesome.css');
require('./css/jquery.formstyler.css');
require('./libs/ie8');

// global.iosDragDropShim = { enableEnterLeave: true };
// require('drag-drop-webkit-mobile');

const app = require('./app');
require('./style.scss');
