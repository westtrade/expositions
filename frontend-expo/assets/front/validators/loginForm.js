'use strict';

const constraint = {
	login: {
		presence: true,
		length: {
			minimum: 6,
			message: "must be at least 6 characters"
		}

	},
	password: {
		presence: true,
		length: {
			minimum: 6,
			message: "must be at least 6 characters"
		}
	}
};


module.exports = constraint;
