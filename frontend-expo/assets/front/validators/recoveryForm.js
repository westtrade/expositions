'use strict';

const constraint = {

	password: {
		presence: true,
		length: {
			minimum: 6,
			message: "must be at least 6 characters"
		}
	},

	repeat: {
		presence: true,
		equality: {
			attribute: 'password',
			message: 'is not equal to Password field',
		}
	},

};


module.exports = constraint;
