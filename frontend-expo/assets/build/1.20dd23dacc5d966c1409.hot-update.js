webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(39);
	var safeApply = __webpack_require__(73);
	
	var homePage = __webpack_require__(55);
	var loginPage = __webpack_require__(56);
	var registrationPage = __webpack_require__(57);
	var recoveryPage = __webpack_require__(58);
	var changePasswordPage = __webpack_require__(59);
	
	var getFormData = function getFormData(form) {
		return Object.entries(form.elements).reduce(toData, {});
	};
	
	var validateForm = function validateForm($scope, form) {
		var constraint = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
		formData = getFormData(form);
	
		var error = validate(formData, constraint);
		angular.extend($scope.error, error);
	};
	
	var formApply = function formApply($scope, form, constraint) {
		var _ref = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
		    _ref$success = _ref.success,
		    success = _ref$success === undefined ? function () {} : _ref$success,
		    _ref$error = _ref.error,
		    error = _ref$error === undefined ? function () {} : _ref$error;
	
		var isDirty = false;
		var formData = {};
	
		$scope.error = {};
	
		form.addEventListener('input', function (event) {
	
			if (isDirty) {
				validateForm($scope, form, constraint);
			}
		}, true);
	
		$scope.submit = function ($event) {
			isDirty = true;
			validateForm($scope, form, constraint);
			$event.preventDefault();
		};
	};
	
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	var validate = __webpack_require__(92);
	validate.Promise = Promise;
	
	var loginConstraints = {
		login: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
	
		},
		password: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	var toData = function toData() {
		var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var _ref2 = arguments[1];
	
		var _ref3 = _slicedToArray(_ref2, 2),
		    fieldName = _ref3[0],
		    input = _ref3[1];
	
		result[fieldName] = input.value;
		return result;
	};
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/',
			templateUrl: homePage,
			controller: ['$scope', function ($scope) {}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage,
			controller: ['$scope', '$element', function ($scope, $element) {
				var form = $element.find('form')[0];
				formApply($scope, form, loginConstraints);
			}]
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', function ($scope) {
	
				$scope.data = {
					type: 'company'
				};
	
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					$scope.randomSeed = guid();
				};
	
				$scope.select = function (type) {
					$scope.data.type = type;
				};
	
				$scope.error = {};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', function ($scope) {
				$scope.email = null;
				$scope.data = {
					email: null
				};
	
				$scope.setMail = function () {
					safeApply($scope, function () {
						$scope.email = $scope.data.email;
					});
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.20dd23dacc5d966c1409.hot-update.js.map