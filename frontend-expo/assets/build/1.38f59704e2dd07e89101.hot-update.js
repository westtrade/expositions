webpackHotUpdate(1,{

/***/ 33:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(34);
	
	module.exports = function () {
		return {
			priority: 15,
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.38f59704e2dd07e89101.hot-update.js.map