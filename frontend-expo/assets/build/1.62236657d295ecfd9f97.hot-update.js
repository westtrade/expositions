webpackHotUpdate(1,{

/***/ 105:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(60);
	var angular = __webpack_require__(44);
	
	var ROLES = ['guest', 'exponent', 'organizer', 'moderator', 'administrator'];
	
	var RULES = {
		navbar: ['!guest']
	};
	
	var ANONYMOUS_USER = {
		role: 'guest'
	};
	
	var User = function () {
		function User() {
			_classCallCheck(this, User);
	
			this.state = {};
			this.setState(ANONYMOUS_USER);
	
			this.watchers = [];
		}
	
		_createClass(User, [{
			key: 'bind',
			value: function bind($scope) {
	
				this.watchers.push($scope);
	
				console.log(this.watchers.indexOf($scope));
			}
		}, {
			key: 'logout',
			value: function logout() {
				var _this = this;
	
				return new Promise(function (resolve, reject) {
					_this.setState(ANONYMOUS_USER);
					resolve();
				});
			}
		}, {
			key: 'setState',
			value: function setState() {
				var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
				this.state = angular.extend({}, this.state, data);
			}
		}, {
			key: 'getState',
			value: function getState() {
				return angular.extend({}, this.state);
			}
		}, {
			key: 'hasAccess',
			value: function hasAccess(ruleId) {
	
				var ruleExists = rule in RULES;
	
				if (!ruleExists) {
					return false;
				}
	
				var rule = RULES[ruleId];
				var result = false;
	
				return result;
			}
		}]);
	
		return User;
	}();
	
	var currentUser = new User();
	
	module.exports = function () {
		return currentUser;
	};

/***/ }

})
//# sourceMappingURL=1.62236657d295ecfd9f97.hot-update.js.map