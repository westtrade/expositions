webpackHotUpdate(1,{

/***/ 61:
/***/ function(module, exports) {

	'use strict';
	
	var customEvents = {
		onResizeEnd: []
	};
	
	var delta = 100;
	var timer = false,
	    startTime = void 0;
	
	var onResizeEnd = function onResizeEnd() {
	
		if (new Date() - startTime < delta) {
			timer = setTimeout(onResizeEnd, delta);
		} else {
			timer = false;
	
			if ('onResizeEnd' in customEvents) {
				customEvents['onResizeEnd'].forEach(function (fn) {
					return fn();
				});
			}
		}
	};
	
	window.addEventListener('resize', function () {
		startTime = new Date();
		if (timer === false) {
			timer = setTimeout(onResizeEnd, delta);
		}
	}, true);
	
	module.exports = customEvents;

/***/ }

})
//# sourceMappingURL=1.6bb3daa32af6faa5d266.hot-update.js.map