webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($scope) {
		var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
	
		if (!$scope.$root) {
			return fn();
		}
	
		var $$phase = $scope.$root.$$phase;
	
		$$phase == '$apply' || $$phase == '$digest' ? fn() : $scope.$apply(fn);
	};

/***/ }

})
//# sourceMappingURL=1.43912974f624b13d60c4.hot-update.js.map