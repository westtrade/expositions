webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var changePasswordPage = __webpack_require__(61);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var registrationConstraints = __webpack_require__(80);
	
	var changePassword = {
		url: '/user/change-password',
		templateUrl: changePasswordPage,
		controller: ['$scope', '$element', function ($scope, $element) {
			var form = $element.find('form')[0];
			formApply($scope, form, registrationConstraints);
	
			$scope.setMail = function ($event) {
	
				form.validate(function (error, formData) {});
	
				$event.preventDefault();
			};
		}]
	};
	
	var offersCatalog = {
		url: '/user/offers',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	var eventsCatalog = {
		url: '/user/events',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	var accountPage = {
		url: '/user/account',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	var cart = {
		url: '/user/cart',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	module.exports = {
		changePassword: changePassword,
		offersCatalog: offersCatalog,
		eventsCatalog: eventsCatalog,
		accountPage: accountPage,
		cart: cart
	};

/***/ }

})
//# sourceMappingURL=1.45182d9b0ca019fa5afe.hot-update.js.map