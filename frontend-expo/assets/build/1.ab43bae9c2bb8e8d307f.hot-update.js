webpackHotUpdate(1,{

/***/ 87:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var angular = __webpack_require__(39);
	var $body = angular.element(document.body);
	
	var isFunction = function isFunction(obj) {
		return !!(obj && obj.constructor && obj.call && obj.apply);
	};
	
	var clean = function clean() {
		var inputData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
		var json = angular.toJson(inputData);
		return JSON.parse(json);
	};
	
	var ModalManager = function () {
		function ModalManager($rootScope) {
			var _this = this;
	
			_classCallCheck(this, ModalManager);
	
			this.active_window = null;
			this.$root = $rootScope;
	
			var actions = {
				27: function _() {
					return _this.close();
				}
			};
	
			document.addEventListener('keyup', function (event) {
				var keyCode = event.keyCode;
	
				actions[keyCode] && actions[keyCode]();
			});
	
			this.promise = {
				reject: null,
				resolve: null
			};
		}
	
		_createClass(ModalManager, [{
			key: 'isActive',
			value: function isActive(modalId) {
				return modalId && modalId.length ? this.getActiveWindowID() === modalId : !!this.getActiveWindowID();
			}
		}, {
			key: 'getActiveWindowID',
			value: function getActiveWindowID() {
				var openedModalID = $body.attr('data-active-modal');
				return openedModalID;
			}
		}, {
			key: 'isOpened',
			value: function isOpened() {
				var modalWindowId = this.getActiveWindowID();
				return modalWindowId && modalWindowId.length;
			}
		}, {
			key: 'open',
			value: function open(openingModalWindowId) {
				var _this2 = this;
	
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
				if (typeof data === 'function') {
					callback = data;
					data = {};
				}
	
				data = clean(data);
				// let activeModalID = this.getActiveWindowID();
				this.close();
	
				if (!openingModalWindowId) {
					//TODO error message
					return false;
				}
	
				$body.attr('data-active-modal', openingModalWindowId);
				this.$root.$broadcast('modal-toggled', this, true, data);
	
				return new Promise(function (resolve, reject) {
					_this2.promise.resolve = resolve;
					_this2.promise.reject = reject;
				});
			}
		}, {
			key: 'close',
			value: function close(result) {
	
				var modalWindowId = this.getActiveWindowID();
	
				if (!modalWindowId || !modalWindowId.length) {
					return false;
				}
	
				this.$root.$broadcast('modal-toggled', this, false, result);
				$body.removeAttr('data-active-modal');
	
				if (result instanceof Error) {
	
					if (isFunction(this.promise.reject)) {
						reject(result);
					}
				} else {
	
					if (isFunction(this.promise.resolve)) {
						resolve(result);
					}
				}
	
				this.promise.reject = null;
				this.promise.resolve = null;
	
				return false;
			}
		}, {
			key: 'toggleModal',
			value: function toggleModal() {
	
				return this.isActive(openModalId) ? this.close() : this.open(openModalId);
			}
		}]);
	
		return ModalManager;
	}();
	
	module.exports = ['$rootScope', function ($rootScope) {
		return new ModalManager($rootScope);
	}];

/***/ }

})
//# sourceMappingURL=1.ab43bae9c2bb8e8d307f.hot-update.js.map