webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	module.exports = ['$parse', function ($parse) {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			replace: true,
			scope: {
				error: '='
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.b68c0f6d24f7dc6b390f.hot-update.js.map