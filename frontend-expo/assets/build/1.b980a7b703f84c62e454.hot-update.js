webpackHotUpdate(1,{

/***/ 271:
/***/ function(module, exports) {

	'use strict';
	
	var createAppend = function createAppend(element) {
	
		var appendElement = document.createElement('div');
	
		appendElement.classList.add('append');
	
		if (element.nextElementSibling) {
			element.parentNode.insertBefore(appendElement, element.nextElementSibling);
		} else {
			element.appendChild(appendElement);
		}
	
		return appendElement;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			link: function link($scope, $element, $attrs) {
				var _$attrs$letterCounter = $attrs.letterCounter,
				    letterCounter = _$attrs$letterCounter === undefined ? -1 : _$attrs$letterCounter;
	
				var element = $element[0];
	
				var appendElement = element.nextElementSibling && element.nextElementSibling.classList.contains('append') ? element.nextElementSibling : createAppend(element);
	
				letterCounter = parseInt(letterCounter);
				console.log(letterCounter);
	
				if (letterCounter > 0) {
					appendElement.innerHTML = element.value.length + ' / ' + letterCounter;
				}
	
				element.addEventListener('input', function (event) {
					appendElement.innerHTML = element.value.length + ' / ' + letterCounter;
				});
	
				element.addEventListener('keypress', function (event) {
					if (element.value.length >= letterCounter) {
						event.preventDefault();
					}
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.b980a7b703f84c62e454.hot-update.js.map