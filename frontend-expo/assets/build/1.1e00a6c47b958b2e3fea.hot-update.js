webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var gmapIsInitialized = false;
	
	var getMap = function getMap() {
	
		if (gmapIsInitialized) {
			return Promise.resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=' + randomCallbackName;
	
		var result = new Promise(function (resolve) {
	
			global[randomCallbackName] = function () {
				gmapIsInitialized = true;
				delete global[randomCallbackName];
				resolve();
			};
		});
	
		document.body.appendChild(script);
		return result;
	};
	
	module.exports = {
		getMap: getMap
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.1e00a6c47b958b2e3fea.hot-update.js.map