webpackHotUpdate(1,{

/***/ 115:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(69),
	    shortId = _require.shortId;
	
	var TAB_ID = shortId();
	
	console.log('initial tab id');
	
	var getStorage = function getStorage() {
		try {
			if ('localStorage' in window && window['localStorage'] !== null) {
				return localStorage;
			}
		} catch (e) {
			return false;
		}
	
		return false;
	};
	
	var on = function on() {
		var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'message';
		var messageReciever = arguments[1];
	
		window.addEventListener("storage", function (event) {
			if (event.key === type) {
				var _JSON$parse = JSON.parse(event.newValue),
				    data = _JSON$parse.data,
				    id = _JSON$parse.id;
	
				console.log(TAB_ID, id);
	
				if (TAB_ID !== id) {
					messageReciever(event, data);
				}
			}
		});
	};
	
	var broadcast = function broadcast(type) {
		var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
		var storage = getStorage();
	
		if (storage) {
	
			var eventData = {
				id: TAB_ID,
				data: data
			};
			console.log('Broadcast', eventData);
	
			storage.setItem(type, JSON.stringify(eventData));
		} else {
			console.error('Local storage dosn\'t support!');
		}
	};
	
	var get = function get(type) {
		var defaultData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		var storage = getStorage();
		var eventData = storage.getItem(type);
	
		var _ref = JSON.parse(eventData) || {},
		    _ref$data = _ref.data,
		    data = _ref$data === undefined ? defaultData : _ref$data,
		    recievedTabId = _ref.TAB_ID;
	
		return data;
	};
	
	module.exports = {
		getStorage: getStorage,
		on: on,
		broadcast: broadcast,
		get: get
	};

/***/ }

})
//# sourceMappingURL=1.67981d4c1238585bc6e1.hot-update.js.map