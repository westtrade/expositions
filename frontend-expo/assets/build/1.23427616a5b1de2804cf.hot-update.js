webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			transclude: 'element',
			restrict: 'C',
			priority: 600,
			terminal: true,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
							var modalElement = angular.element(modalSource);
							$element.after(newModal);
							modalElement.removeClass('modal-source').addClass('modal selected');
							var $modalScope = $rootScope.$new(true);
							$modalScope.initial = data;
	
							newModal = $compile(modalElement)($modalScope);
						} else if (newModal) {
							newModal.scope().$destroy();
							newModal.remove();
							newModal = null;
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.23427616a5b1de2804cf.hot-update.js.map