webpackHotUpdate(1,{

/***/ 38:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var qwery = __webpack_require__(34);
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
				$scope.active = active;
	
				qwery('.slide', $element[0]).forEach(function (item, idx) {
					if (item.classList.contains('active')) {
						item.classList.remove('active');
					}
	
					if (idx === active) {
						item.classList.add('active');
					}
				});
			};
	
			$scope.next = function () {
				var next = $scope.active + 1;
				var current = next >= $scope.total ? 0 : next;
				$scope.setActive(current);
			};
			$scope.prev = function () {
				var prev = $scope.active - 1;
				var current = prev <= 0 ? $scope.total : next;
				$scope.setActive(current);
			};
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.43aafbffca5d7d8cd15d.hot-update.js.map