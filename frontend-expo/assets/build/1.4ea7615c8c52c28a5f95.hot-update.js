webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var ModalManager = function () {
		function ModalManager() {
			_classCallCheck(this, ModalManager);
	
			this.active_window = null;
		}
	
		_createClass(ModalManager, [{
			key: 'open',
			value: function open(window_name) {
				this.active_window = window_name;
			}
		}, {
			key: 'close',
			value: function close() {}
		}]);
	
		return ModalManager;
	}();
	
	var modalManager = new ModalManager();
	
	module.exports = function () {
		return modalManager;
	};

/***/ }

})
//# sourceMappingURL=1.4ea7615c8c52c28a5f95.hot-update.js.map