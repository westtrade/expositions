webpackHotUpdate(1,{

/***/ 46:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
		range: function range() {
			return function (input, total) {
	
				total = parseInt(total);
	
				for (var i = 0; i < total; i++) {
					input.push(i);
				}
	
				return input;
			};
		},
		groupBy: function groupBy() {
			return function (collection, property) {
	
				if (!collection) {
					return collection;
				}
	
				return collection.reduce(function (result, currentItem) {
	
					var group = currentItem[property] || 'udnefined';
	
					if (!(group in result)) {
						result[group] = [];
					}
	
					result[group].push(currentItem);
				}, {});
			};
		}
	};

/***/ }

})
//# sourceMappingURL=1.02d71e61837c3e38ef78.hot-update.js.map