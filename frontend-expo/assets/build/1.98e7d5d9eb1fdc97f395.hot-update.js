webpackHotUpdate(1,{

/***/ 109:
/***/ function(module, exports) {

	'use strict';
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var ExpoError = function (_Error) {
		_inherits(ExpoError, _Error);
	
		function ExpoError(_ref) {
			var message = _ref.message,
			    stack = _ref.stack;
	
			_classCallCheck(this, ExpoError);
	
			var _this = _possibleConstructorReturn(this, (ExpoError.__proto__ || Object.getPrototypeOf(ExpoError)).call(this, message));
	
			_this.serverStack = stack;
			return _this;
		}
	
		return ExpoError;
	}(Error);
	
	module.exports = ExpoError;

/***/ }

})
//# sourceMappingURL=1.98e7d5d9eb1fdc97f395.hot-update.js.map