webpackHotUpdate(1,{

/***/ 57:
/***/ function(module, exports) {

	'use strict';
	
	var EDITOR_MODES = ['list', 'edit', 'success'];
	
	module.exports = ['$scope', '$modals', function ($scope, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		$scope.mode = EDITOR_MODES[0];
		// $scope.mode = EDITOR_MODES[1];
	
		// $scope.slide = $scope.sliderList[0];
	
		$scope.delete = function (index) {
			$scope.sliderList.splice(parseInt(index), 1);
		};
	
		$scope.save = function () {
			$modals.close({ sliderList: sliderList });
		};
	
		$scope.create = function () {};
	
		$scope.saveSlider = function () {};
	
		$scope.editSlide = function (slide) {
			$scope.slide = slide;
			$scope.mode = EDITOR_MODES[1];
		};
	
		$scope.deleteCover = function () {
			$scope.slide.cover = null;
		};
	
		$scope.createSlide = function () {
			$scope.slide = {};
			$scope.mode = EDITOR_MODES[1];
		};
	
		$scope.toList = function () {
			$scope.slide = null;
			$scope.mode = EDITOR_MODES[0];
		};
	
		// $scope.close = () => {
		// 	const {sliderList} = $scope;
		// 	$modals.close({sliderList});
		// }
	}];

/***/ }

})
//# sourceMappingURL=1.099894b3e97e58b9bfe5.hot-update.js.map