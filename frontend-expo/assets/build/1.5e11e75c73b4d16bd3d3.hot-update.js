webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var qwery = __webpack_require__(83);
	var loadGMap = __webpack_require__(116);
	var GMap = __webpack_require__(101);
	
	var templateUrl = __webpack_require__(102);
	
	var centerMap = function centerMap() {
		var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { geometry: {} },
		    location = _ref.geometry.location;
	
		var cb = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
		if (!location) {
			throw new Error('Location must be defined into information');
		}
	
		var lat = location.lat,
		    lng = location.lng;
	
		map.setCenter(lat(), lng(), cb);
	};
	
	var link = function link($scope, $element, $attrs, $transclude) {
	
		var element = $element[0];
	
		var _qwery = qwery('.map', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    mapElement = _qwery2[0];
	
		loadGMap(function () {
	
			var map = new GMap({ el: mapElement, lat: 0, lng: 0, zoom: 15 });
			$scope.$watch('selector.address', function (value) {
	
				GMap.geocode({
					address: value,
					callback: function callback(result, status) {
						console.log(result);
					}
				});
			}, 'AIzaSyCQS8Ysgs4w8GDUJhe-ge3bm2w42eZhNBU');
	
			GMap.geocode({
				address: 'Россия',
				callback: function callback(_ref2, status) {
					var _ref3 = _slicedToArray(_ref2, 1),
					    location = _ref3[0];
	
					var _location$geometry$lo = location.geometry.location,
					    lat = _location$geometry$lo.lat,
					    lng = _location$geometry$lo.lng;
	
					map.setCenter(lat(), lng(), function () {
						console.log('Centred');
					});
				}
			});
		});
	};
	
	module.exports = function () {
		return _defineProperty({
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			link: link
		}, 'scope', true);
	};

/***/ }

})
//# sourceMappingURL=1.5e11e75c73b4d16bd3d3.hot-update.js.map