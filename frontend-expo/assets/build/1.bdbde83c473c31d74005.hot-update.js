webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', function ($rootScope) {
		return {
			transclude: 'element',
			restrict: 'C',
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
	
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					// const currentModalId = clone[0].id;
	
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
	
							newModal = angular.element(modalSource);
							$element.after(newModal);
							newModal.removeClass('modal-source').addClass('modal selected');
	
							console.log(newModal);
						} else {
							if (newModal) {
								console.log(newModal);
								newModal.remove();
								newModal = null;
							}
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.bdbde83c473c31d74005.hot-update.js.map