webpackHotUpdate(1,{

/***/ 72:
/***/ function(module, exports) {

	'use strict';
	
	var translate = {
		'Login can\'t be blank': 'Поле `Логин` не может быть пустым',
		'Login must be at least 6 characters': 'Поле `Логин` должно быть больше 6 символов',
		'Password can\'t be blank': 'Поле `Пароль` не может быть пустым',
		'Password must be at least 6 characters': 'Поле `Пароль` должно быть больше 6 символов',
		'Full name can\'t be blank': 'Поле `Имя, Фамилия, Отчество` не может быть пустым',
		'Full name must be at least 6 characters': 'Поле `Имя, Фамилия, Отчество` должно быть больше 6 символов',
		'Email can\'t be blank': 'Поле Email не может быть пустым',
		'Email must be at least 6 characters': 'Поле `Email` должно быть больше 6 символов',
		'Email is not a valid email': 'Поле `Email` заполнено не правильно',
		'Phone can\'t be blank': 'Поле `Телефон` не может быть пустым',
		'Phone must be at least 6 characters': 'Поле `Телефон` должно быть больше 6 символов',
		'Repeat can\'t be blank': 'Поле `Повторите пароль` не может быть пустым',
		'Captcha can\'t be blank': 'Поле `Код` не может быть пустым'
	};
	
	module.exports = translate;

/***/ }

})
//# sourceMappingURL=1.0b2438242f5ae52019cc.hot-update.js.map