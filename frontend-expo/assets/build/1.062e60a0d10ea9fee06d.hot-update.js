webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var Dropzone = __webpack_require__(49);
	var templateUrl = __webpack_require__(240);
	var qwery = __webpack_require__(93);
	// require('dropzone/src/dropzone.scss');
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			scope: {},
			link: function link($scope, $element, $attrs, $controllers, $transclude) {
	
				var element = $element[0];
				var _$attrs$url = $attrs.url,
				    url = _$attrs$url === undefined ? '/' : _$attrs$url,
				    _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name;
	
				element.classList.add('dropzone');
	
				var _qwery = qwery('input', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    inputField = _qwery2[0];
	
				inputField.setAttribute('name', name);
	
				$transclude(function (clone) {
					// const currentDropzone = new Dropzone(element, {
					// 	url
					// });
	
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.062e60a0d10ea9fee06d.hot-update.js.map