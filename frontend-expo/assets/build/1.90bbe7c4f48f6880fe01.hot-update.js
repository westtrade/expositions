webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var ymapInitialized = false;
	
	var loadYMap = function loadYMap(resolve, key) {
	
		if (ymapInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=' + randomCallbackName;
	
		if (key) {
			script.src = script.src + '&key=' + key;
		}
	
		global[randomCallbackName] = function () {
			ymapInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var gmapIsInitialized = false;
	var loadGMap = function loadGMap(resolve, key) {
	
		if (gmapIsInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=' + randomCallbackName;
	
		if (key) {
			script.src = script.src + '&key=' + key;
		}
	
		global[randomCallbackName] = function () {
			gmapIsInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var loadMultiplie = function loadMultiplie() {
		var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
		    gMapKey = _ref.gMapKey,
		    yMapKey = _ref.yMapKey;
	
		var resolve = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
	
		var loadStack = [loadGMap, loadYMap];
		var results = {};
	
		var next = function next(caller) {
			return function next(result) {
				var idx = loadStack.indexOf(caller);
	
				console.log(idx);
	
				if (idx >= 0) {
					loadStack.splice(idx, 1);
					results[idx] = result;
				}
	
				console.log('loadStack.length', loadStack.length);
	
				if (loadStack.length === 0) {
					results = Array.from(results);
					resolve(results);
	
					results = null;
					loadStack = null;
				}
	
				caller = null;
			};
		};
	
		loadGMap(next(loadGMap), gMapKey);
		loadYMap(next(loadYMap), yMapKey);
	};
	
	module.exports = function (resolve, gMapKey) {
		loadMultiplie({ gMapKey: gMapKey }, resolve);
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.90bbe7c4f48f6880fe01.hot-update.js.map