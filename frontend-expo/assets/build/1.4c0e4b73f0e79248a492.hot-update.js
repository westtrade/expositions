webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var ymapInitialized = false;
	
	var loadYMap = function loadYMap(resolve, key) {
	
		if (ymapInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=' + randomCallbackName;
	
		if (key) {
			script.src = script.src + '&key=' + key;
		}
	
		global[randomCallbackName] = function () {
			ymapInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var gmapIsInitialized = false;
	var loadGMap = function loadGMap(resolve, key) {
	
		if (gmapIsInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=' + randomCallbackName;
	
		if (key) {
			script.src = script.src + '&key=' + key;
		}
	
		global[randomCallbackName] = function () {
			gmapIsInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var next = function next(result) {
		console.log(arguments.callee());
	};
	
	var loadMultiplie = function loadMultiplie() {
		var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
		    gMapKey = _ref.gMapKey,
		    yMapKey = _ref.yMapKey;
	
		var resolve = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
	
		var loadStack = [loadGMap, loadYMap];
	
		loadGMap(next, gMapKey);
		loadYMap(next, yMapKey);
	};
	
	module.exports = function (resolve, gMapKey) {
		loadMultiplie({ gMapKey: gMapKey }, resolve);
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.4c0e4b73f0e79248a492.hot-update.js.map