webpackHotUpdate(1,{

/***/ 79:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(80);
	
	module.exports = function () {
		return {
			priority: 15,
			restrict: 'AEM',
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				attrs.$observe('styledInput', function () {
					console.log('Observer');
					styleInputs(element);
				});
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.12551c340ea4fb4b6b7a.hot-update.js.map