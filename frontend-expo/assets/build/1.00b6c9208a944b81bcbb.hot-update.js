webpackHotUpdate(1,{

/***/ 83:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(44);
	
	var _require = __webpack_require__(81),
	    getOptions = _require.getOptions,
	    getSearchOptions = _require.getSearchOptions;
	
	var _require2 = __webpack_require__(84),
	    attributesToString = _require2.attributesToString,
	    makeHtml = _require2.makeHtml,
	    copyAttributes = _require2.copyAttributes;
	
	var qwery = __webpack_require__(78);
	var compileElement = __webpack_require__(85);
	
	var isiOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/i) && !navigator.userAgent.match(/(Windows\sPhone)/i) ? true : false;
	var isAndroid = navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/(Windows\sPhone)/i) ? true : false;
	
	var disableScrollingListener = function disableScrollingListener(event) {
	
		var scrollTo = null;
		switch (true) {
			case event.type == 'mousewheel':
				scrollTo = event.wheelDelta * -1;
				break;
			case event.type == 'DOMMouseScroll':
				scrollTo = 40 * event.detail;
				break;
		}
	
		if (scrollTo) {
			event.stopPropagation();
			event.preventDefault();
			this.scrollTop = scrollTo + this.scrollTop;
		}
	};
	
	var preventScrolling = function preventScrolling(elements) {
		var elementsCount = elements.length;
		while (elementsCount--) {
			var element = elements[elementsCount];
			element.removeEventListener('mousewheel', disableScrollingListener);
			element.removeEventListener('DOMMouseScroll', disableScrollingListener);
			element.addEventListener('mousewheel', disableScrollingListener);
			element.addEventListener('DOMMouseScroll', disableScrollingListener);
		}
	};
	
	var generateOptionsList = function generateOptionsList(optionsList, styleOptions) {
	
		var stringOptionsList = [];
		for (var i = 0; i < optionsList.length; i++) {
	
			var currentOption = optionsList[i];
	
			var classList = '';
			if (currentOption.parentNode.selectedIndex === i) {
				classList = classList + ' selected sel';
			}
	
			if (currentOption.disabled) {
				classList = classList + ' disabled';
			}
	
			var overridedAttributes = { class: classList };
			if (currentOption.className && currentOption.className.length) {
				overridedAttributes['data-jqfs-class'] = currentOption.className;
			}
	
			if (currentOption.parentNode.tagName.toLowerCase() === 'optgroup') {
				overridedAttributes['class'] += ' option';
	
				var optGroupClasses = currentOption.parentNode.className && currentOption.parentNode.className.length ? currentOption.parentNode.className : '';
	
				if (optGroupClasses.length) {
					overridedAttributes['class'] += ' ' + optGroupClasses;
				}
	
				if (currentOption.parentNode.firstChild === currentOption) {
					stringOptionsList.push('<li class="optgroup ' + optGroupClasses + '">' + currentOption.parentNode.getAttribute('label') + '</li>');
				}
			}
	
			// if (i === 0 && currentOption.innerHTML.trim() === '' && !currentOption.parentNode.hasAttribute('placeholder')) {
			// 	overridedAttributes['style'] = 'display: none;';
			// }
	
			var optionAttrs = attributesToString(currentOption, overridedAttributes, styleOptions);
			stringOptionsList.push('<li ' + optionAttrs.trim() + '>' + currentOption.innerHTML + '</li>');
		}
	
		return stringOptionsList;
	};
	
	document.addEventListener('click', function (event) {
		var openedItems = qwery('.jq-selectbox.opened');
		var openedCount = openedItems.length;
	
		while (openedCount--) {
			var item = openedItems[openedCount];
			item.classList.remove('opened');
	
			var _qwery = qwery('.jq-selectbox__dropdown', item),
			    _qwery2 = _slicedToArray(_qwery, 1),
			    dropdownMenu = _qwery2[0];
			// dropdownMenu.style.display = 'none';
	
		}
	});
	
	var parseRepeatItems = /[<][!]-- ngRepeat:([^-]+) --[>]/gim;
	
	var render = function render(element, styleOptions) {
	
		var optionsList = qwery('option', element) || [];
		optionsList = optionsList.filter(function (option) {
			console.log(option.innerHTML, option.innerHTML.length);
			return option.value !== '? number:1 ?' && option.value === '?' && !option.innerHTML;
		});
	
		var stringOptionsList = generateOptionsList(optionsList, styleOptions);
	
		var placeholder = element.getAttribute('placeholder');
	
		var searchOptions = getSearchOptions(styleOptions, element);
		var searchTemplate = searchOptions.enable ? '\n\t\t\t<div ' + (stringOptionsList.length ? '' : 'style="display: none;"') + ' class="jq-selectbox__search">\n\t\t\t\t<input type="search" autocomplete="off" placeholder="' + searchOptions.placeholder + '">\n\t\t\t</div>\n\t\t\t<div style="display: none;" class="jq-selectbox__not-found">' + searchOptions.notFound + '</div>\n\t\t' : '';
	
		var selectedElement = optionsList[element.selectedIndex];
	
		var placeholderText = selectedElement ? selectedElement.innerHTML.trim() : '';
		placeholderText = placeholderText === '' ? placeholder : placeholderText;
	
		var template = '\n\t\t<div class="jq-selectbox jqselect">\n\t\t\t<div class="jq-selectbox__select">\n\t\t\t\t<div class="jq-selectbox__select-text ' + (placeholderText === placeholder ? 'placeholder' : '') + '">\n\t\t\t\t\t' + (placeholderText || '') + '\n\t\t\t\t</div>\n\t\t\t\t<div class="jq-selectbox__trigger">\n\t\t\t\t\t<div class="jq-selectbox__trigger-arrow"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class="jq-selectbox__dropdown">\n\t\t\t\t\t' + searchTemplate + '\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t' + stringOptionsList.join('') + '\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t';
	
		var styledElement = compileElement(template, element, styleOptions);
	
		styledElement.style.display = 'inline-block';
		styledElement.style.position = 'relative';
		// styledElement.style.zIndex = styleOptions.singleSelectzIndex;
	
		element.style.margin = 0;
		element.style.padding = 0;
		element.style.position = 'absolute';
		element.style.left = 0;
		element.style.top = 0;
		element.style.width = '100%';
		element.style.height = '100%';
		element.style.opacity = 0;
	
		if (element.disabled) {
			return false;
		}
	
		var _qwery3 = qwery('.jq-selectbox__select', styledElement),
		    _qwery4 = _slicedToArray(_qwery3, 1),
		    dropdownToggler = _qwery4[0];
	
		var _qwery5 = qwery('.jq-selectbox__dropdown', styledElement),
		    _qwery6 = _slicedToArray(_qwery5, 1),
		    dropdownMenu = _qwery6[0];
	
		var _qwery7 = qwery('.jq-selectbox__select-text', styledElement),
		    _qwery8 = _slicedToArray(_qwery7, 1),
		    selectPlaceholder = _qwery8[0];
	
		dropdownToggler.addEventListener('click', function (event) {
	
			var anotherSelectos = qwery('.jq-selectbox.opened');
	
			anotherSelectos.filter(function (item) {
				return item !== styledElement;
			}).forEach(function (item) {
				item.classList.remove('opened');
			});
	
			if (styledElement.className.indexOf('opened') >= 0) {
				var _event = new CustomEvent('close', {});
				element.dispatchEvent(_event);
			}
	
			element.focus();
	
			if (isiOS) {
				return;
			}
	
			var isValidSelector = event.target.hasAttribute('value') && event.target.className.indexOf('disabled') < 0 && event.target.className.indexOf('optgroup') < 0;
	
			if (isValidSelector) {
				element.value = event.target.getAttribute('value');
				// let inputEvent = new CustomEvent('input', {});
				// element.dispatchEvent(inputEvent);
				var changeEvent = new CustomEvent('change', {});
				element.dispatchEvent(changeEvent);
				selectPlaceholder.innerHTML = event.target.innerHTML;
			}
	
			styledElement.classList.toggle('opened');
			// dropdownMenu.style.display = styledElement.className.indexOf('opened') >= 0
			// 	? null : 'none';
	
			event.preventDefault();
			event.stopPropagation();
		});
	};
	
	var makeSelect = function makeSelect(element) {
		var styleOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
		var $element = angular.element(element);
		var $scope = $element.scope();
		var $injector = $element.injector();
		var $timeout = $injector.get('$timeout');
	
		if ($scope) {
			(function () {
	
				var content = element.innerHTML;
				var watchers = content.match(parseRepeatItems) || [];
	
				watchers = watchers.map(function (item) {
					return parseRepeatItems.exec(item).pop().trim();
				}).map(function (item) {
					return item.split(' ').pop();
				});
	
				var ngModel = $element.controller('ngModel');
				if (ngModel) {
					var modelAttr = ngModel.$$attr.ngModel;
					watchers.push(modelAttr);
				}
	
				$scope.$watch(function ($currentScope) {
	
					var index = watchers.reduce(function (result, key) {
						result[key] = $currentScope[key];
						return result;
					}, {});
	
					return JSON.stringify(index);
				}, function () {
					return $timeout(function () {
						return render(element, styleOptions);
					}, 0);
				});
			})();
		}
	
		render(element, styleOptions);
	};
	
	module.exports = function (element) {
		var styleOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		styleOptions = getOptions(styleOptions);
		makeSelect(element, styleOptions);
	};

/***/ }

})
//# sourceMappingURL=1.00b6c9208a944b81bcbb.hot-update.js.map