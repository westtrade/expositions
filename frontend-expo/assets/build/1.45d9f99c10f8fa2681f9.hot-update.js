webpackHotUpdate(1,{

/***/ 109:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var qwery = __webpack_require__(90);
	var loadMap = __webpack_require__(110);
	var GMap = __webpack_require__(111);
	
	var templateUrl = __webpack_require__(112);
	
	var centerMap = function centerMap(map) {
		var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { geometry: {} },
		    location = _ref.geometry.location;
	
		var cb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
		if (!location) {
			throw new Error('Location must be defined into information');
		}
	
		var lat = location.lat,
		    lng = location.lng;
	
		map.setCenter(lat(), lng(), cb);
	};
	
	var link = function link($scope, $element, $attrs, $transclude) {
	
		var element = $element[0];
	
		var _qwery = qwery('.map', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    mapElement = _qwery2[0];
	
		var _qwery3 = qwery('input', element),
		    _qwery4 = _slicedToArray(_qwery3, 1),
		    addressSelector = _qwery4[0];
	
		// When the user selects an address from the dropdown, populate the address
		// fields in the form.
	
	
		loadMap(function () {
			console.log('Map selector - laoded!');
	
			// const map = new GMap({ el: mapElement, lat: 0, lng: 0, zoom: 15 });
	
	
			// const autocomplete = new ymaps.SuggestView(addressSelector);
			// console.log(autocomplete.events);
			//
			// autocomplete.events.add('select', (event) => {
			// 	// console.log(autocomplete.state);
			// 	console.log(event.get('item'));
			// })
	
	
			// const autocomplete = new google.maps.places.Autocomplete(addressSelector, {types: ['geocode']});
			// autocomplete.addListener('place_changed', () => {
			// 	const place = autocomplete.getPlace();
			//
			// 	// console.log(place);
			// 	// console.log(place.address_components);
			// 	centerMap(map, place);
			// });
	
	
			//
			// $scope.$watch('selector.address', (value) => {
			//
			// 	GMap.geocode({
			// 		address: value,
			// 		callback: (result, status) => {
			// 			console.log(result);
			// 		}
			// 	});
			//
			// }, 'AIzaSyCQS8Ysgs4w8GDUJhe-ge3bm2w42eZhNBU');
	
	
			// GMap.geocode({
			// 	address: 'Россия',
			// 	callback: ([location], status) => {
			//
			//
			//
			// 		const {geometry: {location: {lat, lng}}} = location;
			// 		map.setCenter(lat(), lng(), () => {
			// 			console.log('Centred');
			// 		})
			// 	}
			// });
		});
	};
	
	module.exports = function () {
		return _defineProperty({
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			link: link
		}, 'scope', true);
	};

/***/ }

})
//# sourceMappingURL=1.45d9f99c10f8fa2681f9.hot-update.js.map