webpackHotUpdate(1,{

/***/ 99:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		login: {
			presence: {
				message: "Поле обязательно для заполнения"
			},
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
	
		},
		password: {
			presence: {
				message: "Поле обязательно для заполнения"
			},
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.e0b021b29b39d9c74221.hot-update.js.map