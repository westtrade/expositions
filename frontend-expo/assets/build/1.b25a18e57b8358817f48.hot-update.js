webpackHotUpdate(1,{

/***/ 71:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var itemTemplate = __webpack_require__(72);
	var offerFormTemplate = __webpack_require__(127);
	
	var _require = __webpack_require__(62),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(60);
	
	var create = {
		url: '/offer/create',
		templateUrl: offerFormTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {}]
	};
	
	var update = {
		url: '/offer/edit/:id',
		templateUrl: offerFormTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {}]
	};
	
	var index = {
		url: '/offer/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			User.bind($scope);
	
			$scope.offer = {};
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = { create: create, update: update, index: index };

/***/ },

/***/ 127:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/form-offer.html';
	var html = "<form action=\"/\" name=\"create-offer\" id=\"create-offer-form\" class=\"event-page\">\n\n\t<div class=\"container\">\n\t\t<div class=\"header\">\n\t\t\t<h1 ng-if=\"!offer.id\">Добавить предложение</h1>\n\t\t\t<h1 ng-if=\"offer.id\">Редактировать предложение</h1>\n\n\t\t\t<div class=\"additional\">\n\t\t\t\tСтатус карточки\n\t\t\t</div>\n\t\t\t<div class=\"expo-tabs right\">\n\t\t\t\t<div class=\"head\"><a href=\"#\">Шаг 1</a><a href=\"#\">Шаг 2</a></div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<input type=\"text\" ng-class=\"{error: error.name}\" placeholder=\"Заголовок предложения\" name=\"title\">\n\t\t<field-error error=\"error.name\"></field-error>\n\n\t\t<div class=\"field place\">\n\t\t\t<i class=\"fa fa-map-marker fa-fw\"></i>\n\t\t\t{{ offer.place }}\n\t\t\t<!-- <i ng-if=\"!offer.place\">Место проведения отображается после выбора выставки</i> -->\n\t\t\tг. УФА\n\t\t</div>\n\n\n\n\n\t</div>\n\n\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.b25a18e57b8358817f48.hot-update.js.map