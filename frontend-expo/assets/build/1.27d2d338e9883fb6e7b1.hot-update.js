webpackHotUpdate(1,{

/***/ 101:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var validate = __webpack_require__(92);
	validate.Promise = Promise;
	
	var i18n = __webpack_require__(103);
	
	validate.formatters.grouped = function (errors) {
	
		return errors.reduce(function () {
			var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
			var _ref = arguments[1];
			var attribute = _ref.attribute,
			    error = _ref.error;
	
	
			result[attribute] = i18n.__(error);
			return result;
		}, {});
	};
	
	module.exports = validate;

/***/ }

})
//# sourceMappingURL=1.27d2d338e9883fb6e7b1.hot-update.js.map