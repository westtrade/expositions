webpackHotUpdate(1,{

/***/ 104:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var qwery = __webpack_require__(88);
	
	var _require = __webpack_require__(69),
	    shortId = _require.shortId;
	
	var restrict = 'ACE';
	
	var createTabHead = function createTabHead() {
		var tabHead = document.createElement('div');
		tabHead.className = 'head';
	
		return tabHead;
	};
	
	var setActiveTab = function setActiveTab(activeIdx) {
		return function (tab, tabIdx) {
			tab.classList.toggle('active', activeIdx === tabIdx);
		};
	};
	
	var link = function link($scope, $element, $attrs) {
	
		var element = $element[0];
	
		var _qwery = qwery('.head', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    _qwery2$ = _qwery2[0],
		    tabHead = _qwery2$ === undefined ? createTabHead() : _qwery2$;
	
		$scope.switchTab = function () {
			var activeTab = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
	
	
			activeTab = parseInt(activeTab);
	
			qwery('a', tabHead).forEach(setActiveTab(activeTab));
			qwery('.tab', element).forEach(setActiveTab(activeTab));
		};
	
		tabHead.addEventListener('click', function (event) {
	
			var tabIndex = qwery('a', tabHead).indexOf(event.target);
	
			if (tabIndex < 0) {
				return;
			}
	
			$scope.switchTab(tabIndex);
	
			event.preventDefault();
			event.stopPropagation();
		});
	
		var activeIdx = qwery('.tab').reduce(function (activeIdx, tab, index) {
	
			if (tab.classList.contains('active') && !activeIdx) {
				activeIdx = index;
			}
	
			return activeIdx;
		}, 0);
	
		var _$scope$activeTab = $scope.activeTab,
		    activeTab = _$scope$activeTab === undefined ? activeIdx : _$scope$activeTab;
	
		activeTab = parseInt(activeTab);
	
		if (isNaN(activeTab)) {
			activeTab = activeIdx;
		}
	
		tabHead = qwery('.tab', element).map(function (tab, idx) {
			return [tab, tab.title || 'Без названия ' + idx];
		}).reduce(function (_ref, _ref2) {
			var tabHead = _ref.tabHead,
			    current = _ref.current;
	
			var _ref3 = _slicedToArray(_ref2, 2),
			    tab = _ref3[0],
			    tabTile = _ref3[1];
	
			var link = document.createElement('a');
			link.href = '#';
			link.innerHTML = tabTile;
	
			var tabId = shortId();
	
			link.setAttribute('data-tab', tabId);
			tab.setAttribute('data-tab', tabId);
	
			var nextElement = current ? current.nextSiblingElement : tabHead.firstChildElement;
	
			console.log(link, nextElement);
	
			current = nextElement ? tabHead.insertBefore(link, nextElement) : tabHead.appendChild(link);
			return { tabHead: tabHead, current: current };
		}, { tabHead: tabHead });
	
		if (!tabHead.parentNode) {
			element.insertBefore(tabHead, element.firstChild);
		}
	
		$scope.switchTab(activeTab);
		$scope.$watch('activeTab', function (prevTab, activeTab) {
	
			if (activeTab === undefined) {
				return;
			}
	
			$scope.switchTab(activeTab);
		});
	};
	
	module.exports = [function () {
	
		return {
			restrict: restrict,
			link: link,
			scope: {
				activeTab: '='
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.bc05112181008392cf9a.hot-update.js.map