webpackHotUpdate(1,{

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var loadMap = __webpack_require__(110);
	var qwery = __webpack_require__(90);
	
	var setValue = function setValue(input, value) {
	
		input.value = value;
	
		var changeEvent = new Event('change');
		input.dispatchEvent(changeEvent);
	
		var inputEvent = new Event('input');
		input.dispatchEvent(inputEvent);
	};
	
	var providers = {
	
		yandex: {
	
			init: function init(element) {
	
				var suggestView = new global.ymaps.SuggestView(element);
				suggestView.events.add('select', function (event) {
					var _event$get = event.get('item'),
					    value = _event$get.value,
					    displayName = _event$get.displayName;
	
					global.ymaps.geocode(value).then(function (result) {
						console.log(result);
					});
				});
	
				// suggestView.state.events.add('change', (event) => {
				//
				// 	let activeIndex = suggestView.state.get('activeIndex');
				// 	if (typeof activeIndex == 'number') {
				// 		let activeItem = suggestView.state.get('items')[activeIndex];
				// 		console.log(activeItem);
				// 		// if (activeItem && activeItem.value != input.value) {
				// 		// 	input.value = activeItem.value;
				// 		// }
				// 	}
				//
				// });
	
				element.$destroy = function () {
					suggestView.destroy();
					suggestView = null;
				};
			},
	
			destroy: function destroy() {
				element.$destroy();
				element.$destroy = null;
			}
		},
	
		google: {
	
			init: function init() {},
	
			destroy: function destroy() {}
		}
	};
	
	var changeProvider = function changeProvider(element, current, prev) {
	
		if (current === prev) {
			return current;
		}
	
		if (prev && prev in providers) {
			providers[prev].destroy(element);
		}
	
		if (current in providers) {
			loadMap(current, function () {
				providers[current].init(element);
			});
		}
	
		return current;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function link($scope, $element, $attrs, ngModel) {
	
				var selectorElement = $element[0];
	
				if (ngModel && ngModel.$modelValue) {
					setValue(selectorElement, ngModel.$modelValue);
				}
	
				setValue(selectorElement, 123123);
	
				var current = changeProvider(selectorElement, $attrs.provider || 'yandex');
				$attrs.$observe('provider', function (next) {
					current = changeProvider(selectorElement, next, current);
				});
			}
		};
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.24e406b885f3b344b489.hot-update.js.map