webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var gmaps = __webpack_require__(101);
	var qwery = __webpack_require__(83);
	
	var gmapIsInitialized = false;
	
	var events = {
		gmap: function gmap() {
			gmapIsInitialized = true;
		}
	};
	
	var getGmap = function getGmap() {
	
		if (gmapIsInitialized) {
			return Promise.resolve(true);
		}
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=initGmaps';
		document.body.appendChild(script);
	
		return new Promise(function (resolve) {
			events.gmap = function () {
				events.gmap();
				resolve();
			};
		});
	};
	
	global.initGmaps = events.gmap;
	
	var templateUrl = __webpack_require__(102);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
	
					getGmap();
	
					$transclude($scope, function (clonedContent) {});
				};
			}
		};
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.3ca169862085c0ed8a17.hot-update.js.map