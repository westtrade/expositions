webpackHotUpdate(1,{

/***/ 39:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/app/directives/expo-slider.html';
	var html = "<ng-transclude></ng-transclude>\n<div class=\"dots\">\n\t<div class=\"dot\"  ng-repeat=\"n in [] | range:total\" ng-class=\"{active: active === n}\" ng-click=\"setActive(n)\"></div>\n</div>\n\n<div class=\"arrow left-arrow\">\n\t<svg class=\"mb3\" enable-background=\"new 0 0 32 64\" height=\"64px\" version=\"1.1\" viewBox=\"0 0 32 64\" width=\"32px\" x=\"0px\" xml:space=\"preserve\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\" y=\"0px\">\n\t<g display=\"none\" id=\"Layer_2\">\n\t<rect display=\"inline\" fill=\"#ffffff\" height=\"112\" width=\"128\" x=\"-7\" y=\"-20\"></rect>\n\t</g>\n\t<g id=\"Layer_1\">\n\t<g>\n\t<path class=\"transition-all\" d=\"M23.877,64c-0.617,0-1.227-0.284-1.618-0.821l-21.875-30c-0.513-0.704-0.512-1.659,0.003-2.361l22-30c0.654-0.89,1.904-1.084,2.796-0.43c0.891,0.653,1.083,1.905,0.431,2.795L4.478,32.003l21.014,28.818c0.65,0.893,0.455,2.144-0.438,2.795C24.698,63.875,24.285,64,23.877,64z\"></path>\n\t</g>\n\t</g>\n\t</svg>\n</div>\n\n<div class=\"arrow right-arrow\">\n\t<svg class=\"mb3\" enable-background=\"new 0 0 32 64\" height=\"64px\" version=\"1.1\" viewBox=\"0 0 32 64\" width=\"32px\" x=\"0px\" xml:space=\"preserve\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\" y=\"0px\">\n\t<g display=\"none\" id=\"Layer_2\">\n\t<rect display=\"inline\" fill=\"#ffffff\" height=\"112\" width=\"128\" x=\"-71\" y=\"-20\"></rect>\n\t</g>\n\t<g id=\"Layer_1\">\n\t<g>\n\t<path class=\"transition-all\" d=\"M7.998,64c-0.41,0-0.824-0.126-1.181-0.387c-0.891-0.653-1.083-1.905-0.431-2.796l21.136-28.821L6.509,3.178c-0.65-0.893-0.455-2.144,0.438-2.794C7.84-0.266,9.091-0.07,9.741,0.822l21.875,30c0.513,0.704,0.512,1.659-0.003,2.361l-22,30C9.221,63.717,8.614,64,7.998,64z\"></path>\n\t</g>\n\t</g>\n\t</svg>\n\n</div>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.ea39d7aa856d3117784a.hot-update.js.map