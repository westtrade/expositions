webpackHotUpdate(1,{

/***/ 68:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var validate = __webpack_require__(69);
	validate.Promise = Promise;
	
	var i18n = __webpack_require__(71);
	
	validate.formatters.grouped = function (errors) {
	
		return errors.reduce(function () {
			var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
			var _ref = arguments[1];
			var attribute = _ref.attribute,
			    error = _ref.error;
	
	
			var hasField = attribute in result;
			if (!hasField) {
				result[attribute] = [];
			}
	
			result[attribute].push(i18n.__(error));
			return result;
		}, {});
	};
	
	validate.validators.captcha = function (value, options, key, attributes) {};
	
	module.exports = validate;

/***/ }

})
//# sourceMappingURL=1.a6ecd9af29bf83c200d1.hot-update.js.map