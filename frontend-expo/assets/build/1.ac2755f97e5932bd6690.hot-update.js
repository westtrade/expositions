webpackHotUpdate(1,{

/***/ 84:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var itemTemplate = __webpack_require__(73);
	var formTemplate = __webpack_require__(85);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(61);
	
	var EXPO_TYPES = ['Выставка', 'Премия', 'Конференция', 'Форум'];
	
	var mainController = ['$scope', '$element', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $element, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
		var pageElement = $element[0];
	
		var _qwery = qwery('form', pageElement),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    form = _qwery2[0];
	
		User.bind($scope);
		formApply($scope, form);
	
		$scope.types = EXPO_TYPES.map(function (label, index) {
			return { label: label, index: index };
		});
	}];
	
	var create = {
		url: '/exposition/create',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var update = {
		url: '/exposition/edit/:id',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var index = {
		url: '/exposition/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			$scope.offer = {};
	
			User.bind($scope);
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
					//TODO 404 error
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = {
		create: create, update: update, index: index
	};

/***/ }

})
//# sourceMappingURL=1.ac2755f97e5932bd6690.hot-update.js.map