webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', function ($rootScope) {
		return {
			transclude: 'element',
			restrict: 'C',
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
	
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					// const currentModalId = clone[0].id;
	
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
							newModal = $element.after(modalSource);
							newModal.removeClass('modal-source').addClass('modal selected');
						} else {
							if (newModal) {
								newModal.remove();
							}
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.2aadca92e7d2ec33f608.hot-update.js.map