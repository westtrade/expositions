webpackHotUpdate(1,{

/***/ 46:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
		range: function range() {
			return function (input, total) {
	
				total = parseInt(total);
	
				for (var i = 0; i < total; i++) {
					input.push(i);
				}
	
				return input;
			};
		},
		groupBy: function groupBy() {
			return function (collection, property) {
	
				if (!collection) {
					return collection;
				}
	
				return collection.reduce(function (result, currentItem) {
	
					var group = currentItem[property] || 'udnefined';
					if (!(group in result)) {
						result[group] = [];
					}
	
					result[group].push(currentItem);
	
					return result;
				}, {});
			};
		}
	};

/***/ }

})
//# sourceMappingURL=1.29044710151424daff61.hot-update.js.map