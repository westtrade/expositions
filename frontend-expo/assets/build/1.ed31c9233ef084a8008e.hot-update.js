webpackHotUpdate(1,{

/***/ 223:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	// const Dropzone = require('dropzone');
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var templateUrl = __webpack_require__(224);
	var qwery = __webpack_require__(175);
	// require('dropzone/src/dropzone.scss');
	
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			replace: true,
			scope: true,
			link: function link($scope, $element, $attrs, $controllers, $transclude) {
	
				var element = $element[0];
				var _$attrs$url = $attrs.url,
				    url = _$attrs$url === undefined ? '/' : _$attrs$url,
				    _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name;
	
				element.classList.add('dropzone');
	
				var _qwery = qwery('input', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    inputField = _qwery2[0];
	
				inputField.setAttribute('name', name);
	
				// element.addEventListener('click', (event) => {
				//
				// 	console.log('Test');
				//
				// 	// const clickEvent = new CustomEvent('click', {});
				// 	// inputField.dispatchEvent(clickEvent);
				//
				// 	// inputField.click();
				//
				// 	console.log(inputField);
				//
				//
				//
				// 	event.preventDefault();
				//
				// })
	
	
				var dragHandler = function dragHandler(event) {
	
					// console.log(event.type);
					// event.preventDefault();
	
					switch (event.type) {
						case 'drop':
						case 'dragenter':
						case 'dragover':
							// event.preventDefault();
							break;
					}
				};
	
				element.addEventListener('drag', dragHandler, false);
				element.addEventListener('dragstart', dragHandler, false);
				element.addEventListener('dragend', dragHandler, false);
				element.addEventListener('dragover', dragHandler, false);
				element.addEventListener('dragenter', dragHandler, false);
				element.addEventListener('dragleave', dragHandler, false);
				element.addEventListener('drop', dragHandler, false);
	
				inputField.addEventListener('change', function (event) {
					console.log(event);
				});
	
				var placeholder = $attrs.placeholder,
				    multiple = $attrs.multiple,
				    accept = $attrs.accept;
	
	
				Object.entries({ placeholder: placeholder, multiple: multiple, accept: accept }).filter(function (_ref) {
					var _ref2 = _slicedToArray(_ref, 2),
					    key = _ref2[0],
					    value = _ref2[1];
	
					return value !== undefined;
				}).reduce(function (inputField, _ref3) {
					var _ref4 = _slicedToArray(_ref3, 2),
					    key = _ref4[0],
					    value = _ref4[1];
	
					inputField.setAttribute(key, value);
					return inputField;
				}, inputField);
	
				$scope.hasChilds = false;
				$scope.placeholder = placeholder;
	
				$transclude(function (clone) {
	
					$scope.hasChilds = !clone.length;
	
					// console.log(clone.length);
	
	
					// const currentDropzone = new Dropzone(element, {
					// 	url
					// });
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.ed31c9233ef084a8008e.hot-update.js.map