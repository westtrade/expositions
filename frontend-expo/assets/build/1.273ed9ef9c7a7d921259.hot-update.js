webpackHotUpdate(1,{

/***/ 79:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(80);
	
	module.exports = function () {
		return {
			priority: 15,
			restrict: 'AEM',
			// require: 'ngModel',
			link: function link(scope, element, attrs) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				element.addClass('ng-binding').data('$binding', attr.ngBind);
	
				// attrs.$observe('styledInput', () => {
				// 	console.log('Observer');
				// 	styleInputs(element);
				// })
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.273ed9ef9c7a7d921259.hot-update.js.map