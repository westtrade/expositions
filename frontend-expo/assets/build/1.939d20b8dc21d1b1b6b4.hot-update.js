webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	var debugPage = __webpack_require__(62);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var home = __webpack_require__(70);
	var notFound = __webpack_require__(72);
	var offers = __webpack_require__(73);
	var auth = __webpack_require__(75);
	var account = __webpack_require__(116);
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', home).state('404', notFound).state('offer-page', offers.catalog).state('user-login', auth.login).state('user-registration', auth.registration).state('user-recovery', auth.recovery);
	
		$stateProvider.state('change-password', account.changePassword).state('user-offers', account.offersCatalog).state('user-events', account.eventsCatalog).state('user-account', account.accountPage).state('user-cart', account.cart);
	
		$stateProvider.state('user-logout', {
			url: '/user/logout',
			controller: ['$scope', '$state', 'User', function ($scope, $state, User) {
				User.logout().catch(function (e) {
					return console.error(e);
				}).then(function () {
					console.log(User.getState());
					$state.go('home');
				});
			}]
		});
	
		$stateProvider.state('dev-debug', {
			url: '/dev/debug',
			templateUrl: debugPage,
			controller: ['$scope', 'User', '$timeout', function ($scope, User, $timeout) {
	
				User.bind($scope);
	
				$scope.selectedUser = 1;
	
				$scope.changeUser = function ($event) {
					User.setState($scope.users[$scope.selectedUser]);
				};
	
				$scope.$watch('user.notificationsCount', function (current, prev) {
					if (current !== prev && typeof current === 'number') {
						User.setState('notificationsCount', current);
					}
				});
	
				$scope.users = [{
					displayName: 'Тумаков Олег Н',
					role: 'guest'
				}, {
					displayName: 'Эрнест Днепропетровский А',
					role: 'exponent',
					notificationsCount: 5,
					id: '56faabde3a1a8cd34bae9584'
				}, {
					displayName: 'Артем Вячеславович Е.',
					role: 'organizer'
				}, {
					displayName: 'Модератор',
					role: 'moderator'
				}, {
					displayName: 'Администратор',
					role: 'administrator'
				}];
	
				$timeout(function () {
					safeApply($scope, function () {
						$scope.users.push({
							displayName: 'Azaza',
							role: 'administrator'
						});
					});
				}, 1300);
			}]
	
		});
	
		$stateProvider.state('dev-styles', {
			url: '/dev/styles',
			templateUrl: debugPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.939d20b8dc21d1b1b6b4.hot-update.js.map