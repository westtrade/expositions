webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	module.exports = function ($scope) {
	
		$scope.catalog = [];
	
		io.socket.get('/offers/origin', {}, function (catalog) {
			var _$scope$catalog;
	
			(_$scope$catalog = $scope.catalog).push.apply(_$scope$catalog, _toConsumableArray(catalog));
		});
	};

/***/ }

})
//# sourceMappingURL=1.c580646c25befe8b61fd.hot-update.js.map