webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var gmapIsInitialized = false;
	
	var loadGMap = function loadGMap(resolve, key) {
	
		if (gmapIsInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&callback=' + randomCallbackName;
	
		if (key) {
			script.src = script.src + '&key=' + key;
		}
	
		console.log(script.src);
	
		global[randomCallbackName] = function () {
			gmapIsInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	module.exports = loadGMap;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.0539e8ea17e9550ce2c0.hot-update.js.map