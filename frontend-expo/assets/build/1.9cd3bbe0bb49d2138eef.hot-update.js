webpackHotUpdate(1,{

/***/ 114:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(60);
	var messages = __webpack_require__(115);
	
	var angular = __webpack_require__(44);
	
	var ROLES = ['guest', 'exponent', 'organizer', 'moderator', 'administrator'];
	
	var isOwner = function isOwner(context, user) {};
	
	var RULES = {
		'show navbar menu': ['!guest'],
		'open slider editor': ['administrator'],
		'change offer': ['administrator', 'moderator'],
		'delete offer': ['administrator', 'moderator']
	};
	
	var ANONYMOUS_USER = {
		role: 'guest',
		displayName: 'Аноним'
	};
	
	var User = function () {
		function User() {
			var _this = this;
	
			_classCallCheck(this, User);
	
			this.watchers = [];
			this.state = {};
	
			var initState = messages.get('user', ANONYMOUS_USER);
	
			this.setState(initState, true);
			messages.on('user', function (event, userState) {
				_this.setState(userState, true);
			});
		}
	
		_createClass(User, [{
			key: 'bind',
			value: function bind($scope) {
				var _this2 = this;
	
				this.watchers.push($scope);
	
				safeApply($scope, function () {
					$scope.user = _this2.getState();
				});
	
				$scope.$on('$destroy', function () {
					var watcherIdx = _this2.watchers.indexOf($scope);
					_this2.watchers.splice(watcherIdx, 1);
				});
			}
		}, {
			key: 'logout',
			value: function logout() {
				var _this3 = this;
	
				return new Promise(function (resolve, reject) {
					_this3.setState(ANONYMOUS_USER);
					resolve();
				});
			}
		}, {
			key: 'setState',
			value: function setState() {
				var _this4 = this;
	
				var name = null,
				    data = void 0,
				    preventBroadcast = false;
	
				if (typeof (arguments.length <= 0 ? undefined : arguments[0]) === 'string') {
					name = arguments.length <= 0 ? undefined : arguments[0];
					data = arguments.length <= 1 ? undefined : arguments[1];
					preventBroadcast = typeof (arguments.length <= 2 ? undefined : arguments[2]) === 'undefined' ? preventBroadcast : !!(arguments.length <= 2 ? undefined : arguments[2]);
				} else {
					data = arguments.length <= 0 ? undefined : arguments[0];
					preventBroadcast = typeof (arguments.length <= 1 ? undefined : arguments[1]) === 'undefined' ? preventBroadcast : !!(arguments.length <= 1 ? undefined : arguments[1]);
				}
	
				if (name) {
					this.state[name] = data;
				} else {
					this.state = data;
				}
	
				this.watchers.forEach(function ($scope) {
					safeApply($scope, function () {
						$scope.user = _this4.getState();
					});
				});
	
				if (!preventBroadcast) {
					messages.broadcast('user', this.getState());
				}
			}
		}, {
			key: 'getState',
			value: function getState() {
				var _this5 = this;
	
				return angular.extend({}, this.state, {
					hasAccess: function hasAccess(ruleId) {
						return _this5.hasAccess(ruleId);
					},
					logout: function logout() {
						return _this5.logout();
					}
				});
			}
		}, {
			key: 'hasAccess',
			value: function hasAccess(ruleId) {
				var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
				var ruleExists = ruleId in RULES;
	
				if (!ruleExists) {
					return false;
				}
	
				var rule = RULES[ruleId];
				var result = false;
	
				var userData = this.getState();
	
				result = rule.reduce(function (result, currentRule) {
	
					if (result) {
						return result;
					}
	
					if (typeof currentRule === 'function') {
						return currentRule(context);
					}
	
					var isNot = currentRule.indexOf('!') === 0;
					if (isNot) {
						currentRule = currentRule.replace(/^!/gim, '');
						result = currentRule !== userData.role;
					} else {
						result = currentRule === userData.role;
					}
	
					return result;
				}, false);
	
				return result;
			}
		}]);
	
		return User;
	}();
	
	var currentUser = new User();
	
	module.exports = function () {
		return currentUser;
	};

/***/ }

})
//# sourceMappingURL=1.9cd3bbe0bb49d2138eef.hot-update.js.map