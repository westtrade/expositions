webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var safeapply = __webpack_require__(44);
	
	var MODAL_MODES = ['catalog', 'filters'];
	var subjects = __webpack_require__(57);
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		console.log($scope.initial);
	
		$scope.mode = MODAL_MODES[1];
	
		$scope.switchMode = function (mode) {
			var modeId = 0;
			if (!mode) {
				modeId = MODAL_MODES.indexOf($scope.mode) === 0 ? 1 : 0;
			}
	
			$scope.mode = MODAL_MODES[modeId];
		};
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
	
			// const selected = Object.values($scope.subjects).reduce((result, items) =>  result
			// 	.concat(items.filter(item => item.selected))
			// 	.map(item => angular.copy(item)), []
			// );
			//
			$modals.close();
		};
	}];

/***/ }

})
//# sourceMappingURL=1.1f59fc8fa63dbbeaef1c.hot-update.js.map