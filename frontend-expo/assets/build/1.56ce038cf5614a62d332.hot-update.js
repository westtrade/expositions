webpackHotUpdate(1,{

/***/ 12:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	__webpack_require__(13);
	__webpack_require__(14);
	__webpack_require__(15);
	__webpack_require__(16).polyfill();
	
	var angular = __webpack_require__(17);
	var uiRouter = __webpack_require__(19);
	
	var app = angular.module('expotestdrive', [uiRouter]);
	var router = __webpack_require__(20);
	app.config(router);
	
	var topBar = __webpack_require__(40);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(42);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(43);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(45);
	Object.entries(common).reduce(function (app, _ref) {
	  var _ref2 = _slicedToArray(_ref, 2),
	      name = _ref2[0],
	      filter = _ref2[1];
	
	  return app.filter(name, filter);
	}, app);
	
	module.exports = app;

/***/ },

/***/ 40:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(41);
	
	var controller = function controller($scope, $element, $attrs) {};
	
	module.exports = function () {
		return {
			templateUrl: templateUrl,
			controller: ['$scope', function ($scope) {}]
		};
	};

/***/ },

/***/ 41:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/app/directive/topbar.html';
	var html = "<a href=\"#\" ui-sref-active=\"active\" ui-sref=\"home\">\n\t<img src=\"/img/logo_new.png\" alt=\"TESTDRIVE logo\">\n</a>\n\n<nav class=\"right\">\n\t<a ui-sref=\"user-login\" ui-sref-active=\"active\" href=\"#\">Войти</a>\n\t<a ui-sref=\"user-registration\" ui-sref-active=\"active\" href=\"#\">Зарегистрироваться</a>\n</nav>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ },

/***/ 42:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(29);
	
	module.exports = function () {
		return {
			link: function link(scope, element, attrs) {
				styleInputs(element);
			}
		};
	};

/***/ },

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(44);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var qwery = __webpack_require__(34);
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
				$scope.active = active;
	
				qwery('.slide', $element[0]).forEach(function (item, idx) {
					if (item.classList.contains('active')) {
						item.classList.remove('active');
					}
	
					if (idx === active) {
						item.classList.add('active');
					}
				});
			};
	
			$scope.next = function () {
				var next = $scope.active + 1;
				var current = next >= $scope.total ? 0 : next;
				$scope.setActive(current);
			};
			$scope.prev = function () {
				var prev = $scope.active - 1;
				var current = prev <= 0 ? $scope.total : next;
				$scope.setActive(current);
			};
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ },

/***/ 44:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/app/directive/expo-slider.html';
	var html = "<ng-transclude></ng-transclude>\n<div class=\"dots\">\n\t<div class=\"dot\"  ng-repeat=\"n in [] | range:total\" ng-class=\"{active: active === n}\" ng-click=\"setActive(n)\"></div>\n</div>\n\n<div class=\"arrow left-arrow\" ng-click=\"prev()\">\n\t<svg class=\"mb3\" enable-background=\"new 0 0 32 64\" height=\"64px\" version=\"1.1\" viewBox=\"0 0 32 64\" width=\"32px\" x=\"0px\" xml:space=\"preserve\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\" y=\"0px\">\n\t<g display=\"none\" id=\"Layer_2\">\n\t<rect display=\"inline\" fill=\"#ffffff\" height=\"112\" width=\"128\" x=\"-7\" y=\"-20\"></rect>\n\t</g>\n\t<g id=\"Layer_1\">\n\t<g>\n\t<path class=\"transition-all\" d=\"M23.877,64c-0.617,0-1.227-0.284-1.618-0.821l-21.875-30c-0.513-0.704-0.512-1.659,0.003-2.361l22-30c0.654-0.89,1.904-1.084,2.796-0.43c0.891,0.653,1.083,1.905,0.431,2.795L4.478,32.003l21.014,28.818c0.65,0.893,0.455,2.144-0.438,2.795C24.698,63.875,24.285,64,23.877,64z\"></path>\n\t</g>\n\t</g>\n\t</svg>\n</div>\n\n<div class=\"arrow right-arrow\" ng-click=\"next()\">\n\t<svg class=\"mb3\" enable-background=\"new 0 0 32 64\" height=\"64px\" version=\"1.1\" viewBox=\"0 0 32 64\" width=\"32px\" x=\"0px\" xml:space=\"preserve\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\" y=\"0px\">\n\t<g display=\"none\" id=\"Layer_2\">\n\t<rect display=\"inline\" fill=\"#ffffff\" height=\"112\" width=\"128\" x=\"-71\" y=\"-20\"></rect>\n\t</g>\n\t<g id=\"Layer_1\">\n\t<g>\n\t<path class=\"transition-all\" d=\"M7.998,64c-0.41,0-0.824-0.126-1.181-0.387c-0.891-0.653-1.083-1.905-0.431-2.796l21.136-28.821L6.509,3.178c-0.65-0.893-0.455-2.144,0.438-2.794C7.84-0.266,9.091-0.07,9.741,0.822l21.875,30c0.513,0.704,0.512,1.659-0.003,2.361l-22,30C9.221,63.717,8.614,64,7.998,64z\"></path>\n\t</g>\n\t</g>\n\t</svg>\n\n</div>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ },

/***/ 45:
/***/ function(module, exports) {

	"use strict";

/***/ }

})
//# sourceMappingURL=1.56ce038cf5614a62d332.hot-update.js.map