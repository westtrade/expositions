webpackHotUpdate(1,{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(44);
	var uiRouter = __webpack_require__(46);
	var uiDND = __webpack_require__(47);
	
	var dropzone = __webpack_require__(49);
	var ngDropzone = __webpack_require__(51);
	
	global.moment = __webpack_require__(128);
	
	console.log(moment);
	
	// const ngMomentPicker = require('angular-moment-picker');
	// console.log(ngMomentPicker);
	
	
	__webpack_require__(53);
	__webpack_require__(54);
	__webpack_require__(55);
	
	var loadingBar = __webpack_require__(56);
	var ngAnimate = __webpack_require__(58);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'chieffancypants.loadingBar', 'thatisuday.dropzone']);
	
	var router = __webpack_require__(60);
	app.config(router);
	
	var common = __webpack_require__(86);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(87);
	
	app.directive('button', __webpack_require__(97)).directive('expoSlider', __webpack_require__(98)).directive('topBar', __webpack_require__(100)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(102)).directive('masonryLayout', __webpack_require__(103)).directive('expoModalManager', __webpack_require__(106)).directive('modalSource', __webpack_require__(108)).directive('expoTabs', __webpack_require__(109)).directive('fieldError', __webpack_require__(110)).directive('inputMap', __webpack_require__(112)).directive('placeAutocomplete', __webpack_require__(116));
	
	app.controller('offersCatalog', __webpack_require__(117)).controller('subjectSelectorModal', __webpack_require__(119)).controller('sliderController', __webpack_require__(121)).controller('expositionSelectorModal', __webpack_require__(122)).controller('sliderEditorModal', __webpack_require__(123)).controller('offerTeaserController', __webpack_require__(124));
	
	app.factory('$modals', __webpack_require__(125)).factory('User', __webpack_require__(126));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.8ab3e3df7f2f54cde23e.hot-update.js.map