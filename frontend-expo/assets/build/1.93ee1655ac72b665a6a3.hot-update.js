webpackHotUpdate(1,{

/***/ 103:
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var translate = {
		'Login can\'t be blank': 'Поле Логин не может быть пустым',
		'Login must be at least 6 characters': 'Поле Логин должно быть больше 6 символов',
		'Password can\'t be blank': 'Поле пароль не может быть пустым',
		'Password must be at least 6 characters': 'Поле пароль должно быть больше 6 символов'
	};
	
	var I18N = function () {
		function I18N() {
			_classCallCheck(this, I18N);
		}
	
		_createClass(I18N, [{
			key: '__',
			value: function __(sentence) {
				if (sentence in translate) {
					sentence = translate[sentence];
				}
	
				return sentence;
			}
		}]);
	
		return I18N;
	}();
	
	module.exports = new I18N();

/***/ }

})
//# sourceMappingURL=1.93ee1655ac72b665a6a3.hot-update.js.map