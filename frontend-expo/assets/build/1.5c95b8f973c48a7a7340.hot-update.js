webpackHotUpdate(1,{

/***/ 101:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeApply = __webpack_require__(60);
	var ExpoError = __webpack_require__(102);
	var angular = __webpack_require__(44);
	
	var events = {
		scroll: null
	};
	
	window.addEventListener('scroll', function (event) {
		if (events.scroll) {
			events.scroll(event);
		}
	});
	
	module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', 'User', function ($scope, $element, cfpLoadingBar, $modals, User) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0,
			subjects: [],
			expositions: [
				// { name: 'Test', id: 1}
			]
		};
	
		$scope.selectSubjects = function () {
	
			var data = {
				initial: $scope.filter.subjects
			};
	
			$modals.open('subject-selector', data).then(function () {
				var subjects = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				safeApply($scope, function () {
					$scope.filter.subjects = [];
					angular.extend($scope.filter.subjects, subjects);
				});
			});
		};
	
		$scope.selectExpositions = function () {
	
			var data = {
				initial: $scope.filter
			};
	
			$modals.open('exposition-selector', data).then(function () {
				var expositionsList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				safeApply($scope, function () {
					$scope.filter.expositions = [];
					$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
				});
			});
		};
	
		$scope.removeFilter = function (removed, filterType) {
			var filterTypeExists = filterType in $scope.filter;
	
			if (!filterTypeExists) {
				throw new Error('Wrong type of filter (filterType argumnt). It must be in allowed list of types: '.Object.keys($scope.filter).join(', '));
			}
	
			$scope.filter[filterType] = $scope.filter[filterType].filter(function (current) {
				return removed.id !== current.id;
			});
		};
	
		$scope.catalog = [];
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers', $scope.filter, function (catalog, jwr) {
				var error = jwr.error;
	
	
				if (error) {
					throw new ExpoError(error);
				}
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
				$scope.loading = false;
	
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		events.scroll = function (event) {
			var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
			var offset = 400;
			var contentScrolledHeight = element.scrollHeight - scrollTop - offset;
			var contentIsScrolled = contentScrolledHeight <= 0; //todo fix this bug
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		};
	
		$scope.replay = function (item) {
			var user = User.getState();
			$modals.open('offer-teaser-form', { item: item, user: user });
		};
	}];

/***/ }

})
//# sourceMappingURL=1.5c95b8f973c48a7a7340.hot-update.js.map