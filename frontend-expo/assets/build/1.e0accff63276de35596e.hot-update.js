webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	
	var homePage = __webpack_require__(61);
	var loginPage = __webpack_require__(62);
	var registrationPage = __webpack_require__(63);
	var recoveryPage = __webpack_require__(64);
	var changePasswordPage = __webpack_require__(65);
	
	var loginConstraints = __webpack_require__(66);
	var registrationConstraints = __webpack_require__(105);
	
	var _require = __webpack_require__(67),
	    formApply = _require.formApply;
	
	var _require2 = __webpack_require__(73),
	    guid = _require2.guid;
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/',
			templateUrl: homePage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage,
			controller: ['$scope', '$element', function ($scope, $element) {
				var form = $element.find('form')[0];
				formApply($scope, form, loginConstraints);
	
				$scope.submit = function ($event) {
	
					form.validate(function (error, formData) {
						console.log(error, formData);
					});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', '$element', function ($scope, $element) {
	
				var form = $element.find('form')[0];
				formApply($scope, form, registrationConstraints);
	
				$scope.form['user-registration'].type = 'company';
	
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					form.captcha.value = '';
					var event = new Event('input');
					form.captcha.dispatchEvent(event);
	
					$scope.randomSeed = guid();
				};
	
				$scope.select = function (type) {
					form.type.value = type;
					var event = new Event('input');
					form.type.dispatchEvent(event);
					// console.log($scope.form);
					// console.log(form.type.value);
				};
	
				$scope.error = {};
	
				$scope.submit = function ($event) {
					form.validate(function (error, formData) {
						if (error) {
							$scope.changeCaptcha();
						}
					});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', function ($scope) {
				$scope.email = null;
				$scope.data = {
					email: null
				};
	
				$scope.setMail = function () {
					safeApply($scope, function () {
						$scope.email = $scope.data.email;
					});
				};
			}]
		});
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.e0accff63276de35596e.hot-update.js.map