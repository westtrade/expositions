webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', function ($rootScope) {
		return {
			transclude: 'element',
			restrict: 'C',
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
	
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					// const currentModalId = clone[0].id;
	
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
						if (modalId != $attr.id) {
							return;
						}
	
						console.log(isOpened);
	
						if (isOpened) {
							newModal = $element.append(modalSource);
							newModal.removeClass('modal-source').addClass('modal selected');
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.7b911c15ff30929c1025.hot-update.js.map