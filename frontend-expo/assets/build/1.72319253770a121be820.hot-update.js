webpackHotUpdate(1,{

/***/ 47:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($compile) {
		return {
			restrict: 'EC',
			link: function link($scope, $element) {
				var element = $element[0];
				element.innerHTML = '<span>' + element.innerHTML + '</span>';
	
				$compile(element)($scope);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.72319253770a121be820.hot-update.js.map