webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	
	var changePasswordPage = __webpack_require__(65);
	
	var debugPage = __webpack_require__(67);
	
	// const recoveryConstraints = require('../validators/recoveryForm');
	
	var _require = __webpack_require__(70),
	    formApply = _require.formApply;
	
	var _require2 = __webpack_require__(76),
	    guid = _require2.guid;
	
	var home = __webpack_require__(112);
	var notFound = __webpack_require__(115);
	var offers = __webpack_require__(113);
	var auth = __webpack_require__(114);
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', home).state('404', notFound).state('offer-page', offers.catalog).state('user-login', auth.login).state('user-registration', auth.registration).state('user-recovery', auth.recovery);
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', '$element', function ($scope, $element) {
				var form = $element.find('form')[0];
				formApply($scope, form, registrationConstraints);
	
				$scope.setMail = function ($event) {
	
					form.validate(function (error, formData) {});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-offers', {
			url: '/user/offers',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-events', {
			url: '/user/events',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-account', {
			url: '/user/account',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-cart', {
			url: '/user/cart',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-logout', {
			url: '/user/logout',
			templateUrl: changePasswordPage,
			controller: ['$scope', '$state', 'User', function ($scope, $state, User) {
				User.logout().catch(function (e) {
					return console.error(e);
				}).then(function () {
					$state.go('home');
				});
			}]
		});
	
		$stateProvider.state('dev-debug', {
			url: '/dev/debug',
			templateUrl: debugPage,
			controller: ['$scope', 'User', '$timeout', function ($scope, User, $timeout) {
	
				User.bind($scope);
	
				$scope.selectedUser = 1;
	
				$scope.changeUser = function ($event) {
					console.log($scope.selectedUser);
					User.setState($scope.users[$scope.selectedUser]);
				};
	
				$scope.users = [{
					displayName: 'Тумаков Олег Н',
					role: 'guest'
				}, {
					displayName: 'Эрнест Днепропетровский А',
					role: 'exponent'
				}, {
					displayName: 'Артем Вячеславович Е.',
					role: 'organizer'
				}, {
					displayName: 'Модератор',
					role: 'moderator'
				}, {
					displayName: 'Администратор',
					role: 'administrator'
				}];
	
				$timeout(function () {
					safeApply($scope, function () {
						$scope.users.push({
							displayName: 'Azaza',
							role: 'administrator'
						});
					});
				}, 1300);
			}]
	
		});
	
		$stateProvider.state('dev-styles', {
			url: '/dev/styles',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.3b7100496a9a9850a678.hot-update.js.map