webpackHotUpdate(1,{

/***/ 272:
/***/ function(module, exports) {

	'use strict';
	
	var isDirty = false;
	
	window.onbeforeunload = function (event) {
	
		if (isDirty) {
	
			var $message = 'It looks like you have been editing something' + ' - if you leave before saving, then your changes will be lost.';
	
			return $message;
		}
	
		return undefined;
	};
	
	module.exports = function () {
	
		return {
			enable: function enable(dirty) {
				isDirty = !!dirty;
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.91e139c5480c0baa59c8.hot-update.js.map