webpackHotUpdate(1,{

/***/ 75:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var templateUrl = __webpack_require__(76);
	var qwery = __webpack_require__(83);
	
	var _qwery = qwery('.page-content'),
	    _qwery2 = _slicedToArray(_qwery, 1),
	    pageContent = _qwery2[0];
	
	var controller = ['$scope', '$element', function ($scope, $element) {
	
		var element = $element[0];
		var userNav = ('.user-nav', element);
		console.log(userNav);
	
		userNav.addEventListener('mouseover', function (event) {
			pageContent.classList.add('blur');
			console.log('over');
		});
	
		userNav.addEventListener('mouseleave', function (event) {
			pageContent.classList.add('remove');
			console.log('leave');
		});
	}];
	
	module.exports = function () {
		return {
			templateUrl: templateUrl,
			controller: controller
		};
	};

/***/ }

})
//# sourceMappingURL=1.e97f49e8534f7f8f83a0.hot-update.js.map