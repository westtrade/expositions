webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			// transclude: 'element',
			restrict: 'C',
			priority: 600,
			terminal: true,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var modalSource = $element[0].outerHTML;
				var modal = void 0;
				var $modalRoot = $element.parent();
				//  $compile.$$createComment('Modal window', $attr.id);
				// $element.after($modalRoot);
				$element.remove();
	
				$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
					if (modalId != $attr.id) {
						return;
					}
	
					if (isOpened) {
	
						var $modalWindow = angular.element(modalSource).removeClass('modal-source').addClass('modal selected');
	
						console.log($modalWindow);
						// modal.after();
						$modalRoot.append($modalWindow);
	
						var $modalScope = $rootScope.$new(true);
						$modalScope.initial = data;
						$modalScope.modalOpened = true;
						modal = $compile($modalWindow)($modalScope);
					} else {
	
						if (modal) {
							console.log('Destroy modal');
							modal.removeClass('selected');
							modal.scope().$destroy();
							modal.remove();
							modal = null;
						}
					}
				});
	
				// let newModal = null;
				// $transclude(function(clone, newScope) {
				// 	const modalSource = clone[0].outerHTML;
				// 	$rootScope.$on('modal-toggled', (event, modalId, isOpened, data) => {
				//
				// 		if (modalId != $attr.id) {
				// 			return ;
				// 		}
				//
				// 		if (isOpened) {
				// 			let modalElement = angular.element(modalSource);
				// 			modalElement.removeClass('modal-source').addClass('modal selected');
				// 			$element.after(modalElement);
				//
				// 			const $modalScope = $rootScope.$new(true);
				// 			$modalScope.initial = data;
				// 			$modalScope.modalOpened = true;
				//
				// 			newModal = $compile(modalElement)($modalScope);
				//
				// 		} else if (newModal) {
				//
				// 			newModal.scope().$destroy();
				// 			newModal.remove();
				// 			newModal = null;
				// 		}
				// 	});
				//
				// });
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.e416d89677f23fc1bdde.hot-update.js.map