webpackHotUpdate(1,{

/***/ 105:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		full_name: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.428b2f76e2202e96117c.hot-update.js.map