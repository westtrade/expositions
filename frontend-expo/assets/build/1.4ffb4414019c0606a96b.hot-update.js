webpackHotUpdate(1,{

/***/ 15:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loginPage = __webpack_require__(16);
	var registrationPage = __webpack_require__(19);
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage
	
		});
	};
	
	module.exports = routes;

/***/ },

/***/ 19:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/registration-page.html';
	var html = "<form class=\"card center\">\n\t<div class=\"offset\">\n\t\t<div class=\"field text-center\">\n\t\t\tЕсть аккаунт? <a href=\"#\" ui-sref=\"user-login\" >Войти?</a>\n\t\t</div>\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\t\t<h1>Регистрация</h1>\n\t\t<input type=\"text\" placeholder=\"Введите номер телефона или email\">\n\t\t<input type=\"password\" placeholder=\"Введите пароль\">\n\t\t<div class=\"field\">\n\t\t\t<a href=\"#\" ui-sref=\"user-recovery\" >Забыли пароль?</a>\n\t\t</div>\n\t\t<button class=\"primary\" type=\"submit\">Войти</button>\n\t\t<div class=\"separator\">\n\t\t\t<div class=\"label\">или</div>\n\t\t</div>\n\n\t\t<div class=\"two-buttons\">\n\t\t\t<a class=\"fb button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-facebook fa-fw\"></i>\n\t\t\t</a><a class=\"vk button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-vk fa-fw\"></i>\n\t\t\t</a>\n\t\t</div>\n\n\t</div>\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.4ffb4414019c0606a96b.hot-update.js.map