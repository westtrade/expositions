webpackHotUpdate(1,{

/***/ 99:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		login: {
			presence: {
				message: "Поле обязательно для заполнения"
			},
			length: {
				minimum: 6,
				message: "Поле должно содержать не менее 6 букв"
			}
	
		},
		password: {
			presence: {
				message: "Поле обязательно для заполнения"
			},
			length: {
				minimum: 6,
				message: "Поле должно содержать не менее 6 букв"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.3b63a6eccad0958dcf48.hot-update.js.map