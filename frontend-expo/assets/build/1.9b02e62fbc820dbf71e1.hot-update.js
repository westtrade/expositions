webpackHotUpdate(1,{

/***/ 179:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var itemTemplate = __webpack_require__(180);
	var offerFormTemplate = __webpack_require__(181);
	var offersCatalogTemplate = __webpack_require__(274);
	
	var _require = __webpack_require__(169),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(167);
	
	var catalog = {
		url: '/offers',
		templateUrl: offersCatalogTemplate
	};
	
	var create = {
		url: '/offer/create',
		templateUrl: offerFormTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {}]
	};
	
	var update = {
		url: '/offer/edit/:id',
		templateUrl: offerFormTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {}]
	};
	
	var item = {
		url: '/offer/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			User.bind($scope);
	
			$scope.offer = {};
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
	
					if (error) {
						throw new ExpoError(error);
					}
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = { catalog: catalog, create: create, update: update, item: item };

/***/ }

})
//# sourceMappingURL=1.9b02e62fbc820dbf71e1.hot-update.js.map