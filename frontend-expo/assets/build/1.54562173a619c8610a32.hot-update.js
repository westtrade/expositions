webpackHotUpdate(1,{

/***/ 51:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var $body = angular.element(document.body);
	var templateUrl = __webpack_require__(52);
	
	var qwery = __webpack_require__(39);
	
	module.exports = ['$modals', '$rootScope', '$compile', function ($modals, $rootScope, $compile) {
	
		return {
			templateUrl: templateUrl,
			link: function link($scope, $element) {
	
				var element = $element[0];
	
				var _qwery = qwery('.background', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    background = _qwery2[0];
	
				// const modals = qwery('.modal', element);
	
				background.addEventListener('click', function (event) {
					$modals.close();
					event.stopPropagation();
					event.preventDefault();
				});
	
				var modalsCache = {};
	
				$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
					var modalInCache = modalId in modalsCache;
					if (!modalInCache) {
						var _qwery3 = qwery('#' + modalId, element),
						    _qwery4 = _slicedToArray(_qwery3, 1),
						    _currentModal = _qwery4[0];
	
						modalsCache[modalId] = _currentModal.innerHTML;
						angular.element(_currentModal).remove();
					}
	
					if (isOpened) {
						// const currentModal = angular.element().clone();
						$element.append(modalsCache[modalId]);
						currentModal[0].classList.toggle('selected', isOpened);
					} else {
						var _qwery5 = qwery('#' + modalId, element),
						    _qwery6 = _slicedToArray(_qwery5, 1),
						    _currentModal2 = _qwery6[0];
	
						_currentModal2.classList.toggle('selected', isOpened);
						angular.element(_currentModal2).remove();
					}
	
					//
					// if (currentModal) {
					//
	
					// const  $modalScope = angular.element(currentModal).scope();
					// $modalScope.destroy();
					// console.log($modalScope);
	
	
					// $provide.value('$modalData', () => data);
					// console.log(data);
					// }
	
	
					//subject-selector
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.54562173a619c8610a32.hot-update.js.map