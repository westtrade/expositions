webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	__webpack_require__(232);
	__webpack_require__(235);
	
	__webpack_require__(245);
	__webpack_require__(246);
	__webpack_require__(247);
	__webpack_require__(260);
	
	var content_style = '\nbody {\n\tfont-family: "Roboto", sans-serif;\n}\n';
	
	module.exports = function () {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
					},
	
					content_style: content_style,
					// height: 500,
					menubar: false,
					skin: false,
					// inline: true,
					plugins: ['paste', 'link', 'autoresize', 'textpattern'],
					textpattern_patterns: [{ start: '*', end: '*', format: 'italic' }, { start: '**', end: '**', format: 'bold' }, { start: '#', format: 'h1' }, { start: '##', format: 'h2' }, { start: '###', format: 'h3' }, { start: '####', format: 'h4' }, { start: '#####', format: 'h5' }, { start: '######', format: 'h6' }, { start: '1. ', cmd: 'InsertOrderedList' }, { start: '* ', cmd: 'InsertUnorderedList' }, { start: '- ', cmd: 'InsertUnorderedList' }]
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.c8964aef0ed06f8d7a3a.hot-update.js.map