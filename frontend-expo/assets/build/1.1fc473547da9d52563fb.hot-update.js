webpackHotUpdate(1,{

/***/ 103:
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _translates;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var translates = (_translates = {
		'Login can\'t be blank': 'Поле Логин не может быть пустым',
		'Login must be at least 6 characters': 'Поле Логин должно быть больше 6 символов'
	}, _defineProperty(_translates, 'Login can\'t be blank', 'Поле пароль не может быть пустым'), _defineProperty(_translates, 'Login must be at least 6 characters', 'Поле пароль должно быть больше 6 символов'), _translates);
	
	var I18N = function () {
		function I18N() {
			_classCallCheck(this, I18N);
		}
	
		_createClass(I18N, [{
			key: '__',
			value: function __(sentence) {
				console.log(sentence);
				return sentence;
			}
		}]);
	
		return I18N;
	}();
	
	module.exports = new I18N();

/***/ }

})
//# sourceMappingURL=1.1fc473547da9d52563fb.hot-update.js.map