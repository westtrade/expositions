webpackHotUpdate(1,{

/***/ 73:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/create-offer.html';
	var html = "<form action=\"/\" name=\"create-offer\" id=\"create-offer-form\" class=\"event-page\">\n\n\t<div class=\"container\">\n\t\t<div class=\"header\">\n\t\t\t<h1 ng-if=\"!offer.id\">Добавить предложение</h1>\n\t\t\t<h1 ng-if=\"offer.id\">Редактировать предложение</h1>\n\n\t\t\t<div class=\"additional\">\n\t\t\t\tСтатус карточки\n\t\t\t</div>\n\t\t\t<div class=\"expo-tabs right\">\n\t\t\t\t<div class=\"head\"><a href=\"#\">Шаг 1</a><a href=\"#\">Шаг 2</a></div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<input type=\"text\" ng-class=\"{error: error.name}\" placeholder=\"Заголовок предложения\" name=\"title\">\n\t\t<field-error error=\"error.name\"></field-error>\n\n\n\t\t<place-selector name=\"place\" class=\"field\" ng-class=\"{error: error.place}\">\n\t\t\t<i class=\"fa fa-map-marker fa-fw\"></i>\n\t\t\t{{ offer.place }}\n\t\t\t<field-error error=\"error.place\"></field-error>\n\n\t\t</place-selector>\n\n\n\t</div>\n\n\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.65e46519ffa73b41426d.hot-update.js.map