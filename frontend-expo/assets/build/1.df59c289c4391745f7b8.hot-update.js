webpackHotUpdate(1,{

/***/ 78:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(79);
	
	module.exports = function () {
		return {
			priority: 15,
			restrict: 'AEM',
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				console.log(element);
	
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.df59c289c4391745f7b8.hot-update.js.map