webpackHotUpdate(1,{

/***/ 33:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(34);
	
	module.exports = function () {
		return {
			priority: 15,
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack
				}
	
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.469c2993faa98171cf4d.hot-update.js.map