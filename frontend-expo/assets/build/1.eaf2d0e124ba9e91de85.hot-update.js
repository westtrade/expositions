webpackHotUpdate(1,{

/***/ 8:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loginPage = __webpack_require__(11);
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration'
		});
	};
	
	module.exports = routes;

/***/ },

/***/ 11:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/login-page.html';
	var html = "<form class=\"card center\">\n\n\t<div class=\"offset\">\n\t\t<h1>Вход</h1>\n\n\t\t<input type=\"text\" placeholder=\"Введите номер телефона или email\">\n\t\t<input type=\"password\" placeholder=\"Введите пароль\">\n\n\t\t<a href=\"#\" ui-sref=\"user-recovery\" class=\"field\">Забыли пароль?</a>\n\t\t<button type=\"primary\"></button>\n\t\t<div class=\"separator\">или</div>\n\t</div>\n\n\t<div class=\"separator\"></div>\n\n\t<div class=\"offset\">\n\n\t</div>\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.eaf2d0e124ba9e91de85.hot-update.js.map