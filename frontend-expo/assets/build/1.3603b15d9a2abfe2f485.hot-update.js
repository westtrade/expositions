webpackHotUpdate(1,{

/***/ 76:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var templateUrl = __webpack_require__(77);
	var qwery = __webpack_require__(78);
	
	var bindEvents = function bindEvents(element) {
		var _qwery = qwery('.user-nav', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    userNav = _qwery2[0];
	
		if (userNav) {
	
			userNav.addEventListener('mouseover', function (event) {
				document.body.classList.add('open-menu');
			});
	
			userNav.addEventListener('mouseleave', function (event) {
				document.body.classList.remove('open-menu');
			});
	
			userNav.addEventListener('click', function (event) {
				document.body.classList.remove('open-menu');
			});
		}
	};
	
	var controller = ['$scope', '$element', '$timeout', 'User', function ($scope, $element, $timeout, User) {
	
		User.bind($scope);
	
		var element = $element[0];
	
		bindEvents(element);
	
		$scope.$watch('user', function () {
			$timeout(function () {
				bindEvents(element);
			}, 0);
		});
	}];
	
	module.exports = function () {
		return {
			templateUrl: templateUrl,
			controller: controller
		};
	};

/***/ }

})
//# sourceMappingURL=1.3603b15d9a2abfe2f485.hot-update.js.map