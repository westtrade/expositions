webpackHotUpdate(1,{

/***/ 37:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			console.log($element.find('.slide'));
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.12ad15690b6898eb98bf.hot-update.js.map