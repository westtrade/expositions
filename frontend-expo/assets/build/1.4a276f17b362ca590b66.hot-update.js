webpackHotUpdate(1,{

/***/ 99:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		login: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
	
		},
		password: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.4a276f17b362ca590b66.hot-update.js.map