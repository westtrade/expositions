webpackHotUpdate(1,{

/***/ 87:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var angular = __webpack_require__(39);
	var $body = angular.element(document.body);
	
	var clean = function clean() {
		var inputData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
		var json = angular.toJson(inputData);
		return JSON.parse(json);
	};
	
	var ModalManager = function () {
		function ModalManager($rootScope) {
			var _this = this;
	
			_classCallCheck(this, ModalManager);
	
			this.active_window = null;
			this.$root = $rootScope;
	
			this.activeCallback = null;
	
			var actions = {
				27: function _() {
					return _this.close();
				}
			};
	
			document.addEventListener('keyup', function (event) {
				var keyCode = event.keyCode;
	
				actions[keyCode] && actions[keyCode]();
			});
		}
	
		_createClass(ModalManager, [{
			key: 'isActive',
			value: function isActive(modalId) {
				return modalId && modalId.length ? this.getActiveWindowID() === modalId : !!this.getActiveWindowID();
			}
		}, {
			key: 'getActiveWindowID',
			value: function getActiveWindowID() {
				var openedModalID = $body.attr('data-active-modal');
				return openedModalID;
			}
		}, {
			key: 'isOpened',
			value: function isOpened() {
				var modalWindowId = this.getActiveWindowID();
				return modalWindowId && modalWindowId.length;
			}
		}, {
			key: 'open',
			value: function open(openingModalWindowId) {
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
				var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
	
				if (typeof data === 'function') {
					callback = data;
					data = {};
				}
	
				data = clean(data);
	
				console.log(data);
	
				// let activeModalID = this.getActiveWindowID();
				this.close();
	
				if (!openingModalWindowId) {
					//TODO error message
					return false;
				}
	
				this.activeCallback = callback;
	
				$body.attr('data-active-modal', openingModalWindowId);
				this.$root.$broadcast('modal-toggled', this, true, data);
	
				return true;
			}
		}, {
			key: 'close',
			value: function close() {
				var _$root;
	
				for (var _len = arguments.length, reasons = Array(_len), _key = 0; _key < _len; _key++) {
					reasons[_key] = arguments[_key];
				}
	
				var modalWindowId = this.getActiveWindowID();
	
				if (!modalWindowId || !modalWindowId.length) {
					return false;
				}
	
				(_$root = this.$root).$broadcast.apply(_$root, ['modal-toggled', this, false].concat(_toConsumableArray(reasons)));
				$body.removeAttr('data-active-modal');
	
				reasons = reasons.map(clean);
	
				if (this.activeCallback) {
					this.activeCallback.apply(this, _toConsumableArray(reasons));
					this.activeCallback = null;
				}
	
				return false;
			}
		}, {
			key: 'toggleModal',
			value: function toggleModal() {
	
				return this.isActive(openModalId) ? this.close() : this.open(openModalId);
			}
		}]);
	
		return ModalManager;
	}();
	
	module.exports = ['$rootScope', function ($rootScope) {
		return new ModalManager($rootScope);
	}];

/***/ }

})
//# sourceMappingURL=1.7cda8b3a349c7b58b49e.hot-update.js.map