webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	//
	// const getChildren = (element) => {
	// 	console.log(element);
	// };
	
	
	// onDelete - rebuild tree under element (nextSibling)
	// onSize (element - maybe load image) change - rebuild tree under  (nextSibling) - image on load -> id -> rebuildElement by id (style changed ?)
	// onAdd
	//
	// (element) => nextSibling -> whileEnd
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var qwery = __webpack_require__(40);
	
	var _require = __webpack_require__(55),
	    markItem = _require.markItem,
	    onePixel = _require.onePixel;
	
	var elementPosiition = function elementPosiition(container, element) {
		var containerPosition = container.getBoundingClientRect();
		var elementPosition = element.getBoundingClientRect();
	
		var width = elementPosition.width;
		var height = elementPosition.height;
		var left = elementPosition.left - containerPosition.left;
		var top = elementPosition.top - containerPosition.top;
	
		return { width: width, height: height, left: left, top: top };
	
		// const name = value;
	};
	
	var calculateGutter = function calculateGutter(container, first) {
	
		'use asm';
	
		var modulo = container.offsetWidth % first.offsetWidth;
		var columns = (container.offsetWidth - modulo) / first.offsetWidth;
		var gutter = parseInt(modulo / (columns - 1));
	
		return { gutter: gutter, columns: columns };
	};
	
	var calculateContainerHeight = function calculateContainerHeight(container) {
		var info = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		var gutter = info.gutter,
		    columns = info.columns;
	
	
		var height = 0;
	
		var lastElement = container.lastElementChild;
		if (!lastElement) {
			return height;
		}
	
		var count = columns;
		while (count-- && lastElement) {
			var position = elementPosiition(container, lastElement);
			var currentHeight = position.top + lastElement.offsetHeight;
	
			if (currentHeight > height) {
				height = currentHeight;
			}
	
			lastElement = lastElement.previousElementSibling;
		}
	
		return height;
	};
	
	var updateContainerHeight = function updateContainerHeight(container) {
		var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
		    gutter = _ref.gutter,
		    columns = _ref.columns;
	
		var containerHeight = calculateContainerHeight(container, { gutter: gutter, columns: columns });
		container.style.height = containerHeight + 'px';
	};
	
	var rebuildItem = function rebuildItem(container, current) {
		var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
		    _ref2$gutter = _ref2.gutter,
		    gutter = _ref2$gutter === undefined ? 32 : _ref2$gutter,
		    _ref2$columns = _ref2.columns,
		    columns = _ref2$columns === undefined ? 4 : _ref2$columns,
		    _ref2$onlyOne = _ref2.onlyOne,
		    onlyOne = _ref2$onlyOne === undefined ? true : _ref2$onlyOne;
	
		var previous = current.previousElementSibling;
		if (!previous) {
	
			current.style.top = '0px';
			current.style.left = '0px';
	
			var info = calculateGutter(container, current);
	
			gutter = info.gutter;
			columns = info.columns;
	
			return { gutter: gutter, columns: columns };
		}
	
		// const container = value;
		var positionPrev = elementPosiition(container, previous);
		var left = positionPrev.left + positionPrev.width + gutter;
		var isOverflowed = left + current.offsetWidth > container.offsetWidth;
	
		var childrenArray = Array.from(container.children);
		var currentPosition = childrenArray.indexOf(current);
	
		console.log(current);
	
		if (!isOverflowed) {
			current.style.left = left + 'px';
			current.style.top = positionPrev.top + 'px';
		} else {
			current.style.left = '0px';
			current.style.top = positionPrev.top + positionPrev.height + gutter + 'px';
		}
	
		if (onlyOne) {
			updateContainerHeight(container, { gutter: gutter, columns: columns });
		}
	
		// const positionCurrent = elementPosiition(container, current);
		// let containerHeight = positionCurrent.top + positionCurrent.height;
		//
		//
		// if (containerHeight > container.offsetHeight) {
		// 	container.style.height = containerHeight + 'px';
		// }
	
	
		// console.log((positionPrev.left + positionPrev.width + GUTTER) + 'px');
	
		// debugger;
	
	
		// console.log(positionPrev);
	
	
		// containerElement.clientWidth
	
	
		// console.log();
	
		return { gutter: gutter, columns: columns };
	};
	
	var rebuildItems = function rebuildItems(containerElement, startElement) {
		var endElement = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	
		var _ref3 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
		    _ref3$gutter = _ref3.gutter,
		    gutter = _ref3$gutter === undefined ? 32 : _ref3$gutter,
		    _ref3$columns = _ref3.columns,
		    columns = _ref3$columns === undefined ? 4 : _ref3$columns;
	
		var info = { gutter: gutter, columns: columns, onlyOne: false };
	
		// if (!endElement) {
		// 	return rebuildItem(containerElement, startElement, info);
		// }
	
		var current = startElement;
		while (current && current !== endElement) {
			info = rebuildItem(containerElement, current, info);
			current = current.nextElementSibling;
		}
	
		updateContainerHeight(containerElement, { gutter: gutter, columns: columns });
	
		return info;
	};
	
	var link = function link($scope, $element, $attributes, $controller) {
	
		var container = $element[0];
		var rebuildInfo = { gutter: 32, columns: 4 };
	
		// if (!element.hasChildNodes()) {
		// 	return;
		// }
		//
		// let currentElement = value;
		//
		// let name = value;
	
		// let elementsMap = {
		// 	new: {},
		//
		// };
	
	
		// listofupdates - after
	
	
		$scope.$watch(function () {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			var markers = Array.from(container.children).filter(function (item) {
				return item.nodeType === Node.ELEMENT_NODE;
			})
			// console.log(markers);
			// debugger;
			.reduce(markItem, { added: [], changed: [] });
	
			// const markers = qwery('[ng-repeat], [ng-repeat-start]', container)
			// 	.reduce(markItem, {added: [], changed: []});
	
			// console.log(markers);
			// debugger;
			// const repeatItems = qwery('[ng-repeat]');
		});
	
		container.addEventListener('load', function (event) {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			// console.log(event.target.nodeName);
	
	
			if (event.target.src.indexOf(onePixel) >= 0) {
				var _ret = function () {
	
					var marker = event.target;
					var cardElement = marker.parentNode;
					cardElement.removeChild(marker);
	
					var masonryId = cardElement.getAttribute('data-masonry-id', masonryId);;
					var images = qwery('img', cardElement).filter(function (item) {
						return item !== marker;
					}).forEach(function (image) {
						image.masonryId = masonryId;
						return image;
					});
	
					rebuildInfo = rebuildItem(container, cardElement, rebuildInfo);
					// rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
	
					// 		console.log(images);
					// 		debugger;
	
					return {
						v: void 0
					};
				}();
	
				if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
			}
	
			if (event.target.nodeName === 'IMG') {
	
				if (event.target.masonryId) {
					var _qwery = qwery('[data-masonry-id="' + event.target.masonryId + '"]'),
					    _qwery2 = _slicedToArray(_qwery, 1),
					    cardElement = _qwery2[0];
					// debugger;
	
	
					rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
				}
			}
		}, true);
	
		container.addEventListener('error', function (event) {
	
			if (event.target instanceof HTMLImageElement) {
				console.log(event.target);
			}
		}, true);
	
		// debugger;
		// const element = $element[0];
		// let pendingImages = 0;
		//
		// $scope.$watchCollection('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	const cards = qwery('.card', element);
		//
		// 	for (let card of cards) {
		//
		// 		const $card = angular.element(card);
		// 		const $scope = $card.scope();
		//
		// 		console.log($scope);
		// 		console.log($scope.$id);
		//
		// 		let marker = new Image();
		// 		marker.setAttribute('ng-if', true);
		// 		marker.src = onePixel;
		// 		card.appendChild(marker);
		// 	}
		//
		// 	// pendingImages += cards.length * 3;
		//
		// 	// qwery('.card', element).forEach(item => {
		// 	// 	item.length
		// 	// 	console.log(item.offsetHeight);
		// 	// });
		//
		//
		//
		// 	// debugger;
		//
		// });
		//
		// //
		// element.addEventListener('load', (event) => {
		// 	if (event.target.src.indexOf(onePixel) >= 0) {
		// 		const marker = event.target;
		// 		const cardElement = marker.parentNode;
		// 		const images = qwery('img', cardElement).filter(item => item !== marker);
		//
		// 		console.log(images);
		//
		// 		debugger;
		// 	}
		// }, true);
	
	
		// $scope.$watch('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	// console.log(qwery('.card', element));
		// 	debugger;
		// }, () => {
		// 	console.log('Changed');
		//
		// })
	
	
		// console.log(element, $scope);
	
	
		// $scope.$watch((...args) => {
		// 	console.log('DIGEST');
		// 	console.log(args);
		// })
	
	
		// debugger;
	};
	
	module.exports = [function () {
	
		var directive = {
			// priority: 1000,
			// terminal: true,
			restrict: 'CEA',
			link: link
		};
	
		return directive;
	}];

/***/ }

})
//# sourceMappingURL=1.0aeaf0036488536cb68c.hot-update.js.map