webpackHotUpdate(1,{

/***/ 80:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeApply = __webpack_require__(73);
	
	module.exports = ['$scope', '$modals', function ($scope, $modals) {
	
		$scope.$watchCollection('sliderList', function (prev, next, $scope) {
			$scope.$emit('slider:update');
		});
	
		$scope.sliderList = [{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}];
	
		$scope.openEditor = function () {
			var sliderList = $scope.sliderList;
	
			$modals.open('slider-editor', { sliderList: sliderList }, function () {
				var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
				    sliderList = _ref.sliderList;
	
				safeApply(function () {
					$scope.sliderList = sliderList;
					if ($scope.$slider) {
						$scope.$slider.update();
					}
				});
			});
		};
	
		// $scope.openEditor();
	
		// console.log($scope);
		// console.log($scope.$parent.$parent.$slider.update());
		// console.log($scope.$slider);
		// $scope.$slider.update();
	}];

/***/ }

})
//# sourceMappingURL=1.4921cbc3951e654de55f.hot-update.js.map