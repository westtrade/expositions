webpackHotUpdate(1,{

/***/ 18:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(19);
	var uiRouter = __webpack_require__(21);
	var uiDND = __webpack_require__(22);
	
	console.log(uiDND);
	
	__webpack_require__(24);
	var loadingBar = __webpack_require__(25);
	var ngAnimate = __webpack_require__(27);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'chieffancypants.loadingBar']);
	var router = __webpack_require__(29);
	app.config(router);
	
	var topBar = __webpack_require__(35);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(37);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(47);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(50);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var button = __webpack_require__(51);
	app.directive('button', button);
	
	app.controller('offersCatalog', __webpack_require__(52)).controller('subjectSelectorModal', __webpack_require__(53)).controller('sliderController', __webpack_require__(55)).controller('expositionSelectorModal', __webpack_require__(56)).controller('sliderEditorModal', __webpack_require__(57));
	
	var socialLogin = __webpack_require__(58);
	app.directive('socialLogin', socialLogin);
	
	var masonryLayout = __webpack_require__(59);
	app.directive('masonryLayout', masonryLayout);
	
	var Modal = __webpack_require__(62);
	app.factory('$modals', Modal);
	
	var modals = __webpack_require__(63);
	app.directive('expoModalManager', modals);
	
	var modal = __webpack_require__(65);
	app.directive('modalSource', modal);
	
	var User = __webpack_require__(66);
	app.factory('User', User);
	
	module.exports = app;

/***/ }

})
//# sourceMappingURL=1.1c7b80f729883b6c8fef.hot-update.js.map