webpackHotUpdate(1,{

/***/ 84:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var itemTemplate = __webpack_require__(73);
	var formTemplate = __webpack_require__(85);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(61);
	
	var mainController = ['$scope', '$element', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $element, $stateParams, User, cfpLoadingBar, $modals, $state) {
		User.bind($scope);
		formApply($scope, $element[0]);
	}];
	
	var create = {
		url: '/exposition/create',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var update = {
		url: '/exposition/edit/:id',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var index = {
		url: '/exposition/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			User.bind($scope);
	
			$scope.offer = {};
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = {
		create: create, update: update, index: index
	};

/***/ }

})
//# sourceMappingURL=1.e5388c1b1eba13df2da8.hot-update.js.map