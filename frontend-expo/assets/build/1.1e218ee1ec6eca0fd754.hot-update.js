webpackHotUpdate(1,{

/***/ 62:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			// transclude: 'element',
			restrict: 'C',
			priority: 600,
			terminal: true,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var modalSource = $element[0].outerHTML;
				var modal = void 0;
				var $modalRoot = $element.parent();
				$element.remove();
				$rootScope.$on('modal-toggled', function (event, modalService, isOpened, data) {
	
					if (modalService.getActiveWindowID() != $attr.id) {
						return;
					}
	
					if (isOpened) {
	
						var $modalWindow = angular.element(modalSource).removeClass('modal-source').addClass('modal selected');
						$modalRoot.append($modalWindow);
	
						var $modalScope = $rootScope.$new(true);
						Object.assign($modalScope, data);
						$modalScope.modalOpened = true;
						$modalScope.close = function () {
							modalService.close();
						};
	
						modal = $compile($modalWindow)($modalScope);
					} else {
	
						if (modal) {
							// console.log('Destroy modal');
							modal.removeClass('selected');
							modal.scope().$destroy();
							modal.remove();
							modal = null;
						}
					}
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.1e218ee1ec6eca0fd754.hot-update.js.map