webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var gmaps = __webpack_require__(101);
	
	window.addEventListener('load', function () {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=initGmaps';
		document.body.appendChild(script);
	});
	
	global.initGmaps = function initGmaps() {
		console.log('Gmaps initiadted');
	};
	
	var templateUrl = __webpack_require__(102);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
					$transclude($scope, function (clonedContent) {});
				};
			}
		};
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.199508e830bb7f5b0a88.hot-update.js.map