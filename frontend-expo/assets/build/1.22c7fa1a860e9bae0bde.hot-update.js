webpackHotUpdate(1,{

/***/ 241:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var qwery = __webpack_require__(93);
	
	var template = '\n\t<div\n\t\tclass="input"\n\t\tmoment-picker="value"\n\t\tlocale="ru"\n\t\ttoday="true"\n\t\tformat="L"\n\t\tmax-view="month"\n\t\tstart-view="month"\n\t\tplaceholder="\u0414\u0430\u0442\u0430 \u043D\u0430\u0447\u0430\u043B\u0430"\n\t></div>\n\t<input type="hidden">\n\t{{ value }}\n\t<ng-transclude></ng-transclude>\n';
	
	module.exports = function () {
		return {
			template: template,
			transclude: true,
			scope: {},
			link: function link($state, $element, $attrs, controller, $transclude) {
	
				var element = $element[0];
				var input = qwery('input', element);
				var _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name,
				    _$attrs$locale = $attrs.locale,
				    locale = _$attrs$locale === undefined ? 'ru' : _$attrs$locale,
				    _$attrs$today = $attrs.today,
				    today = _$attrs$today === undefined ? 'true' : _$attrs$today,
				    _$attrs$maxView = $attrs.maxView,
				    maxView = _$attrs$maxView === undefined ? 'month' : _$attrs$maxView,
				    _$attrs$startView = $attrs.startView,
				    startView = _$attrs$startView === undefined ? 'month' : _$attrs$startView;
	
				input.setAttribute('name', name);
	
				$scope.$watch('value', function (value) {
					if (name && name.length) {
						input.form.setValue(name, value);
					}
				});
	
				console.log(name, locale, maxView);
	
				$transclude(function (clone) {});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.22c7fa1a860e9bae0bde.hot-update.js.map