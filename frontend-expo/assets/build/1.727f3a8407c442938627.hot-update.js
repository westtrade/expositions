webpackHotUpdate(1,{

/***/ 79:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(80);
	
	module.exports = function () {
		return {
			priority: 15,
			restrict: 'AEM',
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				attrs.$observe('styledInput', function () {
					styleInputs(element);
				});
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.727f3a8407c442938627.hot-update.js.map