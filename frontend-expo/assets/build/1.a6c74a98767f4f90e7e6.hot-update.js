webpackHotUpdate(1,{

/***/ 52:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$scope', function ($scope) {
	
		$scope.$watchCollection('sliderList', function () {
			if ($scope.$parent.$slider) {
				$scope.$parent.$slider.update();
			}
		});
	
		$scope.sliderList = [{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}];
	
		if ($scope.$parent.$slider) {
			$scope.$parent.$slider.run();
		}
	
		// console.log($scope);
	
		// console.log($scope.$parent.$parent.$slider.update());
		// console.log($scope.$slider);
	
		// $scope.$slider.update();
	}];

/***/ }

})
//# sourceMappingURL=1.a6c74a98767f4f90e7e6.hot-update.js.map