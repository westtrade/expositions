webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var qwery = __webpack_require__(83);
	var loadGMap = __webpack_require__(116);
	var GMap = __webpack_require__(101);
	
	var templateUrl = __webpack_require__(102);
	
	var link = function link($scope, $element, $attrs, $transclude) {
	
		var element = $element[0];
	
		var _qwery = qwery('.map', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    mapElement = _qwery2[0];
	
		loadGMap(function () {
	
			var map = new GMap({ el: mapElement, lat: 0, lng: 0, zoom: 15 });
			$scope.$watch('selector.address', function (value) {
	
				GMap.geocode({
					address: value,
					callback: function callback(_ref, status) {
						var _ref2 = _slicedToArray(_ref, 1),
						    location = _ref2[0];
	
						var _location$geometry$lo = location.geometry.location,
						    lat = _location$geometry$lo.lat,
						    lng = _location$geometry$lo.lng;
	
						map.setCenter(lat(), lng(), function () {
							console.log('Centred');
						});
					}
				});
			});
	
			GMap.geocode({
				address: 'Россия',
				callback: function callback(_ref3, status) {
					var _ref4 = _slicedToArray(_ref3, 1),
					    location = _ref4[0];
	
					var _location$geometry$lo2 = location.geometry.location,
					    lat = _location$geometry$lo2.lat,
					    lng = _location$geometry$lo2.lng;
	
					map.setCenter(lat(), lng(), function () {
						console.log('Centred');
					});
				}
			});
		});
	};
	
	module.exports = function () {
		return _defineProperty({
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			link: link
		}, 'scope', true);
	};

/***/ }

})
//# sourceMappingURL=1.76c0a7729fde822c6e61.hot-update.js.map