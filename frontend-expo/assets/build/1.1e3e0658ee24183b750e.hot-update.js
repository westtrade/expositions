webpackHotUpdate(1,{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(44);
	var uiRouter = __webpack_require__(46);
	var uiDND = __webpack_require__(47);
	
	var dropzone = __webpack_require__(49);
	var ngDropzone = __webpack_require__(51);
	
	__webpack_require__(52);
	__webpack_require__(53);
	__webpack_require__(54);
	
	var loadingBar = __webpack_require__(55);
	var ngAnimate = __webpack_require__(57);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'chieffancypants.loadingBar', 'thatisuday.dropzone']);
	
	var router = __webpack_require__(59);
	app.config(router);
	
	var common = __webpack_require__(83);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(84);
	
	app.directive('button', __webpack_require__(94)).directive('expoSlider', __webpack_require__(95)).directive('topBar', __webpack_require__(97)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(99)).directive('masonryLayout', __webpack_require__(100)).directive('expoModalManager', __webpack_require__(103)).directive('modalSource', __webpack_require__(105)).directive('expoTabs', __webpack_require__(106)).directive('fieldError', __webpack_require__(111)).directive('mapSelector', __webpack_require__(125)).directive('placeAutocompleteField', __webpack_require__(113));
	
	app.controller('offersCatalog', __webpack_require__(114)).controller('subjectSelectorModal', __webpack_require__(116)).controller('sliderController', __webpack_require__(118)).controller('expositionSelectorModal', __webpack_require__(119)).controller('sliderEditorModal', __webpack_require__(120)).controller('offerTeaserController', __webpack_require__(121));
	
	app.factory('$modals', __webpack_require__(122)).factory('User', __webpack_require__(123));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;

/***/ },

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var qwery = __webpack_require__(90);
	var loadMap = __webpack_require__(108);
	var GMap = __webpack_require__(109);
	
	var templateUrl = __webpack_require__(126);
	
	var centerMap = function centerMap(map) {
		var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { geometry: {} },
		    location = _ref.geometry.location;
	
		var cb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
		if (!location) {
			throw new Error('Location must be defined into information');
		}
	
		var lat = location.lat,
		    lng = location.lng;
	
		map.setCenter(lat(), lng(), cb);
	};
	
	var link = function link($scope, $element, $attrs, $transclude) {
	
		var element = $element[0];
	
		var _qwery = qwery('.map', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    mapElement = _qwery2[0];
	
		var _qwery3 = qwery('input', element),
		    _qwery4 = _slicedToArray(_qwery3, 1),
		    addressSelector = _qwery4[0];
	
		// When the user selects an address from the dropdown, populate the address
		// fields in the form.
	
	
		loadMap(function () {
	
			var map = new GMap({ el: mapElement, lat: 0, lng: 0, zoom: 15 });
	
			// const autocomplete = new ymaps.SuggestView(addressSelector);
			// console.log(autocomplete.events);
			//
			// autocomplete.events.add('select', (event) => {
			// 	// console.log(autocomplete.state);
			// 	console.log(event.get('item'));
			// })
	
	
			// const autocomplete = new google.maps.places.Autocomplete(addressSelector, {types: ['geocode']});
			// autocomplete.addListener('place_changed', () => {
			// 	const place = autocomplete.getPlace();
			//
			// 	// console.log(place);
			// 	// console.log(place.address_components);
			// 	centerMap(map, place);
			// });
	
	
			//
			// $scope.$watch('selector.address', (value) => {
			//
			// 	GMap.geocode({
			// 		address: value,
			// 		callback: (result, status) => {
			// 			console.log(result);
			// 		}
			// 	});
			//
			// }, 'AIzaSyCQS8Ysgs4w8GDUJhe-ge3bm2w42eZhNBU');
	
	
			// GMap.geocode({
			// 	address: 'Россия',
			// 	callback: ([location], status) => {
			//
			//
			//
			// 		const {geometry: {location: {lat, lng}}} = location;
			// 		map.setCenter(lat(), lng(), () => {
			// 			console.log('Centred');
			// 		})
			// 	}
			// });
		});
	};
	
	module.exports = function () {
		return _defineProperty({
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			link: link
		}, 'scope', true);
	};

/***/ },

/***/ 126:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/app/directive/map-selector/template.html';
	var html = "<ng-transclude></ng-transclude>\n<div class=\"map-selector\">\n\t<place-autocomplete-field />\n\t<div class=\"map\"></div>\n</div>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.1e3e0658ee24183b750e.hot-update.js.map