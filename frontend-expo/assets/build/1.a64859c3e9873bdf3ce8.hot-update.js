webpackHotUpdate(1,{

/***/ 107:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(108);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
					$transclude($scope, function (clonedContent) {});
				};
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.a64859c3e9873bdf3ce8.hot-update.js.map