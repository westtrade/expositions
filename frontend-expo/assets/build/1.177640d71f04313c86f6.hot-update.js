webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	// const Dropzone = require('dropzone');
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var templateUrl = __webpack_require__(240);
	var qwery = __webpack_require__(93);
	// require('dropzone/src/dropzone.scss');
	
	
	var dragHandler = function dragHandler(event) {
	
		console.log(event.type);
	
		switch (event.type) {
			case 'drop':
	
				console.log(event);
	
				event.preventDefault();
				break;
			default:
	
		}
	};
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			scope: {},
			link: function link($scope, $element, $attrs, $controllers, $transclude) {
	
				var element = $element[0];
				var _$attrs$url = $attrs.url,
				    url = _$attrs$url === undefined ? '/' : _$attrs$url,
				    _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name,
				    multiple = $attrs.multiple;
	
				element.classList.add('dropzone');
	
				var _qwery = qwery('input', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    inputField = _qwery2[0];
	
				inputField.setAttribute('name', name);
	
				element.addEventListener('drag', dragHandler, false);
				element.addEventListener('dragstart', dragHandler, false);
				element.addEventListener('dragend', dragHandler, false);
				element.addEventListener('dragover', dragHandler, false);
				element.addEventListener('dragenter', dragHandler, false);
				element.addEventListener('dragleave', dragHandler, false);
				element.addEventListener('drop', dragHandler, false);
	
				$transclude(function (clone) {
					// const currentDropzone = new Dropzone(element, {
					// 	url
					// });
	
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.177640d71f04313c86f6.hot-update.js.map