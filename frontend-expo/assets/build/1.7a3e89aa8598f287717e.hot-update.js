webpackHotUpdate(1,{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(1);
	__webpack_require__(3);
	__webpack_require__(10);
	__webpack_require__(11);
	
	__webpack_require__(13);
	__webpack_require__(14);
	__webpack_require__(15);
	__webpack_require__(16).polyfill();
	
	var app = __webpack_require__(12);

/***/ },

/***/ 12:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var uiRouter = __webpack_require__(19);
	
	__webpack_require__(20);
	var loadingBar = __webpack_require__(21);
	var ngAnimate = __webpack_require__(23);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate]);
	var router = __webpack_require__(25);
	app.config(router);
	
	var topBar = __webpack_require__(31);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(33);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(43);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(46);
	Object.entries(common).reduce(function (app, _ref) {
	  var _ref2 = _slicedToArray(_ref, 2),
	      name = _ref2[0],
	      filter = _ref2[1];
	
	  return app.filter(name, filter);
	}, app);
	
	var button = __webpack_require__(47);
	app.directive('button', button);
	
	var offersController = __webpack_require__(48);
	app.controller('offersController', ['$scope', offersController]);
	
	module.exports = app;

/***/ },

/***/ 13:
/***/ function(module, exports) {

	/*
	 * classList.js: Cross-browser full element.classList implementation.
	 * 2014-07-23
	 *
	 * By Eli Grey, http://eligrey.com
	 * Public Domain.
	 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
	 */
	
	/*global self, document, DOMException */
	
	/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js*/
	
	/* Copied from MDN:
	 * https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
	 */
	
	if ("document" in window.self) {
	
	  // Full polyfill for browsers with no classList support
	  // Including IE < Edge missing SVGElement.classList
	  if (!("classList" in document.createElement("_"))
	    || document.createElementNS && !("classList" in document.createElementNS("http://www.w3.org/2000/svg","g"))) {
	
	  (function (view) {
	
	    "use strict";
	
	    if (!('Element' in view)) return;
	
	    var
	        classListProp = "classList"
	      , protoProp = "prototype"
	      , elemCtrProto = view.Element[protoProp]
	      , objCtr = Object
	      , strTrim = String[protoProp].trim || function () {
	        return this.replace(/^\s+|\s+$/g, "");
	      }
	      , arrIndexOf = Array[protoProp].indexOf || function (item) {
	        var
	            i = 0
	          , len = this.length
	        ;
	        for (; i < len; i++) {
	          if (i in this && this[i] === item) {
	            return i;
	          }
	        }
	        return -1;
	      }
	      // Vendors: please allow content code to instantiate DOMExceptions
	      , DOMEx = function (type, message) {
	        this.name = type;
	        this.code = DOMException[type];
	        this.message = message;
	      }
	      , checkTokenAndGetIndex = function (classList, token) {
	        if (token === "") {
	          throw new DOMEx(
	              "SYNTAX_ERR"
	            , "An invalid or illegal string was specified"
	          );
	        }
	        if (/\s/.test(token)) {
	          throw new DOMEx(
	              "INVALID_CHARACTER_ERR"
	            , "String contains an invalid character"
	          );
	        }
	        return arrIndexOf.call(classList, token);
	      }
	      , ClassList = function (elem) {
	        var
	            trimmedClasses = strTrim.call(elem.getAttribute("class") || "")
	          , classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
	          , i = 0
	          , len = classes.length
	        ;
	        for (; i < len; i++) {
	          this.push(classes[i]);
	        }
	        this._updateClassName = function () {
	          elem.setAttribute("class", this.toString());
	        };
	      }
	      , classListProto = ClassList[protoProp] = []
	      , classListGetter = function () {
	        return new ClassList(this);
	      }
	    ;
	    // Most DOMException implementations don't allow calling DOMException's toString()
	    // on non-DOMExceptions. Error's toString() is sufficient here.
	    DOMEx[protoProp] = Error[protoProp];
	    classListProto.item = function (i) {
	      return this[i] || null;
	    };
	    classListProto.contains = function (token) {
	      token += "";
	      return checkTokenAndGetIndex(this, token) !== -1;
	    };
	    classListProto.add = function () {
	      var
	          tokens = arguments
	        , i = 0
	        , l = tokens.length
	        , token
	        , updated = false
	      ;
	      do {
	        token = tokens[i] + "";
	        if (checkTokenAndGetIndex(this, token) === -1) {
	          this.push(token);
	          updated = true;
	        }
	      }
	      while (++i < l);
	
	      if (updated) {
	        this._updateClassName();
	      }
	    };
	    classListProto.remove = function () {
	      var
	          tokens = arguments
	        , i = 0
	        , l = tokens.length
	        , token
	        , updated = false
	        , index
	      ;
	      do {
	        token = tokens[i] + "";
	        index = checkTokenAndGetIndex(this, token);
	        while (index !== -1) {
	          this.splice(index, 1);
	          updated = true;
	          index = checkTokenAndGetIndex(this, token);
	        }
	      }
	      while (++i < l);
	
	      if (updated) {
	        this._updateClassName();
	      }
	    };
	    classListProto.toggle = function (token, force) {
	      token += "";
	
	      var
	          result = this.contains(token)
	        , method = result ?
	          force !== true && "remove"
	        :
	          force !== false && "add"
	      ;
	
	      if (method) {
	        this[method](token);
	      }
	
	      if (force === true || force === false) {
	        return force;
	      } else {
	        return !result;
	      }
	    };
	    classListProto.toString = function () {
	      return this.join(" ");
	    };
	
	    if (objCtr.defineProperty) {
	      var classListPropDesc = {
	          get: classListGetter
	        , enumerable: true
	        , configurable: true
	      };
	      try {
	        objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
	      } catch (ex) { // IE 8 doesn't support enumerable:true
	        if (ex.number === -0x7FF5EC54) {
	          classListPropDesc.enumerable = false;
	          objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
	        }
	      }
	    } else if (objCtr[protoProp].__defineGetter__) {
	      elemCtrProto.__defineGetter__(classListProp, classListGetter);
	    }
	
	    }(window.self));
	
	    } else {
	    // There is full or partial native classList support, so just check if we need
	    // to normalize the add/remove and toggle APIs.
	
	    (function () {
	      "use strict";
	
	      var testElement = document.createElement("_");
	
	      testElement.classList.add("c1", "c2");
	
	      // Polyfill for IE 10/11 and Firefox <26, where classList.add and
	      // classList.remove exist but support only one argument at a time.
	      if (!testElement.classList.contains("c2")) {
	        var createMethod = function(method) {
	          var original = DOMTokenList.prototype[method];
	
	          DOMTokenList.prototype[method] = function(token) {
	            var i, len = arguments.length;
	
	            for (i = 0; i < len; i++) {
	              token = arguments[i];
	              original.call(this, token);
	            }
	          };
	        };
	        createMethod('add');
	        createMethod('remove');
	      }
	
	      testElement.classList.toggle("c3", false);
	
	      // Polyfill for IE 10 and Firefox <24, where classList.toggle does not
	      // support the second argument.
	      if (testElement.classList.contains("c3")) {
	        var _toggle = DOMTokenList.prototype.toggle;
	
	        DOMTokenList.prototype.toggle = function(token, force) {
	          if (1 in arguments && !this.contains(token) === !force) {
	            return force;
	          } else {
	            return _toggle.call(this, token);
	          }
	        };
	
	      }
	
	      testElement = null;
	    }());
	  }
	}


/***/ },

/***/ 14:
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {/*!
	 * weakmap-polyfill v2.0.0 - ECMAScript6 WeakMap polyfill
	 * https://github.com/polygonplanet/weakmap-polyfill
	 * Copyright (c) 2015-2016 polygon planet <polygon.planet.aqua@gmail.com>
	 * @license MIT
	 */
	
	(function(self) {
	  'use strict';
	
	  if (self.WeakMap) {
	    return;
	  }
	
	  var hasOwnProperty = Object.prototype.hasOwnProperty;
	  var defineProperty = function(object, name, value) {
	    if (Object.defineProperty) {
	      Object.defineProperty(object, name, {
	        configurable: true,
	        writable: true,
	        value: value
	      });
	    } else {
	      object[name] = value;
	    }
	  };
	
	  self.WeakMap = (function() {
	
	    // ECMA-262 23.3 WeakMap Objects
	    function WeakMap() {
	      if (this === void 0) {
	        throw new TypeError("Constructor WeakMap requires 'new'");
	      }
	
	      defineProperty(this, '_id', genId('_WeakMap'));
	
	      // ECMA-262 23.3.1.1 WeakMap([iterable])
	      if (arguments.length > 0) {
	        // Currently, WeakMap `iterable` argument is not supported
	        throw new TypeError('WeakMap iterable is not supported');
	      }
	    }
	
	    // ECMA-262 23.3.3.2 WeakMap.prototype.delete(key)
	    defineProperty(WeakMap.prototype, 'delete', function(key) {
	      checkInstance(this, 'delete');
	
	      if (!isObject(key)) {
	        return false;
	      }
	
	      var entry = key[this._id];
	      if (entry && entry[0] === key) {
	        delete key[this._id];
	        return true;
	      }
	
	      return false;
	    });
	
	    // ECMA-262 23.3.3.3 WeakMap.prototype.get(key)
	    defineProperty(WeakMap.prototype, 'get', function(key) {
	      checkInstance(this, 'get');
	
	      if (!isObject(key)) {
	        return void 0;
	      }
	
	      var entry = key[this._id];
	      if (entry && entry[0] === key) {
	        return entry[1];
	      }
	
	      return void 0;
	    });
	
	    // ECMA-262 23.3.3.4 WeakMap.prototype.has(key)
	    defineProperty(WeakMap.prototype, 'has', function(key) {
	      checkInstance(this, 'has');
	
	      if (!isObject(key)) {
	        return false;
	      }
	
	      var entry = key[this._id];
	      if (entry && entry[0] === key) {
	        return true;
	      }
	
	      return false;
	    });
	
	    // ECMA-262 23.3.3.5 WeakMap.prototype.set(key, value)
	    defineProperty(WeakMap.prototype, 'set', function(key, value) {
	      checkInstance(this, 'set');
	
	      if (!isObject(key)) {
	        throw new TypeError('Invalid value used as weak map key');
	      }
	
	      var entry = key[this._id];
	      if (entry && entry[0] === key) {
	        entry[1] = value;
	        return this;
	      }
	
	      defineProperty(key, this._id, [key, value]);
	      return this;
	    });
	
	
	    function checkInstance(x, methodName) {
	      if (!isObject(x) || !hasOwnProperty.call(x, '_id')) {
	        throw new TypeError(
	          methodName + ' method called on incompatible receiver ' +
	          typeof x
	        );
	      }
	    }
	
	    function genId(prefix) {
	      return prefix + '_' + rand() + '.' + rand();
	    }
	
	    function rand() {
	      return Math.random().toString().substring(2);
	    }
	
	
	    defineProperty(WeakMap, '_polyfill', true);
	    return WeakMap;
	  })();
	
	
	  function isObject(x) {
	    return Object(x) === x;
	  }
	
	})(
	  typeof self !== 'undefined' ? self :
	  typeof window !== 'undefined' ? window :
	  typeof global !== 'undefined' ? global : this
	);
	
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },

/***/ 15:
/***/ function(module, exports) {

	'use strict';
	
	// Polyfill for creating CustomEvents on IE9/10/11
	
	// code pulled from:
	// https://github.com/d4tocchini/customevent-polyfill
	// https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent#Polyfill
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	try {
	  var ce = new window.CustomEvent('test');
	  ce.preventDefault();
	  if (ce.defaultPrevented !== true) {
	    // IE has problems with .preventDefault() on custom events
	    // http://stackoverflow.com/questions/23349191
	    throw new Error('Could not prevent default');
	  }
	} catch (e) {
	  var CustomEvent = function CustomEvent(event, params) {
	    var evt, origPrevent;
	    params = params || {
	      bubbles: false,
	      cancelable: false,
	      detail: undefined
	    };
	
	    evt = document.createEvent("CustomEvent");
	    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
	    origPrevent = evt.preventDefault;
	    evt.preventDefault = function () {
	      origPrevent.call(this);
	      try {
	        Object.defineProperty(this, 'defaultPrevented', {
	          get: function get() {
	            return true;
	          }
	        });
	      } catch (e) {
	        this.defaultPrevented = true;
	      }
	    };
	    return evt;
	  };
	
	  CustomEvent.prototype = window.Event.prototype;
	  window.CustomEvent = CustomEvent; // expose definition to window
	}
	
	!window.addEventListener && function (WindowPrototype, DocumentPrototype, ElementPrototype, addEventListener, removeEventListener, dispatchEvent, registry) {
	  WindowPrototype[addEventListener] = DocumentPrototype[addEventListener] = ElementPrototype[addEventListener] = function (type, listener) {
	    var target = this;
	
	    registry.unshift([target, type, listener, function (event) {
	      event.currentTarget = target;
	      event.preventDefault = function () {
	        event.returnValue = false;
	      };
	      event.stopPropagation = function () {
	        event.cancelBubble = true;
	      };
	      event.target = event.srcElement || target;
	
	      listener.call(target, event);
	    }]);
	
	    this.attachEvent("on" + type, registry[0][3]);
	  };
	
	  WindowPrototype[removeEventListener] = DocumentPrototype[removeEventListener] = ElementPrototype[removeEventListener] = function (type, listener) {
	    for (var index = 0, register; register = registry[index]; ++index) {
	      if (register[0] == this && register[1] == type && register[2] == listener) {
	        return this.detachEvent("on" + type, registry.splice(index, 1)[0][3]);
	      }
	    }
	  };
	
	  WindowPrototype[dispatchEvent] = DocumentPrototype[dispatchEvent] = ElementPrototype[dispatchEvent] = function (eventObject) {
	    return this.fireEvent("on" + eventObject.type, eventObject);
	  };
	}(Window.prototype, HTMLDocument.prototype, Element.prototype, "addEventListener", "removeEventListener", "dispatchEvent", []);
	
	// Шаги алгоритма ECMA-262, 5-е издание, 15.4.4.14
	// Ссылка (en): http://es5.github.io/#x15.4.4.14
	// Ссылка (ru): http://es5.javascript.ru/x15.4.html#x15.4.4.14
	if (!Array.prototype.indexOf) {
	  Array.prototype.indexOf = function (searchElement, fromIndex) {
	    var k;
	
	    // 1. Положим O равным результату вызова ToObject с передачей ему
	    //    значения this в качестве аргумента.
	    if (this == null) {
	      throw new TypeError('"this" is null or not defined');
	    }
	
	    var O = Object(this);
	
	    // 2. Положим lenValue равным результату вызова внутреннего метода Get
	    //    объекта O с аргументом "length".
	    // 3. Положим len равным ToUint32(lenValue).
	    var len = O.length >>> 0;
	
	    // 4. Если len равен 0, вернём -1.
	    if (len === 0) {
	      return -1;
	    }
	
	    // 5. Если был передан аргумент fromIndex, положим n равным
	    //    ToInteger(fromIndex); иначе положим n равным 0.
	    var n = +fromIndex || 0;
	
	    if (Math.abs(n) === Infinity) {
	      n = 0;
	    }
	
	    // 6. Если n >= len, вернём -1.
	    if (n >= len) {
	      return -1;
	    }
	
	    // 7. Если n >= 0, положим k равным n.
	    // 8. Иначе, n<0, положим k равным len - abs(n).
	    //    Если k меньше нуля 0, положим k равным 0.
	    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
	
	    // 9. Пока k < len, будем повторять
	    while (k < len) {
	      // a. Положим Pk равным ToString(k).
	      //   Это неявное преобразование для левостороннего операнда в операторе in
	      // b. Положим kPresent равным результату вызова внутреннего метода
	      //    HasProperty объекта O с аргументом Pk.
	      //   Этот шаг может быть объединён с шагом c
	      // c. Если kPresent равен true, выполним
	      //    i.  Положим elementK равным результату вызова внутреннего метода Get
	      //        объекта O с аргументом ToString(k).
	      //   ii.  Положим same равным результату применения
	      //        Алгоритма строгого сравнения на равенство между
	      //        searchElement и elementK.
	      //  iii.  Если same равен true, вернём k.
	      if (k in O && O[k] === searchElement) {
	        return k;
	      }
	      k++;
	    }
	    return -1;
	  };
	}
	
	if (!String.prototype.trim) {
	  (function () {
	    // Вырезаем BOM и неразрывный пробел
	    String.prototype.trim = function () {
	      return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	    };
	  })();
	}
	
	if (!Object.assign) {
	  Object.defineProperty(Object, 'assign', {
	    enumerable: false,
	    configurable: true,
	    writable: true,
	    value: function value(target, firstSource) {
	      'use strict';
	
	      if (target === undefined || target === null) {
	        throw new TypeError('Cannot convert first argument to object');
	      }
	
	      var to = Object(target);
	      for (var i = 1; i < arguments.length; i++) {
	        var nextSource = arguments[i];
	        if (nextSource === undefined || nextSource === null) {
	          continue;
	        }
	
	        var keysArray = Object.keys(Object(nextSource));
	        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
	          var nextKey = keysArray[nextIndex];
	          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
	          if (desc !== undefined && desc.enumerable) {
	            to[nextKey] = nextSource[nextKey];
	          }
	        }
	      }
	      return to;
	    }
	  });
	}
	
	// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
	if (!Object.keys) {
	  Object.keys = function () {
	    'use strict';
	
	    var hasOwnProperty = Object.prototype.hasOwnProperty,
	        hasDontEnumBug = !{ toString: null }.propertyIsEnumerable('toString'),
	        dontEnums = ['toString', 'toLocaleString', 'valueOf', 'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable', 'constructor'],
	        dontEnumsLength = dontEnums.length;
	
	    return function (obj) {
	      if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' && (typeof obj !== 'function' || obj === null)) {
	        throw new TypeError('Object.keys called on non-object');
	      }
	
	      var result = [],
	          prop,
	          i;
	
	      for (prop in obj) {
	        if (hasOwnProperty.call(obj, prop)) {
	          result.push(prop);
	        }
	      }
	
	      if (hasDontEnumBug) {
	        for (i = 0; i < dontEnumsLength; i++) {
	          if (hasOwnProperty.call(obj, dontEnums[i])) {
	            result.push(dontEnums[i]);
	          }
	        }
	      }
	      return result;
	    };
	  }();
	}

/***/ },

/***/ 16:
/***/ function(module, exports) {

	/* from https://gist.github.com/nuxodin/9250e56a3ce6c0446efa */
	
	function polyfill () {
	  var w = window
	  var d = w.document
	
	  if (w.onfocusin === undefined) {
	    d.addEventListener('focus', addPolyfill, true)
	    d.addEventListener('blur', addPolyfill, true)
	    d.addEventListener('focusin', removePolyfill, true)
	    d.addEventListener('focusout', removePolyfill, true)
	  }
	
	  function addPolyfill (e) {
	    var type = e.type === 'focus' ? 'focusin' : 'focusout'
	    var event = new window.CustomEvent(type, { bubbles: true, cancelable: false })
	    event.c1Generated = true
	    e.target.dispatchEvent(event)
	  }
	
	  function removePolyfill (e) {
	    if (!e.c1Generated) {
	      d.removeEventListener('focus', addPolyfill, true)
	      d.removeEventListener('blur', addPolyfill, true)
	      d.removeEventListener('focusin', removePolyfill, true)
	      d.removeEventListener('focusout', removePolyfill, true)
	    }
	    setTimeout(function () {
	      d.removeEventListener('focusin', removePolyfill, true)
	      d.removeEventListener('focusout', removePolyfill, true)
	    })
	  }
	}
	
	module.exports = {
	  polyfill: polyfill
	}


/***/ },

/***/ 26:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/home-page.html';
	var html = "\n<expo-slider>\n\t<div class=\"slide active\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку 1</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg\" alt=\"\">\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку 2</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/7/72/Wide_view_over_S%C3%B8rfjorden_from_the_coast_of_Sveingard,_2012_June.jpg\" alt=\"\">\n\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку 3</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"http://www.infrastructure.sa.gov.au/__data/assets/image/0003/75126/newell_highway_wide_centreline.JPG\" alt=\"\">\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку 4</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/7/72/Wide_view_over_S%C3%B8rfjorden_from_the_coast_of_Sveingard,_2012_June.jpg\" alt=\"\">\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку 5</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg\" alt=\"\">\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/7/72/Wide_view_over_S%C3%B8rfjorden_from_the_coast_of_Sveingard,_2012_June.jpg\" alt=\"\">\n\n\t</div>\n\t<div class=\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t</div>\n\t\t<img src=\"http://www.infrastructure.sa.gov.au/__data/assets/image/0003/75126/newell_highway_wide_centreline.JPG\" alt=\"\">\n\t</div>\n\t<div class=\t\"slide\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Откройте выставку</h1>\n\t\t\t<p>Для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку</p>\n\t\t\t<a href=\"#\" class=\"green button inline\">Подключить выставку на тест-драйв</a>\n\t\t</div>\n\t\t<img src=\"https://upload.wikimedia.org/wikipedia/commons/7/72/Wide_view_over_S%C3%B8rfjorden_from_the_coast_of_Sveingard,_2012_June.jpg\" alt=\"\">\n\t</div>\n</expo-slider>\n\n\n<div class=\"container\" ng-controller=\"offersCatalog\">\n\n\n\t<div id=\"testdrive-filters\" class=\"card\">\n\t\t<div class=\"mini-offset\">\n\t\t\t<div>Фильтры предложений</div>\n\t\t\t<br>\n\t\t\t<a href=\"#\" class=\"button inline green\">По выставкам</a>\n\t\t\t<a href=\"#\" class=\"button inline blue\">Выбрать тематики</a>\n\t\t\t<a href=\"#\" class=\"button inline outline blue\">\n\t\t\t\tНаука\n\t\t\t\t<span class=\"fa fa-times sup right pointer\"></span>\n\t\t\t</a>\n\t\t\t<a href=\"#\" class=\"button inline outline blue\">\n\t\t\t\tМетталургия\n\t\t\t\t<span class=\"fa fa-times sup right pointer\"></span>\n\t\t\t</a>\n\t\t</div>\n\t</div>\n\n\t<div class=\"card-list masonry\">\n\n\t\t<div id=\"create-testdrive\" class=\"card\">\n\t\t\t<div class=\"offset\">\n\t\t\t\t<img src=\"/icon/create-testdrive.png\" alt=\"\">\n\t\t\t\t<a href=\"\" class=\"button inline blue\">Создать предложение</a>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"card\" ng-repeat=\"n in [] | range:10\">\n\t\t\t<div class=\"offset\">\n\t\t\t\t<img src=\"/icon/create-testdrive.png\" alt=\"\">\n\t\t\t\t<a href=\"\" class=\"button inline blue\">Создать предложение</a>\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\n\n</div>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ },

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($scope) {};

/***/ }

})
//# sourceMappingURL=1.7a3e89aa8598f287717e.hot-update.js.map