webpackHotUpdate(1,{

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loadMap = __webpack_require__(110);
	var qwery = __webpack_require__(90);
	
	var setValue = function setValue(input, value) {
	
		input.value = ngModel.$modelValue;
	
		var changeEvent = new Event('change');
		input.dispatchEvent(changeEvent);
		var inputEvent = new Event('input');
		input.dispatchEvent(inputEvent);
	};
	
	var changeProvider = function changeProvider(element, current, prev) {
	
		if (current === prev) {
			return current;
		}
	
		console.log(current, prev);
	
		return current;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function link($scope, $element, $attrs, ngModel) {
				var selectorElement = $element[0];
	
				if (ngModel && ngModel.$modelValue) {
					setValue(selectorElement, ngModel.$modelValue);
				}
	
				var current = changeProvider(selectorElement, $attrs.provider || 'yandex');
	
				$attrs.$observe('provider', function (next) {
					current = changeProvider(selectorElement, next, current);
				});
	
				loadMap(function () {
					console.log('place selector isLoaded');
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.89fccb5751fd0eebf69f.hot-update.js.map