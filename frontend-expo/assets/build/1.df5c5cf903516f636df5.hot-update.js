webpackHotUpdate(1,{

/***/ 55:
/***/ function(module, exports) {

	'use strict';
	
	var subjects = ['Авиакосмическая промышленность', 'Автомобили и мотоциклы', 'Анализ, измерение и контроль', 'Безопасность', 'Бизнес, инвестиции, финансы', 'Вино, алкоголь, табак', 'Городское хозяйство', 'Гостиничное, ресторанное дело', 'Детские товары и игрушки', 'Животные. Ветеринария', 'ИТ, коммуникация, связь', 'ИТ: Интернет маркетинг', 'ИТ: Интернет-технологии', 'Катера, яхты, судостроение', 'Коcметика и парфюмерия', 'Культура, исскуство, церковь'].map(function (name, id) {
		var category = name[0].toUpperCase();
		return {
			name: name, id: id, category: category, selected: false
		};
	}).reduce(function (result, currentItem) {
		var category = currentItem.category;
	
		if (!(category in result)) {
			result[category] = [];
		}
		result[category].push(currentItem);
		return result;
	});
	
	console.log(subjects);
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		$scope.subjects = subjects;
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
			$modals.close($scope.selected);
		};
	}];

/***/ }

})
//# sourceMappingURL=1.df5c5cf903516f636df5.hot-update.js.map