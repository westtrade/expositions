webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	__webpack_require__(232);
	__webpack_require__(235);
	
	__webpack_require__(245);
	__webpack_require__(246);
	__webpack_require__(247);
	__webpack_require__(260);
	
	var content_style = '\nbody {\n\tfont-family: "Roboto", sans-serif;\n}\n';
	
	module.exports = function () {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
					},
	
					content_style: content_style,
					// height: 500,
					menubar: false,
					skin: false,
					// inline: true,
					plugins: ['paste', 'link', 'autoresize', 'textpattern']
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.aa80c59123f136f7ffb4.hot-update.js.map