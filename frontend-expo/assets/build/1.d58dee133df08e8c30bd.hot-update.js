webpackHotUpdate(1,{

/***/ 16:
/***/ function(module, exports) {

	'use strict';
	
	if (!("previousElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "previousElementSibling", {
	        get: function get() {
	            var e = this.previousSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.previousSibling;
	            }return e;
	        }
	    });
	}
	
	// Source: https://github.com/Alhadis/Snippets/blob/master/js/polyfills/IE8-child-elements.js
	if (!("nextElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "nextElementSibling", {
	        get: function get() {
	            var e = this.nextSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.nextSibling;
	            }return e;
	        }
	    });
	}
	
	if (!("firstElementChild" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "firstElementChild", {
	        get: function get() {
	            for (var nodes = this.children, n, i = 0, l = nodes.length; i < l; ++i) {
	                if (n = nodes[i], 1 === n.nodeType) return n;
	            }return null;
	        }
	    });
	}
	
	// Source: https://github.com/Alhadis/Snippets/blob/master/js/polyfills/IE8-child-elements.js
	if (!("lastElementChild" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "lastElementChild", {
	        get: function get() {
	            for (var nodes = this.children, n, i = nodes.length - 1; i >= 0; --i) {
	                if (n = nodes[i], 1 === n.nodeType) return n;
	            }return null;
	        }
	    });
	}
	
	(function () {
	
	    if (typeof window.CustomEvent === "function") return false;
	
	    function CustomEvent(event, params) {
	        params = params || { bubbles: false, cancelable: false, detail: undefined };
	        var evt = document.createEvent('CustomEvent');
	        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
	        return evt;
	    }
	
	    CustomEvent.prototype = window.Event.prototype;
	
	    window.CustomEvent = CustomEvent;
	})();

/***/ }

})
//# sourceMappingURL=1.d58dee133df08e8c30bd.hot-update.js.map