webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	module.exports = function () {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			scope: {
				error: '@error'
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.c3fc6fb3741c8d93ab7a.hot-update.js.map