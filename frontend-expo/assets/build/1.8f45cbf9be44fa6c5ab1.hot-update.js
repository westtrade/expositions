webpackHotUpdate(1,{

/***/ 16:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var uiRouter = __webpack_require__(19);
	
	__webpack_require__(20);
	var loadingBar = __webpack_require__(21);
	var ngAnimate = __webpack_require__(23);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, 'chieffancypants.loadingBar']);
	var router = __webpack_require__(25);
	app.config(router);
	
	var topBar = __webpack_require__(31);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(33);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(43);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(46);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var button = __webpack_require__(47);
	app.directive('button', button);
	
	var offersCatalog = __webpack_require__(48);
	app.controller('offersCatalog', ['$scope', '$element', 'cfpLoadingBar', offersCatalog]);
	
	// /auth/login?type=facebook
	app.directive('socialLogin', function () {
		return {
			restrict: 'ACE',
			link: function link($scope, $element, $attrs) {
				console.log($attrs);
			}
		};
	});
	
	module.exports = app;

/***/ },

/***/ 28:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/registration-page.html';
	var html = "<form class=\"card center\">\n\t<div class=\"offset\">\n\t\t<div class=\"field text-center bigger\">\n\t\t\tЕсть аккаунт? <a href=\"#\" ui-sref=\"user-login\" >Войти?</a>\n\t\t</div>\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\t\t<h1>Регистрация</h1>\n\t\t<input type=\"text\" placeholder=\"Фамилия Имя Отчество\">\n\t\t<input type=\"email\" placeholder=\"Email\">\n\t\t<input type=\"text\" placeholder=\"Номер телефона\">\n\t\t<input type=\"password\" placeholder=\"Пароль\">\n\t\t<input type=\"password\" placeholder=\"Пароль еще раз\">\n\n\n\t\t<input type=\"password\" class=\"half\" placeholder=\"Код с картинки\">\n\t\t<img class=\"captcha\" ng-src=\"/common/captcha?seed={{randomSeed}}\" src=\"/common/captcha\" ng-click=\"changeCaptcha()\" alt=\"\">\n\t\t<a href=\"#\" ng-click=\"changeCaptcha()\" class=\"circle-button\">\n\t\t\t<i class=\"fa fa-refresh\"></i>\n\t\t</a>\n\n\t\t<br>\n\n\t\t<div class=\"checkbox\">\n\t\t\t<input type=\"checkbox\" name=\"subscribe\" id=\"field-subscribe\" styled-checkbox class=\"styled-checkbox\">\n\t\t\t<label for=\"field-subscribe\">Я хочу получать информационные сообщения от ExpoTestDrive</label>\n\t\t</div>\n\t\t<br>\n\n\n\t\t<button class=\"primary\" type=\"submit\">Регистрация</button>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"field\">\n\t\t\tРегистрируясь, я подтверждаю свое согласие с условиями <a href=\"#\" ui-sref=\"content-agreement\">Лицензионного соглашения</a>\n\t\t</div>\n\n\t\t<div class=\"separator\">\n\t\t\t<div class=\"label\">или</div>\n\t\t</div>\n\t\t<div class=\"two-buttons\">\n\t\t\t<a class=\"fb button\" social-login=\"facebook\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-facebook fa-fw\"></i>\n\t\t\t</a><a class=\"vk button\" social-login=\"vkontakte\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-vk fa-fw\"></i>\n\t\t\t</a>\n\t\t</div>\n\n\t</div>\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.8f45cbf9be44fa6c5ab1.hot-update.js.map