webpackHotUpdate(1,{

/***/ 49:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	
	var subjects = ['Авиакосмическая промышленность', 'Автомобили и мотоциклы', 'Анализ, измерение и контроль', 'Безопасность', 'Бизнес, инвестиции, финансы', 'Вино, алкоголь, табак', 'Городское хозяйство', 'Гостиничное, ресторанное дело', 'Детские товары и игрушки', 'Животные. Ветеринария', 'ИТ, коммуникация, связь', 'ИТ: Интернет маркетинг', 'ИТ: Интернет-технологии', 'Катера, яхты, судостроение', 'Коcметика и парфюмерия', 'Культура, исскуство, церковь'].map(function (name, id) {
		var category = name[0].toUpperCase();
		return {
			name: name, id: id, category: category, selected: false
		};
	});
	
	var safeapply = __webpack_require__(44);
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		var selected = $scope.initial.map(function (item) {
			return item.id;
		});
	
		$scope.subjects = subjects.map(function (item) {
			item.selected = selected.indexOf(item.id) >= 0;
			return item;
		}).reduce(function (result, currentItem) {
			var category = currentItem.category;
	
			if (!(category in result)) {
				result[category] = [];
			}
			result[category].push(currentItem);
			return result;
		}, {});
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
	
			var selected = Object.values($scope.subjects).reduce(function (result, items) {
				return result.concat(items.filter(function (item) {
					return item.selected;
				})).map(function (item) {
					return angular.copy(item);
				});
			}, []);
	
			console.log(selected);
	
			$modals.close(selected);
		};
	}];

/***/ }

})
//# sourceMappingURL=1.7d726c2d641201af003b.hot-update.js.map