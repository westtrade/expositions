webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	var debugPage = __webpack_require__(62);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var home = __webpack_require__(70);
	var notFound = __webpack_require__(72);
	var offers = __webpack_require__(73);
	var auth = __webpack_require__(75);
	var account = __webpack_require__(116);
	
	var loader = function loader(prefix) {
		return function ($stateProvider, _ref) {
			var _ref2 = _slicedToArray(_ref, 2),
			    method = _ref2[0],
			    info = _ref2[1];
	
			console.log(method, info);
			$stateProvider.state(prefix + '-' + method, info);
			return $stateProvider;
		};
	};
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', home).state('404', notFound).state('user-login', auth.login).state('user-registration', auth.registration).state('user-recovery', auth.recovery);
	
		$stateProvider.state('change-password', account.changePassword).state('user-offers', account.offersCatalog).state('user-events', account.eventsCatalog).state('user-account', account.accountPage).state('user-cart', account.cart);
	
		Object.entries(offers).reduce(loader('offer'), $stateProvider);
	
		$stateProvider.state('user-logout', {
			url: '/user/logout',
			controller: ['$scope', '$state', 'User', function ($scope, $state, User) {
				User.logout().catch(function (e) {
					return console.error(e);
				}).then(function () {
					$state.go('home');
				});
			}]
		});
	
		$stateProvider.state('dev-debug', {
			url: '/dev/debug',
			templateUrl: debugPage,
			controller: ['$scope', 'User', '$timeout', function ($scope, User, $timeout) {
	
				User.bind($scope);
				$scope.selectedUser = 1;
				$scope.changeUser = function ($event) {
					User.setState($scope.users[$scope.selectedUser]);
				};
				$scope.$watch('user.notificationsCount', function (current, prev) {
					if (current !== prev && typeof current === 'number') {
						User.setState('notificationsCount', current);
					}
				});
	
				$scope.users = [{
					displayName: 'Тумаков Олег Н',
					role: 'guest'
				}, {
					displayName: 'Эрнест Днепропетровский А',
					role: 'exponent',
					notificationsCount: 5,
					id: '56faabde3a1a8cd34bae9584'
				}, {
					displayName: 'Артем Вячеславович Е.',
					role: 'organizer'
				}, {
					displayName: 'Модератор',
					role: 'moderator'
				}, {
					displayName: 'Администратор',
					role: 'administrator'
				}];
	
				$timeout(function () {
					safeApply($scope, function () {
						$scope.users.push({
							displayName: 'Azaza',
							role: 'administrator'
						});
					});
				}, 1300);
			}]
	
		});
	
		$stateProvider.state('dev-styles', {
			url: '/dev/styles',
			templateUrl: debugPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.4363702221c08c6c85e9.hot-update.js.map