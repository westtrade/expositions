webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var safeapply = __webpack_require__(44);
	
	var MODAL_MODES = ['catalog', 'filters'];
	var subjects = __webpack_require__(57);
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		var selected = $scope.initial.subjects.map(function (item) {
			return item.id;
		});
	
		$scope.subjects = subjects.map(function (item) {
			item.selected = selected.indexOf(item.id) >= 0;
			return item;
		}).reduce(function (result, currentItem) {
			var category = currentItem.category;
	
			if (!(category in result)) {
				result[category] = [];
			}
			result[category].push(currentItem);
			return result;
		}, {});
	
		console.log($scope.initial);
	
		$scope.mode = MODAL_MODES[0];
	
		$scope.switchMode = function (mode) {
	
			var modeId = MODAL_MODES.indexOf(mode);
	
			if (modeId < 0) {
				modeId === 0;
			}
	
			$scope.mode = MODAL_MODES[modeId];
		};
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
	
			// const selected = Object.values($scope.subjects).reduce((result, items) =>  result
			// 	.concat(items.filter(item => item.selected))
			// 	.map(item => angular.copy(item)), []
			// );
			//
			$modals.close();
		};
	}];

/***/ }

})
//# sourceMappingURL=1.5f51ff5c071c8804abdd.hot-update.js.map