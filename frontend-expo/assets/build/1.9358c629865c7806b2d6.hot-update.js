webpackHotUpdate(1,{

/***/ 37:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.9358c629865c7806b2d6.hot-update.js.map