webpackHotUpdate(1,{

/***/ 67:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
	
		full_name: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		email: {
			presence: true,
			email: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		phone: {
			presence: true,
			format: {
				pattern: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/g
			},
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		password: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		repeat: {
			presence: true,
			equality: {
				attribute: 'password',
				message: 'is not equal to Password field'
			}
		},
	
		company_name: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		captcha: {
			presence: true,
			captcha: {
				resolver: function resolver() {}
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.bfb830c36f6e65e08832.hot-update.js.map