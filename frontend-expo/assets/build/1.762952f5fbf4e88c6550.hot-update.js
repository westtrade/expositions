webpackHotUpdate(1,{

/***/ 95:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(89),
	    getOptions = _require.getOptions;
	
	var _require2 = __webpack_require__(92),
	    attributesToString = _require2.attributesToString,
	    makeHtml = _require2.makeHtml,
	    copyAttributes = _require2.copyAttributes;
	
	var qwery = __webpack_require__(93);
	var compileElement = __webpack_require__(94);
	
	var toggleCheckbox = function toggleCheckbox(event, element, styledCheckbox) {
	
		if (element.disabled) {
			return;
		}
	
		switch (true) {
			case event.target === element:
	
				break;
			default:
				element.checked = !element.checked;
	
				var changeEvent = new CustomEvent('change');
				element.dispatchEvent(changeEvent);
	
				event.preventDefault();
				event.stopPropagation();
	
				break;
		}
	
		element.focus();
	};
	
	module.exports = function (element) {
		var styleOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
		styleOptions = getOptions(styleOptions);
	
		var elementTemplate = '<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>';
		var styledCheckbox = compileElement(elementTemplate, element, styleOptions);
	
		if (element.disabled) {
			return false;
		}
	
		element.style.position = 'absolute';
		element.style.zIndex = '-1';
		element.style.opacity = 0;
		element.style.margin = 0;
		element.style.padding = 0;
	
		styledCheckbox.setAttribute('unselectable', 'on');
		styledCheckbox.style['-webkit-user-select'] = 'none';
		styledCheckbox.style['-moz-user-select'] = 'none';
		styledCheckbox.style['-ms-user-select'] = 'none';
		styledCheckbox.style['-o-user-select'] = 'none';
		styledCheckbox.style['user-select'] = 'none';
		styledCheckbox.style.display = 'inline-block';
		styledCheckbox.style.position = 'relative';
		styledCheckbox.style.overflow = 'hidden';
	
		if (element.checked) {
			styledCheckbox.classList.add('checked');
		}
	
		styledCheckbox.addEventListener('click', function (event) {
			toggleCheckbox(event, element, styledCheckbox);
			return false;
		});
	
		element.addEventListener('change', function () {
			styledCheckbox.classList.toggle('checked', element.checked);
		});
	
		element.addEventListener('focus', function () {
			if (!element.disabled) {
				styledCheckbox.classList.add('focused');
			}
		});
	
		element.addEventListener('blur', function () {
			styledCheckbox.classList.remove('focused');
		});
	};

/***/ }

})
//# sourceMappingURL=1.762952f5fbf4e88c6550.hot-update.js.map