webpackHotUpdate(1,{

/***/ 15:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loginPage = __webpack_require__(16);
	var registrationPage = __webpack_require__(17);
	
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', function ($scope) {
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					$scope.randomSeed = guid();
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.2c7add4ec11ef1c7b92b.hot-update.js.map