webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	module.exports = function () {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			scope: {
				error: '@error'
			},
			link: function link($scope, $element, $attrs) {
				console.log($attrs);
	
				$scope.$$watch('error', function (prev, next) {
					console.log(prev, next);
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.c7f94a9e45f6e96f8abe.hot-update.js.map