webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($scope, $element, cfpLoadingBar) {
	
		console.log($element);
	
		// document.addEventListener('scroll', (event) => {
		// })
	
		$scope.filter = {
			page: 0
		};
	
		$scope.flow = false;
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
			cfpLoadingBar.start();
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				catalog.forEach(function (item) {
					console.log(item);
				});
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
	
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	};

/***/ }

})
//# sourceMappingURL=1.2c1db0aac84fa2c7dafd.hot-update.js.map