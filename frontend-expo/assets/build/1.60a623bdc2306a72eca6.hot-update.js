webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(45);
	var templateUrl = __webpack_require__(46);
	var controller = ['$scope', function ($scope) {}];
	
	var qwery = __webpack_require__(40);
	
	var SliderTimer = function () {
		function SliderTimer() {
			_classCallCheck(this, SliderTimer);
	
			this.currentTimer = null;
			this.$scope = null;
			this.timeout = 500;
	
			this.isStopeed = true;
		}
	
		_createClass(SliderTimer, [{
			key: 'run',
			value: function run($scope) {
				var _this = this;
	
				var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
	
	
				this.$scope = $scope;
				this.timeout = timeout;
	
				this.isStopeed = false;
	
				this.currentTimer = setTimeout(function ($scope, currentTimerId, timeout) {
					$scope.next(true);
					clearTimeout(currentTimerId);
					_this.currentTimer = null;
					_this.run($scope, timeout);
				}, timeout, this.$scope, this.currentTimer, timeout);
			}
		}, {
			key: 'pause',
			value: function pause() {
				if (this.currentTimer) {
					clearTimeout(this.currentTimer);
					this.currentTimer = null;
					this.isStopeed = true;
				}
			}
		}, {
			key: 'resume',
			value: function resume() {
				if (this.$scope && this.isStopeed) {
					clearTimeout(this.currentTimer);
					this.currentTimer = null;
					this.run(this.$scope, this.timeout);
				}
			}
		}]);
	
		return SliderTimer;
	}();
	
	;
	
	var sliderTimer = new SliderTimer();
	
	module.exports = ['$rootScope', function ($rootScope) {
	
		var initSlider = function initSlider($scope) {
			var sliderContent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	
	
			var $slider = {
	
				timeout: 4000,
				total: 0,
				acive: 0,
				container: null,
				scope: null,
	
				pause: function pause() {
					sliderTimer.pause();
				},
				resume: function resume() {
					sliderTimer.resume();
				},
				run: function run(args) {
					sliderTimer.run($slider.scope, $slider.timeout);
				},
				setActive: function setActive(active) {
					var isAutomatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
	
					if (!isAutomatic) {
						$slider.pause();
					}
	
					safeApply($slider.scope, function () {
						$scope.active = active;
	
						qwery('.slide', $element[0]).forEach(function (item, idx) {
							if (item.classList.contains('active')) {
								item.classList.remove('active');
							}
	
							if (idx === active) {
								item.classList.add('active');
							}
						});
	
						if (!isAutomatic) {
							sliderTimer.resume();
						}
					});
				},
				next: function next() {
					var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
					var current = parseInt($slider.active);
					var next = current + 1;
					current = next >= $slider.total ? 0 : next;
					$slider.setActive(current, isAutomatic);
				},
				prev: function prev() {
					var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
					var current = parseInt($slider.active);
					var prev = current - 1;
					current = prev < 0 ? $scope.total - 1 : prev;
					$slider.setActive(current, isAutomatic);
				},
				setContent: function setContent(sliderContent) {
	
					var itemsCount = 0;
					var initialActive = 0;
	
					angular.forEach(sliderContent, function (current, key) {
						if (current.classList && current.classList.contains('slide')) {
							itemsCount++;
							if (current.classList.contains('active')) {
								initialActive = parseInt(itemsCount - 1);
								console.log(initialActive);
							}
						}
					});
	
					$slider.total = itemsCount;
					$slider.active = 0;
	
					$slider.setActive(initialActive);
				}
			};
	
			$slider.scope = $scope;
			$slider.setContent(sliderContent);
			$scope.$slider = $slider;
	
			return $slider;
		};
	
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: function link($scope, $element, attrs, ctrl, $transclude) {
	
				var element = $element[0];
				element.addEventListener('mouseenter', function (event) {
					if (['arrow', 'button'].filter(function (name) {
						return event.target.classList.contains(name);
					}).length) {
						$slider.pause();
					}
				}, true);
	
				element.addEventListener('mouseleave', function (event) {
					$slider.resume();
				}, true);
	
				$rootScope.$on('modal-toggled', function (event, modalName, isOpened) {
					return isOpened ? $slider.pause() : $slider.resume();
				});
	
				$transclude($scope, function (sliderContent) {
					$slider = initSlider($scope, sliderContent);
					$slider.run();
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.60a623bdc2306a72eca6.hot-update.js.map