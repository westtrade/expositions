webpackHotUpdate(1,{

/***/ 77:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeapply = __webpack_require__(73);
	var angular = __webpack_require__(39);
	
	module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', function ($scope, $element, cfpLoadingBar, $modals) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0,
			subjects: [],
			expositions: [
				// { name: 'Test', id: 1}
			]
		};
	
		$scope.selectSubjects = function () {
	
			var data = {
				initial: $scope.filter.subjects
			};
	
			$modals.open('subject-selector', data).then(function () {
				var subjects = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				console.log(subjects);
				safeApply(function () {
					$scope.filter.subjects = subjects;
				});
			});
		};
	
		$scope.selectExpositions = function () {
	
			var data = {
				initial: $scope.filter
			};
	
			$modals.open('exposition-selector', data).then(function () {
				var expositionsList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
			});
		};
	
		$scope.removeFilter = function (removed, filterType) {
			var filterTypeExists = filterType in $scope.filter;
	
			if (!filterTypeExists) {
				throw new Error('Wrong type of filter (filterType argumnt). It must be in allowed list of types: '.Object.keys($scope.filter).join(', '));
			}
	
			$scope.filter[filterType] = $scope.filter[filterType].filter(function (current) {
				return removed.id !== current.id;
			});
		};
	
		$scope.catalog = [];
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// console.log(catalog[1]);
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
				$scope.loading = false;
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		document.addEventListener('scroll', function (event) {
			// const contentIsScrolled = element.scrollHeight - document.body.scrollTop >= element.clientHeight;
			var contentIsScrolled = element.scrollHeight - document.body.scrollTop - 400 <= 0; //todo fix this bug
	
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		});
	}];

/***/ }

})
//# sourceMappingURL=1.8c44b2a6f85fcede77d2.hot-update.js.map