webpackHotUpdate(1,{

/***/ 60:
/***/ function(module, exports) {

	'use strict';
	
	if (!("previousElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "previousElementSibling", {
	        get: function get() {
	            var e = this.previousSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.previousSibling;
	            }return e;
	        }
	    });
	}
	
	// Source: https://github.com/Alhadis/Snippets/blob/master/js/polyfills/IE8-child-elements.js
	if (!("nextElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "nextElementSibling", {
	        get: function get() {
	            var e = this.nextSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.nextSibling;
	            }return e;
	        }
	    });
	}

/***/ }

})
//# sourceMappingURL=1.1cf4551d1f3ab762ea2e.hot-update.js.map