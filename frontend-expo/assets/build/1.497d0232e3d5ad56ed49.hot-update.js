webpackHotUpdate(1,{

/***/ 105:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var angular = __webpack_require__(44);
	
	var ROLES = ['exponent', 'organizer', 'moderator', 'administrator'];
	
	var RULES = {};
	
	var ANONYMOUS_USER = {
		role: 'exponent'
	};
	
	var User = function () {
		function User(initialState) {
			_classCallCheck(this, User);
	
			this.state = {};
			this.setState(ANONYMOUS_USER);
		}
	
		_createClass(User, [{
			key: 'logout',
			value: function logout() {
				var _this = this;
	
				return new Promise(function (resolve, reject) {
					_this.setState(ANONYMOUS_USER);
					resolve();
				});
			}
		}, {
			key: 'setState',
			value: function setState() {
				var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
				this.state = angular.extend({}, this.state, data);
			}
		}, {
			key: 'getState',
			value: function getState() {
				return angular.extend({}, this.state);
			}
		}, {
			key: 'hasAccess',
			value: function hasAccess(ruleId) {
	
				if (ruleId) {}
			}
		}]);
	
		return User;
	}();
	
	var currentUser = new User();
	
	module.exports = function () {
		return currentUser;
	};

/***/ }

})
//# sourceMappingURL=1.497d0232e3d5ad56ed49.hot-update.js.map