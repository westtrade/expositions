webpackHotUpdate(1,{

/***/ 105:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		full_name: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
		email: {
			presence: true,
			email: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
		phone: {
			presence: true,
			format: {
				pattern: /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
			},
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
	
		password: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.5e58c77ea0ed824b4b4c.hot-update.js.map