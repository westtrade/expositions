webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var Gmaps = __webpack_require__(101);
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var gmapIsInitialized = false;
	
	var getMap = function getMap() {
		var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	
		if (gmapIsInitialized) {
			var map = new Gmaps(params);
			return Promise.resolve(map);
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=' + randomCallbackName;
	
		var result = new Promise(function (resolve) {
	
			global[randomCallbackName] = function () {
				gmapIsInitialized = true;
				delete global[randomCallbackName];
				var map = new Gmaps(params);
				resolve(map);
			};
		});
	
		document.body.appendChild(script);
		return result;
	};
	
	module.exports = {
		getMap: getMap
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.90d83d47b36175c86a54.hot-update.js.map