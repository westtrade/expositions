webpackHotUpdate(1,{

/***/ 271:
/***/ function(module, exports) {

	'use strict';
	
	var createAppend = function createAppend(element) {
	
		var appendElement = document.createElement('div');
	
		appendElement.classList.add('append');
	
		if (element.nextElementSibling) {
			element.parentNode.insertBefore(appendElement, element.nextElementSibling);
		} else {
			element.appendChild(appendElement);
		}
	
		return appendElement;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			link: function link($scope, $element, $attrs) {
				var _$attrs$letterCounter = $attrs.letterCounter,
				    letterCounter = _$attrs$letterCounter === undefined ? -1 : _$attrs$letterCounter;
	
				var element = $element[0];
	
				var appendElement = element.nextElementSibling && element.nextElementSibling.classList.contains('append') ? element.nextElementSibling : createAppend(element);
	
				letterCounter = parseInt(letterCounter);
				console.log(letterCounter);
	
				if (letterCounter > 0) {
					appendElement.innerHTML = element.value.length + ' / ' + letterCounter;
				}
	
				element.addEventListener('keypress', function (event) {
					// element.addEventListener('input', (event) => {
					appendElement.innerHTML = element.value.length + ' / ' + letterCounter;
	
					if (element.value.length >= letterCounter) {
						event.preventDefault();
					}
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.ec6b8b50b96deb01929e.hot-update.js.map