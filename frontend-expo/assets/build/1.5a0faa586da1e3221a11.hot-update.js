webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	var calculateHeight = function calculateHeight(element) {
		console.log(element.scrollHeight);
	};
	
	module.exports = function ($scope, $element, cfpLoadingBar) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0
		};
	
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				calculateHeight(element);
	
				cfpLoadingBar.complete();
				$scope.loading = false;
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		document.addEventListener('scroll', function (event) {
			var contentIsScrolled = element.scrollHeight - document.body.scrollTop === element.clientHeight;
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				console.log('load');
				$scope.loadMore();
			};
		});
	};

/***/ }

})
//# sourceMappingURL=1.5a0faa586da1e3221a11.hot-update.js.map