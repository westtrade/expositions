webpackHotUpdate(1,{

/***/ 73:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var itemTemplate = __webpack_require__(74);
	var createFormTemplates = __webpack_require__(117);
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(60);
	
	var create = {
		url: '/offer/create'
	
	};
	
	var index = {
		url: '/offer/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			User.bind($scope);
	
			$scope.offer = {};
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = { index: index, create: create, update: update };

/***/ },

/***/ 117:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/create-offer.html';
	var html = "";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.824f10b18a294a60e06a.hot-update.js.map