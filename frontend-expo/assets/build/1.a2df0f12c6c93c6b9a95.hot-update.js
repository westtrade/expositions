webpackHotUpdate(1,{

/***/ 16:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var uiRouter = __webpack_require__(19);
	
	__webpack_require__(20);
	var loadingBar = __webpack_require__(21);
	var ngAnimate = __webpack_require__(23);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, 'chieffancypants.loadingBar']);
	var router = __webpack_require__(25);
	app.config(router);
	
	var topBar = __webpack_require__(31);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(33);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(43);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(46);
	Object.entries(common).reduce(function (app, _ref) {
	  var _ref2 = _slicedToArray(_ref, 2),
	      name = _ref2[0],
	      filter = _ref2[1];
	
	  return app.filter(name, filter);
	}, app);
	
	var button = __webpack_require__(47);
	app.directive('button', button);
	
	var offersCatalog = __webpack_require__(48);
	app.controller('offersCatalog', ['$scope', '$element', 'cfpLoadingBar', offersCatalog]);
	
	function PopupCenter(url, title, w, h) {
	  // Fixes dual-screen position                         Most browsers      Firefox
	  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	  var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
	
	  var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	  var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
	
	  var left = width / 2 - w / 2 + dualScreenLeft;
	  var top = height / 2 - h / 2 + dualScreenTop;
	  var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
	
	  // Puts focus on the newWindow
	  if (window.focus) {
	    newWindow.focus();
	  }
	}
	
	// /auth/login?type=facebook
	app.directive('socialLogin', function () {
	  return {
	    restrict: 'ACE',
	    link: function link($scope, $element, $attrs) {
	      // console.log($attrs.socialLogin);
	
	
	      var element = $element[0];
	      element.addEventListener('click', function (event) {
	        PopupCenter('/auth/login?type=' + $attrs.socialLogin, $attrs.socialLogin, 800, 600);
	        event.preventDefault();
	      });
	    }
	  };
	});
	
	module.exports = app;

/***/ }

})
//# sourceMappingURL=1.a2df0f12c6c93c6b9a95.hot-update.js.map