webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	var THEME = 'lightgray';
	__webpack_require__(232)("./" + THEME + '/content.min.css');
	__webpack_require__(236)("./" + THEME + '/skin.min.css');
	
	__webpack_require__(270);
	
	__webpack_require__(247);
	__webpack_require__(248);
	__webpack_require__(249);
	__webpack_require__(250);
	__webpack_require__(251);
	__webpack_require__(252);
	__webpack_require__(252);
	__webpack_require__(253);
	__webpack_require__(254);
	__webpack_require__(255);
	__webpack_require__(256);
	__webpack_require__(257);
	
	var content_style = '\n\nbody, html {\n\tpadding: 0;\n\tmargin: 0;\n\tfont-family: "Roboto", sans-serif;\n}\n\nbody {\n\t    padding: 18px;\n\t\tcolor: #3d3d3d;\n}\n\np {\n\tborder-bottom: margin-bottom: 5px;\n}\n';
	
	module.exports = function () {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
					},
	
					visual: false,
					statusbar: false,
					content_style: content_style,
					// height: 500,
					menubar: false,
					skin: false,
					// inline: true,
					plugins: 'paste link autoresize textpattern advlist autolink link image lists charmap print preview',
					textpattern_patterns: [{ start: '*', end: '*', format: 'italic' }, { start: '**', end: '**', format: 'bold' }, { start: '#', format: 'h1' }, { start: '##', format: 'h2' }, { start: '###', format: 'h3' }, { start: '####', format: 'h4' }, { start: '#####', format: 'h5' }, { start: '######', format: 'h6' }, { start: '1. ', cmd: 'InsertOrderedList' }, { start: '* ', cmd: 'InsertUnorderedList' }, { start: '- ', cmd: 'InsertUnorderedList' }]
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.c027898a51c0ac938f53.hot-update.js.map