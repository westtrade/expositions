webpackHotUpdate(1,{

/***/ 36:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(37);
	var uiRouter = __webpack_require__(39);
	var uiDND = __webpack_require__(40);
	
	var dropzone = __webpack_require__(42);
	var ngDropzone = __webpack_require__(44);
	
	__webpack_require__(45);
	__webpack_require__(46);
	__webpack_require__(47);
	
	var loadingBar = __webpack_require__(48);
	var ngAnimate = __webpack_require__(50);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'chieffancypants.loadingBar', 'thatisuday.dropzone']);
	
	var router = __webpack_require__(52);
	app.config(router);
	
	var common = __webpack_require__(76);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(77);
	
	app.directive('button', __webpack_require__(87)).directive('expoSlider', __webpack_require__(88)).directive('topBar', __webpack_require__(90)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(92)).directive('masonryLayout', __webpack_require__(93)).directive('expoModalManager', __webpack_require__(96)).directive('modalSource', __webpack_require__(98)).directive('expoTabs', __webpack_require__(99)).directive('placeSelector', __webpack_require__(100)).directive('fieldError', __webpack_require__(103)).directive('placeSelector', __webpack_require__(100));
	
	app.controller('offersCatalog', __webpack_require__(105)).controller('subjectSelectorModal', __webpack_require__(107)).controller('sliderController', __webpack_require__(109)).controller('expositionSelectorModal', __webpack_require__(110)).controller('sliderEditorModal', __webpack_require__(111)).controller('offerTeaserController', __webpack_require__(112));
	
	app.factory('$modals', __webpack_require__(113)).factory('User', __webpack_require__(114));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;

/***/ }

})
//# sourceMappingURL=1.3266d6ef39fa0c60a286.hot-update.js.map