webpackHotUpdate(1,{

/***/ 19:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var homePage = __webpack_require__(38);
	var loginPage = __webpack_require__(20);
	var registrationPage = __webpack_require__(21);
	var recoveryPage = __webpack_require__(35);
	var changePasswordPage = __webpack_require__(36);
	
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/',
			templateUrl: homePage
	
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', function ($scope) {
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					$scope.randomSeed = guid();
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', function ($scope) {
				$scope.email = null;
				$scope.data = {
					email: null
				};
	
				$scope.setMail = function () {
					$scope.email = $scope.data.email;
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	};
	
	module.exports = routes;

/***/ },

/***/ 38:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/home-page.html';
	var html = "<form class=\"card center\">\n\t<div class=\"offset\">\n\t\t<h1>Вход</h1>\n\t\t<input type=\"text\" placeholder=\"Введите номер телефона или email\">\n\t\t<input type=\"password\" placeholder=\"Введите пароль\">\n\t\t<div class=\"field\">\n\t\t\t<a href=\"#\" ui-sref=\"user-recovery\" >Забыли пароль?</a>\n\t\t</div>\n\t\t<button class=\"primary\" type=\"submit\">Войти</button>\n\t\t<div class=\"separator\">\n\t\t\t<div class=\"label\">или</div>\n\t\t</div>\n\n\t\t<div class=\"two-buttons\">\n\t\t\t<a class=\"fb button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-facebook fa-fw\"></i>\n\t\t\t</a><a class=\"vk button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-vk fa-fw\"></i>\n\t\t\t</a>\n\t\t</div>\n\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\t\t<div class=\"field bigger text-center\">\n\t\t\tНет аккаунта? <a href=\"#\" ui-sref=\"user-registration\" >Регистрация?</a>\n\t\t</div>\n\t</div>\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.5f1b917f72a65f263043.hot-update.js.map