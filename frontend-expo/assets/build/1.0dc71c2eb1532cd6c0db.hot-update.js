webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	
	var debugPage = __webpack_require__(62);
	
	// const recoveryConstraints = require('../validators/recoveryForm');
	
	var _require = __webpack_require__(63),
	    formApply = _require.formApply;
	
	var home = __webpack_require__(70);
	var notFound = __webpack_require__(72);
	var offers = __webpack_require__(73);
	var auth = __webpack_require__(75);
	var account = __webpack_require__(116);
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', home).state('404', notFound).state('offer-page', offers.catalog).state('user-login', auth.login).state('user-registration', auth.registration).state('user-recovery', auth.recovery);
	
		$stateProvider.state('change-password', account.changePassword).state('user-offers', account.offersCatalog).state('user-events', account.eventsCatalog);
	
		$stateProvider.state('user-account', {
			url: '/user/account',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-cart', {
			url: '/user/cart',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-logout', {
			url: '/user/logout',
			templateUrl: changePasswordPage,
			controller: ['$scope', '$state', 'User', function ($scope, $state, User) {
				User.logout().catch(function (e) {
					return console.error(e);
				}).then(function () {
					$state.go('home');
				});
			}]
		});
	
		$stateProvider.state('dev-debug', {
			url: '/dev/debug',
			templateUrl: debugPage,
			controller: ['$scope', 'User', '$timeout', function ($scope, User, $timeout) {
	
				User.bind($scope);
	
				$scope.selectedUser = 1;
	
				$scope.changeUser = function ($event) {
					console.log($scope.selectedUser);
					User.setState($scope.users[$scope.selectedUser]);
				};
	
				$scope.users = [{
					displayName: 'Тумаков Олег Н',
					role: 'guest'
				}, {
					displayName: 'Эрнест Днепропетровский А',
					role: 'exponent'
				}, {
					displayName: 'Артем Вячеславович Е.',
					role: 'organizer'
				}, {
					displayName: 'Модератор',
					role: 'moderator'
				}, {
					displayName: 'Администратор',
					role: 'administrator'
				}];
	
				$timeout(function () {
					safeApply($scope, function () {
						$scope.users.push({
							displayName: 'Azaza',
							role: 'administrator'
						});
					});
				}, 1300);
			}]
	
		});
	
		$stateProvider.state('dev-styles', {
			url: '/dev/styles',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.0dc71c2eb1532cd6c0db.hot-update.js.map