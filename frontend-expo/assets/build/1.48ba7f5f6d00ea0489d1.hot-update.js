webpackHotUpdate(1,{

/***/ 191:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var itemTemplate = __webpack_require__(180);
	var formTemplate = __webpack_require__(192);
	var qwery = __webpack_require__(175);
	
	var _require = __webpack_require__(169),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(167);
	
	var EXPO_TYPES = ['Выставка', 'Премия', 'Конференция', 'Форум'];
	
	var mainController = ['$scope', '$element', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $element, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
		// const pageElement = $element[0];
		// const [form] = qwery('form', pageElement);
	
		User.bind($scope);
		formApply($scope, $element);
	
		$scope.currentAdditionalValue = '';
	
		$scope.additionalFields = [];
		$scope.addField = function () {
	
			if (!$scope.currentAdditionalValue || !$scope.currentAdditionalValue.length) {
				return;
			}
	
			safeApply($scope, function () {
				$scope.additionalFields.unshift({
					idx: $scope.additionalFields.length,
					value: $scope.currentAdditionalValue
				});
	
				$scope.currentAdditionalValue = '';
			});
		};
	
		$scope.removeField = function (index) {
			console.log(index);
			safeApply($scope, function () {
				$scope.additionalFields.splice(index, 1);
			});
		};
	
		$scope.subjects = [];
		$scope.openSubjectsSelector = function () {
			$modals.open('subjects-selector', function (result) {});
		};
	
		$scope.types = EXPO_TYPES;
	}];
	
	var create = {
		url: '/exposition/create',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var update = {
		url: '/exposition/edit/:id',
		templateUrl: formTemplate,
		controller: mainController
	};
	
	var index = {
		url: '/exposition/:id',
		templateUrl: itemTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			$scope.offer = {};
	
			User.bind($scope);
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
					//TODO 404 error
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = {
		create: create, update: update, index: index
	};

/***/ }

})
//# sourceMappingURL=1.48ba7f5f6d00ea0489d1.hot-update.js.map