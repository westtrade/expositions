webpackHotUpdate(1,{

/***/ 11:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(12);
	__webpack_require__(13);
	__webpack_require__(14);
	__webpack_require__(15).polyfill();
	
	var angular = __webpack_require__(16);
	var uiRouter = __webpack_require__(18);
	
	var app = angular.module('expotestdrive', [uiRouter]);
	var router = __webpack_require__(19);
	app.config(router);
	
	var topBar = __webpack_require__(22);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(24);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(37);
	app.directive('expoSlider', expoSlider);
	
	module.exports = app;

/***/ },

/***/ 37:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var template = __webpack_require__(37);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	module.exports = function () {
		return {
			template: template,
			controller: controller
		};
	};

/***/ }

})
//# sourceMappingURL=1.8a62cc25036e3b9d6211.hot-update.js.map