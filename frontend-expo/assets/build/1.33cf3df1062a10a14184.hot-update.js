webpackHotUpdate(1,{

/***/ 116:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _require = __webpack_require__(68),
	    shortId = _require.shortId;
	
	var gmapIsInitialized = false;
	
	var loadGMap = function loadGMap(resolve, key) {
	
		if (gmapIsInitialized) {
			resolve();
		}
	
		var randomCallbackName = 'fn' + shortId();
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?keyv=3&callback=' + randomCallbackName;
	
		if (key) {
			script.src = scripts.src + '&key=' + key;
		}
	
		global[randomCallbackName] = function () {
			gmapIsInitialized = true;
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	module.exports = loadGMap;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.33cf3df1062a10a14184.hot-update.js.map