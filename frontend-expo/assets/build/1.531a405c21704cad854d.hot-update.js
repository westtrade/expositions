webpackHotUpdate(1,{

/***/ 37:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
				$scope.active = active;
	
				console.log(active);
			};
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.531a405c21704cad854d.hot-update.js.map