webpackHotUpdate(1,{

/***/ 42:
/***/ function(module, exports) {

	'use strict';
	
	if (!("previousElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "previousElementSibling", {
	        get: function get() {
	            var e = this.previousSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.previousSibling;
	            }return e;
	        }
	    });
	}
	
	// Source: https://github.com/Alhadis/Snippets/blob/master/js/polyfills/IE8-child-elements.js
	if (!("nextElementSibling" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "nextElementSibling", {
	        get: function get() {
	            var e = this.nextSibling;
	            while (e && 1 !== e.nodeType) {
	                e = e.nextSibling;
	            }return e;
	        }
	    });
	}
	
	if (!("firstElementChild" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "firstElementChild", {
	        get: function get() {
	            for (var nodes = this.children, n, i = 0, l = nodes.length; i < l; ++i) {
	                if (n = nodes[i], 1 === n.nodeType) return n;
	            }return null;
	        }
	    });
	}
	
	if (!Array.isArray) {
	    Array.isArray = function (arg) {
	        return Object.prototype.toString.call(arg) === '[object Array]';
	    };
	}
	
	// Source: https://github.com/Alhadis/Snippets/blob/master/js/polyfills/IE8-child-elements.js
	if (!("lastElementChild" in document.documentElement)) {
	    Object.defineProperty(Element.prototype, "lastElementChild", {
	        get: function get() {
	            for (var nodes = this.children, n, i = nodes.length - 1; i >= 0; --i) {
	                if (n = nodes[i], 1 === n.nodeType) return n;
	            }return null;
	        }
	    });
	}
	
	// Шаги алгоритма ECMA-262, 6-е издание, 22.1.2.1
	// Ссылка: https://people.mozilla.org/~jorendorff/es6-draft.html#sec-array.from
	if (!Array.from) {
	    Array.from = function () {
	        var toStr = Object.prototype.toString;
	        var isCallable = function isCallable(fn) {
	            return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
	        };
	        var toInteger = function toInteger(value) {
	            var number = Number(value);
	            if (isNaN(number)) {
	                return 0;
	            }
	            if (number === 0 || !isFinite(number)) {
	                return number;
	            }
	            return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
	        };
	        var maxSafeInteger = Math.pow(2, 53) - 1;
	        var toLength = function toLength(value) {
	            var len = toInteger(value);
	            return Math.min(Math.max(len, 0), maxSafeInteger);
	        };
	
	        // Свойство length метода from равно 1.
	        return function from(arrayLike /*, mapFn, thisArg */) {
	            // 1. Положим C равным значению this.
	            var C = this;
	
	            // 2. Положим items равным ToObject(arrayLike).
	            var items = Object(arrayLike);
	
	            // 3. ReturnIfAbrupt(items).
	            if (arrayLike == null) {
	                throw new TypeError('Array.from requires an array-like object - not null or undefined');
	            }
	
	            // 4. Если mapfn равен undefined, положим mapping равным false.
	            var mapFn = arguments[1];
	            if (typeof mapFn !== 'undefined') {
	                mapFn = arguments.length > 1 ? arguments[1] : void undefined;
	                // 5. иначе
	                // 5. a. Если вызов IsCallable(mapfn) равен false, выкидываем исключение TypeError.
	                if (!isCallable(mapFn)) {
	                    throw new TypeError('Array.from: when provided, the second argument must be a function');
	                }
	
	                // 5. b. Если thisArg присутствует, положим T равным thisArg; иначе положим T равным undefined.
	                if (arguments.length > 2) {
	                    T = arguments[2];
	                }
	            }
	
	            // 10. Положим lenValue равным Get(items, "length").
	            // 11. Положим len равным ToLength(lenValue).
	            var len = toLength(items.length);
	
	            // 13. Если IsConstructor(C) равен true, то
	            // 13. a. Положим A равным результату вызова внутреннего метода [[Construct]]
	            //     объекта C со списком аргументов, содержащим единственный элемент len.
	            // 14. a. Иначе, положим A равным ArrayCreate(len).
	            var A = isCallable(C) ? Object(new C(len)) : new Array(len);
	
	            // 16. Положим k равным 0.
	            var k = 0;
	            // 17. Пока k < len, будем повторять... (шаги с a по h)
	            var kValue;
	            while (k < len) {
	                kValue = items[k];
	                if (mapFn) {
	                    A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
	                } else {
	                    A[k] = kValue;
	                }
	                k += 1;
	            }
	            // 18. Положим putStatus равным Put(A, "length", len, true).
	            A.length = len;
	            // 20. Вернём A.
	            return A;
	        };
	    }();
	}

/***/ }

})
//# sourceMappingURL=1.e8607456cf9e7aad26e9.hot-update.js.map