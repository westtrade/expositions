webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var Dropzone = __webpack_require__(49);
	var templateUrl = __webpack_require__(240);
	var qwery = __webpack_require__(93);
	// require('dropzone/src/dropzone.scss');
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			scope: {},
			link: function link($scope, $element, $attrs, $controllers, $transclude) {
	
				var element = $element[0];
				var _$attrs$url = $attrs.url,
				    url = _$attrs$url === undefined ? '/' : _$attrs$url,
				    _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name;
	
				element.classList.add('dropzone');
	
				var inputField = qwery('input', element);
				inputField.setAttribute('name', name);
	
				$transclude(function (clone) {
					// const currentDropzone = new Dropzone(element, {
					// 	url
					// });
	
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.2cd72ff43045668f6e83.hot-update.js.map