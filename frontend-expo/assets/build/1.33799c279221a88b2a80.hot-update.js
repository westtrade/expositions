webpackHotUpdate(1,{

/***/ 49:
/***/ function(module, exports) {

	'use strict';
	
	var subjects = ['Авиакосмическая промышленность', 'Автомобили и мотоциклы', 'Анализ, измерение и контроль', 'Безопасность', 'Бизнес, инвестиции, финансы', 'Вино, алкоголь, табак', 'Городское хозяйство', 'Гостиничное, ресторанное дело', 'Детские товары и игрушки', 'Животные. Ветеринария', 'ИТ, коммуникация, связь', 'ИТ: Интернет маркетинг', 'ИТ: Интернет-технологии', 'Катера, яхты, судостроение', 'Коcметика и парфюмерия', 'Культура, исскуство, церковь'].map(function (name, id) {
		var category = name[0].toUpperCase();
		return {
			name: name, id: id, category: category, selected: false
		};
	}).reduce(function (result, currentItem) {
		var category = currentItem.category;
	
		if (!(category in result)) {
			result[category] = [];
		}
		result[category].push(currentItem);
		return result;
	}, {});
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		$scope.subjects = subjects;
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
			console.log($scope.subjects);
			var selected = Object.values($scope.subjects).reduce(function (result, items) {
				return result.concat(items.filter(function (item) {
					return item.selected;
				}));
			});
	
			$modals.close(selected);
		};
	}];

/***/ }

})
//# sourceMappingURL=1.33799c279221a88b2a80.hot-update.js.map