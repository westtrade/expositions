webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			transclude: 'element',
			restrict: 'C',
			priority: 600,
			terminal: true,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
							var modalElement = angular.element(modalSource);
							modalElement.removeClass('modal-source').addClass('modal selected');
							$element.after(modalElement);
	
							var $modalScope = $rootScope.$new(true);
							$modalScope.initial = data;
							$modalScope.modalOpened = true;
	
							newModal = $compile(modalElement)($modalScope);
						} else if (newModal) {
	
							newModal.scope().$destroy();
							newModal.remove();
							newModal = null;
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.bd3b911be5072c34bb68.hot-update.js.map