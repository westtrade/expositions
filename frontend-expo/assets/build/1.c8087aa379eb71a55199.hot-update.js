webpackHotUpdate(1,{

/***/ 189:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var changePasswordPage = __webpack_require__(190);
	
	var _require = __webpack_require__(169),
	    formApply = _require.formApply;
	
	var registrationConstraints = __webpack_require__(188);
	
	var changePassword = {
		url: '/user/change-password',
		templateUrl: changePasswordPage,
		controller: ['$scope', '$element', function ($scope, $element) {
			var form = $element.find('form')[0];
			formApply($scope, form, registrationConstraints);
	
			$scope.setMail = function ($event) {
	
				form.validate(function (error, formData) {});
	
				$event.preventDefault();
			};
		}]
	};
	
	var eventsCatalog = {
		url: '/user/events',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	var accountPage = {
		url: '/user/account',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	var cart = {
		url: '/user/cart',
		templateUrl: changePasswordPage,
		controller: ['$scope', function ($scope) {}]
	};
	
	module.exports = {
		changePassword: changePassword,
		offersCatalog: offersCatalog,
		eventsCatalog: eventsCatalog,
		accountPage: accountPage,
		cart: cart
	};

/***/ }

})
//# sourceMappingURL=1.c8087aa379eb71a55199.hot-update.js.map