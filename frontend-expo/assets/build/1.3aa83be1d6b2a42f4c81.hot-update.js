webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	__webpack_require__(232);
	__webpack_require__(235);
	
	__webpack_require__(245);
	__webpack_require__(261);
	__webpack_require__(262);
	__webpack_require__(263);
	__webpack_require__(264);
	__webpack_require__(265);
	__webpack_require__(265);
	__webpack_require__(266);
	__webpack_require__(267);
	__webpack_require__(246);
	__webpack_require__(247);
	__webpack_require__(260);
	
	var content_style = '\nbody {\n\tfont-family: "Roboto", sans-serif;\n}\n';
	
	module.exports = function () {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
					},
	
					content_style: content_style,
					// height: 500,
					menubar: false,
					skin: false,
					// inline: true,
					plugins: ['paste', 'link', 'autoresize', 'textpattern'],
					textpattern_patterns: [{ start: '*', end: '*', format: 'italic' }, { start: '**', end: '**', format: 'bold' }, { start: '#', format: 'h1' }, { start: '##', format: 'h2' }, { start: '###', format: 'h3' }, { start: '####', format: 'h4' }, { start: '#####', format: 'h5' }, { start: '######', format: 'h6' }, { start: '1. ', cmd: 'InsertOrderedList' }, { start: '* ', cmd: 'InsertUnorderedList' }, { start: '- ', cmd: 'InsertUnorderedList' }]
				});
			}
		};
	};

/***/ },

/***/ 261:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('advlist', function(editor) {
		var olMenuItems, ulMenuItems;
	
		var hasPlugin = function (editor, plugin) {
			var plugins = editor.settings.plugins ? editor.settings.plugins : '';
			return tinymce.util.Tools.inArray(plugins.split(/[ ,]/), plugin) !== -1;
		};
	
		function isChildOfBody(elm) {
			return editor.$.contains(editor.getBody(), elm);
		}
	
		function isListNode(node) {
			return node && (/^(OL|UL|DL)$/).test(node.nodeName) && isChildOfBody(node);
		}
	
		function buildMenuItems(listName, styleValues) {
			var items = [];
			if (styleValues) {
				tinymce.each(styleValues.split(/[ ,]/), function(styleValue) {
					items.push({
						text: styleValue.replace(/\-/g, ' ').replace(/\b\w/g, function(chr) {
							return chr.toUpperCase();
						}),
						data: styleValue == 'default' ? '' : styleValue
					});
				});
			}
			return items;
		}
	
		olMenuItems = buildMenuItems('OL', editor.getParam(
			"advlist_number_styles",
			"default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman"
		));
	
		ulMenuItems = buildMenuItems('UL', editor.getParam("advlist_bullet_styles", "default,circle,disc,square"));
	
		function applyListFormat(listName, styleValue) {
			editor.undoManager.transact(function() {
				var list, dom = editor.dom, sel = editor.selection;
	
				// Check for existing list element
				list = dom.getParent(sel.getNode(), 'ol,ul');
	
				// Switch/add list type if needed
				if (!list || list.nodeName != listName || styleValue === false) {
					var detail = {
						'list-style-type': styleValue ? styleValue : ''
					};
	
					editor.execCommand(listName == 'UL' ? 'InsertUnorderedList' : 'InsertOrderedList', false, detail);
				}
	
				list = dom.getParent(sel.getNode(), 'ol,ul');
				if (list) {
					tinymce.util.Tools.each(dom.select('ol,ul', list).concat([list]), function (list) {
						if (list.nodeName !== listName && styleValue !== false) {
							list = dom.rename(list, listName);
						}
	
						dom.setStyle(list, 'listStyleType', styleValue ? styleValue : null);
						list.removeAttribute('data-mce-style');
					});
				}
	
				editor.focus();
			});
		}
	
		function updateSelection(e) {
			var listStyleType = editor.dom.getStyle(editor.dom.getParent(editor.selection.getNode(), 'ol,ul'), 'listStyleType') || '';
	
			e.control.items().each(function(ctrl) {
				ctrl.active(ctrl.settings.data === listStyleType);
			});
		}
	
		var listState = function (listName) {
			return function () {
				var self = this;
	
				editor.on('NodeChange', function (e) {
					var lists = tinymce.util.Tools.grep(e.parents, isListNode);
					self.active(lists.length > 0 && lists[0].nodeName === listName);
				});
			};
		};
	
		if (hasPlugin(editor, "lists")) {
			editor.addCommand('ApplyUnorderedListStyle', function (ui, value) {
				applyListFormat('UL', value['list-style-type']);
			});
	
			editor.addCommand('ApplyOrderedListStyle', function (ui, value) {
				applyListFormat('OL', value['list-style-type']);
			});
	
			editor.addButton('numlist', {
				type: (olMenuItems.length > 0) ? 'splitbutton' : 'button',
				tooltip: 'Numbered list',
				menu: olMenuItems,
				onPostRender: listState('OL'),
				onshow: updateSelection,
				onselect: function(e) {
					applyListFormat('OL', e.control.settings.data);
				},
				onclick: function() {
					applyListFormat('OL', false);
				}
			});
	
			editor.addButton('bullist', {
				type: (ulMenuItems.length > 0) ? 'splitbutton' : 'button',
				tooltip: 'Bullet list',
				onPostRender: listState('UL'),
				menu: ulMenuItems,
				onshow: updateSelection,
				onselect: function(e) {
					applyListFormat('UL', e.control.settings.data);
				},
				onclick: function() {
					applyListFormat('UL', false);
				}
			});
		}
	});
	
	}.call(window));

/***/ },

/***/ 262:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('autolink', function(editor) {
		var AutoUrlDetectState;
		var AutoLinkPattern = /^(https?:\/\/|ssh:\/\/|ftp:\/\/|file:\/|www\.|(?:mailto:)?[A-Z0-9._%+\-]+@)(.+)$/i;
	
		if (editor.settings.autolink_pattern) {
			AutoLinkPattern = editor.settings.autolink_pattern;
		}
	
		editor.on("keydown", function(e) {
			if (e.keyCode == 13) {
				return handleEnter(editor);
			}
		});
	
		// Internet Explorer has built-in automatic linking for most cases
		if (tinymce.Env.ie) {
			editor.on("focus", function() {
				if (!AutoUrlDetectState) {
					AutoUrlDetectState = true;
	
					try {
						editor.execCommand('AutoUrlDetect', false, true);
					} catch (ex) {
						// Ignore
					}
				}
			});
	
			return;
		}
	
		editor.on("keypress", function(e) {
			if (e.keyCode == 41) {
				return handleEclipse(editor);
			}
		});
	
		editor.on("keyup", function(e) {
			if (e.keyCode == 32) {
				return handleSpacebar(editor);
			}
		});
	
		function handleEclipse(editor) {
			parseCurrentLine(editor, -1, '(', true);
		}
	
		function handleSpacebar(editor) {
			parseCurrentLine(editor, 0, '', true);
		}
	
		function handleEnter(editor) {
			parseCurrentLine(editor, -1, '', false);
		}
	
		function parseCurrentLine(editor, end_offset, delimiter) {
			var rng, end, start, endContainer, bookmark, text, matches, prev, len, rngText;
	
			function scopeIndex(container, index) {
				if (index < 0) {
					index = 0;
				}
	
				if (container.nodeType == 3) {
					var len = container.data.length;
	
					if (index > len) {
						index = len;
					}
				}
	
				return index;
			}
	
			function setStart(container, offset) {
				if (container.nodeType != 1 || container.hasChildNodes()) {
					rng.setStart(container, scopeIndex(container, offset));
				} else {
					rng.setStartBefore(container);
				}
			}
	
			function setEnd(container, offset) {
				if (container.nodeType != 1 || container.hasChildNodes()) {
					rng.setEnd(container, scopeIndex(container, offset));
				} else {
					rng.setEndAfter(container);
				}
			}
	
			// Never create a link when we are inside a link
			if (editor.selection.getNode().tagName == 'A') {
				return;
			}
	
			// We need at least five characters to form a URL,
			// hence, at minimum, five characters from the beginning of the line.
			rng = editor.selection.getRng(true).cloneRange();
			if (rng.startOffset < 5) {
				// During testing, the caret is placed between two text nodes.
				// The previous text node contains the URL.
				prev = rng.endContainer.previousSibling;
				if (!prev) {
					if (!rng.endContainer.firstChild || !rng.endContainer.firstChild.nextSibling) {
						return;
					}
	
					prev = rng.endContainer.firstChild.nextSibling;
				}
	
				len = prev.length;
				setStart(prev, len);
				setEnd(prev, len);
	
				if (rng.endOffset < 5) {
					return;
				}
	
				end = rng.endOffset;
				endContainer = prev;
			} else {
				endContainer = rng.endContainer;
	
				// Get a text node
				if (endContainer.nodeType != 3 && endContainer.firstChild) {
					while (endContainer.nodeType != 3 && endContainer.firstChild) {
						endContainer = endContainer.firstChild;
					}
	
					// Move range to text node
					if (endContainer.nodeType == 3) {
						setStart(endContainer, 0);
						setEnd(endContainer, endContainer.nodeValue.length);
					}
				}
	
				if (rng.endOffset == 1) {
					end = 2;
				} else {
					end = rng.endOffset - 1 - end_offset;
				}
			}
	
			start = end;
	
			do {
				// Move the selection one character backwards.
				setStart(endContainer, end >= 2 ? end - 2 : 0);
				setEnd(endContainer, end >= 1 ? end - 1 : 0);
				end -= 1;
				rngText = rng.toString();
	
				// Loop until one of the following is found: a blank space, &nbsp;, delimiter, (end-2) >= 0
			} while (rngText != ' ' && rngText !== '' && rngText.charCodeAt(0) != 160 && (end - 2) >= 0 && rngText != delimiter);
	
			if (rng.toString() == delimiter || rng.toString().charCodeAt(0) == 160) {
				setStart(endContainer, end);
				setEnd(endContainer, start);
				end += 1;
			} else if (rng.startOffset === 0) {
				setStart(endContainer, 0);
				setEnd(endContainer, start);
			} else {
				setStart(endContainer, end);
				setEnd(endContainer, start);
			}
	
			// Exclude last . from word like "www.site.com."
			text = rng.toString();
			if (text.charAt(text.length - 1) == '.') {
				setEnd(endContainer, start - 1);
			}
	
			text = rng.toString();
			matches = text.match(AutoLinkPattern);
	
			if (matches) {
				if (matches[1] == 'www.') {
					matches[1] = 'http://www.';
				} else if (/@$/.test(matches[1]) && !/^mailto:/.test(matches[1])) {
					matches[1] = 'mailto:' + matches[1];
				}
	
				bookmark = editor.selection.getBookmark();
	
				editor.selection.setRng(rng);
				editor.execCommand('createlink', false, matches[1] + matches[2]);
	
				if (editor.settings.default_link_target) {
					editor.dom.setAttrib(editor.selection.getNode(), 'target', editor.settings.default_link_target);
				}
	
				editor.selection.moveToBookmark(bookmark);
				editor.nodeChanged();
			}
		}
	});
	
	}.call(window));

/***/ },

/***/ 263:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('image', function(editor) {
		function getImageSize(url, callback) {
			var img = document.createElement('img');
	
			function done(width, height) {
				if (img.parentNode) {
					img.parentNode.removeChild(img);
				}
	
				callback({width: width, height: height});
			}
	
			img.onload = function() {
				done(Math.max(img.width, img.clientWidth), Math.max(img.height, img.clientHeight));
			};
	
			img.onerror = function() {
				done();
			};
	
			var style = img.style;
			style.visibility = 'hidden';
			style.position = 'fixed';
			style.bottom = style.left = 0;
			style.width = style.height = 'auto';
	
			document.body.appendChild(img);
			img.src = url;
		}
	
		function buildListItems(inputList, itemCallback, startItems) {
			function appendItems(values, output) {
				output = output || [];
	
				tinymce.each(values, function(item) {
					var menuItem = {text: item.text || item.title};
	
					if (item.menu) {
						menuItem.menu = appendItems(item.menu);
					} else {
						menuItem.value = item.value;
						itemCallback(menuItem);
					}
	
					output.push(menuItem);
				});
	
				return output;
			}
	
			return appendItems(inputList, startItems || []);
		}
	
		function createImageList(callback) {
			return function() {
				var imageList = editor.settings.image_list;
	
				if (typeof imageList == "string") {
					tinymce.util.XHR.send({
						url: imageList,
						success: function(text) {
							callback(tinymce.util.JSON.parse(text));
						}
					});
				} else if (typeof imageList == "function") {
					imageList(callback);
				} else {
					callback(imageList);
				}
			};
		}
	
		function showDialog(imageList) {
			var win, data = {}, dom = editor.dom, imgElm, figureElm;
			var width, height, imageListCtrl, classListCtrl, imageDimensions = editor.settings.image_dimensions !== false;
	
			function recalcSize() {
				var widthCtrl, heightCtrl, newWidth, newHeight;
	
				widthCtrl = win.find('#width')[0];
				heightCtrl = win.find('#height')[0];
	
				if (!widthCtrl || !heightCtrl) {
					return;
				}
	
				newWidth = widthCtrl.value();
				newHeight = heightCtrl.value();
	
				if (win.find('#constrain')[0].checked() && width && height && newWidth && newHeight) {
					if (width != newWidth) {
						newHeight = Math.round((newWidth / width) * newHeight);
	
						if (!isNaN(newHeight)) {
							heightCtrl.value(newHeight);
						}
					} else {
						newWidth = Math.round((newHeight / height) * newWidth);
	
						if (!isNaN(newWidth)) {
							widthCtrl.value(newWidth);
						}
					}
				}
	
				width = newWidth;
				height = newHeight;
			}
	
			function onSubmitForm() {
				var figureElm, oldImg;
	
				function waitLoad(imgElm) {
					function selectImage() {
						imgElm.onload = imgElm.onerror = null;
	
						if (editor.selection) {
							editor.selection.select(imgElm);
							editor.nodeChanged();
						}
					}
	
					imgElm.onload = function() {
						if (!data.width && !data.height && imageDimensions) {
							dom.setAttribs(imgElm, {
								width: imgElm.clientWidth,
								height: imgElm.clientHeight
							});
						}
	
						selectImage();
					};
	
					imgElm.onerror = selectImage;
				}
	
				updateStyle();
				recalcSize();
	
				data = tinymce.extend(data, win.toJSON());
	
				if (!data.alt) {
					data.alt = '';
				}
	
				if (!data.title) {
					data.title = '';
				}
	
				if (data.width === '') {
					data.width = null;
				}
	
				if (data.height === '') {
					data.height = null;
				}
	
				if (!data.style) {
					data.style = null;
				}
	
				// Setup new data excluding style properties
				/*eslint dot-notation: 0*/
				data = {
					src: data.src,
					alt: data.alt,
					title: data.title,
					width: data.width,
					height: data.height,
					style: data.style,
					caption: data.caption,
					"class": data["class"]
				};
	
				editor.undoManager.transact(function() {
					if (!data.src) {
						if (imgElm) {
							dom.remove(imgElm);
							editor.focus();
							editor.nodeChanged();
						}
	
						return;
					}
	
					if (data.title === "") {
						data.title = null;
					}
	
					if (!imgElm) {
						data.id = '__mcenew';
						editor.focus();
						editor.selection.setContent(dom.createHTML('img', data));
						imgElm = dom.get('__mcenew');
						dom.setAttrib(imgElm, 'id', null);
					} else {
						dom.setAttribs(imgElm, data);
					}
	
					editor.editorUpload.uploadImagesAuto();
	
					if (data.caption === false) {
						if (dom.is(imgElm.parentNode, 'figure.image')) {
							figureElm = imgElm.parentNode;
							dom.insertAfter(imgElm, figureElm);
							dom.remove(figureElm);
						}
					}
	
					function isTextBlock(node) {
						return editor.schema.getTextBlockElements()[node.nodeName];
					}
	
					if (data.caption === true) {
						if (!dom.is(imgElm.parentNode, 'figure.image')) {
							oldImg = imgElm;
							imgElm = imgElm.cloneNode(true);
							figureElm = dom.create('figure', {'class': 'image'});
							figureElm.appendChild(imgElm);
							figureElm.appendChild(dom.create('figcaption', {contentEditable: true}, 'Caption'));
							figureElm.contentEditable = false;
	
							var textBlock = dom.getParent(oldImg, isTextBlock);
							if (textBlock) {
								dom.split(textBlock, oldImg, figureElm);
							} else {
								dom.replace(figureElm, oldImg);
							}
	
							editor.selection.select(figureElm);
						}
	
						return;
					}
	
					waitLoad(imgElm);
				});
			}
	
			function removePixelSuffix(value) {
				if (value) {
					value = value.replace(/px$/, '');
				}
	
				return value;
			}
	
			function srcChange(e) {
				var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};
	
				if (imageListCtrl) {
					imageListCtrl.value(editor.convertURL(this.value(), 'src'));
				}
	
				tinymce.each(meta, function(value, key) {
					win.find('#' + key).value(value);
				});
	
				if (!meta.width && !meta.height) {
					srcURL = editor.convertURL(this.value(), 'src');
	
					// Pattern test the src url and make sure we haven't already prepended the url
					prependURL = editor.settings.image_prepend_url;
					absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
					if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
						srcURL = prependURL + srcURL;
					}
	
					this.value(srcURL);
	
					getImageSize(editor.documentBaseURI.toAbsolute(this.value()), function(data) {
						if (data.width && data.height && imageDimensions) {
							width = data.width;
							height = data.height;
	
							win.find('#width').value(width);
							win.find('#height').value(height);
						}
					});
				}
			}
	
			function onBeforeCall(e) {
				e.meta = win.toJSON();
			}
	
			imgElm = editor.selection.getNode();
			figureElm = dom.getParent(imgElm, 'figure.image');
			if (figureElm) {
				imgElm = dom.select('img', figureElm)[0];
			}
	
			if (imgElm && (imgElm.nodeName != 'IMG' || imgElm.getAttribute('data-mce-object') || imgElm.getAttribute('data-mce-placeholder'))) {
				imgElm = null;
			}
	
			if (imgElm) {
				width = dom.getAttrib(imgElm, 'width');
				height = dom.getAttrib(imgElm, 'height');
	
				data = {
					src: dom.getAttrib(imgElm, 'src'),
					alt: dom.getAttrib(imgElm, 'alt'),
					title: dom.getAttrib(imgElm, 'title'),
					"class": dom.getAttrib(imgElm, 'class'),
					width: width,
					height: height,
					caption: !!figureElm
				};
			}
	
			if (imageList) {
				imageListCtrl = {
					type: 'listbox',
					label: 'Image list',
					values: buildListItems(
						imageList,
						function(item) {
							item.value = editor.convertURL(item.value || item.url, 'src');
						},
						[{text: 'None', value: ''}]
					),
					value: data.src && editor.convertURL(data.src, 'src'),
					onselect: function(e) {
						var altCtrl = win.find('#alt');
	
						if (!altCtrl.value() || (e.lastControl && altCtrl.value() == e.lastControl.text())) {
							altCtrl.value(e.control.text());
						}
	
						win.find('#src').value(e.control.value()).fire('change');
					},
					onPostRender: function() {
						/*eslint consistent-this: 0*/
						imageListCtrl = this;
					}
				};
			}
	
			if (editor.settings.image_class_list) {
				classListCtrl = {
					name: 'class',
					type: 'listbox',
					label: 'Class',
					values: buildListItems(
						editor.settings.image_class_list,
						function(item) {
							if (item.value) {
								item.textStyle = function() {
									return editor.formatter.getCssText({inline: 'img', classes: [item.value]});
								};
							}
						}
					)
				};
			}
	
			// General settings shared between simple and advanced dialogs
			var generalFormItems = [
				{
					name: 'src',
					type: 'filepicker',
					filetype: 'image',
					label: 'Source',
					autofocus: true,
					onchange: srcChange,
					onbeforecall: onBeforeCall
				},
				imageListCtrl
			];
	
			if (editor.settings.image_description !== false) {
				generalFormItems.push({name: 'alt', type: 'textbox', label: 'Image description'});
			}
	
			if (editor.settings.image_title) {
				generalFormItems.push({name: 'title', type: 'textbox', label: 'Image Title'});
			}
	
			if (imageDimensions) {
				generalFormItems.push({
					type: 'container',
					label: 'Dimensions',
					layout: 'flex',
					direction: 'row',
					align: 'center',
					spacing: 5,
					items: [
						{name: 'width', type: 'textbox', maxLength: 5, size: 3, onchange: recalcSize, ariaLabel: 'Width'},
						{type: 'label', text: 'x'},
						{name: 'height', type: 'textbox', maxLength: 5, size: 3, onchange: recalcSize, ariaLabel: 'Height'},
						{name: 'constrain', type: 'checkbox', checked: true, text: 'Constrain proportions'}
					]
				});
			}
	
			generalFormItems.push(classListCtrl);
	
			if (editor.settings.image_caption && tinymce.Env.ceFalse) {
				generalFormItems.push({name: 'caption', type: 'checkbox', label: 'Caption'});
			}
	
			function mergeMargins(css) {
				if (css.margin) {
	
					var splitMargin = css.margin.split(" ");
	
					switch (splitMargin.length) {
						case 1: //margin: toprightbottomleft;
							css['margin-top'] = css['margin-top'] || splitMargin[0];
							css['margin-right'] = css['margin-right'] || splitMargin[0];
							css['margin-bottom'] = css['margin-bottom'] || splitMargin[0];
							css['margin-left'] = css['margin-left'] || splitMargin[0];
							break;
						case 2: //margin: topbottom rightleft;
							css['margin-top'] = css['margin-top'] || splitMargin[0];
							css['margin-right'] = css['margin-right'] || splitMargin[1];
							css['margin-bottom'] = css['margin-bottom'] || splitMargin[0];
							css['margin-left'] = css['margin-left'] || splitMargin[1];
							break;
						case 3: //margin: top rightleft bottom;
							css['margin-top'] = css['margin-top'] || splitMargin[0];
							css['margin-right'] = css['margin-right'] || splitMargin[1];
							css['margin-bottom'] = css['margin-bottom'] || splitMargin[2];
							css['margin-left'] = css['margin-left'] || splitMargin[1];
							break;
						case 4: //margin: top right bottom left;
							css['margin-top'] = css['margin-top'] || splitMargin[0];
							css['margin-right'] = css['margin-right'] || splitMargin[1];
							css['margin-bottom'] = css['margin-bottom'] || splitMargin[2];
							css['margin-left'] = css['margin-left'] || splitMargin[3];
					}
					delete css.margin;
				}
				return css;
			}
	
			function updateStyle() {
				function addPixelSuffix(value) {
					if (value.length > 0 && /^[0-9]+$/.test(value)) {
						value += 'px';
					}
	
					return value;
				}
	
				if (!editor.settings.image_advtab) {
					return;
				}
	
				var data = win.toJSON(),
					css = dom.parseStyle(data.style);
	
				css = mergeMargins(css);
	
				if (data.vspace) {
					css['margin-top'] = css['margin-bottom'] = addPixelSuffix(data.vspace);
				}
				if (data.hspace) {
					css['margin-left'] = css['margin-right'] = addPixelSuffix(data.hspace);
				}
				if (data.border) {
					css['border-width'] = addPixelSuffix(data.border);
				}
	
				win.find('#style').value(dom.serializeStyle(dom.parseStyle(dom.serializeStyle(css))));
			}
	
			function updateVSpaceHSpaceBorder() {
				if (!editor.settings.image_advtab) {
					return;
				}
	
				var data = win.toJSON(),
					css = dom.parseStyle(data.style);
	
				win.find('#vspace').value("");
				win.find('#hspace').value("");
	
				css = mergeMargins(css);
	
				//Move opposite equal margins to vspace/hspace field
				if ((css['margin-top'] && css['margin-bottom']) || (css['margin-right'] && css['margin-left'])) {
					if (css['margin-top'] === css['margin-bottom']) {
						win.find('#vspace').value(removePixelSuffix(css['margin-top']));
					} else {
						win.find('#vspace').value('');
					}
					if (css['margin-right'] === css['margin-left']) {
						win.find('#hspace').value(removePixelSuffix(css['margin-right']));
					} else {
						win.find('#hspace').value('');
					}
				}
	
				//Move border-width
				if (css['border-width']) {
					win.find('#border').value(removePixelSuffix(css['border-width']));
				}
	
				win.find('#style').value(dom.serializeStyle(dom.parseStyle(dom.serializeStyle(css))));
	
			}
	
			if (editor.settings.image_advtab) {
				// Parse styles from img
				if (imgElm) {
					if (imgElm.style.marginLeft && imgElm.style.marginRight && imgElm.style.marginLeft === imgElm.style.marginRight) {
						data.hspace = removePixelSuffix(imgElm.style.marginLeft);
					}
					if (imgElm.style.marginTop && imgElm.style.marginBottom && imgElm.style.marginTop === imgElm.style.marginBottom) {
						data.vspace = removePixelSuffix(imgElm.style.marginTop);
					}
					if (imgElm.style.borderWidth) {
						data.border = removePixelSuffix(imgElm.style.borderWidth);
					}
	
					data.style = editor.dom.serializeStyle(editor.dom.parseStyle(editor.dom.getAttrib(imgElm, 'style')));
				}
	
				// Advanced dialog shows general+advanced tabs
				win = editor.windowManager.open({
					title: 'Insert/edit image',
					data: data,
					bodyType: 'tabpanel',
					body: [
						{
							title: 'General',
							type: 'form',
							items: generalFormItems
						},
	
						{
							title: 'Advanced',
							type: 'form',
							pack: 'start',
							items: [
								{
									label: 'Style',
									name: 'style',
									type: 'textbox',
									onchange: updateVSpaceHSpaceBorder
								},
								{
									type: 'form',
									layout: 'grid',
									packV: 'start',
									columns: 2,
									padding: 0,
									alignH: ['left', 'right'],
									defaults: {
										type: 'textbox',
										maxWidth: 50,
										onchange: updateStyle
									},
									items: [
										{label: 'Vertical space', name: 'vspace'},
										{label: 'Horizontal space', name: 'hspace'},
										{label: 'Border', name: 'border'}
									]
								}
							]
						}
					],
					onSubmit: onSubmitForm
				});
			} else {
				// Simple default dialog
				win = editor.windowManager.open({
					title: 'Insert/edit image',
					data: data,
					body: generalFormItems,
					onSubmit: onSubmitForm
				});
			}
		}
	
		editor.on('preInit', function() {
			function hasImageClass(node) {
				var className = node.attr('class');
				return className && /\bimage\b/.test(className);
			}
	
			function toggleContentEditableState(state) {
				return function(nodes) {
					var i = nodes.length, node;
	
					function toggleContentEditable(node) {
						node.attr('contenteditable', state ? 'true' : null);
					}
	
					while (i--) {
						node = nodes[i];
	
						if (hasImageClass(node)) {
							node.attr('contenteditable', state ? 'false' : null);
							tinymce.each(node.getAll('figcaption'), toggleContentEditable);
						}
					}
				};
			}
	
			editor.parser.addNodeFilter('figure', toggleContentEditableState(true));
			editor.serializer.addNodeFilter('figure', toggleContentEditableState(false));
		});
	
		editor.addButton('image', {
			icon: 'image',
			tooltip: 'Insert/edit image',
			onclick: createImageList(showDialog),
			stateSelector: 'img:not([data-mce-object],[data-mce-placeholder]),figure.image'
		});
	
		editor.addMenuItem('image', {
			icon: 'image',
			text: 'Image',
			onclick: createImageList(showDialog),
			context: 'insert',
			prependToContext: true
		});
	
		editor.addCommand('mceImage', createImageList(showDialog));
	});
	
	}.call(window));

/***/ },

/***/ 264:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	(function () {
	
	var defs = {}; // id -> {dependencies, definition, instance (possibly undefined)}
	
	// Used when there is no 'main' module.
	// The name is probably (hopefully) unique so minification removes for releases.
	var register_3795 = function (id) {
	  var module = dem(id);
	  var fragments = id.split('.');
	  var target = Function('return this;')();
	  for (var i = 0; i < fragments.length - 1; ++i) {
	    if (target[fragments[i]] === undefined)
	      target[fragments[i]] = {};
	    target = target[fragments[i]];
	  }
	  target[fragments[fragments.length - 1]] = module;
	};
	
	var instantiate = function (id) {
	  var actual = defs[id];
	  var dependencies = actual.deps;
	  var definition = actual.defn;
	  var len = dependencies.length;
	  var instances = new Array(len);
	  for (var i = 0; i < len; ++i)
	    instances[i] = dem(dependencies[i]);
	  var defResult = definition.apply(null, instances);
	  if (defResult === undefined)
	     throw 'module [' + id + '] returned undefined';
	  actual.instance = defResult;
	};
	
	var def = function (id, dependencies, definition) {
	  if (typeof id !== 'string')
	    throw 'module id must be a string';
	  else if (dependencies === undefined)
	    throw 'no dependencies for ' + id;
	  else if (definition === undefined)
	    throw 'no definition function for ' + id;
	  defs[id] = {
	    deps: dependencies,
	    defn: definition,
	    instance: undefined
	  };
	};
	
	var dem = function (id) {
	  var actual = defs[id];
	  if (actual === undefined)
	    throw 'module [' + id + '] was undefined';
	  else if (actual.instance === undefined)
	    instantiate(id);
	  return actual.instance;
	};
	
	var req = function (ids, callback) {
	  var len = ids.length;
	  var instances = new Array(len);
	  for (var i = 0; i < len; ++i)
	    instances.push(dem(ids[i]));
	  callback.apply(null, callback);
	};
	
	var ephox = {};
	
	ephox.bolt = {
	  module: {
	    api: {
	      define: def,
	      require: req,
	      demand: dem
	    }
	  }
	};
	
	var define = def;
	var require = req;
	var demand = dem;
	// this helps with minificiation when using a lot of global references
	var defineGlobal = function (id, ref) {
	  define(id, [], function () { return ref; });
	};
	/*jsc
	["tinymce.lists.Plugin","global!tinymce.PluginManager","global!tinymce.util.Tools","global!tinymce.util.VK","tinymce.lists.core.NodeType","tinymce.lists.core.Delete","tinymce.lists.actions.Indent","tinymce.lists.actions.Outdent","tinymce.lists.actions.ToggleList","global!tinymce.dom.TreeWalker","global!tinymce.dom.RangeUtils","tinymce.lists.core.Selection","tinymce.lists.core.Bookmark","tinymce.lists.core.Range","tinymce.lists.core.NormalizeLists","global!tinymce.dom.BookmarkManager","tinymce.lists.core.SplitList","global!tinymce.dom.DOMUtils.DOM","tinymce.lists.core.TextBlock","global!tinymce.Env"]
	jsc*/
	defineGlobal("global!tinymce.PluginManager", tinymce.PluginManager);
	defineGlobal("global!tinymce.util.Tools", tinymce.util.Tools);
	defineGlobal("global!tinymce.util.VK", tinymce.util.VK);
	/**
	 * NodeType.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.NodeType", [
	], function () {
		var isTextNode = function (node) {
			return node && node.nodeType === 3;
		};
	
		var isListNode = function (node) {
			return node && (/^(OL|UL|DL)$/).test(node.nodeName);
		};
	
		var isListItemNode = function (node) {
			return node && /^(LI|DT|DD)$/.test(node.nodeName);
		};
	
		var isBr = function (node) {
			return node && node.nodeName === 'BR';
		};
	
		var isFirstChild = function (node) {
			return node.parentNode.firstChild === node;
		};
	
		var isLastChild = function (node) {
			return node.parentNode.lastChild === node;
		};
	
		var isTextBlock = function (editor, node) {
			return node && !!editor.schema.getTextBlockElements()[node.nodeName];
		};
	
		var isBogusBr = function (dom, node) {
			if (!isBr(node)) {
				return false;
			}
	
			if (dom.isBlock(node.nextSibling) && !isBr(node.previousSibling)) {
				return true;
			}
	
			return false;
		};
	
		var isEmpty = function (dom, elm, keepBookmarks) {
			var empty = dom.isEmpty(elm);
	
			if (keepBookmarks && dom.select('span[data-mce-type=bookmark]', elm).length > 0) {
				return false;
			}
	
			return empty;
		};
	
		var isChildOfBody = function (dom, elm) {
			return dom.isChildOf(elm, dom.getRoot());
		};
	
		return {
			isTextNode: isTextNode,
			isListNode: isListNode,
			isListItemNode: isListItemNode,
			isBr: isBr,
			isFirstChild: isFirstChild,
			isLastChild: isLastChild,
			isTextBlock: isTextBlock,
			isBogusBr: isBogusBr,
			isEmpty: isEmpty,
			isChildOfBody: isChildOfBody
		};
	});
	
	
	defineGlobal("global!tinymce.dom.TreeWalker", tinymce.dom.TreeWalker);
	defineGlobal("global!tinymce.dom.RangeUtils", tinymce.dom.RangeUtils);
	/**
	 * Selection.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.Selection", [
		"global!tinymce.util.Tools",
		"tinymce.lists.core.NodeType"
	], function (Tools, NodeType) {
		var getSelectedListItems = function (editor) {
			return Tools.grep(editor.selection.getSelectedBlocks(), function (block) {
				return NodeType.isListItemNode(block);
			});
		};
	
		return {
			getSelectedListItems: getSelectedListItems
		};
	});
	
	
	defineGlobal("global!tinymce.dom.DOMUtils.DOM", tinymce.dom.DOMUtils.DOM);
	/**
	 * Range.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.Range", [
		"global!tinymce.dom.RangeUtils",
		"tinymce.lists.core.NodeType"
	], function (RangeUtils, NodeType) {
		var getNormalizedEndPoint = function (container, offset) {
			var node = RangeUtils.getNode(container, offset);
	
			if (NodeType.isListItemNode(container) && NodeType.isTextNode(node)) {
				var textNodeOffset = offset >= container.childNodes.length ? node.data.length : 0;
				return {container: node, offset: textNodeOffset};
			}
	
			return {container: container, offset: offset};
		};
	
		var normalizeRange = function (rng) {
			var outRng = rng.cloneRange();
	
			var rangeStart = getNormalizedEndPoint(rng.startContainer, rng.startOffset);
			outRng.setStart(rangeStart.container, rangeStart.offset);
	
			var rangeEnd = getNormalizedEndPoint(rng.endContainer, rng.endOffset);
			outRng.setEnd(rangeEnd.container, rangeEnd.offset);
	
			return outRng;
		};
	
		return {
			getNormalizedEndPoint: getNormalizedEndPoint,
			normalizeRange: normalizeRange
		};
	});
	
	
	/**
	 * Bookmark.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.Bookmark", [
		"global!tinymce.dom.DOMUtils.DOM",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Range"
	], function (DOM, NodeType, Range) {
		/**
		 * Returns a range bookmark. This will convert indexed bookmarks into temporary span elements with
		 * index 0 so that they can be restored properly after the DOM has been modified. Text bookmarks will not have spans
		 * added to them since they can be restored after a dom operation.
		 *
		 * So this: <p><b>|</b><b>|</b></p>
		 * becomes: <p><b><span data-mce-type="bookmark">|</span></b><b data-mce-type="bookmark">|</span></b></p>
		 *
		 * @param  {DOMRange} rng DOM Range to get bookmark on.
		 * @return {Object} Bookmark object.
		 */
		var createBookmark = function (rng) {
			var bookmark = {};
	
			var setupEndPoint = function (start) {
				var offsetNode, container, offset;
	
				container = rng[start ? 'startContainer' : 'endContainer'];
				offset = rng[start ? 'startOffset' : 'endOffset'];
	
				if (container.nodeType === 1) {
					offsetNode = DOM.create('span', {'data-mce-type': 'bookmark'});
	
					if (container.hasChildNodes()) {
						offset = Math.min(offset, container.childNodes.length - 1);
	
						if (start) {
							container.insertBefore(offsetNode, container.childNodes[offset]);
						} else {
							DOM.insertAfter(offsetNode, container.childNodes[offset]);
						}
					} else {
						container.appendChild(offsetNode);
					}
	
					container = offsetNode;
					offset = 0;
				}
	
				bookmark[start ? 'startContainer' : 'endContainer'] = container;
				bookmark[start ? 'startOffset' : 'endOffset'] = offset;
			};
	
			setupEndPoint(true);
	
			if (!rng.collapsed) {
				setupEndPoint();
			}
	
			return bookmark;
		};
	
		var resolveBookmark = function (bookmark) {
			function restoreEndPoint (start) {
				var container, offset, node;
	
				var nodeIndex = function (container) {
					var node = container.parentNode.firstChild, idx = 0;
	
					while (node) {
						if (node === container) {
							return idx;
						}
	
						// Skip data-mce-type=bookmark nodes
						if (node.nodeType !== 1 || node.getAttribute('data-mce-type') !== 'bookmark') {
							idx++;
						}
	
						node = node.nextSibling;
					}
	
					return -1;
				};
	
				container = node = bookmark[start ? 'startContainer' : 'endContainer'];
				offset = bookmark[start ? 'startOffset' : 'endOffset'];
	
				if (!container) {
					return;
				}
	
				if (container.nodeType === 1) {
					offset = nodeIndex(container);
					container = container.parentNode;
					DOM.remove(node);
				}
	
				bookmark[start ? 'startContainer' : 'endContainer'] = container;
				bookmark[start ? 'startOffset' : 'endOffset'] = offset;
			}
	
			restoreEndPoint(true);
			restoreEndPoint();
	
			var rng = DOM.createRng();
	
			rng.setStart(bookmark.startContainer, bookmark.startOffset);
	
			if (bookmark.endContainer) {
				rng.setEnd(bookmark.endContainer, bookmark.endOffset);
			}
	
			return Range.normalizeRange(rng);
		};
	
		return {
			createBookmark: createBookmark,
			resolveBookmark: resolveBookmark
		};
	});
	
	
	/**
	 * NormalizeLists.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.NormalizeLists", [
		"global!tinymce.dom.DOMUtils.DOM",
		"global!tinymce.util.Tools",
		"tinymce.lists.core.NodeType"
	], function (DOM, Tools, NodeType) {
		var normalizeList = function (dom, ul) {
			var sibling, parentNode = ul.parentNode;
	
			// Move UL/OL to previous LI if it's the only child of a LI
			if (parentNode.nodeName === 'LI' && parentNode.firstChild === ul) {
				sibling = parentNode.previousSibling;
				if (sibling && sibling.nodeName === 'LI') {
					sibling.appendChild(ul);
	
					if (NodeType.isEmpty(dom, parentNode)) {
						DOM.remove(parentNode);
					}
				} else {
					DOM.setStyle(parentNode, 'listStyleType', 'none');
				}
			}
	
			// Append OL/UL to previous LI if it's in a parent OL/UL i.e. old HTML4
			if (NodeType.isListNode(parentNode)) {
				sibling = parentNode.previousSibling;
				if (sibling && sibling.nodeName === 'LI') {
					sibling.appendChild(ul);
				}
			}
		};
	
		var normalizeLists = function (dom, element) {
			Tools.each(Tools.grep(dom.select('ol,ul', element)), function (ul) {
				normalizeList(dom, ul);
			});
		};
	
		return {
			normalizeList: normalizeList,
			normalizeLists: normalizeLists
		};
	});
	
	
	defineGlobal("global!tinymce.dom.BookmarkManager", tinymce.dom.BookmarkManager);
	defineGlobal("global!tinymce.Env", tinymce.Env);
	/**
	 * TextBlock.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.TextBlock", [
		"global!tinymce.dom.DOMUtils.DOM",
		"global!tinymce.Env"
	], function (DOM, Env) {
		var createNewTextBlock = function (editor, contentNode, blockName) {
			var node, textBlock, fragment = DOM.createFragment(), hasContentNode;
			var blockElements = editor.schema.getBlockElements();
	
			if (editor.settings.forced_root_block) {
				blockName = blockName || editor.settings.forced_root_block;
			}
	
			if (blockName) {
				textBlock = DOM.create(blockName);
	
				if (textBlock.tagName === editor.settings.forced_root_block) {
					DOM.setAttribs(textBlock, editor.settings.forced_root_block_attrs);
				}
	
				fragment.appendChild(textBlock);
			}
	
			if (contentNode) {
				while ((node = contentNode.firstChild)) {
					var nodeName = node.nodeName;
	
					if (!hasContentNode && (nodeName !== 'SPAN' || node.getAttribute('data-mce-type') !== 'bookmark')) {
						hasContentNode = true;
					}
	
					if (blockElements[nodeName]) {
						fragment.appendChild(node);
						textBlock = null;
					} else {
						if (blockName) {
							if (!textBlock) {
								textBlock = DOM.create(blockName);
								fragment.appendChild(textBlock);
							}
	
							textBlock.appendChild(node);
						} else {
							fragment.appendChild(node);
						}
					}
				}
			}
	
			if (!editor.settings.forced_root_block) {
				fragment.appendChild(DOM.create('br'));
			} else {
				// BR is needed in empty blocks on non IE browsers
				if (!hasContentNode && (!Env.ie || Env.ie > 10)) {
					textBlock.appendChild(DOM.create('br', {'data-mce-bogus': '1'}));
				}
			}
	
			return fragment;
		};
	
		return {
			createNewTextBlock: createNewTextBlock
		};
	});
	
	/**
	 * SplitList.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.SplitList", [
		"global!tinymce.dom.DOMUtils.DOM",
		"global!tinymce.util.Tools",
		"tinymce.lists.core.TextBlock",
		"tinymce.lists.core.NodeType"
	], function (DOM, Tools, TextBlock, NodeType) {
		var splitList = function (editor, ul, li, newBlock) {
			var tmpRng, fragment, bookmarks, node;
	
			var removeAndKeepBookmarks = function (targetNode) {
				Tools.each(bookmarks, function (node) {
					targetNode.parentNode.insertBefore(node, li.parentNode);
				});
	
				DOM.remove(targetNode);
			};
	
			bookmarks = DOM.select('span[data-mce-type="bookmark"]', ul);
			newBlock = newBlock || TextBlock.createNewTextBlock(editor, li);
			tmpRng = DOM.createRng();
			tmpRng.setStartAfter(li);
			tmpRng.setEndAfter(ul);
			fragment = tmpRng.extractContents();
	
			for (node = fragment.firstChild; node; node = node.firstChild) {
				if (node.nodeName === 'LI' && editor.dom.isEmpty(node)) {
					DOM.remove(node);
					break;
				}
			}
	
			if (!editor.dom.isEmpty(fragment)) {
				DOM.insertAfter(fragment, ul);
			}
	
			DOM.insertAfter(newBlock, ul);
	
			if (NodeType.isEmpty(editor.dom, li.parentNode)) {
				removeAndKeepBookmarks(li.parentNode);
			}
	
			DOM.remove(li);
	
			if (NodeType.isEmpty(editor.dom, ul)) {
				DOM.remove(ul);
			}
		};
	
		return {
			splitList: splitList
		};
	});
	
	
	/**
	 * Outdent.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.actions.Outdent", [
		"global!tinymce.dom.DOMUtils.DOM",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Bookmark",
		"tinymce.lists.core.Selection",
		"tinymce.lists.core.SplitList",
		"tinymce.lists.core.NormalizeLists",
		"tinymce.lists.core.TextBlock"
	], function (DOM, NodeType, Bookmark, Selection, SplitList, NormalizeLists, TextBlock) {
		var removeEmptyLi = function (dom, li) {
			if (NodeType.isEmpty(dom, li)) {
				DOM.remove(li);
			}
		};
	
		var outdent = function (editor, li) {
			var ul = li.parentNode, ulParent = ul.parentNode, newBlock;
	
			if (ul === editor.getBody()) {
				return true;
			}
	
			if (li.nodeName === 'DD') {
				DOM.rename(li, 'DT');
				return true;
			}
	
			if (NodeType.isFirstChild(li) && NodeType.isLastChild(li)) {
				if (ulParent.nodeName === "LI") {
					DOM.insertAfter(li, ulParent);
					removeEmptyLi(editor.dom, ulParent);
					DOM.remove(ul);
				} else if (NodeType.isListNode(ulParent)) {
					DOM.remove(ul, true);
				} else {
					ulParent.insertBefore(TextBlock.createNewTextBlock(editor, li), ul);
					DOM.remove(ul);
				}
	
				return true;
			} else if (NodeType.isFirstChild(li)) {
				if (ulParent.nodeName === "LI") {
					DOM.insertAfter(li, ulParent);
					li.appendChild(ul);
					removeEmptyLi(editor.dom, ulParent);
				} else if (NodeType.isListNode(ulParent)) {
					ulParent.insertBefore(li, ul);
				} else {
					ulParent.insertBefore(TextBlock.createNewTextBlock(editor, li), ul);
					DOM.remove(li);
				}
	
				return true;
			} else if (NodeType.isLastChild(li)) {
				if (ulParent.nodeName === "LI") {
					DOM.insertAfter(li, ulParent);
				} else if (NodeType.isListNode(ulParent)) {
					DOM.insertAfter(li, ul);
				} else {
					DOM.insertAfter(TextBlock.createNewTextBlock(editor, li), ul);
					DOM.remove(li);
				}
	
				return true;
			}
	
			if (ulParent.nodeName === 'LI') {
				ul = ulParent;
				newBlock = TextBlock.createNewTextBlock(editor, li, 'LI');
			} else if (NodeType.isListNode(ulParent)) {
				newBlock = TextBlock.createNewTextBlock(editor, li, 'LI');
			} else {
				newBlock = TextBlock.createNewTextBlock(editor, li);
			}
	
			SplitList.splitList(editor, ul, li, newBlock);
			NormalizeLists.normalizeLists(editor.dom, ul.parentNode);
	
			return true;
		};
	
		var outdentSelection = function (editor) {
			var listElements = Selection.getSelectedListItems(editor);
	
			if (listElements.length) {
				var bookmark = Bookmark.createBookmark(editor.selection.getRng(true));
				var i, y, root = editor.getBody();
	
				i = listElements.length;
				while (i--) {
					var node = listElements[i].parentNode;
	
					while (node && node !== root) {
						y = listElements.length;
						while (y--) {
							if (listElements[y] === node) {
								listElements.splice(i, 1);
								break;
							}
						}
	
						node = node.parentNode;
					}
				}
	
				for (i = 0; i < listElements.length; i++) {
					if (!outdent(editor, listElements[i]) && i === 0) {
						break;
					}
				}
	
				editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
				editor.nodeChanged();
	
				return true;
			}
		};
	
		return {
			outdent: outdent,
			outdentSelection: outdentSelection
		};
	});
	
	
	/**
	 * ToggleList.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.actions.ToggleList", [
		"global!tinymce.util.Tools",
		"global!tinymce.dom.BookmarkManager",
		"tinymce.lists.core.Selection",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Bookmark",
		"tinymce.lists.core.SplitList",
		"tinymce.lists.core.NormalizeLists",
		"tinymce.lists.actions.Outdent"
	], function (Tools, BookmarkManager, Selection, NodeType, Bookmark, SplitList, NormalizeLists, Outdent) {
		var updateListStyle = function (dom, el, detail) {
			var type = detail['list-style-type'] ? detail['list-style-type'] : null;
			dom.setStyle(el, 'list-style-type', type);
		};
	
		var setAttribs = function (elm, attrs) {
			Tools.each(attrs, function (value, key) {
				elm.setAttribute(key, value);
			});
		};
	
		var updateListAttrs = function (dom, el, detail) {
			setAttribs(el, detail['list-attributes']);
			Tools.each(dom.select('li', el), function (li) {
				setAttribs(li, detail['list-item-attributes']);
			});
		};
	
		var updateListWithDetails = function (dom, el, detail) {
			updateListStyle(dom, el, detail);
			updateListAttrs(dom, el, detail);
		};
	
		var getEndPointNode = function (editor, rng, start) {
			var container, offset, root = editor.getBody();
	
			container = rng[start ? 'startContainer' : 'endContainer'];
			offset = rng[start ? 'startOffset' : 'endOffset'];
	
			// Resolve node index
			if (container.nodeType === 1) {
				container = container.childNodes[Math.min(offset, container.childNodes.length - 1)] || container;
			}
	
			while (container.parentNode !== root) {
				if (NodeType.isTextBlock(editor, container)) {
					return container;
				}
	
				if (/^(TD|TH)$/.test(container.parentNode.nodeName)) {
					return container;
				}
	
				container = container.parentNode;
			}
	
			return container;
		};
	
		var getSelectedTextBlocks = function (editor, rng) {
			var textBlocks = [], root = editor.getBody(), dom = editor.dom;
	
			var startNode = getEndPointNode(editor, rng, true);
			var endNode = getEndPointNode(editor, rng, false);
			var block, siblings = [];
	
			for (var node = startNode; node; node = node.nextSibling) {
				siblings.push(node);
	
				if (node === endNode) {
					break;
				}
			}
	
			Tools.each(siblings, function (node) {
				if (NodeType.isTextBlock(editor, node)) {
					textBlocks.push(node);
					block = null;
					return;
				}
	
				if (dom.isBlock(node) || NodeType.isBr(node)) {
					if (NodeType.isBr(node)) {
						dom.remove(node);
					}
	
					block = null;
					return;
				}
	
				var nextSibling = node.nextSibling;
				if (BookmarkManager.isBookmarkNode(node)) {
					if (NodeType.isTextBlock(editor, nextSibling) || (!nextSibling && node.parentNode === root)) {
						block = null;
						return;
					}
				}
	
				if (!block) {
					block = dom.create('p');
					node.parentNode.insertBefore(block, node);
					textBlocks.push(block);
				}
	
				block.appendChild(node);
			});
	
			return textBlocks;
		};
	
		var applyList = function (editor, listName, detail) {
			var rng = editor.selection.getRng(true), bookmark, listItemName = 'LI';
			var dom = editor.dom;
	
			detail = detail ? detail : {};
	
			if (dom.getContentEditable(editor.selection.getNode()) === "false") {
				return;
			}
	
			listName = listName.toUpperCase();
	
			if (listName === 'DL') {
				listItemName = 'DT';
			}
	
			bookmark = Bookmark.createBookmark(rng);
	
			Tools.each(getSelectedTextBlocks(editor, rng), function (block) {
				var listBlock, sibling;
	
				var hasCompatibleStyle = function (sib) {
					var sibStyle = dom.getStyle(sib, 'list-style-type');
					var detailStyle = detail ? detail['list-style-type'] : '';
	
					detailStyle = detailStyle === null ? '' : detailStyle;
	
					return sibStyle === detailStyle;
				};
	
				sibling = block.previousSibling;
				if (sibling && NodeType.isListNode(sibling) && sibling.nodeName === listName && hasCompatibleStyle(sibling)) {
					listBlock = sibling;
					block = dom.rename(block, listItemName);
					sibling.appendChild(block);
				} else {
					listBlock = dom.create(listName);
					block.parentNode.insertBefore(listBlock, block);
					listBlock.appendChild(block);
					block = dom.rename(block, listItemName);
				}
	
				updateListWithDetails(dom, listBlock, detail);
				mergeWithAdjacentLists(editor.dom, listBlock);
			});
	
			editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
		};
	
		var removeList = function (editor) {
			var bookmark = Bookmark.createBookmark(editor.selection.getRng(true)), root = editor.getBody();
			var listItems = Selection.getSelectedListItems(editor);
			var emptyListItems = Tools.grep(listItems, function (li) {
				return editor.dom.isEmpty(li);
			});
	
			listItems = Tools.grep(listItems, function (li) {
				return !editor.dom.isEmpty(li);
			});
	
			Tools.each(emptyListItems, function (li) {
				if (NodeType.isEmpty(editor.dom, li)) {
					Outdent.outdent(editor, li);
					return;
				}
			});
	
			Tools.each(listItems, function (li) {
				var node, rootList;
	
				if (li.parentNode === editor.getBody()) {
					return;
				}
	
				for (node = li; node && node !== root; node = node.parentNode) {
					if (NodeType.isListNode(node)) {
						rootList = node;
					}
				}
	
				SplitList.splitList(editor, rootList, li);
				NormalizeLists.normalizeLists(editor.dom, rootList.parentNode);
			});
	
			editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
		};
	
		var isValidLists = function (list1, list2) {
			return list1 && list2 && NodeType.isListNode(list1) && list1.nodeName === list2.nodeName;
		};
	
		var hasSameListStyle = function (dom, list1, list2) {
			var targetStyle = dom.getStyle(list1, 'list-style-type', true);
			var style = dom.getStyle(list2, 'list-style-type', true);
			return targetStyle === style;
		};
	
		var hasSameClasses = function (elm1, elm2) {
			return elm1.className === elm2.className;
		};
	
		var shouldMerge = function (dom, list1, list2) {
			return isValidLists(list1, list2) && hasSameListStyle(dom, list1, list2) && hasSameClasses(list1, list2);
		};
	
		var mergeWithAdjacentLists = function (dom, listBlock) {
			var sibling, node;
	
			sibling = listBlock.nextSibling;
			if (shouldMerge(dom, listBlock, sibling)) {
				while ((node = sibling.firstChild)) {
					listBlock.appendChild(node);
				}
	
				dom.remove(sibling);
			}
	
			sibling = listBlock.previousSibling;
			if (shouldMerge(dom, listBlock, sibling)) {
				while ((node = sibling.lastChild)) {
					listBlock.insertBefore(node, listBlock.firstChild);
				}
	
				dom.remove(sibling);
			}
		};
	
		var toggleList = function (editor, listName, detail) {
			var parentList = editor.dom.getParent(editor.selection.getStart(), 'OL,UL,DL');
	
			detail = detail ? detail : {};
	
			if (parentList === editor.getBody()) {
				return;
			}
	
			if (parentList) {
				if (parentList.nodeName === listName) {
					removeList(editor, listName);
				} else {
					var bookmark = Bookmark.createBookmark(editor.selection.getRng(true));
					updateListWithDetails(editor.dom, parentList, detail);
					mergeWithAdjacentLists(editor.dom, editor.dom.rename(parentList, listName));
					editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
				}
			} else {
				applyList(editor, listName, detail);
			}
		};
	
		return {
			toggleList: toggleList,
			removeList: removeList,
			mergeWithAdjacentLists: mergeWithAdjacentLists
		};
	});
	
	
	/**
	 * Delete.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.core.Delete", [
		"global!tinymce.dom.TreeWalker",
		"global!tinymce.dom.RangeUtils",
		"global!tinymce.util.VK",
		"tinymce.lists.core.Selection",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Bookmark",
		"tinymce.lists.core.Range",
		"tinymce.lists.core.NormalizeLists",
		"tinymce.lists.actions.ToggleList"
	], function (
		TreeWalker, RangeUtils, VK, Selection, NodeType, Bookmark, Range, NormalizeLists, ToggleList
	) {
		var findNextCaretContainer = function (editor, rng, isForward) {
			var node = rng.startContainer, offset = rng.startOffset;
			var nonEmptyBlocks, walker;
	
			if (node.nodeType === 3 && (isForward ? offset < node.data.length : offset > 0)) {
				return node;
			}
	
			nonEmptyBlocks = editor.schema.getNonEmptyElements();
			if (node.nodeType === 1) {
				node = RangeUtils.getNode(node, offset);
			}
	
			walker = new TreeWalker(node, editor.getBody());
	
			// Delete at <li>|<br></li> then jump over the bogus br
			if (isForward) {
				if (NodeType.isBogusBr(editor.dom, node)) {
					walker.next();
				}
			}
	
			while ((node = walker[isForward ? 'next' : 'prev2']())) {
				if (node.nodeName === 'LI' && !node.hasChildNodes()) {
					return node;
				}
	
				if (nonEmptyBlocks[node.nodeName]) {
					return node;
				}
	
				if (node.nodeType === 3 && node.data.length > 0) {
					return node;
				}
			}
		};
	
		var mergeLiElements = function (dom, fromElm, toElm) {
			var node, listNode, ul = fromElm.parentNode;
	
			if (!NodeType.isChildOfBody(dom, fromElm) || !NodeType.isChildOfBody(dom, toElm)) {
				return;
			}
	
			if (NodeType.isListNode(toElm.lastChild)) {
				listNode = toElm.lastChild;
			}
	
			if (ul === toElm.lastChild) {
				if (NodeType.isBr(ul.previousSibling)) {
					dom.remove(ul.previousSibling);
				}
			}
	
			node = toElm.lastChild;
			if (node && NodeType.isBr(node) && fromElm.hasChildNodes()) {
				dom.remove(node);
			}
	
			if (NodeType.isEmpty(dom, toElm, true)) {
				dom.$(toElm).empty();
			}
	
			if (!NodeType.isEmpty(dom, fromElm, true)) {
				while ((node = fromElm.firstChild)) {
					toElm.appendChild(node);
				}
			}
	
			if (listNode) {
				toElm.appendChild(listNode);
			}
	
			dom.remove(fromElm);
	
			if (NodeType.isEmpty(dom, ul) && ul !== dom.getRoot()) {
				dom.remove(ul);
			}
		};
	
		var backspaceDeleteFromListToListCaret = function (editor, isForward) {
			var dom = editor.dom, selection = editor.selection;
			var li = dom.getParent(selection.getStart(), 'LI'), ul, rng, otherLi;
	
			if (li) {
				ul = li.parentNode;
				if (ul === editor.getBody() && NodeType.isEmpty(dom, ul)) {
					return true;
				}
	
				rng = Range.normalizeRange(selection.getRng(true));
				otherLi = dom.getParent(findNextCaretContainer(editor, rng, isForward), 'LI');
	
				if (otherLi && otherLi !== li) {
					var bookmark = Bookmark.createBookmark(rng);
	
					if (isForward) {
						mergeLiElements(dom, otherLi, li);
					} else {
						mergeLiElements(dom, li, otherLi);
					}
	
					editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
	
					return true;
				} else if (!otherLi) {
					if (!isForward && ToggleList.removeList(editor, ul.nodeName)) {
						return true;
					}
				}
			}
	
			return false;
		};
	
		var backspaceDeleteIntoListCaret = function (editor, isForward) {
			var dom = editor.dom;
			var block = dom.getParent(editor.selection.getStart(), dom.isBlock);
	
			if (block && dom.isEmpty(block)) {
				var rng = Range.normalizeRange(editor.selection.getRng(true));
				var otherLi = dom.getParent(findNextCaretContainer(editor, rng, isForward), 'LI');
	
				if (otherLi) {
					editor.undoManager.transact(function () {
						dom.remove(block);
						ToggleList.mergeWithAdjacentLists(dom, otherLi.parentNode);
						editor.selection.select(otherLi, true);
						editor.selection.collapse(isForward);
					});
	
					return true;
				}
			}
	
			return false;
		};
	
		var backspaceDeleteCaret = function (editor, isForward) {
			return backspaceDeleteFromListToListCaret(editor, isForward) || backspaceDeleteIntoListCaret(editor, isForward);
		};
	
		var backspaceDeleteRange = function (editor) {
			var startListParent = editor.dom.getParent(editor.selection.getStart(), 'LI,DT,DD');
	
			if (startListParent || Selection.getSelectedListItems(editor).length > 0) {
				editor.undoManager.transact(function () {
					editor.execCommand('Delete');
					NormalizeLists.normalizeLists(editor.dom, editor.getBody());
				});
	
				return true;
			}
	
			return false;
		};
	
		var backspaceDelete = function (editor, isForward) {
			return editor.selection.isCollapsed() ? backspaceDeleteCaret(editor, isForward) : backspaceDeleteRange(editor);
		};
	
		var setup = function (editor) {
			editor.on('keydown', function (e) {
				if (e.keyCode === VK.BACKSPACE) {
					if (backspaceDelete(editor, false)) {
						e.preventDefault();
					}
				} else if (e.keyCode === VK.DELETE) {
					if (backspaceDelete(editor, true)) {
						e.preventDefault();
					}
				}
			});
		};
	
		return {
			setup: setup,
			backspaceDelete: backspaceDelete
		};
	});
	
	
	/**
	 * Indent.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.actions.Indent", [
		"global!tinymce.dom.DOMUtils.DOM",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Bookmark",
		"tinymce.lists.core.Selection"
	], function (DOM, NodeType, Bookmark, Selection) {
		var mergeLists = function (from, to) {
			var node;
	
			if (NodeType.isListNode(from)) {
				while ((node = from.firstChild)) {
					to.appendChild(node);
				}
	
				DOM.remove(from);
			}
		};
	
		var indent = function (li) {
			var sibling, newList, listStyle;
	
			if (li.nodeName === 'DT') {
				DOM.rename(li, 'DD');
				return true;
			}
	
			sibling = li.previousSibling;
	
			if (sibling && NodeType.isListNode(sibling)) {
				sibling.appendChild(li);
				return true;
			}
	
			if (sibling && sibling.nodeName === 'LI' && NodeType.isListNode(sibling.lastChild)) {
				sibling.lastChild.appendChild(li);
				mergeLists(li.lastChild, sibling.lastChild);
				return true;
			}
	
			sibling = li.nextSibling;
	
			if (sibling && NodeType.isListNode(sibling)) {
				sibling.insertBefore(li, sibling.firstChild);
				return true;
			}
	
			/*if (sibling && sibling.nodeName === 'LI' && isListNode(li.lastChild)) {
				return false;
			}*/
	
			sibling = li.previousSibling;
			if (sibling && sibling.nodeName === 'LI') {
				newList = DOM.create(li.parentNode.nodeName);
				listStyle = DOM.getStyle(li.parentNode, 'listStyleType');
				if (listStyle) {
					DOM.setStyle(newList, 'listStyleType', listStyle);
				}
				sibling.appendChild(newList);
				newList.appendChild(li);
				mergeLists(li.lastChild, newList);
				return true;
			}
	
			return false;
		};
	
		var indentSelection = function (editor) {
			var listElements = Selection.getSelectedListItems(editor);
	
			if (listElements.length) {
				var bookmark = Bookmark.createBookmark(editor.selection.getRng(true));
	
				for (var i = 0; i < listElements.length; i++) {
					if (!indent(listElements[i]) && i === 0) {
						break;
					}
				}
	
				editor.selection.setRng(Bookmark.resolveBookmark(bookmark));
				editor.nodeChanged();
	
				return true;
			}
		};
	
		return {
			indentSelection: indentSelection
		};
	});
	
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2017 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	define("tinymce.lists.Plugin", [
		"global!tinymce.PluginManager",
		"global!tinymce.util.Tools",
		"global!tinymce.util.VK",
		"tinymce.lists.core.NodeType",
		"tinymce.lists.core.Delete",
		"tinymce.lists.actions.Indent",
		"tinymce.lists.actions.Outdent",
		"tinymce.lists.actions.ToggleList"
	], function (PluginManager, Tools, VK, NodeType, Delete, Indent, Outdent, ToggleList) {
		var queryListCommandState = function (editor, listName) {
			return function () {
				var parentList = editor.dom.getParent(editor.selection.getStart(), 'UL,OL,DL');
				return parentList && parentList.nodeName === listName;
			};
		};
	
		var setupCommands = function (editor) {
			editor.on('BeforeExecCommand', function (e) {
				var cmd = e.command.toLowerCase(), isHandled;
	
				if (cmd === "indent") {
					if (Indent.indentSelection(editor)) {
						isHandled = true;
					}
				} else if (cmd === "outdent") {
					if (Outdent.outdentSelection(editor)) {
						isHandled = true;
					}
				}
	
				if (isHandled) {
					editor.fire('ExecCommand', {command: e.command});
					e.preventDefault();
					return true;
				}
			});
	
			editor.addCommand('InsertUnorderedList', function (ui, detail) {
				ToggleList.toggleList(editor, 'UL', detail);
			});
	
			editor.addCommand('InsertOrderedList', function (ui, detail) {
				ToggleList.toggleList(editor, 'OL', detail);
			});
	
			editor.addCommand('InsertDefinitionList', function (ui, detail) {
				ToggleList.toggleList(editor, 'DL', detail);
			});
		};
	
		var setupStateHandlers = function (editor) {
			editor.addQueryStateHandler('InsertUnorderedList', queryListCommandState(editor, 'UL'));
			editor.addQueryStateHandler('InsertOrderedList', queryListCommandState(editor, 'OL'));
			editor.addQueryStateHandler('InsertDefinitionList', queryListCommandState(editor, 'DL'));
		};
	
		var setupTabKey = function (editor) {
			editor.on('keydown', function (e) {
				// Check for tab but not ctrl/cmd+tab since it switches browser tabs
				if (e.keyCode !== 9 || VK.metaKeyPressed(e)) {
					return;
				}
	
				if (editor.dom.getParent(editor.selection.getStart(), 'LI,DT,DD')) {
					e.preventDefault();
	
					if (e.shiftKey) {
						Outdent.outdentSelection(editor);
					} else {
						Indent.indentSelection(editor);
					}
				}
			});
		};
	
		var setupUi = function (editor) {
			var listState = function (listName) {
				return function () {
					var self = this;
	
					editor.on('NodeChange', function (e) {
						var lists = Tools.grep(e.parents, NodeType.isListNode);
						self.active(lists.length > 0 && lists[0].nodeName === listName);
					});
				};
			};
	
			var hasPlugin = function (editor, plugin) {
				var plugins = editor.settings.plugins ? editor.settings.plugins : '';
				return Tools.inArray(plugins.split(/[ ,]/), plugin) !== -1;
			};
	
			if (!hasPlugin(editor, 'advlist')) {
				editor.addButton('numlist', {
					title: 'Numbered list',
					cmd: 'InsertOrderedList',
					onPostRender: listState('OL')
				});
	
				editor.addButton('bullist', {
					title: 'Bullet list',
					cmd: 'InsertUnorderedList',
					onPostRender: listState('UL')
				});
			}
	
			editor.addButton('indent', {
				icon: 'indent',
				title: 'Increase indent',
				cmd: 'Indent',
				onPostRender: function (e) {
					var ctrl = e.control;
	
					editor.on('nodechange', function () {
						var blocks = editor.selection.getSelectedBlocks();
						var disable = false;
	
						for (var i = 0, l = blocks.length; !disable && i < l; i++) {
							var tag = blocks[i].nodeName;
	
							disable = (tag === 'LI' && NodeType.isFirstChild(blocks[i]) || tag === 'UL' || tag === 'OL' || tag === 'DD');
						}
	
						ctrl.disabled(disable);
					});
				}
			});
		};
	
		PluginManager.add('lists', function (editor) {
			setupUi(editor);
			Delete.setup(editor);
	
			editor.on('init', function () {
				setupCommands(editor);
				setupStateHandlers(editor);
				setupTabKey(editor);
			});
	
			return {
				backspaceDelete: function (isForward) {
					Delete.backspaceDelete(editor, isForward);
				}
			};
		});
	
		return function () {};
	});
	
	
	dem('tinymce.lists.Plugin')();
	})();
	
	}.call(window));

/***/ },

/***/ 265:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('charmap', function(editor) {
		var isArray = tinymce.util.Tools.isArray;
	
		function getDefaultCharMap() {
			return [
				['160', 'no-break space'],
				['173', 'soft hyphen'],
				['34', 'quotation mark'],
			// finance
				['162', 'cent sign'],
				['8364', 'euro sign'],
				['163', 'pound sign'],
				['165', 'yen sign'],
			// signs
				['169', 'copyright sign'],
				['174', 'registered sign'],
				['8482', 'trade mark sign'],
				['8240', 'per mille sign'],
				['181', 'micro sign'],
				['183', 'middle dot'],
				['8226', 'bullet'],
				['8230', 'three dot leader'],
				['8242', 'minutes / feet'],
				['8243', 'seconds / inches'],
				['167', 'section sign'],
				['182', 'paragraph sign'],
				['223', 'sharp s / ess-zed'],
			// quotations
				['8249', 'single left-pointing angle quotation mark'],
				['8250', 'single right-pointing angle quotation mark'],
				['171', 'left pointing guillemet'],
				['187', 'right pointing guillemet'],
				['8216', 'left single quotation mark'],
				['8217', 'right single quotation mark'],
				['8220', 'left double quotation mark'],
				['8221', 'right double quotation mark'],
				['8218', 'single low-9 quotation mark'],
				['8222', 'double low-9 quotation mark'],
				['60', 'less-than sign'],
				['62', 'greater-than sign'],
				['8804', 'less-than or equal to'],
				['8805', 'greater-than or equal to'],
				['8211', 'en dash'],
				['8212', 'em dash'],
				['175', 'macron'],
				['8254', 'overline'],
				['164', 'currency sign'],
				['166', 'broken bar'],
				['168', 'diaeresis'],
				['161', 'inverted exclamation mark'],
				['191', 'turned question mark'],
				['710', 'circumflex accent'],
				['732', 'small tilde'],
				['176', 'degree sign'],
				['8722', 'minus sign'],
				['177', 'plus-minus sign'],
				['247', 'division sign'],
				['8260', 'fraction slash'],
				['215', 'multiplication sign'],
				['185', 'superscript one'],
				['178', 'superscript two'],
				['179', 'superscript three'],
				['188', 'fraction one quarter'],
				['189', 'fraction one half'],
				['190', 'fraction three quarters'],
			// math / logical
				['402', 'function / florin'],
				['8747', 'integral'],
				['8721', 'n-ary sumation'],
				['8734', 'infinity'],
				['8730', 'square root'],
				['8764', 'similar to'],
				['8773', 'approximately equal to'],
				['8776', 'almost equal to'],
				['8800', 'not equal to'],
				['8801', 'identical to'],
				['8712', 'element of'],
				['8713', 'not an element of'],
				['8715', 'contains as member'],
				['8719', 'n-ary product'],
				['8743', 'logical and'],
				['8744', 'logical or'],
				['172', 'not sign'],
				['8745', 'intersection'],
				['8746', 'union'],
				['8706', 'partial differential'],
				['8704', 'for all'],
				['8707', 'there exists'],
				['8709', 'diameter'],
				['8711', 'backward difference'],
				['8727', 'asterisk operator'],
				['8733', 'proportional to'],
				['8736', 'angle'],
			// undefined
				['180', 'acute accent'],
				['184', 'cedilla'],
				['170', 'feminine ordinal indicator'],
				['186', 'masculine ordinal indicator'],
				['8224', 'dagger'],
				['8225', 'double dagger'],
			// alphabetical special chars
				['192', 'A - grave'],
				['193', 'A - acute'],
				['194', 'A - circumflex'],
				['195', 'A - tilde'],
				['196', 'A - diaeresis'],
				['197', 'A - ring above'],
				['256', 'A - macron'],
				['198', 'ligature AE'],
				['199', 'C - cedilla'],
				['200', 'E - grave'],
				['201', 'E - acute'],
				['202', 'E - circumflex'],
				['203', 'E - diaeresis'],
				['274', 'E - macron'],
				['204', 'I - grave'],
				['205', 'I - acute'],
				['206', 'I - circumflex'],
				['207', 'I - diaeresis'],
				['298', 'I - macron'],
				['208', 'ETH'],
				['209', 'N - tilde'],
				['210', 'O - grave'],
				['211', 'O - acute'],
				['212', 'O - circumflex'],
				['213', 'O - tilde'],
				['214', 'O - diaeresis'],
				['216', 'O - slash'],
				['332', 'O - macron'],
				['338', 'ligature OE'],
				['352', 'S - caron'],
				['217', 'U - grave'],
				['218', 'U - acute'],
				['219', 'U - circumflex'],
				['220', 'U - diaeresis'],
				['362', 'U - macron'],
				['221', 'Y - acute'],
				['376', 'Y - diaeresis'],
				['562', 'Y - macron'],
				['222', 'THORN'],
				['224', 'a - grave'],
				['225', 'a - acute'],
				['226', 'a - circumflex'],
				['227', 'a - tilde'],
				['228', 'a - diaeresis'],
				['229', 'a - ring above'],
				['257', 'a - macron'],
				['230', 'ligature ae'],
				['231', 'c - cedilla'],
				['232', 'e - grave'],
				['233', 'e - acute'],
				['234', 'e - circumflex'],
				['235', 'e - diaeresis'],
				['275', 'e - macron'],
				['236', 'i - grave'],
				['237', 'i - acute'],
				['238', 'i - circumflex'],
				['239', 'i - diaeresis'],
				['299', 'i - macron'],
				['240', 'eth'],
				['241', 'n - tilde'],
				['242', 'o - grave'],
				['243', 'o - acute'],
				['244', 'o - circumflex'],
				['245', 'o - tilde'],
				['246', 'o - diaeresis'],
				['248', 'o slash'],
				['333', 'o macron'],
				['339', 'ligature oe'],
				['353', 's - caron'],
				['249', 'u - grave'],
				['250', 'u - acute'],
				['251', 'u - circumflex'],
				['252', 'u - diaeresis'],
				['363', 'u - macron'],
				['253', 'y - acute'],
				['254', 'thorn'],
				['255', 'y - diaeresis'],
				['563', 'y - macron'],
				['913', 'Alpha'],
				['914', 'Beta'],
				['915', 'Gamma'],
				['916', 'Delta'],
				['917', 'Epsilon'],
				['918', 'Zeta'],
				['919', 'Eta'],
				['920', 'Theta'],
				['921', 'Iota'],
				['922', 'Kappa'],
				['923', 'Lambda'],
				['924', 'Mu'],
				['925', 'Nu'],
				['926', 'Xi'],
				['927', 'Omicron'],
				['928', 'Pi'],
				['929', 'Rho'],
				['931', 'Sigma'],
				['932', 'Tau'],
				['933', 'Upsilon'],
				['934', 'Phi'],
				['935', 'Chi'],
				['936', 'Psi'],
				['937', 'Omega'],
				['945', 'alpha'],
				['946', 'beta'],
				['947', 'gamma'],
				['948', 'delta'],
				['949', 'epsilon'],
				['950', 'zeta'],
				['951', 'eta'],
				['952', 'theta'],
				['953', 'iota'],
				['954', 'kappa'],
				['955', 'lambda'],
				['956', 'mu'],
				['957', 'nu'],
				['958', 'xi'],
				['959', 'omicron'],
				['960', 'pi'],
				['961', 'rho'],
				['962', 'final sigma'],
				['963', 'sigma'],
				['964', 'tau'],
				['965', 'upsilon'],
				['966', 'phi'],
				['967', 'chi'],
				['968', 'psi'],
				['969', 'omega'],
			// symbols
				['8501', 'alef symbol'],
				['982', 'pi symbol'],
				['8476', 'real part symbol'],
				['978', 'upsilon - hook symbol'],
				['8472', 'Weierstrass p'],
				['8465', 'imaginary part'],
			// arrows
				['8592', 'leftwards arrow'],
				['8593', 'upwards arrow'],
				['8594', 'rightwards arrow'],
				['8595', 'downwards arrow'],
				['8596', 'left right arrow'],
				['8629', 'carriage return'],
				['8656', 'leftwards double arrow'],
				['8657', 'upwards double arrow'],
				['8658', 'rightwards double arrow'],
				['8659', 'downwards double arrow'],
				['8660', 'left right double arrow'],
				['8756', 'therefore'],
				['8834', 'subset of'],
				['8835', 'superset of'],
				['8836', 'not a subset of'],
				['8838', 'subset of or equal to'],
				['8839', 'superset of or equal to'],
				['8853', 'circled plus'],
				['8855', 'circled times'],
				['8869', 'perpendicular'],
				['8901', 'dot operator'],
				['8968', 'left ceiling'],
				['8969', 'right ceiling'],
				['8970', 'left floor'],
				['8971', 'right floor'],
				['9001', 'left-pointing angle bracket'],
				['9002', 'right-pointing angle bracket'],
				['9674', 'lozenge'],
				['9824', 'black spade suit'],
				['9827', 'black club suit'],
				['9829', 'black heart suit'],
				['9830', 'black diamond suit'],
				['8194', 'en space'],
				['8195', 'em space'],
				['8201', 'thin space'],
				['8204', 'zero width non-joiner'],
				['8205', 'zero width joiner'],
				['8206', 'left-to-right mark'],
				['8207', 'right-to-left mark']
			];
		}
	
		function charmapFilter(charmap) {
			return tinymce.util.Tools.grep(charmap, function(item) {
				return isArray(item) && item.length == 2;
			});
		}
	
		function getCharsFromSetting(settingValue) {
			if (isArray(settingValue)) {
				return [].concat(charmapFilter(settingValue));
			}
	
			if (typeof settingValue == "function") {
				return settingValue();
			}
	
			return [];
		}
	
		function extendCharMap(charmap) {
			var settings = editor.settings;
	
			if (settings.charmap) {
				charmap = getCharsFromSetting(settings.charmap);
			}
	
			if (settings.charmap_append) {
				return [].concat(charmap).concat(getCharsFromSetting(settings.charmap_append));
			}
	
			return charmap;
		}
	
		function getCharMap() {
			return extendCharMap(getDefaultCharMap());
		}
	
		function insertChar(chr) {
			editor.fire('insertCustomChar', {chr: chr}).chr;
			editor.execCommand('mceInsertContent', false, chr);
		}
	
		function showDialog() {
			var gridHtml, x, y, win;
	
			function getParentTd(elm) {
				while (elm) {
					if (elm.nodeName == 'TD') {
						return elm;
					}
	
					elm = elm.parentNode;
				}
			}
	
			gridHtml = '<table role="presentation" cellspacing="0" class="mce-charmap"><tbody>';
	
			var charmap = getCharMap();
			var width = Math.min(charmap.length, 25);
			var height = Math.ceil(charmap.length / width);
			for (y = 0; y < height; y++) {
				gridHtml += '<tr>';
	
				for (x = 0; x < width; x++) {
					var index = y * width + x;
					if (index < charmap.length) {
						var chr = charmap[index];
						var chrText = chr ? String.fromCharCode(parseInt(chr[0], 10)) : '&nbsp;';
	
						gridHtml += (
							'<td title="' + chr[1] + '"><div tabindex="-1" title="' + chr[1] + '" role="button" data-chr="' + chrText + '">' +
								chrText +
							'</div></td>'
						);
					} else {
						gridHtml += '<td />';
					}
				}
	
				gridHtml += '</tr>';
			}
	
			gridHtml += '</tbody></table>';
	
			var charMapPanel = {
				type: 'container',
				html: gridHtml,
				onclick: function(e) {
					var target = e.target;
	
					if (/^(TD|DIV)$/.test(target.nodeName)) {
						if (getParentTd(target).firstChild) {
							insertChar(target.getAttribute('data-chr'));
	
							if (!e.ctrlKey) {
								win.close();
							}
						}
					}
				},
				onmouseover: function(e) {
					var td = getParentTd(e.target);
	
					if (td && td.firstChild) {
						win.find('#preview').text(td.firstChild.firstChild.data);
						win.find('#previewTitle').text(td.title);
					} else {
						win.find('#preview').text(' ');
						win.find('#previewTitle').text(' ');
					}
				}
			};
	
			win = editor.windowManager.open({
				title: "Special character",
				spacing: 10,
				padding: 10,
				items: [
					charMapPanel,
					{
						type: 'container',
						layout: 'flex',
						direction: 'column',
						align: 'center',
						spacing: 5,
						minWidth: 160,
						minHeight: 160,
						items: [
							{
								type: 'label',
								name: 'preview',
								text: ' ',
								style: 'font-size: 40px; text-align: center',
								border: 1,
								minWidth: 140,
								minHeight: 80
							},
							{
								type: 'label',
								name: 'previewTitle',
								text: ' ',
								style: 'text-align: center',
								border: 1,
								minWidth: 140,
								minHeight: 80
							}
						]
					}
				],
				buttons: [
					{text: "Close", onclick: function() {
						win.close();
					}}
				]
			});
		}
	
		editor.addCommand('mceShowCharmap', showDialog);
	
		editor.addButton('charmap', {
			icon: 'charmap',
			tooltip: 'Special character',
			cmd: 'mceShowCharmap'
		});
	
		editor.addMenuItem('charmap', {
			icon: 'charmap',
			text: 'Special character',
			cmd: 'mceShowCharmap',
			context: 'insert'
		});
	
		return {
			getCharMap: getCharMap,
			insertChar: insertChar
		};
	});
	
	}.call(window));

/***/ },

/***/ 266:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('print', function(editor) {
		editor.addCommand('mcePrint', function() {
			editor.getWin().print();
		});
	
		editor.addButton('print', {
			title: 'Print',
			cmd: 'mcePrint'
		});
	
		editor.addShortcut('Meta+P', '', 'mcePrint');
	
		editor.addMenuItem('print', {
			text: 'Print',
			cmd: 'mcePrint',
			icon: 'print',
			shortcut: 'Meta+P',
			context: 'file'
		});
	});
	
	}.call(window));

/***/ },

/***/ 267:
/***/ function(module, exports) {

	/*** IMPORTS FROM imports-loader ***/
	(function() {
	
	/**
	 * plugin.js
	 *
	 * Released under LGPL License.
	 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
	 *
	 * License: http://www.tinymce.com/license
	 * Contributing: http://www.tinymce.com/contributing
	 */
	
	/*global tinymce:true */
	
	tinymce.PluginManager.add('preview', function(editor) {
		var settings = editor.settings, sandbox = !tinymce.Env.ie;
	
		editor.addCommand('mcePreview', function() {
			editor.windowManager.open({
				title: 'Preview',
				width: parseInt(editor.getParam("plugin_preview_width", "650"), 10),
				height: parseInt(editor.getParam("plugin_preview_height", "500"), 10),
				html: '<iframe src="javascript:\'\'" frameborder="0"' + (sandbox ? ' sandbox="allow-scripts"' : '') + '></iframe>',
				buttons: {
					text: 'Close',
					onclick: function() {
						this.parent().parent().close();
					}
				},
				onPostRender: function() {
					var previewHtml, headHtml = '';
	
					headHtml += '<base href="' + editor.documentBaseURI.getURI() + '">';
	
					tinymce.each(editor.contentCSS, function(url) {
						headHtml += '<link type="text/css" rel="stylesheet" href="' + editor.documentBaseURI.toAbsolute(url) + '">';
					});
	
					var bodyId = settings.body_id || 'tinymce';
					if (bodyId.indexOf('=') != -1) {
						bodyId = editor.getParam('body_id', '', 'hash');
						bodyId = bodyId[editor.id] || bodyId;
					}
	
					var bodyClass = settings.body_class || '';
					if (bodyClass.indexOf('=') != -1) {
						bodyClass = editor.getParam('body_class', '', 'hash');
						bodyClass = bodyClass[editor.id] || '';
					}
	
					var preventClicksOnLinksScript = (
						'<script>' +
							'document.addEventListener && document.addEventListener("click", function(e) {' +
								'for (var elm = e.target; elm; elm = elm.parentNode) {' +
									'if (elm.nodeName === "A") {' +
										'e.preventDefault();' +
									'}' +
								'}' +
							'}, false);' +
						'</script> '
					);
	
					var dirAttr = editor.settings.directionality ? ' dir="' + editor.settings.directionality + '"' : '';
	
					previewHtml = (
						'<!DOCTYPE html>' +
						'<html>' +
						'<head>' +
							headHtml +
						'</head>' +
						'<body id="' + bodyId + '" class="mce-content-body ' + bodyClass + '"' + dirAttr + '>' +
							editor.getContent() +
							preventClicksOnLinksScript +
						'</body>' +
						'</html>'
					);
	
					if (!sandbox) {
						// IE 6-11 doesn't support data uris on iframes
						// so I guess they will have to be less secure since we can't sandbox on those
						// TODO: Use sandbox if future versions of IE supports iframes with data: uris.
						var doc = this.getEl('body').firstChild.contentWindow.document;
						doc.open();
						doc.write(previewHtml);
						doc.close();
					} else {
						this.getEl('body').firstChild.src = 'data:text/html;charset=utf-8,' + encodeURIComponent(previewHtml);
					}
				}
			});
		});
	
		editor.addButton('preview', {
			title: 'Preview',
			cmd: 'mcePreview'
		});
	
		editor.addMenuItem('preview', {
			text: 'Preview',
			cmd: 'mcePreview',
			context: 'view'
		});
	});
	
	}.call(window));

/***/ }

})
//# sourceMappingURL=1.3aa83be1d6b2a42f4c81.hot-update.js.map