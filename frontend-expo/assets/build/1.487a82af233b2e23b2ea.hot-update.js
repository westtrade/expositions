webpackHotUpdate(1,{

/***/ 110:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var safeapply = __webpack_require__(60);
	var registrationConstraints = __webpack_require__(68);
	
	var _require = __webpack_require__(69),
	    formApply = _require.formApply;
	
	module.exports = ['$scope', '$modals', '$element', 'User', function ($scope, $element, $modals, User) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		User.bind($scope);
	
		var _$element = _slicedToArray($element, 1),
		    form = _$element[0];
	
		formApply($scope, form, registrationConstraints);
	
		$scope.submit = function ($event) {
	
			form.validate(function () {
				var err = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
				var resultData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
			});
	
			$event.preventDefault();
		};
	}];

/***/ }

})
//# sourceMappingURL=1.487a82af233b2e23b2ea.hot-update.js.map