webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var qwery = __webpack_require__(39);
	var onePixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=';
	
	var newMarkerId = function newMarkerId() {
		return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
	};
	
	// style
	
	var createMarker = function createMarker(masonryId) {
		var marker = new Image();
	
		marker.setAttribute('data-masonry-id', masonryId);
	
		marker.src = onePixel;
		marker.style.position = 'fixed';
		marker.style.left = '0';
		marker.style.top = '0';
	
		return marker;
	};
	
	var markNgRepeat = function markNgRepeat(masonryId, item) {
		var marker = createMarker(masonryId);
		item.setAttribute('data-masonry-id', masonryId);
		item.appendChild(marker);
	};
	
	var markNgRepeatStart = function markNgRepeatStart(masonryId, item) {};
	
	var markItem = function markItem(repeatItem) {
	
		var ngRepeat = repeatItemsList.getAttribute('ng-repeat');
		var ngRepeatStart = repeatItemsList.getAttribute('ng-repeat');
	
		var masonryId = newMarkerId();
	
		if (ngRepeat) {
			markNgRepeat(masonryId, item);
		} else if (ngRepeatStart) {
			markNgRepeatStart(masonryId, item);
		}
	
		return masonryId;
	};
	
	module.exports = {
		markItem: markItem,
		onePixel: onePixel
	};

/***/ }

})
//# sourceMappingURL=1.24e1587224f483fae4da.hot-update.js.map