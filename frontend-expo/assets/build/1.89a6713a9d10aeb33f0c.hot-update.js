webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			// transclude: 'element',
			restrict: 'C',
			priority: 600,
			terminal: true,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var modalSource = $element[0].outerHTML;
	
				$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
					var modal = void 0;
	
					if (isOpened) {
						modal = angular.element(modalSource).removeClass('modal-source').addClass('modal selected');
	
						var $modalScope = $rootScope.$new(true);
						$modalScope.initial = data;
						$modalScope.modalOpened = true;
						modal = $compile(modal)($modalScope);
					} else if (modal) {
						modal.removeClass('selected');
						modal.scope().$destroy();
						modal = null;
					}
				});
	
				// let newModal = null;
				// $transclude(function(clone, newScope) {
				// 	const modalSource = clone[0].outerHTML;
				// 	$rootScope.$on('modal-toggled', (event, modalId, isOpened, data) => {
				//
				// 		if (modalId != $attr.id) {
				// 			return ;
				// 		}
				//
				// 		if (isOpened) {
				// 			let modalElement = angular.element(modalSource);
				// 			modalElement.removeClass('modal-source').addClass('modal selected');
				// 			$element.after(modalElement);
				//
				// 			const $modalScope = $rootScope.$new(true);
				// 			$modalScope.initial = data;
				// 			$modalScope.modalOpened = true;
				//
				// 			newModal = $compile(modalElement)($modalScope);
				//
				// 		} else if (newModal) {
				//
				// 			newModal.scope().$destroy();
				// 			newModal.remove();
				// 			newModal = null;
				// 		}
				// 	});
				//
				// });
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.89a6713a9d10aeb33f0c.hot-update.js.map