webpackHotUpdate(1,{

/***/ 47:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($compile) {
		return {
			restrict: 'EC',
			link: function link($scope, $element) {
				var element = $element[0];
				var spanWrapper = document.createElement('span');
				spanWrapper.innerHTML = element.innerHTML;
				element.innerHTML = '';
				element.appendChild(spanWrapper);
	
				// element.innerHTML = `<span>${element.innerHTML}</span>`;
	
				$compile(spanWrapper)($scope);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.f3bf07257d706b7ff85d.hot-update.js.map