webpackHotUpdate(1,{

/***/ 8:
/***/ function(module, exports) {

	'use strict';
	
	var routes = function routes($stateProvider) {
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login'
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration'
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.853125bc5da0ffcc3e49.hot-update.js.map