webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var qwery = __webpack_require__(39);
	var onePixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=';
	
	var newMarkerId = function newMarkerId() {
		return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
	};
	
	// style
	
	var createMarker = function createMarker(masonryId) {
	
		var marker = new Image();
	
		marker.setAttribute('data-masonry-id', masonryId);
	
		marker.src = onePixel;
		marker.style.position = 'fixed';
		marker.style.left = '0';
		marker.style.top = '0';
	
		return marker;
	};
	
	var markElement = function markElement(masonryId, item) {
		var marker = createMarker(masonryId);
		item.setAttribute('data-masonry-id', masonryId);
		item.appendChild(marker);
	};
	
	var markNgRepeatStart = function markNgRepeatStart(masonryId, item) {};
	
	var elementIsChanged = function elementIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var ngRepeatStartIsChanged = function ngRepeatStartIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var markItem = function markItem() {
		var info = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var item = arguments[1];
		var _info$added = info.added,
		    added = _info$added === undefined ? [] : _info$added,
		    _info$changed = info.changed,
		    changed = _info$changed === undefined ? [] : _info$changed;
	
	
		var masonryId = item.getAttribute('data-masonry-id');
		var ngRepeatStart = item.getAttribute('ng-repeat');
	
		if (masonryId) {
			var isChanged = ngRepeatStart ? ngRepeatStartIsChanged(masonryId, item) : elementIsChanged(masonryId, item);
	
			if (isChanged) {
				changed.push(masonryId);
				info.changed = changed;
			}
	
			return info;
		}
	
		// const ngRepeat = item.getAttribute('ng-repeat');
	
		// if (!ngRepeat && !ngRepeatStart) {
		// 	return info;
		// }
	
		masonryId = newMarkerId();
	
		console.log(item);
	
		ngRepeatStart ? markNgRepeatStart(masonryId, item) : markElement(masonryId, item);
	
		added.push(masonryId);
		info.added = added;
	
		return info;
	};
	
	module.exports = {
		markItem: markItem,
		onePixel: onePixel
	};

/***/ }

})
//# sourceMappingURL=1.53a61d8bcd29533ed752.hot-update.js.map