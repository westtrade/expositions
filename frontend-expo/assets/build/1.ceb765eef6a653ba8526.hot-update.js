webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	var getErrors = function getErrors() {
		var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
	
		var result = [];
	
		if (input && input.length) {
			try {
				result = JSON.parse(input);
			} catch (e) {
				console.error(e);
			}
		}
	
		return result;
	};
	
	module.exports = ['$parse', function ($parse) {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			replace: true,
			scope: true,
			link: function link($scope, $element, $attrs) {
				// $scope.error = getErrors($attrs.error);
				// $attrs.$observe('error', (value) => {
				// 	console.log(value);
				// 	$scope.parsedError = getErrors(value);
				// });
	
				// $scope.$watch($attrs.error, () => {
				//
				// })
	
				// console.log($scope);
	
				$scope.$parent.$watch($attrs.error, function (currentValue) {
					console.log(currentValue);
				}, true);
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.ceb765eef6a653ba8526.hot-update.js.map