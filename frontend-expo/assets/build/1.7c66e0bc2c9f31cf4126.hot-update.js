webpackHotUpdate(1,{

/***/ 105:
/***/ function(module, exports) {

	'use strict';
	
	var constraint = {
		full_name: {
			presence: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
		email: {
			presence: true,
			email: true,
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		},
		phone: {
			presence: true,
			forma: {
				pattern: /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
			},
			length: {
				minimum: 6,
				message: "must be at least 6 characters"
			}
		}
	};
	
	module.exports = constraint;

/***/ }

})
//# sourceMappingURL=1.7c66e0bc2c9f31cf4126.hot-update.js.map