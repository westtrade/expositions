webpackHotUpdate(1,{

/***/ 64:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/recovery-page.html';
	var html = "<form class=\"card center\">\n\n\t<div ng-if=\"!email\">\n\t\t<div class=\"offset\">\n\t\t\t<div class=\"field text-center bigger\">\n\t\t\t\tЕсть аккаунт? <a href=\"#\" ui-sref=\"user-login\" >Войти</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"separator\"></div>\n\t\t<div class=\"offset\">\n\n\t\t\t<h1>Восстановление доступа</h1>\n\t\t\t<div class=\"field\">\n\t\t\t\tВведите адрес электронной почты, который был указан при регистрации.\n\t\t\t</div>\n\t\t\t<br>\n\n\t\t\t<input type=\"email\" placeholder=\"Email\" name=\"email\" ng-class=\"{error: error.email }\" >\t\t\n\t\t\t<div ng-if=\"error.password\" class=\"message\">\n\t\t\t\t<div ng-repeat=\"message in error.email\">{{message}}</div>\n\t\t\t</div>\n\t\t\t<br>\n\t\t\t<br>\n\t\t\t<br>\n\t\t\t<br>\n\n\t\t\t<button ng-click=\"setMail()\" class=\"primary\" type=\"submit\">Сбросить пароль</button>\n\t\t\t<br>\n\t\t</div>\n\t</div>\n\t<div ng-if=\"email\" class=\"offset\">\n\t\t<h1>Восстановление доступа</h1>\n\n\t\t<p>\n\t\t\tНа электронную почту <strong>{{email}}</strong> отправлено письмо. Перейдите по\n\t\t\tссылке в письме, чтобы задать новый пароль от Вашей учетной записи.\n\t\t</p>\n\n\t\t<p>\n\t\t\tЕсли Вы не увидите письма, обязательно проверьте папку СПАМ.\n\t\t</p>\n\n\t\t<br>\n\t\t<br>\n\n\t\t<a class=\"button primary\" href=\"#\" ui-sref=\"home\">Вернуться на главную</a>\n\t</div>\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.7b6fe7d3cda2b9182200.hot-update.js.map