webpackHotUpdate(1,{

/***/ 67:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var validate = __webpack_require__(68);
	var safeApply = __webpack_require__(60);
	
	var getFormData = function getFormData(form) {
		return Object.entries(form.elements).reduce(toData, {});
	};
	
	var validateForm = function validateForm(form) {
		var constraint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		var data = getFormData(form);
		// TODO REFACTOR!
		return validate.async(data, constraint).then(function (field) {
			return { data: data, error: null, field: field };
		}).catch(function (error) {
			return { data: data, error: error, field: null };
		});
	};
	
	var toData = function toData() {
		var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var _ref = arguments[1];
	
		var _ref2 = _slicedToArray(_ref, 2),
		    fieldName = _ref2[0],
		    input = _ref2[1];
	
		result[fieldName] = input.value;
		return result;
	};
	
	var formApply = function formApply($scope, form, constraint) {
		var cb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (err, data) {};
	
	
		var isDirty = false;
	
		$scope.form = $scope.form ? $scope.form : {};
		$scope.error = $scope.error ? $scope.error : {};
	
		var name = form.name;
		if (!name || !name.length) {
			name = Array.from(document.forms).indexOf(form).toString();
		}
	
		$scope.form[name] = $scope.form[name] ? $scope.form[name] : getFormData(form);
	
		form.addEventListener('input', function (event) {
	
			var constraintName = event.target.name;
	
			var fieldConstraint = constraintName in constraint ? constraint[constraintName] : null;
	
			validateForm(form, _defineProperty({}, constraintName, fieldConstraint)).then(function (_ref3) {
				var error = _ref3.error,
				    data = _ref3.data;
	
	
				safeApply($scope, function () {
					$scope.form[name][constraintName] = data[constraintName];
				});
	
				safeApply($scope, function () {
					if (isDirty) {
	
						if (!error) {
							delete $scope.error[constraintName];
						} else {
							Object.assign($scope.error, error);
						}
					}
				});
			});
		}, true);
	
		var validate = function validate() {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {
				var error = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
				var result = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			};
	
	
			isDirty = true;
	
			validateForm(form, constraint).then(function (_ref4) {
				var error = _ref4.error,
				    data = _ref4.data;
	
				if (error) {
					safeApply($scope, function () {
						$scope.error = error;
					});
				}
	
				cb(error, data);
			}).catch(function (e) {
				return cb(e, null);
			});
		};
	
		$scope.form[name].validate = validate;
		form.validate = validate;
	};
	
	module.exports = {
		getFormData: getFormData,
		validateForm: validateForm,
		formApply: formApply,
		toData: toData
	};

/***/ }

})
//# sourceMappingURL=1.89bd8c89fa9a1b908f84.hot-update.js.map