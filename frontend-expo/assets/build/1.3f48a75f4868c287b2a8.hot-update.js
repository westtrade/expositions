webpackHotUpdate(1,{

/***/ 241:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var qwery = __webpack_require__(93);
	
	var template = '\n\t<div\n\t\tclass="input"\n\t\tmoment-picker="value"\n\t\tlocale="ru"\n\t\ttoday="true"\n\t\tformat="L"\n\t\tmax-view="month"\n\t\tstart-view="month"\n\t\tplaceholder="\u0414\u0430\u0442\u0430 \u043D\u0430\u0447\u0430\u043B\u0430"\n\t>\n\t{{ value }}\n\t</div>\n\t<input type="hidden">\n\t<ng-transclude></ng-transclude>\n';
	
	module.exports = function () {
		return {
			template: template,
			transclude: true,
			scope: {},
			link: function link($scope, $element, $attrs, controller, $transclude) {
	
				var element = $element[0];
	
				var _qwery = qwery('input', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    input = _qwery2[0];
	
				var _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name,
				    _$attrs$locale = $attrs.locale,
				    locale = _$attrs$locale === undefined ? 'ru' : _$attrs$locale,
				    _$attrs$today = $attrs.today,
				    today = _$attrs$today === undefined ? 'true' : _$attrs$today,
				    _$attrs$maxView = $attrs.maxView,
				    maxView = _$attrs$maxView === undefined ? 'month' : _$attrs$maxView,
				    _$attrs$startView = $attrs.startView,
				    startView = _$attrs$startView === undefined ? 'month' : _$attrs$startView,
				    _$attrs$format = $attrs.format,
				    format = _$attrs$format === undefined ? 'L' : _$attrs$format,
				    _$attrs$minView = $attrs.minView,
				    minView = _$attrs$minView === undefined ? 'day' : _$attrs$minView,
				    _$attrs$placeholder = $attrs.placeholder,
				    placeholder = _$attrs$placeholder === undefined ? '' : _$attrs$placeholder;
	
	
				input.setAttribute('name', name);
	
				$scope.$watch('value', function (value) {
					if (name && name.length) {
						input.form.setValue(name, value);
					}
				});
	
				console.log(name, locale, maxView);
	
				$transclude(function (clone) {});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.3f48a75f4868c287b2a8.hot-update.js.map