webpackHotUpdate(1,{

/***/ 16:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var uiRouter = __webpack_require__(19);
	
	__webpack_require__(20);
	var loadingBar = __webpack_require__(21);
	var ngAnimate = __webpack_require__(23);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, 'chieffancypants.loadingBar']);
	var router = __webpack_require__(25);
	app.config(router);
	
	var topBar = __webpack_require__(31);
	app.directive('topBar', topBar);
	
	var checkbox = __webpack_require__(33);
	app.directive('styledCheckbox', checkbox);
	
	var expoSlider = __webpack_require__(43);
	app.directive('expoSlider', expoSlider);
	
	var common = __webpack_require__(46);
	Object.entries(common).reduce(function (app, _ref) {
	  var _ref2 = _slicedToArray(_ref, 2),
	      name = _ref2[0],
	      filter = _ref2[1];
	
	  return app.filter(name, filter);
	}, app);
	
	var button = __webpack_require__(47);
	app.directive('button', button);
	
	var offersCatalog = __webpack_require__(48);
	app.controller('offersCatalog', offersCatalog);
	
	var subjectSelectorModal = __webpack_require__(49);
	app.controller('subjectSelectorModal', subjectSelectorModal);
	
	var expositionSelectorModal = __webpack_require__(56);
	app.controller('expositionSelectorModal', expositionSelectorModal);
	
	var socialLogin = __webpack_require__(50);
	app.directive('socialLogin', socialLogin);
	
	var Modal = __webpack_require__(51);
	app.factory('$modals', Modal);
	var modals = __webpack_require__(52);
	app.directive('expoModalManager', modals);
	
	var modal = __webpack_require__(54);
	app.directive('modalSource', modal);
	
	var User = __webpack_require__(55);
	app.factory('User', User);
	
	module.exports = app;

/***/ },

/***/ 56:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var safeapply = __webpack_require__(44);
	
	var MODAL_MODES = ['catalog', 'filters'];
	
	module.exports = ['$scope', '$element', '$modals', function ($scope, $element, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		$scope.mode = MODAL_MODES[0];
	
		$scope.switchMode = function (mode) {
			var modeId = 0;
			if (!mode) {
				modeId = MODAL_MODES.indexOf($scope.mode) === 0 ? 1 : 0;
			}
	
			$scope.mode = MODAL_MODES[modeId];
		};
	
		$scope.close = function () {
			return $modals.close();
		};
		$scope.select = function () {
	
			// const selected = Object.values($scope.subjects).reduce((result, items) =>  result
			// 	.concat(items.filter(item => item.selected))
			// 	.map(item => angular.copy(item)), []
			// );
			//
			$modals.close();
		};
	}];

/***/ }

})
//# sourceMappingURL=1.41dcaa94e815941f199b.hot-update.js.map