webpackHotUpdate(1,{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(44);
	var templateUrl = __webpack_require__(45);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var qwery = __webpack_require__(39);
	
	var SliderTimer = function () {
		function SliderTimer() {
			_classCallCheck(this, SliderTimer);
	
			this.currentTimer = null;
			this.$scope = null;
			this.timeout = 500;
		}
	
		_createClass(SliderTimer, [{
			key: 'run',
			value: function run($scope) {
				var _this = this;
	
				var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
	
	
				this.$scope = $scope;
				this.timeout = timeout;
	
				this.currentTimer = setTimeout(function ($scope, currentTimerId, timeout) {
					$scope.next(true);
					clearTimeout(currentTimerId);
					_this.currentTimer = null;
					_this.run($scope, timeout);
				}, timeout, this.$scope, this.currentTimer, timeout);
			}
		}, {
			key: 'pause',
			value: function pause() {
				if (this.currentTimer) {
					clearTimeout(this.currentTimer);
					this.currentTimer = null;
				}
			}
		}, {
			key: 'resume',
			value: function resume() {
				if (this.$scope) {
					clearTimeout(this.currentTimer);
					this.currentTimer = null;
					this.run(this.$scope, this.timeout);
				}
			}
		}]);
	
		return SliderTimer;
	}();
	
	;
	
	var sliderTimer = new SliderTimer();
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
		var _$element = _slicedToArray($element, 1),
		    element = _$element[0];
	
		element.addEventListener('mouseenter', function (_ref) {
			var target = _ref.target;
	
			console.log(target);
		});
		element.addEventListener('mouseleave', function (_ref2) {
			var target = _ref2.target;
	
			console.log(target);
		});
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
				var isAutomatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
	
				if (!isAutomatic) {
					sliderTimer.pause();
				}
	
				safeApply($scope, function () {
					$scope.active = active;
	
					qwery('.slide', $element[0]).forEach(function (item, idx) {
						if (item.classList.contains('active')) {
							item.classList.remove('active');
						}
	
						if (idx === active) {
							item.classList.add('active');
						}
					});
	
					if (!isAutomatic) {
						sliderTimer.resume();
					}
				});
			};
	
			$scope.next = function () {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($scope.active);
				var next = current + 1;
				current = next >= $scope.total ? 0 : next;
				$scope.setActive(current, isAutomatic);
			};
	
			$scope.prev = function () {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($scope.active);
				var prev = current - 1;
				current = prev <= 0 ? $scope.total - 1 : prev;
				$scope.setActive(current, isAutomatic);
			};
	
			sliderTimer.run($scope, 4000);
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.eb65f7a03ef32e6b1ee4.hot-update.js.map