webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	var calculateHeight = function calculateHeight(element) {
		console.log(element.scrollHeight);
	};
	
	module.exports = function ($scope, $element, cfpLoadingBar) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0
		};
	
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				console.log(catalog[2]);
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				calculateHeight(element);
	
				cfpLoadingBar.complete();
				$scope.loading = false;
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		document.addEventListener('scroll', function (event) {
			// const contentIsScrolled = element.scrollHeight - document.body.scrollTop >= element.clientHeight;
			var contentIsScrolled = element.scrollHeight - document.body.scrollTop - 400 <= 0; //todo fix this bug
	
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		});
	};

/***/ }

})
//# sourceMappingURL=1.5c9ac321670b40634972.hot-update.js.map