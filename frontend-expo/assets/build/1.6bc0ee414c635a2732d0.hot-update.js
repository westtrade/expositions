webpackHotUpdate(1,{

/***/ 110:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeapply = __webpack_require__(60);
	var registrationConstraints = __webpack_require__(68);
	
	var _require = __webpack_require__(69),
	    formApply = _require.formApply;
	
	module.exports = ['$scope', '$modals', '$element', 'User', function ($scope, $element, $modals, User) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		User.bind($scope);
		var form = $element[0];
		formApply($scope, form, registrationConstraints);
	
		$scope.submit = function ($event) {
	
			form.validate(function () {
				var err = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
				var resultData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
			});
	
			$event.preventDefault();
		};
	}];

/***/ }

})
//# sourceMappingURL=1.6bc0ee414c635a2732d0.hot-update.js.map