webpackHotUpdate(1,{

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var loadMap = __webpack_require__(110);
	var qwery = __webpack_require__(90);
	
	var setValue = function setValue(input, value) {
	
		input.value = ngModel.$modelValue;
	
		var changeEvent = new Event('change');
		input.dispatchEvent(changeEvent);
		var inputEvent = new Event('input');
		input.dispatchEvent(inputEvent);
	};
	
	var providers = {
	
		yandex: {
	
			init: function init(element) {
	
				var suggestView = new global.ymaps.SuggestView(element);
				suggestView.events.add('select', function (event) {
					console.log(event);
				});
	
				element.$destroy = function () {
					suggestView.destroy();
				};
			},
	
			destroy: function destroy() {}
		},
	
		google: {
	
			init: function init() {},
			destroy: function destroy() {}
		}
	};
	
	var changeProvider = function changeProvider(element, current, prev) {
	
		if (current === prev) {
			return current;
		}
	
		if (prev && prev in providers) {
			providers[prev].destroy(element);
		}
	
		if (current in providers) {
			providers[current].init(element);
		}
	
		return current;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function link($scope, $element, $attrs, ngModel) {
				var selectorElement = $element[0];
	
				if (ngModel && ngModel.$modelValue) {
					setValue(selectorElement, ngModel.$modelValue);
				}
	
				var current = changeProvider(selectorElement, $attrs.provider || 'yandex');
				loadMap(function () {
	
					$attrs.$observe('provider', function (next) {
						current = changeProvider(selectorElement, next, current);
					});
	
					console.log('place selector isLoaded');
				});
			}
		};
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.8e7ce98b3dbd6c8575de.hot-update.js.map