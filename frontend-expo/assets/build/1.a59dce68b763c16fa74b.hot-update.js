webpackHotUpdate(1,{

/***/ 107:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(108);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
					$transclude($scope, function (clonedContent) {});
				};
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.a59dce68b763c16fa74b.hot-update.js.map