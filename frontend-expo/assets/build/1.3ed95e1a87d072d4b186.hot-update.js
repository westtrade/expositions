webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	var THEME = 'lightgray';
	__webpack_require__(232)("./" + THEME + '/content.min.css');
	__webpack_require__(236)("./" + THEME + '/skin.min.css');
	
	__webpack_require__(270);
	
	__webpack_require__(247);
	__webpack_require__(248);
	__webpack_require__(249);
	__webpack_require__(250);
	__webpack_require__(251);
	__webpack_require__(252);
	__webpack_require__(253);
	__webpack_require__(254);
	__webpack_require__(255);
	__webpack_require__(256);
	__webpack_require__(257);
	
	var content_style = '\n\nbody, html {\n\tpadding: 0;\n\tmargin: 0;\n\tfont-family: "Roboto", sans-serif;\n}\n\n#tinymce {\n\tpadding: 18px!important;\n\n}\n\nbody {\n\t    padding: 18px;\n\t\tcolor: #3d3d3d;\n}\n\np {\n\tmargin: 0 0 5px 0;\n}\n';
	
	module.exports = ['$modals', function ($modals) {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				var onChange = function onChange(event) {
					console.log(event.type);
				};
	
				element.addEventListener('change', onChange);
				element.addEventListener('input', onChange);
	
				var instance = tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
	
						editor.on('change', function (e) {
							// console.log('change event', e);
							// editor.getContent()
							element.value = editor.getContent();
	
							var changeEvent = new Event('change');
							element.dispatchEvent(inputEvent);
						});
	
						editor.on('input', function (e) {
							// console.log('input event', e);
							// editor.getContent()
	
							// console.log(element.value);
							element.value = editor.getContent();
	
							var inputEvent = new Event('input');
							element.dispatchEvent(inputEvent);
						});
	
						editor.addButton('expoimage', {
							icon: 'image',
							onclick: function onclick() {
	
								$modals.open('');
							}
						});
	
						editor.addButton('expolink', {
							icon: 'image',
							onclick: function onclick() {
	
								$modals.open('');
							}
						});
					},
	
					visual: false,
					statusbar: false,
					content_style: content_style,
					// height: 500,
					menubar: false,
					// toolbar: 'undo redo | styleselect | bold italic | link image',
					toolbar: 'undo redo | bold italic | link image expoimage',
					skin: false,
					// inline: true,
					plugins: 'paste link autoresize textpattern advlist autolink link image lists charmap print preview',
					// block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
					textpattern_patterns: [{ start: '*', end: '*', format: 'italic' }, { start: '**', end: '**', format: 'bold' },
					// {start: '#', format: 'h1'},
					// {start: '##', format: 'h2'},
					// {start: '###', format: 'h3'},
					// {start: '####', format: 'h4'},
					// {start: '#####', format: 'h5'},
					// {start: '######', format: 'h6'},
					{ start: '1. ', cmd: 'InsertOrderedList' }, { start: '* ', cmd: 'InsertUnorderedList' }, { start: '- ', cmd: 'InsertUnorderedList' }]
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.3ed95e1a87d072d4b186.hot-update.js.map