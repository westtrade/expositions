webpackHotUpdate(1,{

/***/ 61:
/***/ function(module, exports) {

	'use strict';
	
	var customEvents = {
		onResizeEnd: []
	};
	
	var delta = 5;
	var timer = false,
	    startTime = void 0;
	
	var onResizeEnd = function onResizeEnd() {
	
		if (new Date() - startTime < delta) {
			timer = setTimeout(onResizeEnd, delta);
		} else {
			timer = false;
	
			if ('onResizeEnd' in customEvents) {
				customEvents['onResizeEnd'].forEach(function (fn) {
					return fn();
				});
			}
		}
	};
	
	window.addEventListener('resize', function () {
		startTime = new Date();
		if (timer === false) {
			timer = setTimeout(onResizeEnd, delta);
		}
	}, true);
	
	module.exports = customEvents;

/***/ }

})
//# sourceMappingURL=1.9c876b76f7de7ee1e077.hot-update.js.map