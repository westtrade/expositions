webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var angular = __webpack_require__(11);
	var $body = angular.element(document.body);
	// const $rootScope = angular.injector().get('$rootScope');
	
	console.log($body.parent(0));
	
	var ModalManager = function () {
		function ModalManager() {
			_classCallCheck(this, ModalManager);
	
			this.active_window = null;
		}
	
		_createClass(ModalManager, [{
			key: 'isActive',
			value: function isActive(modalId) {
				return modalId && modalId.length ? this.getActiveWindowID() === modalId : !!this.getActiveWindowID();
			}
		}, {
			key: 'getActiveWindowID',
			value: function getActiveWindowID() {
				var openedModalID = $body.attr('data-active-modal');
				return openedModalID;
			}
		}, {
			key: 'open',
			value: function open(openingModalWindowId) {
				var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
				// let activeModalID = this.getActiveWindowID();
				this.close();
	
				if (!openingModalWindowId) {
					//TODO error message
					return false;
				}
	
				$body.attr('data-active-modal', openingModalWindowId);
				$rootScope.$broadcast('modal-toggled', openingModalWindowId, true);
	
				return true;
			}
		}, {
			key: 'close',
			value: function close() {
	
				var modalWindowId = this.getActiveWindowID();
	
				$body.removeAttr('data-active-modal');
				$rootScope.$broadcast('modal-toggled', modalWindowId, false);
	
				return false;
			}
		}, {
			key: 'toggleModal',
			value: function toggleModal() {
	
				return this.isActive(openModalId) ? this.close() : this.open(openModalId);
			}
		}]);
	
		return ModalManager;
	}();
	
	var modalManager = new ModalManager();
	global.$modals = modalManager;
	
	module.exports = function () {
		return modalManager;
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.cec832e4c7621326a917.hot-update.js.map