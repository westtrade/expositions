webpackHotUpdate(1,{

/***/ 108:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(75),
	    shortId = _require.shortId;
	
	var currentTabId = shortId();
	
	var getStorage = function getStorage() {
		try {
			if ('localStorage' in window && window['localStorage'] !== null) {
				return localStorage;
			}
		} catch (e) {
			return false;
		}
	
		return false;
	};
	
	var on = function on() {
		var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'message';
		var messageReciever = arguments[1];
	
		window.addEventListener("storage", function (event) {
			if (event.key === type) {
				var _JSON$parse = JSON.parse(event.newValue),
				    data = _JSON$parse.data,
				    recievedTabId = _JSON$parse.currentTabId;
	
				if (currentTabId !== recievedTabId) {
					messageReciever(event, data);
				}
			}
		});
	};
	
	var broadcast = function broadcast(type) {
		var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		var storage = getStorage();
	
		if (storage) {
			var eventData = {
				currentTabId: currentTabId,
				data: data
			};
	
			storage.setItem(type, JSON.stringify(eventData));
		} else {
			console.error('Local storage dosn\'t support!');
		}
	};
	
	module.exports = {
		getStorage: getStorage,
		on: on,
		broadcast: broadcast
	};

/***/ }

})
//# sourceMappingURL=1.ebfffb38424ddb1892f5.hot-update.js.map