webpackHotUpdate(1,{

/***/ 47:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($compile) {
		return {
			restrict: 'EC',
			link: function link($scope, $element) {
				var element = $element[0];
				var spanWrapper = document.createElement('span');
				spanWrapper.innerHTML = element.innerHTML;
				element.innerHTML = '';
				element.appendChild(spanWrapper);
				$compile(spanWrapper)($scope);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.e1c0f166e3b89e61e80b.hot-update.js.map