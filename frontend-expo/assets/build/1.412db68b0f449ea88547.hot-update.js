webpackHotUpdate(1,{

/***/ 108:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(75),
	    shortId = _require.shortId;
	
	var currentTabId = shortId;
	
	var getStorage = function getStorage() {
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
			return false;
		}
	};
	
	var on = function on() {
		var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'message';
		var messageReciever = arguments[1];
	
		window.addEventListener("storage", function (event) {
			if (event.key === type) {
				cb(event, JSON.parse(event.newValue));
			}
		});
	};
	
	var broadcast = function broadcast(type) {
		var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		var storage = getStorage();
	
		if (storage) {
			storage.setItem(type, JSON.stringify(data));
		} else {
			console.error('Local storage dosn\'t support!');
		}
	};
	
	module.exports = {
		getStorage: getStorage,
		on: on,
		broadcast: broadcast
	};

/***/ }

})
//# sourceMappingURL=1.412db68b0f449ea88547.hot-update.js.map