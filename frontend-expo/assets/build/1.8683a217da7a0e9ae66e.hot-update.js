webpackHotUpdate(1,{

/***/ 76:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/login-page.html';
	var html = "<form class=\"card center\" ng-submit=\"submit($event)\" name=\"login-form\">\n\t<div class=\"offset\">\n\t\t<h1>Вход</h1>\n\t\t<input type=\"text\" name=\"login\" ng-class=\"{error: error.login.length }\" placeholder=\"Введите номер телефона или email\">\n\t\t<div ng-if=\"error.login.length\" class=\"message\">\n\t\t\t<div ng-repeat=\"message in error.login\">{{message}}</div>\n\t\t</div>\n\n\t\t<input type=\"password\" name=\"password\" ng-class=\"{error: error.password }\" placeholder=\"Введите пароль\">\n\t\t<field-error field=\"password\"></field-error>\n\n\n\t\t<div class=\"field\">\n\t\t\t<a href=\"#\" ui-sref=\"user-recovery\" >Забыли пароль?</a>\n\t\t</div>\n\t\t<button class=\"primary\" type=\"submit\">Войти</button>\n\t\t<div class=\"separator\">\n\t\t\t<div class=\"label\">или</div>\n\t\t</div>\n\n\t\t<div class=\"two-buttons\">\n\t\t\t<a class=\"fb button\" social-login=\"facebook\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-facebook fa-fw\"></i>\n\t\t\t</a><a class=\"vk button\" social-login=\"vkontakte\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-vk fa-fw\"></i>\n\t\t\t</a>\n\t\t</div>\n\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\t\t<div class=\"field bigger text-center\">\n\t\t\tНет аккаунта? <a href=\"#\" ui-sref=\"user-registration\" >Регистрация</a>\n\t\t</div>\n\t</div>\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.8683a217da7a0e9ae66e.hot-update.js.map