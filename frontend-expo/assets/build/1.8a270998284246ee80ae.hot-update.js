webpackHotUpdate(1,{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(44);
	var uiRouter = __webpack_require__(46);
	var uiDND = __webpack_require__(47);
	
	global.moment = __webpack_require__(128);
	__webpack_require__(52);
	__webpack_require__(238);
	
	__webpack_require__(54);
	__webpack_require__(55);
	
	var loadingBar = __webpack_require__(56);
	var ngAnimate = __webpack_require__(58);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'moment-picker', 'chieffancypants.loadingBar']);
	
	var router = __webpack_require__(60);
	app.config(router);
	
	var common = __webpack_require__(86);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(87);
	
	app.directive('button', __webpack_require__(97)).directive('expoSlider', __webpack_require__(98)).directive('topBar', __webpack_require__(100)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(102)).directive('masonryLayout', __webpack_require__(103)).directive('expoModalManager', __webpack_require__(106)).directive('modalSource', __webpack_require__(108)).directive('expoTabs', __webpack_require__(109)).directive('fieldError', __webpack_require__(110)).directive('inputMap', __webpack_require__(112)).directive('placeAutocomplete', __webpack_require__(116)).directive('dropzoneField', __webpack_require__(239)).directive('datetimeField', __webpack_require__(241));
	
	app.controller('offersCatalog', __webpack_require__(117)).controller('subjectSelectorModal', __webpack_require__(119)).controller('sliderController', __webpack_require__(121)).controller('expositionSelectorModal', __webpack_require__(122)).controller('sliderEditorModal', __webpack_require__(123)).controller('offerTeaserController', __webpack_require__(124));
	
	app.factory('$modals', __webpack_require__(125)).factory('User', __webpack_require__(126));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },

/***/ 241:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var qwery = __webpack_require__(93);
	
	var template = '\n\t<div\n\t\tclass="input"\n\t\tmoment-picker="value"\n\t\tlocale="ru"\n\t\ttoday="true"\n\t\tformat="L"\n\t\tmax-view="month"\n\t\tstart-view="month"\n\t\tplaceholder="\u0414\u0430\u0442\u0430 \u043D\u0430\u0447\u0430\u043B\u0430"\n\t></div>\n\t<input type="hidden" name="startDate" ng-value="exposition.startDate">\n\t{{ form[\'create-offer\'].startDate }}\n\t<ng-transclude></ng-transclude>\n';
	
	module.exports = function () {
		return {
			template: template,
			transclude: true,
			scope: {},
			link: function link($state, $element, $attrs, controller, $transclude) {
	
				var element = $element[0];
				var input = qwery('input', element);
				var _$attrs$name = $attrs.name,
				    name = _$attrs$name === undefined ? '' : _$attrs$name,
				    _$attrs$locale = $attrs.locale,
				    locale = _$attrs$locale === undefined ? 'ru' : _$attrs$locale,
				    _$attrs$today = $attrs.today,
				    today = _$attrs$today === undefined ? 'true' : _$attrs$today,
				    _$attrs$maxView = $attrs.maxView,
				    maxView = _$attrs$maxView === undefined ? 'month' : _$attrs$maxView,
				    _$attrs$startView = $attrs.startView,
				    startView = _$attrs$startView === undefined ? 'month' : _$attrs$startView;
	
	
				console.log(name, locale, maxView);
	
				$transclude(function (clone) {});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.8a270998284246ee80ae.hot-update.js.map