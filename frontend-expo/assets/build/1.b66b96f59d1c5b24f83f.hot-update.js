webpackHotUpdate(1,{

/***/ 72:
/***/ function(module, exports) {

	'use strict';
	
	var translate = {
		'Login can\'t be blank': 'Поле `Логин` обязательно для заполнения',
		'Login must be at least 6 characters': 'Поле `Логин` должно быть больше 6 символов',
		'Password can\'t be blank': 'Поле `Пароль` обязательно для заполнения',
		'Password must be at least 6 characters': 'Поле `Пароль` должно быть больше 6 символов',
		'Full name can\'t be blank': 'Поле `Имя, Фамилия, Отчество` обязательно для заполнения',
		'Full name must be at least 6 characters': 'Поле `Имя, Фамилия, Отчество` должно быть больше 6 символов',
		'Email can\'t be blank': 'Поле Email обязательно для заполнения',
		'Email must be at least 6 characters': 'Поле `Email` должно быть больше 6 символов',
		'Email is not a valid email': 'Поле `Email` заполнено не правильно',
		'Phone can\'t be blank': 'Поле `Телефон` обязательно для заполнения',
		'Phone must be at least 6 characters': 'Поле `Телефон` должно быть больше 6 символов',
		'Repeat can\'t be blank': 'Поле `Повторите пароль` обязательно для заполнения',
		'Captcha can\'t be blank': 'Поле `Код` обязательно для заполнения'
	};
	
	module.exports = translate;

/***/ }

})
//# sourceMappingURL=1.b66b96f59d1c5b24f83f.hot-update.js.map