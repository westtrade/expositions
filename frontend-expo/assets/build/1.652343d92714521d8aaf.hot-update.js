webpackHotUpdate(1,{

/***/ 36:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(37);
	var uiRouter = __webpack_require__(39);
	var uiDND = __webpack_require__(40);
	
	var dropzone = __webpack_require__(42);
	var ngDropzone = __webpack_require__(44);
	
	__webpack_require__(45);
	__webpack_require__(46);
	__webpack_require__(47);
	
	var loadingBar = __webpack_require__(48);
	var ngAnimate = __webpack_require__(50);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'chieffancypants.loadingBar', 'thatisuday.dropzone']);
	
	var router = __webpack_require__(52);
	app.config(router);
	
	var common = __webpack_require__(76);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(77);
	
	app.directive('button', __webpack_require__(87)).directive('expoSlider', __webpack_require__(88)).directive('topBar', __webpack_require__(90)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(92)).directive('masonryLayout', __webpack_require__(93)).directive('expoModalManager', __webpack_require__(96)).directive('modalSource', __webpack_require__(98)).directive('expoTabs', __webpack_require__(99)).directive('placeSelector', __webpack_require__(100)).directive('fieldError', __webpack_require__(103)).directive('placeAutocompleteField', __webpack_require__(118));
	
	app.controller('offersCatalog', __webpack_require__(105)).controller('subjectSelectorModal', __webpack_require__(107)).controller('sliderController', __webpack_require__(109)).controller('expositionSelectorModal', __webpack_require__(110)).controller('sliderEditorModal', __webpack_require__(111)).controller('offerTeaserController', __webpack_require__(112));
	
	app.factory('$modals', __webpack_require__(113)).factory('User', __webpack_require__(114));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;

/***/ },

/***/ 118:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	//
	// const getChildren = (element) => {
	// 	console.log(element);
	// };
	
	
	// onDelete - rebuild tree under element (nextSibling)
	// onSize (element - maybe load image) change - rebuild tree under  (nextSibling) - image on load -> id -> rebuildElement by id (style changed ?)
	// onAdd
	//
	// (element) => nextSibling -> whileEnd
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var qwery = __webpack_require__(83);
	
	var _require = __webpack_require__(119),
	    markItem = _require.markItem,
	    onePixel = _require.onePixel;
	
	var elementPosiition = function elementPosiition(container, element) {
		var containerPosition = container.getBoundingClientRect();
		var elementPosition = element.getBoundingClientRect();
	
		var width = elementPosition.width;
		var height = elementPosition.height;
		var left = elementPosition.left - containerPosition.left;
		var top = elementPosition.top - containerPosition.top;
	
		return { width: width, height: height, left: left, top: top };
	
		// const name = value;
	};
	
	var calculateGutter = function calculateGutter(container, first) {
		var modulo = container.offsetWidth % first.offsetWidth;
		var columns = (container.offsetWidth - modulo) / first.offsetWidth;
		var gutter = parseInt(modulo / (columns - 1));
	
		return { gutter: gutter, columns: columns };
	};
	
	var calculateContainerHeight = function calculateContainerHeight(container) {
		var info = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		var gutter = info.gutter,
		    columns = info.columns;
	
	
		var height = 0;
	
		var lastElement = container.lastElementChild;
		if (!lastElement) {
			return height;
		}
	
		var count = columns;
		while (count-- && lastElement) {
			var position = elementPosiition(container, lastElement);
			var currentHeight = position.top + lastElement.offsetHeight;
	
			if (currentHeight > height) {
				height = currentHeight;
			}
	
			lastElement = lastElement.previousElementSibling;
		}
	
		return height;
	};
	
	var updateContainerHeight = function updateContainerHeight(container) {
		var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
		    gutter = _ref.gutter,
		    columns = _ref.columns;
	
		var containerHeight = calculateContainerHeight(container, { gutter: gutter, columns: columns });
		container.style.height = containerHeight + 'px';
	};
	
	var rebuildItem = function rebuildItem(container, current) {
		var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
		    _ref2$gutter = _ref2.gutter,
		    gutter = _ref2$gutter === undefined ? 32 : _ref2$gutter,
		    _ref2$columns = _ref2.columns,
		    columns = _ref2$columns === undefined ? 4 : _ref2$columns,
		    _ref2$onlyOne = _ref2.onlyOne,
		    onlyOne = _ref2$onlyOne === undefined ? true : _ref2$onlyOne;
	
		var previous = current.previousElementSibling;
		if (!previous) {
	
			current.style.top = '0px';
			current.style.left = '0px';
	
			var info = calculateGutter(container, current);
	
			gutter = info.gutter;
			columns = info.columns;
	
			return { gutter: gutter, columns: columns };
		}
	
		// const container = value;
		var positionPrev = elementPosiition(container, previous);
		var left = positionPrev.left + positionPrev.width + gutter;
		var isOverflowed = left + current.offsetWidth > container.offsetWidth;
	
		var childrenArray = Array.from(container.children);
		var currentPosition = childrenArray.indexOf(current);
	
		var prevUpperPosition = currentPosition - columns;
		var prevUpperElement = childrenArray[prevUpperPosition];
	
		var top = 0;
		if (prevUpperElement) {
			var upperPosition = elementPosiition(container, prevUpperElement);
			top = upperPosition.top + upperPosition.height + gutter;
		}
	
		if (!isOverflowed) {
			current.style.left = left + 'px';
		} else {
			current.style.left = '0px';
		}
	
		current.style.top = top + 'px';
	
		if (onlyOne) {
			updateContainerHeight(container, { gutter: gutter, columns: columns });
		}
	
		return { gutter: gutter, columns: columns };
	};
	
	var rebuildItems = function rebuildItems(containerElement, startElement) {
		var endElement = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	
		var _ref3 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
		    _ref3$gutter = _ref3.gutter,
		    gutter = _ref3$gutter === undefined ? 32 : _ref3$gutter,
		    _ref3$columns = _ref3.columns,
		    columns = _ref3$columns === undefined ? 4 : _ref3$columns;
	
		var info = { gutter: gutter, columns: columns, onlyOne: false };
	
		// if (!endElement) {
		// 	return rebuildItem(containerElement, startElement, info);
		// }
	
		var current = startElement;
		while (current && current !== endElement) {
			info = rebuildItem(containerElement, current, info);
			current = current.nextElementSibling;
		}
	
		updateContainerHeight(containerElement, { gutter: gutter, columns: columns });
	
		return info;
	};
	
	var customEvents = __webpack_require__(120);
	
	var link = function link($scope, $element, $attributes, $controller) {
	
		var container = $element[0];
		var rebuildInfo = { gutter: 32, columns: 4 };
	
		// if (!element.hasChildNodes()) {
		// 	return;
		// }
		//
		// let currentElement = value;
		//
		// let name = value;
	
		// let elementsMap = {
		// 	new: {},
		//
		// };
	
	
		// listofupdates - after
	
	
		$scope.$watch(function () {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			var markers = Array.from(container.children).filter(function (item) {
				return item.nodeType === Node.ELEMENT_NODE;
			}).reduce(markItem, { added: [], changed: [] });
	
			// const markers = qwery('[ng-repeat], [ng-repeat-start]', container)
			// 	.reduce(markItem, {added: [], changed: []});
	
			// console.log(markers);
			// debugger;
			// const repeatItems = qwery('[ng-repeat]');
		});
	
		container.addEventListener('load', function (event) {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			// console.log(event.target.nodeName);
	
	
			if (event.target.src.indexOf(onePixel) >= 0) {
				var _ret = function () {
	
					var marker = event.target;
					var cardElement = marker.parentNode;
					cardElement.removeChild(marker);
	
					var masonryId = cardElement.getAttribute('data-masonry-id', masonryId);
					var images = qwery('img', cardElement).filter(function (item) {
						return item !== marker;
					}).forEach(function (image) {
						image.masonryId = masonryId;
						return image;
					});
	
					rebuildInfo = rebuildItem(container, cardElement, rebuildInfo);
					// rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
	
					// 		console.log(images);
					// 		debugger;
	
					return {
						v: void 0
					};
				}();
	
				if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
			}
	
			if (event.target instanceof HTMLImageElement && event.target.masonryId) {
				var _qwery = qwery('[data-masonry-id="' + event.target.masonryId + '"]'),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    cardElement = _qwery2[0];
	
				rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
			}
		}, true);
	
		container.addEventListener('error', function (event) {
	
			if (event.target instanceof HTMLImageElement && event.target.masonryId) {
				var _qwery3 = qwery('[data-masonry-id="' + event.target.masonryId + '"]'),
				    _qwery4 = _slicedToArray(_qwery3, 1),
				    cardElement = _qwery4[0];
	
				rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
			}
		}, true);
	
		customEvents.onResizeEnd.push(function () {
			// console.log('resized');
	
			var newGutter = calculateGutter(container, container.firstElementChild);
			if (newGutter.columns !== rebuildInfo.columns) {
				rebuildInfo = rebuildItems(container, container.firstElementChild, null, rebuildInfo);
				updateContainerHeight(container, rebuildInfo);
			}
		});
	
		// debugger;
		// const element = $element[0];
		// let pendingImages = 0;
		//
		// $scope.$watchCollection('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	const cards = qwery('.card', element);
		//
		// 	for (let card of cards) {
		//
		// 		const $card = angular.element(card);
		// 		const $scope = $card.scope();
		//
		// 		console.log($scope);
		// 		console.log($scope.$id);
		//
		// 		let marker = new Image();
		// 		marker.setAttribute('ng-if', true);
		// 		marker.src = onePixel;
		// 		card.appendChild(marker);
		// 	}
		//
		// 	// pendingImages += cards.length * 3;
		//
		// 	// qwery('.card', element).forEach(item => {
		// 	// 	item.length
		// 	// 	console.log(item.offsetHeight);
		// 	// });
		//
		//
		//
		// 	// debugger;
		//
		// });
		//
		// //
		// element.addEventListener('load', (event) => {
		// 	if (event.target.src.indexOf(onePixel) >= 0) {
		// 		const marker = event.target;
		// 		const cardElement = marker.parentNode;
		// 		const images = qwery('img', cardElement).filter(item => item !== marker);
		//
		// 		console.log(images);
		//
		// 		debugger;
		// 	}
		// }, true);
	
	
		// $scope.$watch('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	// console.log(qwery('.card', element));
		// 	debugger;
		// }, () => {
		// 	console.log('Changed');
		//
		// })
	
	
		// console.log(element, $scope);
	
	
		// $scope.$watch((...args) => {
		// 	console.log('DIGEST');
		// 	console.log(args);
		// })
	
	
		// debugger;
	};
	
	module.exports = [function () {
	
		var directive = {
			// priority: 1000,
			// terminal: true,
			restrict: 'CEA',
			link: link
		};
	
		return directive;
	}];

/***/ },

/***/ 119:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(37);
	var qwery = __webpack_require__(83);
	
	var _require = __webpack_require__(68),
	    newMarkerId = _require.shortId;
	
	var onePixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=';
	
	// style
	
	var createMarker = function createMarker(masonryId) {
	
		var marker = new Image();
	
		marker.setAttribute('data-masonry-id', masonryId);
	
		marker.src = onePixel;
		marker.style.position = 'fixed';
		marker.style.left = '0';
		marker.style.top = '0';
	
		return marker;
	};
	
	var markElement = function markElement(masonryId, item) {
		var marker = createMarker(masonryId);
		item.setAttribute('data-masonry-id', masonryId);
		item.appendChild(marker);
	};
	
	var markNgRepeatStart = function markNgRepeatStart(masonryId, item) {};
	
	var elementIsChanged = function elementIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var ngRepeatStartIsChanged = function ngRepeatStartIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var markItem = function markItem() {
		var info = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var item = arguments[1];
		var _info$added = info.added,
		    added = _info$added === undefined ? [] : _info$added,
		    _info$changed = info.changed,
		    changed = _info$changed === undefined ? [] : _info$changed;
	
	
		var masonryId = item.getAttribute('data-masonry-id');
		var ngRepeatStart = item.getAttribute('ng-repeat-start');
	
		if (masonryId) {
			var isChanged = ngRepeatStart ? ngRepeatStartIsChanged(masonryId, item) : elementIsChanged(masonryId, item);
	
			if (isChanged) {
				changed.push(masonryId);
				info.changed = changed;
			}
	
			return info;
		}
	
		// const ngRepeat = item.getAttribute('ng-repeat');
	
		// if (!ngRepeat && !ngRepeatStart) {
		// 	return info;
		// }
	
		masonryId = newMarkerId();
	
		!!ngRepeatStart ? markNgRepeatStart(masonryId, item) : markElement(masonryId, item);
	
		added.push(masonryId);
		info.added = added;
	
		return info;
	};
	
	module.exports = {
		markItem: markItem,
		onePixel: onePixel
	};

/***/ },

/***/ 120:
/***/ function(module, exports) {

	'use strict';
	
	var customEvents = {
		onResizeEnd: []
	};
	
	var delta = 5;
	var timer = false,
	    startTime = void 0;
	
	var onResizeEnd = function onResizeEnd() {
	
		if (new Date() - startTime < delta) {
			timer = setTimeout(onResizeEnd, delta);
		} else {
			timer = false;
	
			if ('onResizeEnd' in customEvents) {
				customEvents['onResizeEnd'].forEach(function (fn) {
					return fn();
				});
			}
		}
	};
	
	window.addEventListener('resize', function () {
		startTime = new Date();
		if (timer === false) {
			timer = setTimeout(onResizeEnd, delta);
		}
	}, true);
	
	module.exports = customEvents;

/***/ },

/***/ 121:
/***/ function(module, exports, __webpack_require__, __webpack_module_template_argument_0__, __webpack_module_template_argument_1__) {

	'use strict';
	
	//
	// const getChildren = (element) => {
	// 	console.log(element);
	// };
	
	
	// onDelete - rebuild tree under element (nextSibling)
	// onSize (element - maybe load image) change - rebuild tree under  (nextSibling) - image on load -> id -> rebuildElement by id (style changed ?)
	// onAdd
	//
	// (element) => nextSibling -> whileEnd
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var qwery = __webpack_require__(83);
	
	var _require = __webpack_require__(__webpack_module_template_argument_0__),
	    markItem = _require.markItem,
	    onePixel = _require.onePixel;
	
	var elementPosiition = function elementPosiition(container, element) {
		var containerPosition = container.getBoundingClientRect();
		var elementPosition = element.getBoundingClientRect();
	
		var width = elementPosition.width;
		var height = elementPosition.height;
		var left = elementPosition.left - containerPosition.left;
		var top = elementPosition.top - containerPosition.top;
	
		return { width: width, height: height, left: left, top: top };
	
		// const name = value;
	};
	
	var calculateGutter = function calculateGutter(container, first) {
		var modulo = container.offsetWidth % first.offsetWidth;
		var columns = (container.offsetWidth - modulo) / first.offsetWidth;
		var gutter = parseInt(modulo / (columns - 1));
	
		return { gutter: gutter, columns: columns };
	};
	
	var calculateContainerHeight = function calculateContainerHeight(container) {
		var info = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		var gutter = info.gutter,
		    columns = info.columns;
	
	
		var height = 0;
	
		var lastElement = container.lastElementChild;
		if (!lastElement) {
			return height;
		}
	
		var count = columns;
		while (count-- && lastElement) {
			var position = elementPosiition(container, lastElement);
			var currentHeight = position.top + lastElement.offsetHeight;
	
			if (currentHeight > height) {
				height = currentHeight;
			}
	
			lastElement = lastElement.previousElementSibling;
		}
	
		return height;
	};
	
	var updateContainerHeight = function updateContainerHeight(container) {
		var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
		    gutter = _ref.gutter,
		    columns = _ref.columns;
	
		var containerHeight = calculateContainerHeight(container, { gutter: gutter, columns: columns });
		container.style.height = containerHeight + 'px';
	};
	
	var rebuildItem = function rebuildItem(container, current) {
		var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
		    _ref2$gutter = _ref2.gutter,
		    gutter = _ref2$gutter === undefined ? 32 : _ref2$gutter,
		    _ref2$columns = _ref2.columns,
		    columns = _ref2$columns === undefined ? 4 : _ref2$columns,
		    _ref2$onlyOne = _ref2.onlyOne,
		    onlyOne = _ref2$onlyOne === undefined ? true : _ref2$onlyOne;
	
		var previous = current.previousElementSibling;
		if (!previous) {
	
			current.style.top = '0px';
			current.style.left = '0px';
	
			var info = calculateGutter(container, current);
	
			gutter = info.gutter;
			columns = info.columns;
	
			return { gutter: gutter, columns: columns };
		}
	
		// const container = value;
		var positionPrev = elementPosiition(container, previous);
		var left = positionPrev.left + positionPrev.width + gutter;
		var isOverflowed = left + current.offsetWidth > container.offsetWidth;
	
		var childrenArray = Array.from(container.children);
		var currentPosition = childrenArray.indexOf(current);
	
		var prevUpperPosition = currentPosition - columns;
		var prevUpperElement = childrenArray[prevUpperPosition];
	
		var top = 0;
		if (prevUpperElement) {
			var upperPosition = elementPosiition(container, prevUpperElement);
			top = upperPosition.top + upperPosition.height + gutter;
		}
	
		if (!isOverflowed) {
			current.style.left = left + 'px';
		} else {
			current.style.left = '0px';
		}
	
		current.style.top = top + 'px';
	
		if (onlyOne) {
			updateContainerHeight(container, { gutter: gutter, columns: columns });
		}
	
		return { gutter: gutter, columns: columns };
	};
	
	var rebuildItems = function rebuildItems(containerElement, startElement) {
		var endElement = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	
		var _ref3 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
		    _ref3$gutter = _ref3.gutter,
		    gutter = _ref3$gutter === undefined ? 32 : _ref3$gutter,
		    _ref3$columns = _ref3.columns,
		    columns = _ref3$columns === undefined ? 4 : _ref3$columns;
	
		var info = { gutter: gutter, columns: columns, onlyOne: false };
	
		// if (!endElement) {
		// 	return rebuildItem(containerElement, startElement, info);
		// }
	
		var current = startElement;
		while (current && current !== endElement) {
			info = rebuildItem(containerElement, current, info);
			current = current.nextElementSibling;
		}
	
		updateContainerHeight(containerElement, { gutter: gutter, columns: columns });
	
		return info;
	};
	
	var customEvents = __webpack_require__(__webpack_module_template_argument_1__);
	
	var link = function link($scope, $element, $attributes, $controller) {
	
		var container = $element[0];
		var rebuildInfo = { gutter: 32, columns: 4 };
	
		// if (!element.hasChildNodes()) {
		// 	return;
		// }
		//
		// let currentElement = value;
		//
		// let name = value;
	
		// let elementsMap = {
		// 	new: {},
		//
		// };
	
	
		// listofupdates - after
	
	
		$scope.$watch(function () {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			var markers = Array.from(container.children).filter(function (item) {
				return item.nodeType === Node.ELEMENT_NODE;
			}).reduce(markItem, { added: [], changed: [] });
	
			// const markers = qwery('[ng-repeat], [ng-repeat-start]', container)
			// 	.reduce(markItem, {added: [], changed: []});
	
			// console.log(markers);
			// debugger;
			// const repeatItems = qwery('[ng-repeat]');
		});
	
		container.addEventListener('load', function (event) {
	
			if (!container.hasChildNodes()) {
				return;
			}
	
			// console.log(event.target.nodeName);
	
	
			if (event.target.src.indexOf(onePixel) >= 0) {
				var _ret = function () {
	
					var marker = event.target;
					var cardElement = marker.parentNode;
					cardElement.removeChild(marker);
	
					var masonryId = cardElement.getAttribute('data-masonry-id', masonryId);
					var images = qwery('img', cardElement).filter(function (item) {
						return item !== marker;
					}).forEach(function (image) {
						image.masonryId = masonryId;
						return image;
					});
	
					rebuildInfo = rebuildItem(container, cardElement, rebuildInfo);
					// rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
	
					// 		console.log(images);
					// 		debugger;
	
					return {
						v: void 0
					};
				}();
	
				if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
			}
	
			if (event.target instanceof HTMLImageElement && event.target.masonryId) {
				var _qwery = qwery('[data-masonry-id="' + event.target.masonryId + '"]'),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    cardElement = _qwery2[0];
	
				rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
			}
		}, true);
	
		container.addEventListener('error', function (event) {
	
			if (event.target instanceof HTMLImageElement && event.target.masonryId) {
				var _qwery3 = qwery('[data-masonry-id="' + event.target.masonryId + '"]'),
				    _qwery4 = _slicedToArray(_qwery3, 1),
				    cardElement = _qwery4[0];
	
				rebuildInfo = rebuildItems(container, cardElement, null, rebuildInfo);
			}
		}, true);
	
		customEvents.onResizeEnd.push(function () {
			// console.log('resized');
	
			var newGutter = calculateGutter(container, container.firstElementChild);
			if (newGutter.columns !== rebuildInfo.columns) {
				rebuildInfo = rebuildItems(container, container.firstElementChild, null, rebuildInfo);
				updateContainerHeight(container, rebuildInfo);
			}
		});
	
		// debugger;
		// const element = $element[0];
		// let pendingImages = 0;
		//
		// $scope.$watchCollection('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	const cards = qwery('.card', element);
		//
		// 	for (let card of cards) {
		//
		// 		const $card = angular.element(card);
		// 		const $scope = $card.scope();
		//
		// 		console.log($scope);
		// 		console.log($scope.$id);
		//
		// 		let marker = new Image();
		// 		marker.setAttribute('ng-if', true);
		// 		marker.src = onePixel;
		// 		card.appendChild(marker);
		// 	}
		//
		// 	// pendingImages += cards.length * 3;
		//
		// 	// qwery('.card', element).forEach(item => {
		// 	// 	item.length
		// 	// 	console.log(item.offsetHeight);
		// 	// });
		//
		//
		//
		// 	// debugger;
		//
		// });
		//
		// //
		// element.addEventListener('load', (event) => {
		// 	if (event.target.src.indexOf(onePixel) >= 0) {
		// 		const marker = event.target;
		// 		const cardElement = marker.parentNode;
		// 		const images = qwery('img', cardElement).filter(item => item !== marker);
		//
		// 		console.log(images);
		//
		// 		debugger;
		// 	}
		// }, true);
	
	
		// $scope.$watch('catalog', () => {
		// 	console.log('DIGEST Collection');
		//
		// 	// console.log(qwery('.card', element));
		// 	debugger;
		// }, () => {
		// 	console.log('Changed');
		//
		// })
	
	
		// console.log(element, $scope);
	
	
		// $scope.$watch((...args) => {
		// 	console.log('DIGEST');
		// 	console.log(args);
		// })
	
	
		// debugger;
	};
	
	module.exports = [function () {
	
		var directive = {
			// priority: 1000,
			// terminal: true,
			restrict: 'CEA',
			link: link
		};
	
		return directive;
	}];

/***/ }

})
//# sourceMappingURL=1.652343d92714521d8aaf.hot-update.js.map