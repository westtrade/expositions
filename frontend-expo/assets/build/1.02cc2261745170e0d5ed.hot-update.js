webpackHotUpdate(1,{

/***/ 106:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(60);
	var messages = __webpack_require__(108);
	
	var angular = __webpack_require__(44);
	
	var ROLES = ['guest', 'exponent', 'organizer', 'moderator', 'administrator'];
	
	var RULES = {
		navbar: ['!guest']
	};
	
	var ANONYMOUS_USER = {
		role: 'guest',
		displayName: 'Аноним'
	};
	
	var User = function () {
		function User() {
			var _this = this;
	
			_classCallCheck(this, User);
	
			this.watchers = [];
			this.state = {};
	
			this.setState(ANONYMOUS_USER);
	
			messages.on('user', function (event, userState) {
				_this.setState(userState);
			});
		}
	
		_createClass(User, [{
			key: 'bind',
			value: function bind($scope) {
				var _this2 = this;
	
				this.watchers.push($scope);
	
				safeApply($scope, function () {
					$scope.user = _this2.getState();
				});
	
				$scope.$on('$destroy', function () {
					console.log('Scope destroyed');
					var watcherIdx = _this2.watchers.indexOf($scope);
					_this2.watchers.splice(watcherIdx, 1);
				});
			}
		}, {
			key: 'logout',
			value: function logout() {
				var _this3 = this;
	
				return new Promise(function (resolve, reject) {
					_this3.setState(ANONYMOUS_USER);
					resolve();
				});
			}
		}, {
			key: 'setState',
			value: function setState() {
				var _this4 = this;
	
				var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	
				this.state = angular.extend({}, this.state, data);
	
				this.watchers.forEach(function ($scope) {
					safeApply($scope, function () {
						$scope.user = _this4.getState();
					});
				});
			}
		}, {
			key: 'getState',
			value: function getState() {
				var _this5 = this;
	
				return angular.extend({}, this.state, {
					hasAccess: function hasAccess(ruleId) {
						return _this5.hasAccess(ruleId);
					},
					logout: function logout() {
						return _this5.logout();
					}
				});
			}
		}, {
			key: 'hasAccess',
			value: function hasAccess(ruleId) {
	
				var ruleExists = rule in RULES;
	
				if (!ruleExists) {
					return false;
				}
	
				var rule = RULES[ruleId];
				var result = false;
	
				return result;
			}
		}]);
	
		return User;
	}();
	
	var currentUser = new User();
	
	module.exports = function () {
		return currentUser;
	};

/***/ }

})
//# sourceMappingURL=1.02cc2261745170e0d5ed.hot-update.js.map