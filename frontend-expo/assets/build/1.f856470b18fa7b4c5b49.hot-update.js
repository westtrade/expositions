webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	var getErrors = function getErrors() {
		var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
	
		var result = [];
	
		if (input && input.length) {
			try {
				result = JSON.parse(input);
			} catch (e) {
				console.error(e);
			}
		}
	
		return result;
	};
	
	module.exports = ['$parse', function ($parse) {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			replace: true,
			scope: {
				error: '@'
			},
			link: function link($scope, $element, $attrs) {
				$scope.error = getErrors($attrs.error);
				$attrs.$observe('error', function (value) {
					console.log(value);
					$scope.error = getErrors(value);
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.f856470b18fa7b4c5b49.hot-update.js.map