webpackHotUpdate(1,{

/***/ 19:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loginPage = __webpack_require__(20);
	var registrationPage = __webpack_require__(21);
	var recoveryPage = __webpack_require__(35);
	
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', function ($scope) {
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					$scope.randomSeed = guid();
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', function ($scope) {}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	};
	
	module.exports = routes;

/***/ },

/***/ 35:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/recovery-page.html';
	var html = "<form class=\"card center\">\n\t<div class=\"offset\">\n\t\t<div class=\"field text-center bigger\">\n\t\t\tЕсть аккаунт? <a href=\"#\" ui-sref=\"user-login\" >Войти?</a>\n\t\t</div>\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\t\t<h1>Регистрация</h1>\n\t\t<input type=\"text\" placeholder=\"Фамилия Имя Отчество\">\n\t\t<input type=\"email\" placeholder=\"Email\">\n\t\t<input type=\"text\" placeholder=\"Номер телефона\">\n\t\t<input type=\"password\" placeholder=\"Пароль\">\n\t\t<input type=\"password\" placeholder=\"Пароль еще раз\">\n\n\n\t\t<input type=\"password\" class=\"half\" placeholder=\"Код с картинки\">\n\t\t<img class=\"captcha\" ng-src=\"/common/captcha?seed={{randomSeed}}\" src=\"/common/captcha\" ng-click=\"changeCaptcha()\" alt=\"\">\n\t\t<a href=\"#\" ng-click=\"changeCaptcha()\" class=\"circle-button\">\n\t\t\t<i class=\"fa fa-refresh\"></i>\n\t\t</a>\n\n\t\t<br>\n\n\t\t<div class=\"checkbox\">\n\t\t\t<input type=\"checkbox\" name=\"subscribe\" id=\"field-subscribe\" styled-checkbox class=\"styled-checkbox\">\n\t\t\t<label for=\"field-subscribe\">Я хочу получать информационные сообщения от ExpoTestDrive</label>\n\t\t</div>\n\t\t<br>\n\n\n\t\t<button class=\"primary\" type=\"submit\">Регистрация</button>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"field\">\n\t\t\tРегистрируясь, я подтверждаю свое согласие с условиями <a href=\"#\" ui-sref=\"content-agreement\">Лицензионного соглашения</a>\n\t\t</div>\n\n\t\t<div class=\"separator\">\n\t\t\t<div class=\"label\">или</div>\n\t\t</div>\n\t\t<div class=\"two-buttons\">\n\t\t\t<a class=\"fb button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-facebook fa-fw\"></i>\n\t\t\t</a><a class=\"vk button\" href=\"#\">\n\t\t\t\t<i class=\"fa fa-vk fa-fw\"></i>\n\t\t\t</a>\n\t\t</div>\n\n\t</div>\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.245e87463ff48d111dd7.hot-update.js.map