webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	var calculateHeight = function calculateHeight(element) {
		console.log(element.outerHeight);
	};
	
	module.exports = function ($scope, $element, cfpLoadingBar) {
	
		var element = $element[0];
	
		// document.addEventListener('scroll', (event) => {
		// })
	
		$scope.filter = {
			page: 0
		};
	
		$scope.flow = false;
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
			cfpLoadingBar.start();
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				calculateHeight(element);
	
				cfpLoadingBar.complete();
	
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	};

/***/ }

})
//# sourceMappingURL=1.7f94d64e0c796838e343.hot-update.js.map