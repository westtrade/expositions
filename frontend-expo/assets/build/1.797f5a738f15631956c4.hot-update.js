webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	module.exports = ['$parse', function ($parse) {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			scope: {
				error: '@error'
			},
			link: function link($scope, $element, $attrs) {
				console.log($attrs);
	
				console.log($attrs.error);
	
				console.log($parse($attrs.error)($scope));
	
				$scope.$watch('error', function (prev, next) {
					console.log(prev, next);
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.797f5a738f15631956c4.hot-update.js.map