webpackHotUpdate(1,{

/***/ 271:
/***/ function(module, exports) {

	'use strict';
	
	var createAppend = function createAppend(element) {
	
		var appendElement = document.createElement('div');
	
		appendElement.classList.add('append');
	
		if (element.nextElementSibling) {
			element.parentNode.insertBefore(appendElement, element.nextElementSibling);
		} else {
			element.appendChild(appendElement);
		}
	
		return appendElement;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			link: function link($scope, $element, $attrs) {
				var _$attrs$letterCounter = $attrs.letterCounter,
				    letterCounter = _$attrs$letterCounter === undefined ? -1 : _$attrs$letterCounter;
	
				var element = $element[0];
	
				console.log(letterCounter);
	
				console.log(element.nextElementSibling);
	
				var appendElement = element.nextElementSibling && element.nextElementSibling.classList.has('append') ? element.nextElementSibling : createAppend(element);
	
				letterCounter = parseInt(letterCounter);
	
				if (letterCounter > 0) {
					appendElement.innerHtml = element.value.length + ' / ' + letterCounter;
				}
	
				element.addEventListener('input', function (event) {});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.6499f556bab1144255dd.hot-update.js.map