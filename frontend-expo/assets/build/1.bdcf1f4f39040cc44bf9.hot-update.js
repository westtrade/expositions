webpackHotUpdate(1,{

/***/ 80:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(81),
	    getOptions = _require.getOptions;
	
	var elements = __webpack_require__(82);
	
	module.exports = function (styledElementsList) {
		var styleOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
		styleOptions = getOptions(styleOptions);
		var styledCount = styledElementsList.length;
		while (styledCount--) {
	
			var element = styledElementsList[styledCount];
			var isInput = element.tagName.toLowerCase() === 'input';
	
			switch (true) {
				case isInput && element.getAttribute('type') === 'checkbox':
					elements.checkbox(element, styleOptions);
					break;
				case isInput && element.getAttribute('type') === 'radio':
					elements.radiobox(element, styleOptions);
					break;
	
				case element.tagName.toLowerCase() === 'select':
					elements.select(element, styleOptions);
					break;
	
				default:
	
			}
		}
	};
	
	module.exports.elements = elements;

/***/ }

})
//# sourceMappingURL=1.bdcf1f4f39040cc44bf9.hot-update.js.map