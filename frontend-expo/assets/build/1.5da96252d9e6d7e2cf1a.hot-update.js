webpackHotUpdate(1,{

/***/ 226:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var tinymce = __webpack_require__(227);
	__webpack_require__(231);
	
	var THEME = 'lightgray';
	__webpack_require__(268)("./" + THEME + '/content.min.css');
	__webpack_require__(269)("./" + THEME + '/skin.min.css');
	
	__webpack_require__(245);
	__webpack_require__(261);
	__webpack_require__(262);
	__webpack_require__(263);
	__webpack_require__(264);
	__webpack_require__(265);
	__webpack_require__(265);
	__webpack_require__(266);
	__webpack_require__(267);
	__webpack_require__(246);
	__webpack_require__(247);
	__webpack_require__(260);
	
	var content_style = '\nbody {\n\tfont-family: "Roboto", sans-serif;\n}\n';
	
	module.exports = function () {
		return {
			scope: true,
			link: function link($scope, $element, $attrs, $controllers) {
	
				var element = $element[0];
	
				tinymce.init({
					target: element,
					setup: function setup(editor) {
						// console.log(editor);
					},
	
					visual: false,
					content_style: content_style,
					// height: 500,
					menubar: false,
					skin: false,
					// inline: true,
					plugins: 'paste link autoresize textpattern advlist autolink link image lists charmap print preview',
					textpattern_patterns: [{ start: '*', end: '*', format: 'italic' }, { start: '**', end: '**', format: 'bold' }, { start: '#', format: 'h1' }, { start: '##', format: 'h2' }, { start: '###', format: 'h3' }, { start: '####', format: 'h4' }, { start: '#####', format: 'h5' }, { start: '######', format: 'h6' }, { start: '1. ', cmd: 'InsertOrderedList' }, { start: '* ', cmd: 'InsertUnorderedList' }, { start: '- ', cmd: 'InsertUnorderedList' }]
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.5da96252d9e6d7e2cf1a.hot-update.js.map