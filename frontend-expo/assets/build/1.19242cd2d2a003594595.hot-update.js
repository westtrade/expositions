webpackHotUpdate(1,{

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var loadMap = __webpack_require__(110);
	var qwery = __webpack_require__(90);
	
	var setValue = function setValue(inputElement, value) {
		var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
		    _ref$input = _ref.input,
		    input = _ref$input === undefined ? false : _ref$input,
		    _ref$change = _ref.change,
		    change = _ref$change === undefined ? true : _ref$change;
	
		inputElement.value = value;
	
		if (input) {
			var changeEvent = new Event('change');
			input.dispatchEvent(changeEvent);
		}
	
		if (change) {
			var inputEvent = new Event('input');
			input.dispatchEvent(inputEvent);
		}
	};
	
	var providers = {
	
		yandex: {
	
			init: function init(element) {
	
				var suggestView = new global.ymaps.SuggestView(element);
				suggestView.events.add('select', function (event) {
					var _event$get = event.get('item'),
					    value = _event$get.value,
					    displayName = _event$get.displayName;
					// 
					// global.ymaps.geocode(value, {provider: 'yandex'}).then((result) => {
					// 	console.log(result.geoObjects);
					//
					// 	// debugger;
					// })
	
				});
	
				// global.ymaps.ready(() => {
	
	
				// console.log('ymaps ready');
	
				// })
	
	
				// console.log(element);
	
	
				// suggestView.state.events.add('change', (event) => {
				//
				// 	let activeIndex = suggestView.state.get('activeIndex');
				// 	if (typeof activeIndex == 'number') {
				// 		let activeItem = suggestView.state.get('items')[activeIndex];
				// 		console.log(activeItem);
				// 		// if (activeItem && activeItem.value != input.value) {
				// 		// 	input.value = activeItem.value;
				// 		// }
				// 	}
				//
				// });
	
				element.$destroy = function () {
					suggestView.destroy();
					suggestView = null;
				};
			},
	
			destroy: function destroy(element) {
				// element.$destroy();
				// element.$destroy = null;
			}
		},
	
		google: {
	
			init: function init() {},
	
			destroy: function destroy() {}
		}
	};
	
	var changeProvider = function changeProvider(element, currentProvider, prev) {
	
		if (currentProvider === prev) {
			return currentProvider;
		}
	
		if (prev && prev in providers) {
			providers[prev].destroy(element);
		}
	
		if (currentProvider in providers) {
			setTimeout(function () {
				return loadMap(currentProvider, function () {
					return providers[currentProvider].init(element);
				});
			}, 0);
		}
	
		return currentProvider;
	};
	
	module.exports = function () {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function link($scope, $element, $attrs, ngModel) {
	
				var selectorElement = $element[0];
	
				// if (ngModel && ngModel.$modelValue) {
				// 	setValue(selectorElement, ngModel.$modelValue, {input: true, change: true});
				//
				// 	$scope.$watch($attrs.ngModel, (currentValue) => {
				// 		setValue(selectorElement, ngModel.$modelValue, {input: true, change: true});
				// 	})
				// }
	
				// setValue(selectorElement, 123123);
	
				var current = changeProvider(selectorElement, $attrs.provider || 'yandex');
				$attrs.$observe('provider', function (next) {
					current = changeProvider(selectorElement, next, current);
				});
			}
		};
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.19242cd2d2a003594595.hot-update.js.map