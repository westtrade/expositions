webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			transclude: 'element',
			restrict: 'C',
			priority: -1000,
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
	
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
							newModal = angular.element(modalSource);
							$element.after(newModal);
							newModal.removeClass('modal-source').addClass('modal selected');
							var $modalScope = $rootScope.$new(true);
							newModal = $compile(newModal)($modalScope);
						} else if (newModal) {
							newModal.scope().$destroy();
							newModal.remove();
							newModal = null;
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.5f03d035a2aa06feead4.hot-update.js.map