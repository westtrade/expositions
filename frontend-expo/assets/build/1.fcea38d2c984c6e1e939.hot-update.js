webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeapply = __webpack_require__(44);
	
	module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', function ($scope, $element, cfpLoadingBar, $modals) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0,
			subjects: [],
			expositions: []
		};
	
		$scope.selectSubjects = function () {
			$modals.open('subject-selector', $scope.filter.subjects, function () {
				var subjectList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				$scope.filter.subjects = subjectList;
			});
		};
	
		$scope.selectExpositions = function () {
			$modals.open('exposition-selector', $scope.filter.expositions, function () {
				var expositionsList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
			});
		};
	
		$scope.removeFilter = function (removed, filterType) {
			var filterTypeExists = filterType in $scope.filter;
	
			if (!filterTypeExists) {
				throw new Error('Wrong type of filter (filterType argumnt). It must be in allowed list of types: '.Object.keys($scope.filter).join(', '));
			}
	
			$scope.filter[filterType] = $scope.filter[filterType].filter(function (current) {
				return removed.id !== current.id;
			});
		};
	
		$scope.catalog = [];
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// console.log(catalog[1]);
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
				$scope.loading = false;
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		document.addEventListener('scroll', function (event) {
			// const contentIsScrolled = element.scrollHeight - document.body.scrollTop >= element.clientHeight;
			var contentIsScrolled = element.scrollHeight - document.body.scrollTop - 400 <= 0; //todo fix this bug
	
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		});
	}];

/***/ }

})
//# sourceMappingURL=1.fcea38d2c984c6e1e939.hot-update.js.map