webpackHotUpdate(1,{

/***/ 103:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	var safeApply = __webpack_require__(60);
	
	module.exports = ['$scope', '$modals', '$state', function ($scope, $modals, $state) {
	
		$scope.$watchCollection('sliderList', function (prev, next, $scope) {
			$scope.$emit('slider:update');
		});
	
		$scope.sliderList = [{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {
				type: 'modal',
				args: ['subject-selector']
			},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}];
	
		$scope.openEditor = function () {
			var sliderList = $scope.sliderList;
	
			$modals.open('slider-editor', { sliderList: sliderList }, function () {
				var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
				    sliderList = _ref.sliderList;
	
				safeApply(function () {
					$scope.sliderList = sliderList;
					if ($scope.$slider) {
						$scope.$slider.update();
					}
				});
			});
		};
	
		$scope.go = function () {
			var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
			    type = _ref2.type,
			    _ref2$args = _ref2.args,
			    args = _ref2$args === undefined ? [] : _ref2$args;
	
			switch (type) {
				case 'modal':
					$modals.open.apply($modals, _toConsumableArray(args));
					break;
	
				case 'link':
					$state.go.apply($state, _toConsumableArray(args));
					break;
	
				default:
			}
		};
	
		// $scope.openEditor();
	
		// console.log($scope);
		// console.log($scope.$parent.$parent.$slider.update());
		// console.log($scope.$slider);
		// $scope.$slider.update();
	}];

/***/ }

})
//# sourceMappingURL=1.751c640ba6d3439edb15.hot-update.js.map