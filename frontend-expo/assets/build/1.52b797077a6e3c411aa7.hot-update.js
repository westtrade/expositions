webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var qwery = __webpack_require__(40);
	var safeApply = __webpack_require__(45);
	var templateUrl = __webpack_require__(46);
	
	var SliderTimer = function () {
		function SliderTimer() {
			_classCallCheck(this, SliderTimer);
	
			this.currentTimer = null;
			this.$slider = null;
		}
	
		_createClass(SliderTimer, [{
			key: 'run',
			value: function run() {
				var _this = this;
	
				var $slider = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	
	
				if ($slider) {
					this.$slider = $slider;
				}
	
				if (!this.$slider) {
					return;
				}
	
				this.currentTimer = setTimeout(function () {
	
					_this.$slider.next(true);
					_this.currentTimer = clearTimeout(_this.currentTimer);
					_this.run();
				}, this.$slider.timeout);
			}
		}, {
			key: 'pause',
			value: function pause() {
				if (this.currentTimer) {
					this.currentTimer = clearTimeout(this.currentTimer);
				}
			}
		}, {
			key: 'resume',
			value: function resume() {
				if (!this.currentTimer) {
					this.currentTimer = clearTimeout(this.currentTimer);
					this.run();
				}
			}
		}]);
	
		return SliderTimer;
	}();
	
	;
	
	var init = function init($scope, container) {
		var sliderContent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
	
	
		var $slider = {
			timeout: 4000,
			total: 0,
			acive: 0,
			container: container,
			scope: $scope,
			timer: new SliderTimer(),
	
			pause: function pause() {
				$slider.timer.pause();
			},
			resume: function resume() {
				$slider.timer.resume();
			},
			run: function run() {
				$slider.timer.run($slider);
			},
			setActive: function setActive(active) {
				var isAutomatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
	
				if (!isAutomatic) {
					$slider.pause();
				}
	
				safeApply($slider.scope, function () {
	
					$slider.active = active;
					qwery('.slide', $slider.container).forEach(function (item, idx) {
						idx === active ? item.classList.add('active') : item.classList.remove('active');
					});
	
					if (!isAutomatic) {
						$slider.resume();
					}
				});
			},
			next: function next() {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($slider.active);
				var next = current + 1;
				current = next >= $slider.total ? 0 : next;
				$slider.setActive(current, isAutomatic);
			},
			prev: function prev() {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($slider.active);
				var prev = current - 1;
				current = prev < 0 ? $slider.total - 1 : prev;
				$slider.setActive(current, isAutomatic);
			},
			update: function update() {
	
				var itemsCount = 0;
				var initialActive = 0;
	
				var sliderContent = qwery('.slide', $slider.container);
	
				sliderContent.forEach(function (current, key) {
	
					if (!current.classList || !current.classList.contains('slide')) {
						return;
					}
	
					if (current.classList.contains('active')) {
						initialActive = itemsCount;
					}
	
					itemsCount++;
				});
	
				$slider.total = itemsCount;
				$slider.active = 0;
	
				// console.log($slider.total);
	
	
				$slider.setActive(initialActive);
	
				return $slider;
			}
		};
	
		$scope.$slider = $slider;
	
		// $scope.$watch(() => {
		// 	console.log('change');
		// 	$slider.pause();
		// 	$slider.setContent();
		// 	$slider.resume();
		// });
	
		return $slider;
	};
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
	
		return {
			transclude: true,
			scope: true,
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, transclude) {
				return function ($scope, $container, attrs) {
	
					var $slider = init($scope, $container[0]);
	
					transclude($scope, function (content) {
						$container.append(content);
	
						$scope.$on('slider:update', function () {
							setTimeout($slider.update, 20);
						});
					});
				};
			}
	
			// link ($scope, $element, attrs, ctrl, $transclude) {
			//
			// 	const container = $element[0];
			// 	const $slider = init($scope, container);
			// 	container.addEventListener('mouseenter', (event) => {
			// 		if (['arrow', 'button'].filter(name => event.target.classList.contains(name)).length) {
			// 			$slider.pause();
			// 		}
			// 	}, true);
			//
			// 	container.addEventListener('mouseleave', $slider.resume, true);
			// 	$rootScope.$on('modal-toggled', (event, modalName, isOpened) => isOpened ? $slider.pause() : $slider.resume());
			//
			// 	$scope.$on('slider:update', () => {
			// 		console.log('slider update');
			// 		$slider.update();
			// 	});
			//
			//
			// 	// let $innerScope = $scope.$new();
			// 	// console.log($innerScope);
			// 	// $transclude($scope, (sliderContent) => {
			// 	//
			// 	// 	// $slider.update();
			// 	//
			// 	//
			// 	//
			// 	// 	// console.log('transclude', sliderContent);
			// 	// 	// $compile(sliderContent)($scope);
			// 	//
			// 	// 	// $element.append(sliderContent);
			// 	//
			// 	// 	// $element.on('$destroy', function() {
			// 	// 	// 	$innerScope.$destroy();
			// 	// 	// });
			// 	//
			// 	// 	// $slider.update();
			// 	// 	// $slider.run();
			// 	// });
			//
			//
			// },
		};
	}];

/***/ }

})
//# sourceMappingURL=1.52b797077a6e3c411aa7.hot-update.js.map