webpackHotUpdate(1,{

/***/ 75:
/***/ function(module, exports) {

	'use strict';
	
	var shortId = function shortId() {
		return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
	};
	
	var s4 = function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	};
	
	var guid = function guid() {
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	};
	
	module.exports = {
		guid: guid,
		shortId: shortId
	};

/***/ }

})
//# sourceMappingURL=1.49ed8f6694877afe85ba.hot-update.js.map