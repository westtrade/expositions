webpackHotUpdate(1,{

/***/ 120:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var templateUrl = __webpack_require__(121);
	
	module.exports = ['$parse', function ($parse) {
		return {
			restrict: 'E',
			templateUrl: templateUrl,
			scope: {
				error: '@'
			},
			link: function link($scope, $element, $attrs) {
				console.log($attrs);
	
				// console.log($attrs.error);
	
	
				$scope.$watch('error', function (prev, next) {
					// console.log(prev, next);
					console.log($parse($attrs.error)($scope));
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.294e247eac98cd83a9f7.hot-update.js.map