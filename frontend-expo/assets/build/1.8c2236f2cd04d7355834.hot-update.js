webpackHotUpdate(1,{

/***/ 46:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
		range: function range() {
			return function (input, total) {
	
				total = parseInt(total);
	
				for (var i = 0; i < total; i++) {
					input.push(i);
				}
	
				return input;
			};
		},
		groupBy: function groupBy() {
			return function (collection, property) {
	
				if (!collection) {
					return collection;
				}
	
				var result = collection.reduce(function (result, currentItem) {
	
					var group = currentItem[property] || 'undefined';
					// console.log(group);
					if (!(group in result)) {
						result[group] = [];
					}
	
					result[group].push(currentItem);
					return result;
				}, {});
	
				// console.log(result);
	
				return result;
			};
		}
	};

/***/ }

})
//# sourceMappingURL=1.8c2236f2cd04d7355834.hot-update.js.map