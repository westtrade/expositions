webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var Dropzone = __webpack_require__(49);
	__webpack_require__(53);
	var templateUrl = __webpack_require__(240);
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			link: function link($scope, $element, $attrs) {
				var element = $element[0];
				var currentDropzone = new Dropzone(element, {});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.55db4f06b733cc6ce11a.hot-update.js.map