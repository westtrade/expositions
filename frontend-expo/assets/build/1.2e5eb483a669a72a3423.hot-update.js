webpackHotUpdate(1,{

/***/ 63:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$scope', '$modals', function ($scope, $modals) {
	
		if (!$scope.modalOpened) {
			return false;
		}
	
		$scope.delete = function (index) {
			$scope.sliderList = $scope.sliderList.splice(index, 1);
		};
	
		$scope.save = function () {};
	
		$scope.create = function () {};
	
		$scope.close = function () {
			var sliderList = $scope.sliderList;
	
			$modals.close({ sliderList: sliderList });
		};
	}];

/***/ }

})
//# sourceMappingURL=1.2e5eb483a669a72a3423.hot-update.js.map