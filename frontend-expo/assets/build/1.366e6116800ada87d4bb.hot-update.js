webpackHotUpdate(1,{

/***/ 41:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(42);
	var uiRouter = __webpack_require__(44);
	var uiDND = __webpack_require__(45);
	
	global.moment = __webpack_require__(47);
	__webpack_require__(158);
	__webpack_require__(159);
	
	__webpack_require__(160);
	__webpack_require__(161);
	
	var loadingBar = __webpack_require__(162);
	var ngAnimate = __webpack_require__(164);
	
	var app = angular.module('expotestdrive', [uiRouter, loadingBar, ngAnimate, uiDND, 'moment-picker', 'chieffancypants.loadingBar']);
	
	var router = __webpack_require__(166);
	app.config(router);
	
	var requireAll = function requireAll(requireContext) {
		return requireContext.keys().map(requireContext);
	};
	var templates = requireAll(__webpack_require__(273));
	
	console.log(templates);
	
	app.run(['$templateCache', function ($templateCache) {}]);
	
	var common = __webpack_require__(193);
	Object.entries(common).reduce(function (app, _ref) {
		var _ref2 = _slicedToArray(_ref, 2),
		    name = _ref2[0],
		    filter = _ref2[1];
	
		return app.filter(name, filter);
	}, app);
	
	var styledInput = __webpack_require__(194);
	
	app.directive('button', __webpack_require__(203)).directive('expoSlider', __webpack_require__(204)).directive('topBar', __webpack_require__(206)).directive('styledInput', styledInput()).directive('styledCheckbox', styledInput()).directive('socialLogin', __webpack_require__(208)).directive('masonryLayout', __webpack_require__(209)).directive('expoModalManager', __webpack_require__(212)).directive('modalSource', __webpack_require__(214)).directive('expoTabs', __webpack_require__(215)).directive('fieldError', __webpack_require__(216)).directive('inputMap', __webpack_require__(218)).directive('placeAutocomplete', __webpack_require__(222)).directive('dropzoneField', __webpack_require__(223)).directive('datetimeField', __webpack_require__(225)).directive('fieldEditor', __webpack_require__(226)).directive('letterCounter', __webpack_require__(259));
	
	app.controller('offersCatalog', __webpack_require__(260)).controller('subjectSelectorModal', __webpack_require__(262)).controller('sliderController', __webpack_require__(264)).controller('expositionSelectorModal', __webpack_require__(265)).controller('sliderEditorModal', __webpack_require__(266)).controller('offerTeaserController', __webpack_require__(267));
	
	app.factory('$modals', __webpack_require__(268)).factory('User', __webpack_require__(269)).factory('areYouSure', __webpack_require__(271));
	
	app.filter('capitalize', function () {
		return function (input) {
			return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
	});
	
	module.exports = app;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },

/***/ 273:
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./change-password-page.html": 190,
		"./debug.html": 168,
		"./form-exposition.html": 192,
		"./form-offer.html": 181,
		"./home-page.html": 177,
		"./login-page.html": 184,
		"./offer-page.html": 180,
		"./offers-catalog.html": 274,
		"./recovery-page.html": 186,
		"./registration-page.html": 185
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 273;


/***/ },

/***/ 274:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/offers-catalog.html';
	var html = "\n<div class=\"container\" ng-controller=\"offersCatalog\">\n\n\t<div id=\"testdrive-filters\" class=\"card\">\n\t\t<div class=\"mini-offset\">\n\t\t\t<div>Фильтр предложений</div>\n\t\t\t<br>\n\t\t\t<a href=\"#\" class=\"button inline green\" ng-click=\"selectExpositions()\">По выставкам</a>\n\t\t\t<a href=\"#\" class=\"button inline outline green\" ng-repeat=\"exposition in filter.expositions\">\n\t\t\t\t{{exposition.name}}\n\t\t\t\t<span class=\"fa fa-times sup right pointer\" ng-click=\"removeFilter(exposition, 'expositions')\"></span>\n\t\t\t</a>\n\n\t\t\t<a href=\"#\" class=\"button inline blue\" ng-click=\"selectSubjects()\">Выбрать тематики</a>\n\t\t\t<a href=\"#\" class=\"button inline outline blue\" ng-repeat=\"subject in filter.subjects\">\n\t\t\t\t{{subject.name}}\n\t\t\t\t<span class=\"fa fa-times sup right pointer\" ng-click=\"removeFilter(subject, 'subjects')\"></span>\n\t\t\t</a>\n\t\t</div>\n\t</div>\n\n\t<div class=\"card-list masonry-layout\">\n\n\t\t<div id=\"create-testdrive\" class=\"card\">\n\t\t\t<div class=\"offset\">\n\t\t\t\t<img src=\"/icon/create-testdrive.png\" alt=\"\">\n\t\t\t\t<a href=\"\" class=\"button inline blue\">Создать предложение</a>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"card\" ng-repeat=\"offer in catalog\">\n\t\t\t<div class=\"cover\" ui-sref=\"offer-index({id: offer._id})\">\n\t\t\t\t<img ng-if=\"offer.photo\" ng-src=\"http://expotestdrive.ru:3000/img/uploads/{{offer.photo.filename}}\" alt=\"\" class=\"cover\">\n\t\t\t\t<div class=\"slide-button\" ng-click=\"replay($event, offer)\">\n\t\t\t\t\t<img src=\"/icon/icon6.png\" alt=\"\">\n\t\t\t\t\t<span class=\"label\">{{offer.prompt}}</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"offset-card\">\n\t\t\t\t<h2 class=\"title\"  ui-sref=\"offer-index({id: offer._id})\">{{offer.name}}</h2>\n\t\t\t\t<div class=\"exhibition-title\">Выставка: <strong>{{offer.exposition.displayName}}</strong></div>\n\t\t\t\t<p class=\"content\">{{offer.description}}</p>\n\n\t\t\t\t<div class=\"icons\">\n\t\t\t\t\t<span><img src=\"/icon/users.png\" alt=\"\"> &gt; {{offer.viewed}}</span>\n\t\t\t\t\t<span><img src=\"/icon/user.png\" alt=\"\">{{offer.audienceCount}}</span>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<br>\n\t<br>\n\n\t<div class=\"text-center\" ng-if=\"!flow\">\n\t\t<a href=\"#\" class=\"button blue text inline\" ng-click=\"startFlow()\">Загрузить еще</a>\n\t</div>\n\n</div>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.366e6116800ada87d4bb.hot-update.js.map