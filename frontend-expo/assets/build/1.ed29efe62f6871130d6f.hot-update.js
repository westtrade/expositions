webpackHotUpdate(1,{

/***/ 48:
/***/ function(module, exports) {

	'use strict';
	
	var calculateHeight = function calculateHeight(element) {
		console.log(element.scrollHeight);
	};
	
	module.exports = function ($scope, $element, cfpLoadingBar) {
	
		var element = $element[0];
	
		document.addEventListener('scroll', function (event) {
			var contentIsScrolled = element.scrollHeight - element.scrollTop === element.clientHeight;
			console.log(contentIsScrolled);
			console.log(element.scrollHeight, document.body.scrollTop, element.clientHeight);
		});
	
		$scope.filter = {
			page: 0
		};
	
		$scope.flow = false;
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
			cfpLoadingBar.start();
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				calculateHeight(element);
	
				cfpLoadingBar.complete();
	
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	};

/***/ }

})
//# sourceMappingURL=1.ed29efe62f6871130d6f.hot-update.js.map