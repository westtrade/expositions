webpackHotUpdate(1,{

/***/ 4:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(5);
	var uiRouter = __webpack_require__(7);
	
	var app = angular.module('expotestdrive', [uiRouter]);
	var router = __webpack_require__(8);
	app.config(router);
	
	var topBar = __webpack_require__(9);
	app.directive('topBar', topBar);
	
	module.exports = app;

/***/ }

})
//# sourceMappingURL=1.824f8cf7a56b8be548ae.hot-update.js.map