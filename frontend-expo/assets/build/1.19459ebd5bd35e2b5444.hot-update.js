webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeApply = __webpack_require__(60);
	var ExpoError = __webpack_require__(109);
	var angular = __webpack_require__(44);
	
	var events = {
		scroll: null
	};
	
	window.addEventListener('scroll', function (event) {
		if (events.scroll) {
			events.scroll(event);
		}
	});
	
	module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', function ($scope, $element, cfpLoadingBar, $modals) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0,
			subjects: [],
			expositions: [
				// { name: 'Test', id: 1}
			]
		};
	
		$scope.selectSubjects = function () {
	
			var data = {
				initial: $scope.filter.subjects
			};
	
			$modals.open('subject-selector', data).then(function () {
				var subjects = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				safeApply($scope, function () {
					$scope.filter.subjects = [];
					angular.extend($scope.filter.subjects, subjects);
				});
			});
		};
	
		$scope.selectExpositions = function () {
	
			var data = {
				initial: $scope.filter
			};
	
			$modals.open('exposition-selector', data).then(function () {
				var expositionsList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				safeApply($scope, function () {
					$scope.filter.expositions = [];
					$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
				});
			});
		};
	
		$scope.removeFilter = function (removed, filterType) {
			var filterTypeExists = filterType in $scope.filter;
	
			if (!filterTypeExists) {
				throw new Error('Wrong type of filter (filterType argumnt). It must be in allowed list of types: '.Object.keys($scope.filter).join(', '));
			}
	
			$scope.filter[filterType] = $scope.filter[filterType].filter(function (current) {
				return removed.id !== current.id;
			});
		};
	
		$scope.catalog = [];
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog, jwr) {
				var error = jwr.error;
	
	
				if (error) {
					error = new ExpoError(error);
					console.log(error);
					return;
				}
	
				// console.log(catalog[1]);
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
				$scope.loading = false;
	
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		events.scroll = function (event) {
			var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
			var offset = 400;
			var contentScrolledHeight = element.scrollHeight - scrollTop - offset;
			var contentIsScrolled = contentScrolledHeight <= 0; //todo fix this bug
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		};
	}];

/***/ },

/***/ 109:
/***/ function(module, exports) {

	'use strict';
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var ExpoError = function (_Error) {
		_inherits(ExpoError, _Error);
	
		function ExpoError(_ref) {
			var message = _ref.message,
			    stack = _ref.stack;
	
			_classCallCheck(this, ExpoError);
	
			return _possibleConstructorReturn(this, (ExpoError.__proto__ || Object.getPrototypeOf(ExpoError)).call(this, message));
		}
	
		return ExpoError;
	}(Error);
	
	module.exports = ExpoError;

/***/ }

})
//# sourceMappingURL=1.19459ebd5bd35e2b5444.hot-update.js.map