webpackHotUpdate(1,{

/***/ 84:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(83),
	    attributesToString = _require.attributesToString,
	    makeHtml = _require.makeHtml,
	    copyAttributes = _require.copyAttributes;
	
	var angular = __webpack_require__(44);
	
	module.exports = function (elementTemplate, element) {
		var styleOptions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
		var styledElement = void 0;
		styledElement = makeHtml(elementTemplate);
		copyAttributes(element, styledElement, styleOptions);
	
		var $element = angular.element(element);
		var $injector = $element.injector();
		var $scope = $element.scope();
		var $compile = $injector.get('$compile');
	
		styledElement = $compile(styledElement)($scope);
	
		$element.prepend(styledElement);
	
		// element.parentNode.insertBefore(styledElement, element.nextSibling);
		// styledElement.insertBefore(element, styledElement.firstChild);
	
		if (element.disabled) {
			styledElement.classList.add('disabled');
		}
	
		return styledElement;
	};

/***/ }

})
//# sourceMappingURL=1.52393119a9b17a526ba0.hot-update.js.map