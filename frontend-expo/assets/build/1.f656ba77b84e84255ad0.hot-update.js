webpackHotUpdate(1,{

/***/ 169:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var validate = __webpack_require__(170);
	var safeApply = __webpack_require__(167);
	var qwery = __webpack_require__(175);
	
	var getFormData = function getFormData(form) {
		return Object.entries(form.elements || {}).reduce(toData, {});
	};
	
	var validateForm = function validateForm(form) {
		var constraint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
		var data = getFormData(form);
	
		var onlyExists = Object.entries(constraint).reduce(function (constraint, _ref) {
			var _ref2 = _slicedToArray(_ref, 2),
			    fieldName = _ref2[0],
			    rule = _ref2[1];
	
			if (form.elements[fieldName]) {
				constraint[fieldName] = rule;
			}
	
			return constraint;
		}, {});
	
		// TODO REFACTOR!
		return validate.async(data, onlyExists).then(function (field) {
			return { data: data, error: null, field: field };
		}).catch(function (error) {
			return { data: data, error: error, field: null };
		});
	};
	
	// TODO add arrays support
	var toData = function toData() {
		var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var _ref3 = arguments[1];
	
		var _ref4 = _slicedToArray(_ref3, 2),
		    fieldName = _ref4[0],
		    input = _ref4[1];
	
		if (fieldName !== input.name) {
			return result;
		}
	
		var value = input.value;
		if (input.type === 'checkbox') {
			value = input.checked;
		}
	
		result[fieldName] = value;
		return result;
	};
	
	var formApply = function formApply($scope, $element) {
		var constraint = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
		var cb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (err, data) {};
	
	
		console.log($element);
	
		var formElement = $element;
		if ($element.length === 1) {
			formElement = $element[0];
		}
	
		var _qwery = qwery('form', formElement),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    form = _qwery2[0];
	
		var isDirty = false;
	
		$scope.form = $scope.form ? $scope.form : {};
		$scope.error = $scope.error ? $scope.error : {};
	
		var name = form.name;
		if (!name || !name.length) {
			name = Array.from(document.forms).indexOf(form).toString();
		}
	
		$scope.form[name] = $scope.form[name] ? $scope.form[name] : getFormData(form);
	
		var formUdate = function formUdate(event) {
	
			var constraintName = event.target.name;
	
			var fieldConstraint = constraintName in constraint ? constraint[constraintName] : {};
	
			validateForm(form, _defineProperty({}, constraintName, fieldConstraint)).then(function (_ref5) {
				var error = _ref5.error,
				    data = _ref5.data;
	
	
				safeApply($scope, function () {
					$scope.form[name][constraintName] = data[constraintName];
				});
	
				safeApply($scope, function () {
					if (isDirty) {
	
						if (!error) {
							delete $scope.error[constraintName];
						} else {
							Object.assign($scope.error, error);
						}
					}
				});
			});
		};
	
		form.addEventListener('input', formUdate, true);
		form.addEventListener('change', formUdate, true);
	
		var validate = function validate() {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {
				var error = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
				var result = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			};
	
	
			isDirty = true;
	
			validateForm(form, constraint).then(function (_ref6) {
				var error = _ref6.error,
				    data = _ref6.data;
	
				if (error) {
					safeApply($scope, function () {
						$scope.error = error;
					});
				}
	
				cb(error, data);
			}).catch(function (e) {
				return cb(e, null);
			});
		};
	
		// $scope.form[name].validate = validate;
		form.validate = validate;
	
		var setValue = function setValue(fieldName, value, type) {
	
			var field = form[fieldName];
			field.value = value;
	
			if (!type) {
	
				var inputEvent = new Event('input');
				field.dispatchEvent(inputEvent);
	
				var changeEvent = new Event('change');
				field.dispatchEvent(changeEvent);
			} else {
				var _changeEvent = new Event(type);
				field.dispatchEvent(_changeEvent);
			}
	
			return field;
		};
	
		// $scope.form[name].setValue = setValue;
		form.setValue = setValue;
	
		return form;
	};
	
	module.exports = {
		getFormData: getFormData,
		validateForm: validateForm,
		formApply: formApply,
		toData: toData
	};

/***/ }

})
//# sourceMappingURL=1.f656ba77b84e84255ad0.hot-update.js.map