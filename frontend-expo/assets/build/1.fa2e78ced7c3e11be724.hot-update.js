webpackHotUpdate(1,{

/***/ 114:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(60);
	var messages = __webpack_require__(115);
	
	var angular = __webpack_require__(44);
	
	var ROLES = ['guest', 'exponent', 'organizer', 'moderator', 'administrator'];
	
	var isOwner = function isOwner(context, user) {
		console.log(context, user);
	
		return true;
	};
	
	var RULES = {
		'view navbar menu': ['!guest'],
		'open slider editor': ['administrator'],
		'change offer': ['administrator', 'moderator', isOwner],
		'delete offer': ['administrator', 'moderator', isOwner]
	};
	
	var ANONYMOUS_USER = {
		role: 'guest',
		displayName: 'Аноним'
	};
	
	var User = function () {
		function User() {
			var _this = this;
	
			_classCallCheck(this, User);
	
			this.watchers = [];
			this.state = {};
	
			var initState = messages.get('user', ANONYMOUS_USER);
	
			this.setState(initState, true);
			messages.on('user', function (event, userState) {
				_this.setState(userState, true);
			});
		}
	
		_createClass(User, [{
			key: 'bind',
			value: function bind($scope) {
				var _this2 = this;
	
				this.watchers.push($scope);
	
				safeApply($scope, function () {
					$scope.user = _this2.getState();
				});
	
				$scope.$on('$destroy', function () {
					var watcherIdx = _this2.watchers.indexOf($scope);
					_this2.watchers.splice(watcherIdx, 1);
				});
			}
		}, {
			key: 'logout',
			value: function logout() {
				var _this3 = this;
	
				return new Promise(function (resolve, reject) {
					_this3.setState(ANONYMOUS_USER);
					resolve();
				});
			}
		}, {
			key: 'setState',
			value: function setState() {
				var _this4 = this;
	
				var name = null,
				    data = void 0,
				    preventBroadcast = false;
	
				if (typeof (arguments.length <= 0 ? undefined : arguments[0]) === 'string') {
					name = arguments.length <= 0 ? undefined : arguments[0];
					data = arguments.length <= 1 ? undefined : arguments[1];
					preventBroadcast = typeof (arguments.length <= 2 ? undefined : arguments[2]) === 'undefined' ? preventBroadcast : !!(arguments.length <= 2 ? undefined : arguments[2]);
				} else {
					data = arguments.length <= 0 ? undefined : arguments[0];
					preventBroadcast = typeof (arguments.length <= 1 ? undefined : arguments[1]) === 'undefined' ? preventBroadcast : !!(arguments.length <= 1 ? undefined : arguments[1]);
				}
	
				if (name) {
					this.state[name] = data;
				} else {
					this.state = data;
				}
	
				this.watchers.forEach(function ($scope) {
					safeApply($scope, function () {
						$scope.user = _this4.getState();
					});
				});
	
				if (!preventBroadcast) {
					messages.broadcast('user', this.getState());
				}
			}
		}, {
			key: 'getState',
			value: function getState() {
				var _this5 = this;
	
				return angular.extend({}, this.state, {
					can: function can(ruleId) {
						return _this5.can(ruleId);
					},
					logout: function logout() {
						return _this5.logout();
					}
				});
			}
		}, {
			key: 'can',
			value: function can(ruleId) {
				var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	
				var ruleExists = ruleId in RULES;
	
				if (!ruleExists) {
					return false;
				}
	
				var rule = RULES[ruleId];
				var result = false;
	
				var userData = this.getState();
	
				result = rule.reduce(function () {
					var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { allow: null, hasDecision: false };
					var currentRule = arguments[1];
					var _result$allow = result.allow,
					    allow = _result$allow === undefined ? null : _result$allow,
					    _result$hasDecision = result.hasDecision,
					    hasDecision = _result$hasDecision === undefined ? false : _result$hasDecision;
	
	
					if (hasDecision) {
						return result;
					}
	
					var ruleAllows = false;
	
					if (typeof currentRule === 'function') {
						ruleAllows = currentRule(context, userData);
					} else {
						var isNot = currentRule.indexOf('!') === 0;
						if (isNot) {
							currentRule = currentRule.replace(/^!/gim, '');
							ruleAllows = currentRule !== userData.role;
						} else {
							ruleAllows = currentRule === userData.role;
						}
					}
	
					if (allow === null) {
						allow = ruleAllows;
					} else if (allow !== ruleAllows) {
						hasDecision = true;
						allow = false;
					}
	
					return { allow: allow, hasDecision: hasDecision };
				}, { allow: null, hasDecision: false });
	
				console.log(result);
	
				return !!result.allow;
			}
		}]);
	
		return User;
	}();
	
	var currentUser = new User();
	
	module.exports = function () {
		return currentUser;
	};

/***/ }

})
//# sourceMappingURL=1.fa2e78ced7c3e11be724.hot-update.js.map