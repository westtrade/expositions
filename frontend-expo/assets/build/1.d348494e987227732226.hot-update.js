webpackHotUpdate(1,{

/***/ 107:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	window.addEventListener('load', function () {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=initGmaps';
		document.body.appendChild(script);
	});
	
	var templateUrl = __webpack_require__(108);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
					$transclude($scope, function (clonedContent) {});
				};
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.d348494e987227732226.hot-update.js.map