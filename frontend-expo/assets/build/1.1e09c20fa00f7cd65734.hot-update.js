webpackHotUpdate(1,{

/***/ 271:
/***/ function(module, exports) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var createAppend = function createAppend(element) {
	
		var appendElement = document.createElement('div');
	
		appendElement.classList.add('append');
	
		if (element.nextElementSibling) {
			element.parentNode.insertBefore(appendElement, element.nextElementSibling);
		} else {
			element.appendChild(appendElement);
		}
	
		return appendElement;
	};
	
	module.exports = ['$timeout', function ($timeout) {
		return {
			restrict: 'A',
			link: function link($scope, $element, $attrs) {
				var _$attrs$letterCounter = $attrs.letterCounter,
				    letterCounter = _$attrs$letterCounter === undefined ? -1 : _$attrs$letterCounter;
	
				var element = $element[0];
	
				$timeout(function () {
	
					console.log(letterCounter);
	
					console.log(element.nextElementSibling, _typeof(element.nextElementSibling));
	
					var appendElement = element.nextElementSibling && element.nextElementSibling.classList.has('append') ? element.nextElementSibling : createAppend(element);
	
					letterCounter = parseInt(letterCounter);
	
					if (letterCounter > 0) {
						appendElement.innerHtml = element.value.length + ' / ' + letterCounter;
					}
	
					element.addEventListener('input', function (event) {});
				}, 0);
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.1e09c20fa00f7cd65734.hot-update.js.map