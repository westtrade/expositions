webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var Dropzone = __webpack_require__(49);
	__webpack_require__(53);
	var templateUrl = __webpack_require__(240);
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			scope: true,
			link: function link($scope, $element, $attrs, $transclude) {
				var element = $element[0];
	
				console.log(element.parentNode);
	
				$transclude(function (clone) {
					var currentDropzone = new Dropzone(element, {});
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.264df64a46174d3c90ea.hot-update.js.map