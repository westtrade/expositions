webpackHotUpdate(1,{

/***/ 52:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$scope', '$modals', function ($scope, $modals) {
	
		$scope.$watchCollection('sliderList', function (prev, next, $scope) {
			$scope.$emit('slider:update');
		});
	
		$scope.sliderList = [{
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}, {
			title: 'Откройте выставку',
			description: 'для тест-драйва, чтобы привлечь больше новых посетителей и экспонентов на выставку',
			destination: {},
			cover: 'https://upload.wikimedia.org/wikipedia/commons/6/6c/Herjangsfjorden_%26_Ofotfjorden,_wide,_2009_09.jpg'
		}];
	
		if ($scope.$parent.$slider) {
			$scope.$parent.$slider.update();
		}
	
		if ($scope.$parent.$slider) {
			$scope.$parent.$slider.run();
		}
	
		$scope.openEditor = function () {
			var sliderList = $scope.sliderList;
	
			$modals.open('slider-editor', { sliderList: sliderList }, function () {
				var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
				    sliderList = _ref.sliderList;
	
				$scope.sliderList = sliderList;
			});
		};
	
		// console.log($scope);
	
		// console.log($scope.$parent.$parent.$slider.update());
		// console.log($scope.$slider);
	
		// $scope.$slider.update();
	}];

/***/ }

})
//# sourceMappingURL=1.b14780e8a1bdc8b7251f.hot-update.js.map