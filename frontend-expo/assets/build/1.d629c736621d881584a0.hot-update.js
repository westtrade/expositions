webpackHotUpdate(1,{

/***/ 75:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _require = __webpack_require__(69),
	    guid = _require.guid;
	
	var loginTemplate = __webpack_require__(76);
	var registrationTemplate = __webpack_require__(77);
	var recoveryTemplate = __webpack_require__(78);
	
	var loginConstraints = __webpack_require__(79);
	var registrationConstraints = __webpack_require__(80);
	
	var _require2 = __webpack_require__(63),
	    formApply = _require2.formApply;
	
	var login = {
		url: '/user/login',
		templateUrl: loginTemplate,
		controller: ['$scope', '$element', function ($scope, $element) {
			var form = $element.find('form')[0];
			formApply($scope, form, loginConstraints);
	
			$scope.submit = function ($event) {
	
				form.validate(function (error, formData) {
					console.log(error, formData);
				});
	
				$event.preventDefault();
			};
		}]
	};
	
	var registration = {
		url: '/user/registration',
		templateUrl: registrationTemplate,
		controller: ['$scope', '$element', function ($scope, $element) {
	
			var form = $element.find('form')[0];
	
			formApply($scope, form, registrationConstraints);
			form.setValue('type', 'company');
			$scope.randomSeed = '';
	
			$scope.changeCaptcha = function () {
				form.setValue('captcha', '').focus();
				$scope.randomSeed = guid();
			};
	
			$scope.select = function (type) {
				form.setValue('type', type);
				console.log($scope.form);
			};
	
			$scope.submit = function ($event) {
	
				form.validate(function (error, formData) {
					if (error) {
						$scope.changeCaptcha();
					}
				});
	
				$event.preventDefault();
			};
		}]
	};
	
	var recovery = {
		url: '/user/recovery',
		templateUrl: recoveryTemplate,
		controller: ['$scope', '$element', function ($scope, $element) {
	
			var form = $element.find('form')[0];
			formApply($scope, form, registrationConstraints);
	
			$scope.email = null;
	
			$scope.setMail = function ($event) {
	
				form.validate(function (error, formData) {
					if (!error) {
						safeApply($scope, function () {
							$scope.email = formData.email;
						});
					}
				});
	
				$event.preventDefault();
			};
		}]
	};
	
	module.exports = { login: login, registration: registration, recovery: recovery };

/***/ }

})
//# sourceMappingURL=1.d629c736621d881584a0.hot-update.js.map