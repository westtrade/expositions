webpackHotUpdate(1,{

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var qwery = __webpack_require__(83);
	
	var _require = __webpack_require__(116),
	    getMap = _require.getMap;
	
	var gmaps = __webpack_require__(101);
	
	var templateUrl = __webpack_require__(102);
	
	var link = ['$scope', function ($scope) {}];
	
	module.exports = function () {
		return {
			scope: true,
			transclude: true,
			restrict: 'ACE',
			templateUrl: templateUrl,
			compile: function compile(tElement, tAttrs, $transclude) {
				return function ($scope, $element, $attrs) {
	
					var element = $element[0];
	
					$transclude($scope, function (clonedContent) {
						var _qwery = qwery('.map', element),
						    _qwery2 = _slicedToArray(_qwery, 1),
						    mapElement = _qwery2[0];
	
						getMap({
							el: mapElement, lat: 0, lng: 0, zoom: 1
						}).then(function (map) {
	
							// console.log(map);
	
							gmaps.geocode({
								address: 'Россия, Липецк',
								callback: function callback(_ref, status) {
									var _ref2 = _slicedToArray(_ref, 1),
									    location = _ref2[0];
	
									var _location$geometry$lo = location.geometry.location,
									    lat = _location$geometry$lo.lat,
									    lng = _location$geometry$lo.lng;
	
									map.setCenter(lat(), lng(), function () {
										console.log('Centred');
									});
								}
							});
						});
					});
				};
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.6f9361ab299ffaea95d1.hot-update.js.map