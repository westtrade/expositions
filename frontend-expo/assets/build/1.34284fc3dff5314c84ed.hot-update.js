webpackHotUpdate(1,{

/***/ 107:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var qwery = __webpack_require__(78);
	
	var restrict = 'ACE';
	
	var createTabHead = function createTabHead() {
		var tabHead = document.createElement('div');
		tabHead.className = 'head';
	
		return tabHead;
	};
	
	var setActiveTab = function setActiveTab(activeIdx) {
		return function (tab, tabIdx) {
	
			if (activeIdx === tabIdx) {
				console.log(tab);
				tab.classList.add('active');
			} else {
				tab.classList.remove('active');
			}
		};
	};
	
	var link = function link($scope, $element, $attrs) {
	
		var element = $element[0];
	
		var _qwery = qwery('.head', element),
		    _qwery2 = _slicedToArray(_qwery, 1),
		    _qwery2$ = _qwery2[0],
		    tabHead = _qwery2$ === undefined ? createTabHead() : _qwery2$;
	
		$scope.switchTab = function (idx) {
	
			idx = parseInt(idx);
			qwery('a', tabHead).forEach(setActiveTab(idx));
			qwery('.tab', element).forEach(setActiveTab(idx));
		};
	
		tabHead.addEventListener('click', function (event) {
	
			var tabIndex = qwery('a', tabHead).indexOf(event.target);
	
			if (tabIndex < 0) {
				return;
			}
	
			$scope.switchTab(tabIndex);
	
			event.preventDefault();
			event.stopPropagation();
		});
	
		var activeIdx = qwery('.tab').reduce(function (activeIdx, tab, index) {
	
			if (tab.classList.contains('active') && !activeIdx) {
				activeIdx = index;
			}
	
			return activeIdx;
		}, 0);
	
		console.log(activeIdx);
	
		$scope.switchTab(activeIdx);
	
		tabHead = qwery('.tab', element).map(function (tab, idx) {
			return tab.title || '\u0411\u0435\u0437 \u043D\u0430\u0437\u0432\u0430\u043D\u0438\u044F ' + idx;
		}).reduce(function (tabHead, tabTile) {
	
			var link = document.createElement('a');
			link.href = '#';
			link.innerHTML = tabTile;
	
			tabHead.appendChild(link);
			return tabHead;
		}, tabHead);
	
		if (!tabHead.parentNode) {
			element.insertBefore(tabHead, element.firstChild);
		}
	};
	
	module.exports = [function () {
	
		return {
			restrict: restrict,
			link: link,
			scope: true
		};
	}];

/***/ }

})
//# sourceMappingURL=1.34284fc3dff5314c84ed.hot-update.js.map