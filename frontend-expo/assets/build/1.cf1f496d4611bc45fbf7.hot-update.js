webpackHotUpdate(1,{

/***/ 101:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var validate = __webpack_require__(92);
	validate.Promise = Promise;
	
	var i18n = __webpack_require__(104);
	
	validate.formatters.grouped = function (errors) {
	
		return errors.reduce(function () {
			var result = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
			var _ref = arguments[1];
			var attribute = _ref.attribute,
			    error = _ref.error;
	
	
			var hasField = attribute in result;
			if (!hasField) {
				result[attribute] = [];
			}
	
			result[attribute].push(i18n.__(error));
			return result;
		}, {});
	};
	
	module.exports = validate;

/***/ },

/***/ 104:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var translate = __webpack_require__(105);
	
	var I18N = function () {
		function I18N() {
			_classCallCheck(this, I18N);
		}
	
		_createClass(I18N, [{
			key: '__',
			value: function __(sentence) {
	
				if (sentence in translate) {
					sentence = translate[sentence];
				}
	
				return sentence;
			}
		}]);
	
		return I18N;
	}();
	
	module.exports = new I18N();

/***/ },

/***/ 105:
/***/ function(module, exports) {

	'use strict';
	
	var translate = {
		'Login can\'t be blank': 'Поле Логин не может быть пустым',
		'Login must be at least 6 characters': 'Поле Логин должно быть больше 6 символов',
		'Password can\'t be blank': 'Поле Пароль не может быть пустым',
		'Password must be at least 6 characters': 'Поле Пароль должно быть больше 6 символов'
	};
	
	module.exports = translate;

/***/ }

})
//# sourceMappingURL=1.cf1f496d4611bc45fbf7.hot-update.js.map