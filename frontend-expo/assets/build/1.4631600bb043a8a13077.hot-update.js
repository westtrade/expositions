webpackHotUpdate(1,{

/***/ 108:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _require = __webpack_require__(75),
	    shortId = _require.shortId;
	
	var GOOGLE_MAPS_KEY = 'AIzaSyD4cZ9kBDsr35AONvMT743XoBA_bwvkprE';
	var YANDEX_MAPS_KEY = null;
	
	var events = {};
	var status = {};
	
	var createScript = function createScript(src, randomCallbackName) {
		var resolve = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = src;
	
		global[randomCallbackName] = function () {
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var scriptLoader = function scriptLoader(serviceId) {
		var dataResolver = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
		var resolve = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
	
		var loaderName = 'ymap';
		var loadStatus = status[loaderName] || 1;
		if (loadStatus === 3) {
			return resolve();
		}
	
		var _events$loaderName = events[loaderName],
		    stack = _events$loaderName === undefined ? [] : _events$loaderName;
	
		stack.push(resolve);
	
		if (loadStatus === 1) {
			status[loaderName] = 2;
	
			var _dataResolver = dataResolver(),
			    _dataResolver2 = _slicedToArray(_dataResolver, 2),
			    src = _dataResolver2[0],
			    randomCallbackName = _dataResolver2[1];
	
			scriptLoader(src, randomCallbackName, function () {
				status[loaderName] = 3;
				stack.forEach(function (cb) {
					return cb();
				});
			});
		}
	};
	
	var loadYMap = function loadYMap(resolve, key) {
		scriptLoader('ymap', function () {
			var randomCallbackName = 'fn' + shortId();
	
			return ['//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=fn' + randomCallbackName + (key ? '&key=' + key : ''), randomCallbackName];
		}, resolve);
	};
	
	var loadGMap = function loadGMap(resolve, key) {
	
		scriptLoader('gmap', function () {
			var randomCallbackName = 'fn' + shortId();
	
			return ['//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=fn' + randomCallbackName + (key ? '&key=' + key : ''), randomCallbackName];
		}, resolve);
	};
	
	// let ymapInitialized = false;
	//
	// const loadYMap = (resolve, key) => {
	//
	// 	if (ymapInitialized) {
	// 		return resolve();
	// 	}
	//
	// 	const randomCallbackName = 'fn' + shortId();
	// 	const script = document.createElement('script');
	// 	script.type = 'text/javascript';
	// 	script.src = '//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=' + randomCallbackName;
	//
	// 	if (key) {
	// 		script.src = script.src + '&key=' + key;
	// 	}
	//
	// 	global[randomCallbackName] = () => {
	// 		ymapInitialized = true
	// 		delete global[randomCallbackName];
	// 		resolve();
	// 	};
	//
	// 	document.body.appendChild(script);
	// };
	//
	//
	//
	//
	
	// const events = {
	// 	gmap: [],
	// 	ymap: [],
	// };
	//
	//
	// let gmapLoadStatus = 0;
	//
	// const loadGMap = (resolve, key) => {
	//
	// 	if (gmapIsInitialized) {
	// 		return resolve();
	// 	}
	//
	// 	const randomCallbackName = 'fn' + shortId();
	//
	// 	const script = document.createElement('script');
	// 	script.type = 'text/javascript';
	// 	script.src = '//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=' + randomCallbackName;
	//
	// 	if (key) {
	// 		script.src = script.src + '&key=' + key;
	// 	}
	//
	// 	global[randomCallbackName] = () => {
	// 		gmapIsInitialized = true
	// 		delete global[randomCallbackName];
	// 		resolve();
	// 	};
	//
	// 	document.body.appendChild(script);
	// };
	
	
	//
	//
	//
	// const loadMultiplie = ({gMapKey, yMapKey} = {}, resolve = () => {}) => {
	//
	// 	let loadStack = [loadGMap, loadYMap];
	// 	let results = Array.from(loadStack);
	//
	// 	const next = (caller) => {
	// 		return function next (result) {
	// 			const idx = loadStack.indexOf(caller);
	// 			if (idx >= 0) {
	// 				loadStack.splice(idx, 1);
	// 				const resultIdx = results.indexOf(caller);
	// 				if (resultIdx >= 0) {
	// 					results[resultIdx] = result;
	// 				}
	// 			}
	//
	// 			if (loadStack.length === 0) {
	// 				resolve(results);
	// 				results = null;
	// 				loadStack = null;
	// 			}
	//
	// 			caller = null;
	// 		}
	// 	};
	//
	// 	loadGMap(next(loadGMap), gMapKey);
	// 	loadYMap(next(loadYMap), yMapKey);
	// };
	
	
	module.exports = function (resolve) {
		// loadMultiplie({gMapKey: GOOGLE_MAPS_KEY, yMapKey: YANDEX_MAPS_KEY}, resolve);
	
		Promise.all([]).then(resolve);
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.4631600bb043a8a13077.hot-update.js.map