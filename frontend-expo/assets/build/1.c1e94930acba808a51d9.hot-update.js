webpackHotUpdate(1,{

/***/ 113:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var catalogTemplate = __webpack_require__(66);
	
	var _require = __webpack_require__(70),
	    formApply = _require.formApply;
	
	var safeApply = __webpack_require__(60);
	
	var catalog = {
		url: '/offer/:id',
		templateUrl: catalogTemplate,
		controller: ['$scope', '$stateParams', 'User', 'cfpLoadingBar', '$modals', '$state', function ($scope, $stateParams, User, cfpLoadingBar, $modals, $state) {
	
			User.bind($scope);
	
			$scope.offer = {};
	
			if (!$stateParams.id) {
				return $state.go('404');
			}
	
			var loadItem = function loadItem($scope, id, cb) {
	
				cfpLoadingBar.start();
				$scope.loading = true;
	
				io.socket.get('/offers/offer', { id: id }, function (offer, jwr) {
					var error = jwr.error;
	
	
					if (error) {
						throw new ExpoError(error);
					}
	
					console.log(offer);
	
					safeApply($scope, function () {
						$scope.offer = offer;
						$scope.loading = false;
					});
	
					cfpLoadingBar.complete();
					cb();
				});
			};
	
			loadItem($scope, $stateParams.id, function () {
				console.log('Loaded');
			});
	
			$scope.replay = function (item) {
				var user = User.getState();
				$modals.open('offer-teaser-form', { item: item, user: user });
			};
		}]
	};
	
	module.exports = { catalog: catalog };

/***/ }

})
//# sourceMappingURL=1.c1e94930acba808a51d9.hot-update.js.map