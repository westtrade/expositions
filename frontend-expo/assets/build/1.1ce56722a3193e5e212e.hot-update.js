webpackHotUpdate(1,{

/***/ 38:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var safeApply = __webpack_require__(41);
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var qwery = __webpack_require__(34);
	
	var SliderTimer = function () {
		function SliderTimer() {
			_classCallCheck(this, SliderTimer);
	
			this.currentTimer = null;
			this.$scope = null;
			this.timeout = 500;
		}
	
		_createClass(SliderTimer, [{
			key: 'run',
			value: function run($scope) {
				var _this = this;
	
				var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
	
	
				console.log(timeout);
	
				this.$scope = $scope;
				this.timeout = timeout;
	
				this.currentTimer = setTimeout(function ($scope, currentTimerId, timeout) {
					$scope.next();
					clearTimeout(currentTimerId);
					console.log(_this.currentTimer);
					_this.currentTimer = null;
					_this.run($scope, timeout);
				}, timeout, this.$scope, this.currentTimer, timeout);
			}
		}, {
			key: 'pause',
			value: function pause() {
				if (this.currentTimer) {
					console.log('PAUSE');
					clearTimeout(this.currentTimer);
				}
			}
		}, {
			key: 'resume',
			value: function resume() {
				if (this.$scope) {
					this.run(this.$scope, this.timeout);
				}
			}
		}]);
	
		return SliderTimer;
	}();
	
	;
	
	var sliderTimer = new SliderTimer();
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
	
				sliderTimer.pause();
	
				safeApply($scope, function () {
					$scope.active = active;
	
					qwery('.slide', $element[0]).forEach(function (item, idx) {
						if (item.classList.contains('active')) {
							item.classList.remove('active');
						}
	
						if (idx === active) {
							item.classList.add('active');
						}
						setTimeout(function () {
							return sliderTimer.resume();
						}, 100);
					});
				});
			};
	
			$scope.next = function () {
				var current = parseInt($scope.active);
				var next = current + 1;
				current = next >= $scope.total ? 0 : next;
				$scope.setActive(current);
			};
	
			$scope.prev = function () {
				var current = parseInt($scope.active);
				var prev = current - 1;
				current = prev <= 0 ? $scope.total - 1 : prev;
	
				$scope.setActive(current);
			};
	
			sliderTimer.run($scope, 2000);
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ }

})
//# sourceMappingURL=1.1ce56722a3193e5e212e.hot-update.js.map