webpackHotUpdate(1,{

/***/ 239:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var Dropzone = __webpack_require__(49);
	__webpack_require__(53);
	var templateUrl = __webpack_require__(240);
	
	var qwery = __webpack_require__(93);
	
	module.exports = function () {
	
		return {
			restrict: 'EAC',
			templateUrl: templateUrl,
			transclude: true,
			scope: true,
			link: function link($scope, $element, $attrs, $controllers, $transclude) {
				var element = $element[0];
	
				$transclude(function (clone) {
					var currentDropzone = new Dropzone(element, {});
				});
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.517790b1ec5d6261856d.hot-update.js.map