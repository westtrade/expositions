webpackHotUpdate(1,{

/***/ 272:
/***/ function(module, exports) {

	'use strict';
	
	var isDirty = false;
	
	window.onbeforeunload = function (event) {
	
		return undefined;
	
		if (isDirty) {
	
			var $message = 'It looks like you have been editing something' + ' - if you leave before saving, then your changes will be lost.';
	
			return $message;
		}
	
		return undefined;
	};
	
	module.exports = function () {
	
		return {
			enable: function enable(dirty) {
				isDirty = !!dirty;
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.2da1eb118446080c38fd.hot-update.js.map