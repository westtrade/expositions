webpackHotUpdate(1,{

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var qwery = __webpack_require__(39);
	var onePixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=';
	
	var newMarkerId = function newMarkerId() {
		return ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
	};
	
	// style
	
	var createMarker = function createMarker(masonryId) {
		var marker = new Image();
	
		marker.setAttribute('data-masonry-id', masonryId);
	
		marker.src = onePixel;
		marker.style.position = 'fixed';
		marker.style.left = '0';
		marker.style.top = '0';
	
		return marker;
	};
	
	var markNgRepeat = function markNgRepeat(masonryId, item) {
		var marker = createMarker(masonryId);
		item.setAttribute('data-masonry-id', masonryId);
		item.appendChild(marker);
	};
	
	var markNgRepeatStart = function markNgRepeatStart(masonryId, item) {};
	
	var ngRepeatIsChanged = function ngRepeatIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var ngRepeatStartIsChanged = function ngRepeatStartIsChanged(masonryId, item) {
		//todo check if element is changed
		return false;
	};
	
	var markItem = function markItem() {
		var info = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var item = arguments[1];
		var _info$added = info.added,
		    added = _info$added === undefined ? [] : _info$added,
		    _info$changed = info.changed,
		    changed = _info$changed === undefined ? [] : _info$changed;
	
	
		var ngRepeat = item.getAttribute('ng-repeat');
		var ngRepeatStart = item.getAttribute('ng-repeat');
	
		if (!ngRepeat && !ngRepeatStart) {
			return info;
		}
	
		var masonryId = item.getAttribute('data-masonry-id');
	
		if (masonryId) {
			var isChanged = ngRepeat ? ngRepeatIsChanged(masonryId, item) : ngRepeatStartIsChanged(masonryId, item);
	
			if (isChanged) {
				changed.push(masonryId);
				info.changed = changed;
			}
	
			return info;
		}
	
		masonryId = newMarkerId();
	
		if (ngRepeat) {
			markNgRepeat(masonryId, item);
		} else if (ngRepeatStart) {
			markNgRepeatStart(masonryId, item);
		}
	
		added.push(masonryId);
		info.added = added;
	
		return info;
	};
	
	module.exports = {
		markItem: markItem,
		onePixel: onePixel
	};

/***/ }

})
//# sourceMappingURL=1.f57b15bbfe669cc45f91.hot-update.js.map