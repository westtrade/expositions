webpackHotUpdate(1,{

/***/ 51:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var angular = __webpack_require__(17);
	var $body = angular.element(document.body);
	var templateUrl = __webpack_require__(52);
	
	var qwery = __webpack_require__(39);
	
	module.exports = ['$modals', '$rootScope', '$provide', function ($modals, $rootScope, $provide) {
	
		return {
			templateUrl: templateUrl,
			link: function link($scope, $element) {
	
				var element = $element[0];
	
				var _qwery = qwery('.background', element),
				    _qwery2 = _slicedToArray(_qwery, 1),
				    background = _qwery2[0];
	
				background.addEventListener('click', function (event) {
					$modals.close();
					event.stopPropagation();
					event.preventDefault();
				});
	
				$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
					var _qwery3 = qwery('#' + modalId, element),
					    _qwery4 = _slicedToArray(_qwery3, 1),
					    currentModal = _qwery4[0];
	
					if (currentModal) {
						currentModal.classList.toggle('selected', isOpened);
	
						// const  $modalInjector = angular.element(currentModal).injector();
						// $provide.value('$modalData', () => data);
						// console.log(data);
					}
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.ec4a217a636d1231eed7.hot-update.js.map