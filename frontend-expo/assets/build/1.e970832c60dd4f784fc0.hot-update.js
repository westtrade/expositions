webpackHotUpdate(1,{

/***/ 8:
/***/ function(module, exports) {

	'use strict';
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login'
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration'
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.e970832c60dd4f784fc0.hot-update.js.map