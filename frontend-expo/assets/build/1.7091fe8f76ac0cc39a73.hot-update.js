webpackHotUpdate(1,{

/***/ 51:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(17);
	var $body = angular.element(document.body);
	var templateUrl = __webpack_require__(52);
	
	module.exports = ['$modals', function ($modals) {
		return {
			templateUrl: templateUrl,
			link: function link($scope, $element) {
	
				console.log($element);
	
				$element.on('click', function (event) {
					console.log(event);
					event.preventDefault();
				});
	
				$scope.$on('modal-toggled', function () {
					for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
						args[_key] = arguments[_key];
					}
	
					console.log(args);
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.7091fe8f76ac0cc39a73.hot-update.js.map