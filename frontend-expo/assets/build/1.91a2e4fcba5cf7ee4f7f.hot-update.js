webpackHotUpdate(1,{

/***/ 78:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var styleInputs = __webpack_require__(79);
	
	module.exports = function () {
		return {
			priority: 15,
			restrict: 'AEM',
			require: 'ngModel',
			link: function link(scope, element, attrs, ngModel) {
	
				if ('item' in scope && scope.item.selected) {
					element.attr('checked', true); //TODO Remove hack, not work for ngModel
				}
	
				styleInputs(element);
			}
		};
	};

/***/ }

})
//# sourceMappingURL=1.91a2e4fcba5cf7ee4f7f.hot-update.js.map