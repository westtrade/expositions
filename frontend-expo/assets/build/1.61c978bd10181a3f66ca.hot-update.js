webpackHotUpdate(1,{

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var qwery = __webpack_require__(40);
	var safeApply = __webpack_require__(45);
	var templateUrl = __webpack_require__(46);
	
	var SliderTimer = function () {
		function SliderTimer() {
			_classCallCheck(this, SliderTimer);
	
			this.currentTimer = null;
			this.$slider = null;
		}
	
		_createClass(SliderTimer, [{
			key: 'run',
			value: function run() {
				var _this = this;
	
				var $slider = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	
	
				if ($slider) {
					this.$slider = $slider;
				}
	
				this.currentTimer = setTimeout(function () {
	
					_this.$slider.next(true);
					_this.currentTimer = clearTimeout(_this.currentTimer);
					_this.run();
				}, this.$slider.timeout);
			}
		}, {
			key: 'pause',
			value: function pause() {
				if (this.currentTimer) {
					this.currentTimer = clearTimeout(this.currentTimer);
				}
			}
		}, {
			key: 'resume',
			value: function resume() {
				if (!this.currentTimer) {
					this.currentTimer = clearTimeout(this.currentTimer);
					this.run();
				}
			}
		}]);
	
		return SliderTimer;
	}();
	
	;
	
	var init = function init($scope, container) {
		var sliderContent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
	
	
		var $slider = {
			timeout: 4000,
			total: 0,
			acive: 0,
			container: container,
			scope: $scope,
			timer: new SliderTimer(),
	
			pause: function pause() {
				$slider.timer.pause();
			},
			resume: function resume() {
				$slider.timer.resume();
			},
			run: function run() {
				$slider.timer.run($slider);
			},
			setActive: function setActive(active) {
				var isAutomatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
	
				if (!isAutomatic) {
					$slider.pause();
				}
	
				safeApply($slider.scope, function () {
	
					$slider.active = active;
	
					qwery('.slide', $slider.container).forEach(function (item, idx) {
						idx === active ? item.classList.add('active') : item.classList.remove('active');
					});
	
					if (!isAutomatic) {
						$slider.resume();
					}
				});
			},
			next: function next() {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($slider.active);
				var next = current + 1;
				current = next >= $slider.total ? 0 : next;
				$slider.setActive(current, isAutomatic);
			},
			prev: function prev() {
				var isAutomatic = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	
				var current = parseInt($slider.active);
				var prev = current - 1;
				current = prev < 0 ? $slider.total - 1 : prev;
				$slider.setActive(current, isAutomatic);
			},
			setContent: function setContent() {
				var sliderContent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
	
				console.log(sliderContent);
	
				if (!sliderContent || !sliderContent.length) {
					return;
				}
	
				var itemsCount = 0;
				var initialActive = 0;
	
				angular.forEach(sliderContent, function (current, key) {
	
					if (!current.classList || !current.classList.contains('slide')) {
						return;
					}
	
					if (current.classList.contains('active')) {
						initialActive = itemsCount;
					}
	
					itemsCount++;
				});
	
				$slider.total = itemsCount;
				$slider.active = 0;
	
				$slider.setActive(initialActive);
			}
		};
	
		$scope.$slider = $slider;
		$slider.setContent(sliderContent);
	
		return $slider;
	};
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
	
		return {
			transclude: true,
			scope: true,
			templateUrl: templateUrl,
			link: function link($scope, $element, attrs, ctrl, $transclude) {
	
				var container = $element[0];
				var $slider = init($scope, container);
				container.addEventListener('mouseenter', function (event) {
					if (['arrow', 'button'].filter(function (name) {
						return event.target.classList.contains(name);
					}).length) {
						$slider.pause();
					}
				}, true);
	
				container.addEventListener('mouseleave', $slider.resume, true);
				$rootScope.$on('modal-toggled', function (event, modalName, isOpened) {
					return isOpened ? $slider.pause() : $slider.resume();
				});
	
				var $innerScope = $scope.$new();
				console.log($innerScope);
				$transclude($scope, function (sliderContent) {
					// console.log('transclude', sliderContent);
					// $compile(sliderContent)($scope);
	
					$element.append(sliderContent);
	
					$element.on('$destroy', function () {
						$innerScope.$destroy();
					});
	
					$slider.setContent(sliderContent);
					$slider.run();
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.61c978bd10181a3f66ca.hot-update.js.map