webpackHotUpdate(1,{

/***/ 66:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/offer-page.html';
	var html = "<article class=\"container event-page\">\n\n\t<h1>{{offer.name}}</h1>\n\n\t<div class=\"info\">\n\t\t<i class=\"fa fa-fw fa-map-marker\"></i> {{offer.exposition.location.name}}\n\t</div>\n\n\n\n\t<div class=\"page-columns\">\n\n\t\t<div class=\"left\">\n\n\t\t\t<div class=\"cover\">\n\t\t\t\t<img ng-src=\"http://expotestdrive.ru:3000/img/uploads/{{offer.photo.filename}}\" ng-alt=\"{{offer.name}}\">\n\t\t\t</div>\n\n\t\t\t<div class=\"description\">\n\t\t\t\t{{offer.description}}\n\t\t\t</div>\n\n\t\t\t<div class=\"directions\" ng-if=\"offer.themes\">\n\t\t\t\tНаправления:\n\t\t\t\t<a href=\"#\" class=\"button outline inline\" ng-repeat=\"theme in themes\">{{theme.name}}</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"right\">\n\n\n\t\t</div>\n\t</div>\n\n\n\n</article>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.0202c3d7c4ac57fe66ec.hot-update.js.map