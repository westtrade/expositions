webpackHotUpdate(1,{

/***/ 75:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var templateUrl = __webpack_require__(76);
	var qwery = __webpack_require__(83);
	
	var _qwery = qwery('.page-content'),
	    _qwery2 = _slicedToArray(_qwery, 1),
	    pageContent = _qwery2[0];
	
	var controller = ['$scope', '$element', function ($scope, $element) {
	
		var element = $element[0];
	
		var _qwery3 = qwery('.user-nav', element),
		    _qwery4 = _slicedToArray(_qwery3, 1),
		    userNav = _qwery4[0];
	
		userNav.addEventListener('mouseover', function (event) {
			pageContent.classList.add('blur');
		});
	
		userNav.addEventListener('mouseleave', function (event) {
			pageContent.classList.remove('blur');
		});
	}];
	
	module.exports = function () {
		return {
			templateUrl: templateUrl,
			controller: controller
		};
	};

/***/ }

})
//# sourceMappingURL=1.04f4c3eac7dbe5a9d2f5.hot-update.js.map