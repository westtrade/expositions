webpackHotUpdate(1,{

/***/ 56:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$rootScope', '$compile', function ($rootScope, $compile) {
		return {
			transclude: 'element',
			restrict: 'C',
			link: function link($scope, $element, $attr, ctrl, $transclude) {
	
				var newModal = null;
	
				$transclude(function (clone, newScope) {
					var modalSource = clone[0].outerHTML;
					// const currentModalId = clone[0].id;
	
					$rootScope.$on('modal-toggled', function (event, modalId, isOpened, data) {
						if (modalId != $attr.id) {
							return;
						}
	
						if (isOpened) {
	
							newModal = angular.element(modalSource);
							$element.after(newModal);
							newModal.removeClass('modal-source').addClass('modal selected');
							var $modalScope = $rootScope.$new(true);
							var result = $compile(newModal)($modalScope);
							console.log(result);
						} else if (newModal) {
							newModal.remove();
							newModal.scope().$destroy();
							newModal = null;
						}
					});
				});
			}
		};
	}];

/***/ }

})
//# sourceMappingURL=1.dd8833302cb2028f617b.hot-update.js.map