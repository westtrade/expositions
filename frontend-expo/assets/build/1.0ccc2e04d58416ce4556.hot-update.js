webpackHotUpdate(1,{

/***/ 19:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var loginPage = __webpack_require__(20);
	var registrationPage = __webpack_require__(21);
	var recoveryPage = __webpack_require__(35);
	var changePasswordPage = __webpack_require__(36);
	
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/'
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage
	
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', function ($scope) {
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					$scope.randomSeed = guid();
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', function ($scope) {
				$scope.email = null;
				$scope.data = {
					email: null
				};
	
				$scope.setMail = function () {
					$scope.email = $scope.data.email;
				};
			}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
	
			// template: require('./home.html'),
			// controller: 'HomeController',
			// controllerAs: 'home'
		});
	};
	
	module.exports = routes;

/***/ },

/***/ 36:
/***/ function(module, exports) {

	var path = '/var/www/assets/front/views/change-password-page.html';
	var html = "<form class=\"card center\">\n\n\t<div class=\"offset\">\n\t\t<div class=\"field text-center bigger\">\n\t\t\tЕсть аккаунт? <a href=\"#\" ui-sref=\"user-login\" >Войти</a>\n\t\t</div>\n\t</div>\n\t<div class=\"separator\"></div>\n\t<div class=\"offset\">\n\n\t\t<h1>Восстановление доступа</h1>\n\t\t<div class=\"field\">\n\t\t\tВведите адрес электронной почты, который был указан при регистрации.\n\t\t</div>\n\t\t<br>\n\n\t\t<input type=\"email\" placeholder=\"Email\" name=\"email\" ng-model=\"data.email\">\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\n\t\t<button ng-click=\"setMail()\" class=\"primary\" type=\"submit\">Сбросить пароль</button>\n\t\t<br>\n\t</div>\n</div>\n\n</form>\n";
	window.angular.module('ng').run(['$templateCache', function(c) { c.put(path, html) }]);
	module.exports = path;

/***/ }

})
//# sourceMappingURL=1.0ccc2e04d58416ce4556.hot-update.js.map