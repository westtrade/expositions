webpackHotUpdate(1,{

/***/ 108:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _require = __webpack_require__(75),
	    shortId = _require.shortId;
	
	var GOOGLE_MAPS_KEY = 'AIzaSyD4cZ9kBDsr35AONvMT743XoBA_bwvkprE';
	var YANDEX_MAPS_KEY = null;
	
	var events = {};
	var status = {};
	
	var createScript = function createScript(src, randomCallbackName) {
		var resolve = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
	
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = src;
	
		console.log(src, randomCallbackName);
	
		global[randomCallbackName] = function () {
			delete global[randomCallbackName];
			resolve();
		};
	
		document.body.appendChild(script);
	};
	
	var scriptLoader = function scriptLoader(loaderName) {
		var dataResolver = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
		var resolve = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
	
	
		var loadStatus = status[loaderName] || 1;
		if (loadStatus === 3) {
			return resolve();
		}
	
		var _events$loaderName = events[loaderName],
		    stack = _events$loaderName === undefined ? [] : _events$loaderName;
	
		stack.push(resolve);
	
		if (loadStatus === 1) {
			status[loaderName] = 2;
	
			var _dataResolver = dataResolver(),
			    _dataResolver2 = _slicedToArray(_dataResolver, 2),
			    src = _dataResolver2[0],
			    randomCallbackName = _dataResolver2[1];
	
			createScript(src, randomCallbackName, function () {
				status[loaderName] = 3;
	
				console.log('Load complete');
	
				stack.forEach(function (cb) {
					return cb();
				});
			});
		}
	};
	
	var loadYMap = function loadYMap(resolve, key) {
		scriptLoader('ymap', function () {
			var randomCallbackName = 'fn' + shortId();
	
			return ['//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=fn' + randomCallbackName + (key ? '&key=' + key : ''), randomCallbackName];
		}, resolve);
	};
	
	var loadGMap = function loadGMap(resolve, key) {
	
		scriptLoader('gmap', function () {
			var randomCallbackName = 'fn' + shortId();
			return ['//maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=fn' + randomCallbackName + (key ? '&key=' + key : ''), randomCallbackName];
		}, resolve);
	};
	
	var loadMultiplie = function loadMultiplie() {
		var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
		    gMapKey = _ref.gMapKey,
		    yMapKey = _ref.yMapKey;
	
		var resolve = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	
	
		var loadStack = [loadGMap, loadYMap];
		var results = Array.from(loadStack);
	
		var next = function next(caller) {
			return function next(result) {
				var idx = loadStack.indexOf(caller);
				if (idx >= 0) {
					loadStack.splice(idx, 1);
					var resultIdx = results.indexOf(caller);
					if (resultIdx >= 0) {
						results[resultIdx] = result;
					}
				}
	
				if (loadStack.length === 0) {
					resolve(results);
					results = null;
					loadStack = null;
				}
	
				caller = null;
			};
		};
	
		loadGMap(next(loadGMap), gMapKey);
		loadYMap(next(loadYMap), yMapKey);
	};
	
	module.exports = function (resolve) {
		loadMultiplie({ gMapKey: GOOGLE_MAPS_KEY, yMapKey: YANDEX_MAPS_KEY }, resolve);
	};
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }

})
//# sourceMappingURL=1.03330813263e2e4bd757.hot-update.js.map