webpackHotUpdate(1,{

/***/ 42:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = ['$scope', '$element', 'cfpLoadingBar', '$modals', function ($scope, $element, cfpLoadingBar, $modals) {
	
		var element = $element[0];
		$scope.flow = false;
		$scope.loading = false;
	
		$scope.filter = {
			page: 0,
			subjects: [
				// {name: 'Наука', id: 1},
				// {name: 'Метталургия', id: 2},
			],
			expositions: []
		};
	
		$scope.selectSubjects = function () {
			$modal.open('subject-selector', function () {
				var subjectList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				$scope.filter.subjects = $scope.filter.subjects.concat(subjectList);
			});
		};
	
		$scope.selectExpositions = function () {
			$modal.open('exposition-selector', function () {
				var expositionsList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
				$scope.filter.expositions = $scope.filter.expositions.concat(expositionsList);
			});
		};
	
		$scope.removeTag = function (tagData) {
			$scope.filter.tags = $scope.filter.tags.filter(function (tag) {
				return tag.id !== tagData.id;
			});
		};
	
		$scope.catalog = [];
	
		$scope.loadMore = function () {
			var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
	
	
			if ($scope.loading) {
				return;
			}
	
			cfpLoadingBar.start();
			$scope.loading = true;
	
			io.socket.get('/offers/origin', $scope.filter, function (catalog) {
				// console.log(catalog[1]);
				// catalog.forEach((item) => {
				// 	console.log(item);
				// })
	
				$scope.catalog = $scope.catalog.concat(catalog);
				$scope.filter.page += 1;
	
				cfpLoadingBar.complete();
				$scope.loading = false;
				cb();
			});
		};
	
		$scope.loadMore();
		$scope.startFlow = function () {
			$scope.loadMore(function () {
				$scope.flow = true;
			});
		};
	
		document.addEventListener('scroll', function (event) {
			// const contentIsScrolled = element.scrollHeight - document.body.scrollTop >= element.clientHeight;
			var contentIsScrolled = element.scrollHeight - document.body.scrollTop - 400 <= 0; //todo fix this bug
	
	
			if (contentIsScrolled && $scope.flow && !$scope.loading) {
				$scope.loadMore();
			};
		});
	}];

/***/ }

})
//# sourceMappingURL=1.3ffed7e45a9d762c61a9.hot-update.js.map