webpackHotUpdate(1,{

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var angular = __webpack_require__(44);
	var safeApply = __webpack_require__(60);
	
	var homePage = __webpack_require__(61);
	var loginPage = __webpack_require__(62);
	var registrationPage = __webpack_require__(63);
	var recoveryPage = __webpack_require__(64);
	var changePasswordPage = __webpack_require__(65);
	
	var loginConstraints = __webpack_require__(66);
	var registrationConstraints = __webpack_require__(67);
	// const recoveryConstraints = require('../validators/recoveryForm');
	
	
	var _require = __webpack_require__(68),
	    formApply = _require.formApply;
	
	var _require2 = __webpack_require__(74),
	    guid = _require2.guid;
	
	var routes = function routes($stateProvider, $locationProvider) {
	
		$locationProvider.html5Mode(true);
	
		$stateProvider.state('home', {
			url: '/',
			templateUrl: homePage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-login', {
			url: '/user/login',
			templateUrl: loginPage,
			controller: ['$scope', '$element', function ($scope, $element) {
				var form = $element.find('form')[0];
				formApply($scope, form, loginConstraints);
	
				$scope.submit = function ($event) {
	
					form.validate(function (error, formData) {
						console.log(error, formData);
					});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-registration', {
			url: '/user/registration',
			templateUrl: registrationPage,
			controller: ['$scope', '$element', function ($scope, $element) {
	
				var form = $element.find('form')[0];
	
				formApply($scope, form, registrationConstraints);
				form.setValue('type', 'company');
				$scope.randomSeed = '';
	
				$scope.changeCaptcha = function () {
					form.setValue('captcha', '').focus();
					$scope.randomSeed = guid();
				};
	
				$scope.select = function (type) {
					form.setValue('type', type);
					console.log($scope.form);
				};
	
				$scope.submit = function ($event) {
	
					form.validate(function (error, formData) {
						if (error) {
							$scope.changeCaptcha();
						}
					});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-recovery', {
			url: '/user/recovery',
			templateUrl: recoveryPage,
			controller: ['$scope', '$element', function ($scope, $element) {
	
				var form = $element.find('form')[0];
				formApply($scope, form, registrationConstraints);
	
				$scope.email = null;
	
				$scope.setMail = function ($event) {
	
					form.validate(function (error, formData) {
						if (!error) {
							safeApply($scope, function () {
								$scope.email = formData.email;
							});
						}
					});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('change-password', {
			url: '/user/change-password',
			templateUrl: changePasswordPage,
			controller: ['$scope', '$element', function ($scope, $element) {
				var form = $element.find('form')[0];
				formApply($scope, form, registrationConstraints);
	
				$scope.setMail = function ($event) {
	
					form.validate(function (error, formData) {});
	
					$event.preventDefault();
				};
			}]
		});
	
		$stateProvider.state('user-offers', {
			url: '/user/offers',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-events', {
			url: '/user/events',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-account', {
			url: '/user/account',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-cart', {
			url: '/user/cart',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('user-logout', {
			url: '/user/logout',
			templateUrl: changePasswordPage,
			controller: ['$scope', '$state', 'User', function ($scope, $state, User) {
	
				User.logout().catch(function (e) {
					return console.log(e);
				}).then(function () {
					$state.go('home');
				});
			}]
		});
	
		$stateProvider.state('dev-debug', {
			url: '/dev/debug',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	
		$stateProvider.state('dev-styles', {
			url: '/dev/styles',
			templateUrl: changePasswordPage,
			controller: ['$scope', function ($scope) {}]
		});
	};
	
	module.exports = routes;

/***/ }

})
//# sourceMappingURL=1.8c304bc99c1d8c081ca5.hot-update.js.map