webpackHotUpdate(1,{

/***/ 38:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var safeApply = __webpack_require__(41);
	var templateUrl = __webpack_require__(39);
	var controller = ['$scope', function ($scope) {
		console.log('Test');
	}];
	
	var qwery = __webpack_require__(34);
	
	var link = function link($scope, $element, attrs, ctrl, $transclude) {
	
		$transclude($scope, function (clone) {
			var itemsCount = 0;
			angular.forEach(clone, function (current) {
				if (current.classList && current.classList.contains('slide')) {
					itemsCount++;
				}
			});
	
			$scope.total = itemsCount;
			$scope.active = 0;
			$scope.setActive = function (active) {
				$scope.active = active;
	
				qwery('.slide', $element[0]).forEach(function (item, idx) {
					if (item.classList.contains('active')) {
						item.classList.remove('active');
					}
	
					if (idx === active) {
						item.classList.add('active');
					}
				});
			};
	
			$scope.next = function () {
				var current = parseInt($scope.active);
				var next = current + 1;
				current = next >= $scope.total ? 0 : next;
	
				console.log('Next');
	
				$scope.setActive(current);
			};
	
			$scope.prev = function () {
				var current = parseInt($scope.active);
				var prev = current - 1;
				current = prev <= 0 ? $scope.total - 1 : prev;
	
				$scope.setActive(current);
			};
	
			setTimeout($scope.next, 300);
		});
	};
	
	module.exports = function () {
		return {
			transclude: true,
			templateUrl: templateUrl,
			link: link
		};
	};

/***/ },

/***/ 41:
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function ($scope) {
		var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
		var $$phase = $scope.$root.$$phase;
	
		$$phase == '$apply' || $$phase == '$digest' ? fn() : undefined.$apply(fn);
	};

/***/ }

})
//# sourceMappingURL=1.9b60fd81f74143665369.hot-update.js.map