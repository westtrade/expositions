'use strict';

const arangojs = require('arangojs');
const {Database, aql} = arangojs;
const {resolve} = require('path');
const os = require('os');

const fs = require('fs');
const readline = require('readline');

const db = new Database({
	url: `http://root:expo-sudo@expo-database.local:8529/`,
	databaseName: 'expodrive-locations'
});

//
// const Datastore = require('nedb-promise');
// const locationsStore = new Datastore({
// 	filename: resolve(__dirname, 'locations.db'),
// 	autoload: true,
// });
// const namesStore = new Datastore({
// 	filename: resolve(__dirname, 'names.db'),
// 	autoload: true,
// });

const allowedLangs = ['ru', 'ua', 'en'];

async function ensureCollection (db, collectionName, createOptions = {}) {
	const collectionsList = await db.listCollections();
	const exists = collectionsList.map(({name}) => name).includes(collectionName);

	const collection = db.collection(collectionName);

	if (!exists) {
		await collection.create(createOptions);
	}

	return collection;
}

const pushUnique = (value, codeList = []) => {
	if (codeList.indexOf(value) < 0) {
		codeList.push(value);
	}

	return codeList;
};


function saveOnlyNeeded (allowedLangs = []) {

	const namePath = resolve(__dirname, 'alternateNames/alternateNames.txt');
	const nameAllowedPath = resolve(__dirname, 'alternateNames/alternateNames.' + allowedLangs.join('-') + '.txt');


	const input = fs.createReadStream(namePath, {encoding: 'utf-8'});
	const output = fs.createWriteStream(nameAllowedPath, {encoding: 'utf-8', flas: 'w'});

	const lineReader = readline.createInterface({input});
	lineReader.on('line', (line) => {
		const [id, lid, code, name] = line.split('\t');

		if (!allowedLangs.includes(code)) {
			return;
		}

		output.write(line + os.EOL);
	});
}


const EventEmitter = require('events');
class Chunker extends EventEmitter {

	constructor({collection}) {
		super();
		this.__collection = collection;
		this.__count = 0;
		this.__data = [];
	}

	get count () {
		return this.__count();
	}

	collect(data) {

		this.__data.push(data);
		if (this.__data.length === this.__count - 1) {

			setTimeout(() => {

				this.__collection.import(this.__data, {type: 'array', details: true}).then((result) => {
					this.__data = [];
					this.__count = 0;
					this.emit('next');
				}).catch(err => {console.log(err);})

			}, 10);
		}
	}

	start(count) {
		this.__count = count;
	}
}


function loadToDB (namePath, collection, resolveData = (line) => { return {} }) {
	const input = fs.createReadStream(namePath, {encoding: 'utf-8'});

	const chunker = new Chunker({collection});
	chunker.on('next', () => {
		input.resume();
	});

	input.on('data', (chunk) => {
		input.pause();
		chunker.start(chunk.split('\n').length);
	})

	const lineReader = readline.createInterface({input});
	lineReader.on('line', (line) => {
		const data = resolveData(line);
		chunker.collect(data);
	});

	return new Promise((resolve) => {
		input.on('end', resolve);
	});

}

const getPath = (allowedLangs = []) => {
	return resolve(__dirname, `alternateNames/alternateNames${allowedLangs && allowedLangs.length ? `.${ allowedLangs.join('-') }` : '' }.txt`);
};

const loadLocationNamesToDB = () => {
	return ensureCollection(db, 'locationNames').then((locationNames) => {
		const allowedLangs = ['ru'];
		return loadToDB(getPath(), locationNames, (line) => {
			const [id, lid, code, name] = line.split('\t');
			return {id, lid, code, name};
		});
	}).then(() => {
		console.log('end');
	})
};

const loadLocationsData = () => {

	return ensureCollection(db, 'locations').then((locations) => {
		return loadToDB('./RU.txt', locations, (line) => {
			const [geonameid, name, asciiname, alternatenames, latitude, longitude, featureClass, featureCode, countryCode, cc2, admin1, admin2, admin3, admin4, population, elevation, dem, timezone, date] = line.split('\t');
			return {
				geonameid, name, asciiname, alternatenames, latitude, longitude, featureClass, featureCode, countryCode, cc2, admin1, admin2, admin3, admin4, population, elevation, dem, timezone, date
			}
		});
	}).then(() => {
		console.log('end');
	});
};

// loadLocationsData();





// saveOnlyNeeded(allowedLangs);
// saveOnlyNeeded(['ru']);










// async function loadNames () {
//
// 	const input = fs.createReadStream(namePath, {encoding: 'utf-8'});
// 	const lineReader = readline.createInterface({input});
//
// 	const namesCollection = await ensureCollection(db, 'names');
// 	input.on('data', async function (chunk) {
// 		input.pause();
// 		setTimeout(() => input.resume(), 1);
// 	});
//
//
// 	const {count} = await namesCollection.count();
//
// 	let currentItemCount = 0;
// 	lineReader.on('line', async function (chunk) {
//
// 		currentItemCount++;
// 		if (currentItemCount <= count) {
// 			return;
// 		}
//
// 		const [id, lid, code, name] = chunk.split('\t');
//
//
// 		if (!allowedLangs.includes(code)) {
// 			return;
// 		}
//
//
// 		const doc = {id, lid, code, name};
// 		const query = aql`
// 			UPSERT {id: ${id}, lid: ${lid}}
// 			INSERT ${doc}
// 			UPDATE ${doc}
// 			INTO ${namesCollection}
// 			RETURN NEW
// 		`;
// 		// const query = aql`
// 		// 	INSERT ${doc}
// 		// 	INTO ${namesCollection}
// 		// `;
//
// 		// console.log(query);
//
// 		const cursor = await db.query(query);
// 		const result = await cursor.all();
// 	});
// }
//
//
//
//
// loadNames();




//
//
// async function importData () {
//
// 	const info = await db.get();
// 	const locations = await ensureCollection(db, 'locations');
//
// 	// console.log(locations);
//
//
// 	const result = await locations.load(5);
// 	// console.log(result);
//
// 	await locations.unload();
//
//
// 	const locationsPath = resolve(__dirname, './RU.txt');
// 	const input = fs.createReadStream(locationsPath, {encoding: 'utf-8'});
// 	const lineReader = readline.createInterface({
// 		input
// 	});
//
// 	let countShow = 100;
//
// 	lineReader.on('line', (line) => {
// 		if (--countShow) {
// 			const [geonameid, name, asciiname, alternatenames, latitude, longitude, featureClass, featureCode, ...other] = line.split('\t');
// 			console.log(geonameid, name, asciiname);
//
//
// 		}
// 	})
//
//
//
//
//
//
//
//
// 	// const cursor = await db.query(aql`
// 	// 	FOR location in ${locationCollection}
// 	// 	RETURN location
// 	// `);
// 	// console.log(cursor);
//
// }

// importData();
