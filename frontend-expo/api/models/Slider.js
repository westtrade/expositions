/**
 * Slider.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	schema: true,

	attributes: {
		cover: 'string',
		title: 'string',
		description: 'string',
		weight: {
			type: 'integer',
			defaultsTo: 0
		},
		action: {
			type: 'string',
			enum: ['link', 'modal']
		},
		actionArgs: {
			type: 'array',
		}
	}
};
