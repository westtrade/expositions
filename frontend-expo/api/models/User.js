/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */


const ROLES = ['exponent', 'organizer', 'moderator', 'administrator'];

module.exports = {
	schema: true,

	attributes: require('waterlock').models.user.attributes({
		revision: 'string',
		// displayName: 'string',
		phone: 'string',
		role: {
			type: 'integer',
			enum: Array.from(Array(ROLES.length).keys()),
		}
	}),

	beforeCreate: require('waterlock').models.user.beforeCreate,
	beforeUpdate: require('waterlock').models.user.beforeUpdate,
	ROLES,
};
