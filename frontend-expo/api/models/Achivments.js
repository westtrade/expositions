/**
 * Achivments.js
 *
 *
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	schema: true,
	attributes: {

		type: {
			type: 'string',
			enum: ['premium']
		},

		expired: {
			type: 'datetime',
			defaultsTo: null, //null - is never expired
		}
	}
};
