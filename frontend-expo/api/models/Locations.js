'use strict';


module.exports = {
	schema: true,
	
	// identity: 'locations',
	// connection: 'locationDatabase',
	attributes: {
		geonameid : 'integer',
		name : 'string',
		asciiname : 'string',
		alternatenames : 'string',
		latitude : 'float',
		longitude : 'float',
		featureClass : 'string',
		featureCode : 'string',
		countryCode : 'string',
		cc2 : 'string',
		admin1 : 'string',
		admin2 : 'string',
		admin3 : 'string',
		admin4 : 'string',
		population : 'integer',
		elevation : 'integer',
		dem : 'integer',
		timezone : 'string',
		date : 'date',
	}
};
