'use strict';


module.exports = {
	schema: true,
	
	// identity: 'locationNames',
	// connection: 'locationDatabase',
	attributes: {
		name: 'string',
		code: 'string',
		id: 'integer',
		lid: 'integer',
	}
};
