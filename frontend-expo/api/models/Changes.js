/**
 * Changes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	schema: true,
	attributes: {

		model: {
			type: 'string',
			required: true
		},

		objectId: {
			type: 'objectid',
			required: true
		},

		revision: {
			type: 'string',
			required: true
		},

		field: {
			type: 'string',
			required: true
		},

		value: {
			type: 'json',
			required: true
		},

		custom: 'json',
	},

	createRevision(ModelName, data = {}) {

	}
};
