/**
 * OffersController
 *
 * @description :: Server-side logic for managing offers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


const request = require('request');
const qs = require('qs');
const co = require('co');

const audienceCount = (currentItem) => {

	if (!currentItem) {
		return currentItem;
	}

	if (!currentItem.exposition) {
		currentItem.audienceCount = 0;
		return currentItem;
	}

	const {exposition: {audience}} = currentItem || {};

	currentItem.audienceCount = audience.reduce((result, response) => {
		const {feedback = []} = response;

		const answers = feedback.map(({answer = false} = {}) => answer);
		const answer = answers.pop();

		if (answer) {
			result++;
		}

		return result;

	}, 0);

	return currentItem;
};

module.exports = {

	offer (req, res, next) {

		const id = req.param('id');
		request.get(`https://expotest.pw/api/offer/${id}`, (err, response, body) => {
			let data = JSON.parse(body);

			if (data._id === '579ba0002b9fd8e9251dea10') {
				data.name = 'HR&Trainings expo 2016 - Профессиональное мероприятие в области управления';
				data.name = 'BI TO BE - Лучший импульс для развития бизнеса';
			}

			data = audienceCount(data);

			res.json(data);
		});
	},

	index(req, res, next) {

		const itemsPerPage = 10;
		const page = req.param('page') || 0;


		// const page = 0;
		const offset = itemsPerPage * page + itemsPerPage;

		request.get('http://expotestdrive.ru:3000/api/offer', {offset}, (err, httpResponse, body) => {

			try {

				let data = JSON.parse(body);

				data[0].name = "Скидка на обручальные кольца";
				data[0].description = `Дополнительная скидка до 3000 рублей
				на покупку обурчальных колец! В акции учавствуют все ювелирные салоны
				"НАШЕ ЗОЛОТО" в г. Москва`;

				if (page >= 1) {
					data.push(data[0]);
				}

				data = data.map(audienceCount)

				res.json(data);

			} catch (e) {
				res.serverError(e);
			}
		});
	},
};
