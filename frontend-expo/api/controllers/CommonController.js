/**
 * CommonController
 *
 * @description :: Server-side logic for managing commons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const fs = require('fs');
const {resolve} = require('path');
const svgCaptcha = require('svg-captcha');
const co = require('co');

const {
	graphql,
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLList,
	GraphQLInt,
} = require('graphql');


const generateSchema = () => {

	const schemaDefinition = {
		query: new GraphQLObjectType({
			name: 'RootQueryType',
			fields: {
				hello: {
					type: GraphQLString,
					resolve() {
						return 'world';
					}
				}
			}
		})
	};

	const schema = new GraphQLSchema(schemaDefinition);
	return schema;
};


// const { generateSchema } = require('sails-graphql');
let schema = null;

const parseBody = (req) => {

	return new Promise((resolve, reject) => {

		let data = '';

		// resolve(data);

		console.log(req.headers['content-length']);

		req.socket.setEncoding('utf8');

		req.on('data', function(chunk) {
			console.log('DATA', chunk);
			// data += chunk;
		}).on('error', (error) => {
			console.log('error');
			// reject(error);
		}).on('end', function() {
			console.log('END');
			// resolve(data);
		});
	});
};



module.exports = {

	graph (req, res, next) {

		co(function* () {

			const body = yield parseBody(req);
			console.log(body);

			const {query = body} = req.query;
			console.log(query);


			if (!schema) {
				schema = generateSchema(sails.models);
			}

			try {

				const result = yield graphql(schema, query);

				let { errors } = result;
				if (errors && errors.length) {
					result.errors = errors.map(({ message }) => { return { message }});
					res.serverError(result);
				} else {
					res.json(result)
				}

			} catch (e) {
				res.serverError(e);
			}

		}).catch(e => res.serverError(e));
	},

	favicon (req, res, next) {
		const favicon = fs.createReadStream(resolve('./assets/favicon.ico'));
		res.pipe(favicon);
	},

	captcha (req, res, next) {
		// const captchaMiddleware = captcha({ color:'#0064cd', background: 'rgb(20,30,200)' });
		const captcha = svgCaptcha.create();
		req.session.captcha = captcha.text;

		res.set('Content-Type', 'image/svg+xml');
		res.status(200).send(captcha.data);
	}
};
