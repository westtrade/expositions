/**
 * SliderController
 *
 * @description :: Server-side logic for managing sliders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	cover: {
		type: 'string'
	},

	type: {
		type: 'string',
		enum: ['default', 'card']
	},

	link: {
		type: 'string'
	}
};
