'use strict';

const co = require('co');
const pluralize = require('pluralize');

const {
	graphql,
	GraphQLSchema,
} = require('graphql');

const {
	GraphQLInt,
	GraphQLList,
	GraphQLObjectType,
	GraphQLInputObjectType,
	GraphQLBoolean,
	GraphQLFloat,
	GraphQLID,
	GraphQLString,
} = require('graphql');

const arango = require('arangojs');
const {Database, aql} = arango;

const db = new Database({
	url: `http://root:expo-sudo@expo-database.local:8529/`,
	databaseName: 'expodrive-locations'
});

function ensureCollections (db, models = {}, createOptions = {}) {

	return co(function* () {

		const collections = {};
		const modelsList = Array.isArray(models) ? models : Object.entries(models);

		const collectionsList = (yield db.listCollections())
			.map(({name}) => name);

		let modelsCount = modelsList.length;
		while (modelsCount--) {

			const [name, info] = modelsList[modelsCount];
			const {identity} = info;

			const collection = db.collection(identity);
			const exists = collectionsList.includes(identity);

			if (!exists) {
				yield collection.create(createOptions);
			}

			collections[identity] = collection;
		}

		return collections;
	});
}

/**
 * Make the first letter of the `string` upper cased
 * @param {string} string
 * @returns {string}
 */
function firstLetterToUpperCase(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Make the first letter of the `string` lower cased
 * @param {string} string
 * @returns {string}
 */
function firstLetterToLowerCase(string) {
	return string.charAt(0).toLowerCase() + string.slice(1);
}

const getName = (model, fieldKey = '') => {
	// Get model globalId to have original modal name (with correct case)
	// Basically it should start with upper cased letter
	const modelTypeName = model.globalId;

	// Make sure the first letter of field name is upper cased
	const fieldName = firstLetterToUpperCase(fieldKey);

	// Will be used as GraphQL query field identifier
	// Basically it should start with lower cased letter
	const modelName = firstLetterToLowerCase(modelTypeName);

	// TODO make an ability to customize this names
	// (for example from model definition file)
	return {
		modelTypeName,
		typeName: `${modelTypeName}Type`,
		inputTypeName: `${modelTypeName}InputType`,
		connectionTypeName: `${modelTypeName}ConnectionType`,
		fieldConnectionTypeName: `${modelTypeName}${fieldName}ConnectionType`,
		fieldUnionTypeName: `${modelTypeName}${fieldName}UnionType`,
		queryName: `${modelName}`,
		queryPluralName: `${pluralize(modelName)}`,
		queryTypeName: `${modelTypeName}Query`,
		queryPluralTypeName: `${modelTypeName}RangeQuery`,
		mutationCreateName: `create${modelTypeName}`,
		mutationCreateTypeName: `Create${modelTypeName}Mutation`,
		mutationDeleteName: `delete${modelTypeName}`,
		mutationDeleteTypeName: `Delete${modelTypeName}Mutation`,
		mutationUpdateName: `update${modelTypeName}`,
		mutationUpdateTypeName: `Update${modelTypeName}Mutation`
	};
};


const GraphQLJSON = require('graphql-type-json');

const dataTypes = {
	string: GraphQLString,
	email: GraphQLString,
	text: GraphQLString,
	integer: GraphQLInt,
	float: GraphQLFloat,
	number: GraphQLFloat,
	json: GraphQLJSON,
	date: GraphQLString,
	// array: GraphQLList,
	datetime: GraphQLString,
	boolean: GraphQLBoolean,
	objectid: GraphQLID
};


const getFields = (model, {isInput = false, fieldTypes = {}, models = {}} = {}) => {

	const { _attributes: attributes } = model;
	const { queryName, modelTypeName } = getName(model);

	return Object.entries(attributes).reduce((fields = {}, [fieldName, info]) => {

		const {type, collection, model} = info;

		if (type) {

			let fieldType = dataTypes[type];
			if (type === 'array') {
				fieldType = new GraphQLList(GraphQLString)
			}

			if (!fieldType) {
				throw new Error(`Each field must have either 'type', 'collection' or 'model' property defined. Field '${fieldName}' was omitted. %j`, info);
			}


			fields[fieldName] = {
				type: fieldType
			}


			return fields;
		}







		const attrType = type || collection || model;







		//
		// if (!attrType) {
		// 	throw new Error(`Each field must have either 'type', 'collection' or 'model' property defined. Field '${name}' was omitted.`);
		// }
		//
		// const childModel = collection || model
		// 	? models[attrType.toLowerCase()]
		// 	: null;





		// fields[name] = {
		// 	type:
		// };

		return fields;

		// let fieldTypeName;
		// let childModel;
		//
		// if (model || collection) {
		//
		// 	childModel = models.reduce((model, [key, currentModel]) => {
		// 		if (model) {
		// 			return model;
		// 		} else if (attrType.toLowerCase() === key) {
		// 			return currentModel;
		// 		}
		// 	}, null);
		//
		// 	const {inputTypeName, typeName} = getName(childModel);
		// 	fieldTypeName = isInput ? inputTypeName : typeName;
		// }
		//
		// if (!dataTypes[attrType] && !childModel) {
		// 	console.error(`Field '${name}' has unsupported type '${attrType}' in '${modelTypeName}' model and was omitted. Field %j`, field);
		// 	return fields;
		// }
		// const fieldType = dataTypes[attrType] || objectTypes[fieldTypeName];


	}, {});
};


const makeModel = (model, {fieldTypes = {}, models = {}, isInput = false} = {}) => {

	const { typeName, inputTypeName, } = getName(model);
	const name = isInput ? inputTypeName : typeName;

	const fields = getFields(model, {fieldTypes, models, isInput});

	// console.log(fields);

	const modelType = isInput
		? new GraphQLObjectType({name, fields})
		: new GraphQLInputObjectType({name, fields});

	return [name, modelType];
};


const createTypes = (models = []) => {

	const modelsList = Array.isArray(models)
		? models
		: Object.entries(models);

	return modelsList.reduce((types = [], [identity, model] = []) => {

		const [name, fields] = makeModel(model, {types, models});
		types[name] = fields;

		const [inputName, inputFields] = makeModel(model, {types, models, isInput: true});
		types[inputName] = inputFields;

		return types;

	}, []);
};


const schema = null;

const ensureSchema = (db, sailsModels = {}) => {

	// sailsModels = Object.entries(sailsModels);

	return co(function* () {

		if (schema) {
			return schema;
		}

		const collections = yield ensureCollections(db, sailsModels);
		try {

			const objectTypes = createTypes(sailsModels);
			const currentSchema = new GraphQLSchema({
				query: new GraphQLObjectType({
					name: 'RootQueryType',
					fields: () => {

						const queries = Object.entries(sailsModels).reduce((queries = {}, [identity, model] = []) => {

							const {
								queryName,
								queryTypeName,
								typeName,
								queryPluralName,
								queryPluralTypeName,
								connectionTypeName
							} = getName(model);


							queries[queryName] = {
								name: queryTypeName,
								type: objectTypes[typeName],
								// type: getConnectionType(connectionTypeName, objectTypes[typeName]),
								// args: connectionArgs,
								// resolve: resolveGetRange(model)

								resolve: afb


							};
							//
							// queries[queryPluralName] = {
							// 	name: queryPluralTypeName,
							// 	// type: getConnectionType(connectionTypeName, objectTypes[typeName]),
							// 	// args: connectionArgs,
							// 	// resolve: resolveGetRange(model)
							// };


							return queries;

						}, {});


						// console.log(queries);




						return queries;
					}
				}),
				// mutation: new GraphQLObjectType({
				// 	name: 'RootMutationType',
				// 	fields: () => {
				// 		return {
				//
				// 		};
				// 	}
				// })
			});



		} catch (e) {
			console.log(e);
		}
	});
};


module.exports = {

	query(query) {

		const { models } = sails;
		return co(function* () {

			const collections = yield ensureCollections(db, sails.models);
			const currentSchema = new GraphQLSchema({
				query: new GraphQLObjectType({
					name: 'RootQueryType',
					fields: () => {
						const fields = {

							locations: {
								type: GraphQLObjectType,
								resolve() {
									return {
										test: 'world'
									};
								}
							},

							locationNames: {
								type: GraphQLObjectType,
								resolve() {
									return {
										test: 'world'
									};
								}
							},
						};

						return fields;
					}
				}),
				// mutation: new GraphQLObjectType({
				// 	name: 'RootMutationType',
				// 	fields: () => {
				// 		return {
				//
				// 		};
				// 	}
				// })
			});

			return graphql(currentSchema, query);
		});










		// return ensureSchema(db, models);


		// return Promise
		// 	.resolve(schema)
		// 	.then((schema) => {
		//
		//
		//
		// 	})
		//
		//
		// ensureCollections(db, Object.entries(sails.models))


		// console.log(sails.models);
		// Object.entries(sails.models).reduce((schemas, [name, model]) => {
		//
		// 	console.log(name);
		//
		//
		// 	return schemas;
		// }, schemas);

		// const schema = new GraphQLSchema({
			// query: new GraphQLObjectType({
			// 	name: 'RootQueryType',
			// 	fields: {
			// 		hello: {
			// 			type: GraphQLString,
			// 			resolve() {
			// 				return 'world';
			// 			}
			// 		}
			// 	}
			// }),

			// mutation: new GraphQLObjectType({
			// 	name: 'RootQueryType',
			// 	fields: {
			// 		hello: {
			// 			type: GraphQLString,
			// 			resolve() {
			// 				return 'world';
			// 			}
			// 		}
			// 	}
			// }),
		// });



	}


};
