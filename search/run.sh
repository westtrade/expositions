#!/bin/bash

# @Author: Popov Gennadiy <dio>
# @Date:   2016-11-14T05:39:04+03:00
# @Email:  me@westtrade.tk
# @Last modified by:   dio
# @Last modified time: 2016-12-10T21:48:06+03:00

# docker-compose run --service-ports --rm indexstream $@
if [[ $# -eq 0 ]]; then
	docker-compose up $@
elif [ $1 = 'sync' ]; then
	docker-compose run --rm 'indexstream' sh -c 'npm run sync'
else
	docker-compose up -d $@
fi
