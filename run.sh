#!/usr/bin/env bash

SERVICE_NAME="frontend"

if [[ $# -eq 0 ]]; then
	docker-compose up $@ -d
elif [ $1 = 'build' ]; then
	docker-compose build $SERVICE_NAME
	# docker-compose run --rm $SERVICE_NAME npm run build
elif [ $1 = 'dev' ]; then
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml run --rm --service-ports $SERVICE_NAME /bin/sh
else
	docker-compose up -d $@
fi
